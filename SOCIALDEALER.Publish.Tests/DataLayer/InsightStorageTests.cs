﻿using System;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using DataLayer;
using DataLayerInsights;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SOCIALDEALER.Publish.Tests.DataLayer
{
    [TestClass]
    public class InsightStorageTests
    {
        [TestMethod]
        public void Facebook()
        {
            //Method Signature = public static List<WorkList> GetActiveWorkList(int socialNetwork = 2)
            var sw = Stopwatch.StartNew();

            var actualresult = InsightStorageDL.GetActiveWorkList();
            Debug.WriteLine("GetActiveWorkList Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, actualresult.Count);
            Assert.IsTrue(actualresult.Count > 0);
        }
    }
}
