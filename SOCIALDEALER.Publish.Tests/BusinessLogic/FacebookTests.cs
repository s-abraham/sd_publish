﻿using System;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;
using BusinessLogic;
using System.Linq;
using DataLayer;
using DataLayerInsights;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SOCIALDEALER.Publish.Tests.BusinessLogic
{
    [TestClass]
    public class FacebookTests
    {
        [TestMethod]
        public void GetFacebookInsights()
        {
            //Method Signature = public static FacebookInsights GetFacebookInsights(int dealerLocationId, string uniqueId, string token, string startDate, string endDate, string periods)
            var sw = Stopwatch.StartNew();

            var worklist = InsightStorageDL.GetActiveWorkList(2);
            Debug.WriteLine("GetActiveWorkList Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, worklist.Count);

            var dealerLocationId = 2;
            var uniqueId = "120358044649547";
            var token = "234628835828|00b6a9b2966c907d79aae7ea.1-32810622|120358044649547|OlL4B14FVtkUGkSnqqOEb74sRAo";
            var startDate = DateTime.Parse("10-01-2012 00:00:00");
            var endDate = DateTime.Parse("10-01-2012 23:59:59");
            var periods = "";

            var actualResult = FaceBook.GetFacebookInsights(dealerLocationId, uniqueId, token, startDate.ToString(), endDate.ToString(), periods);
            Debug.WriteLine("GetFacebookInsights Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, actualResult.insights.data.Count);

            var actualObjectResult = FaceBook.GetFacebookInsightsObject(actualResult);
            Debug.WriteLine("GetFacebookInsightsObject Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, actualObjectResult.Count);


            Assert.IsTrue(actualResult.insights.data.Count > 0);
        }

        [TestMethod]
        public void GetFacebookActiveInsights()
        {
            //Method Signature = public static FacebookInsights GetFacebookInsights(int dealerLocationId, string uniqueId, string token, string startDate, string endDate, string periods)
            var sw = Stopwatch.StartNew();

            var startDate = DateTime.Parse("10-01-2012 00:00:00");
            var endDate = DateTime.Parse("10-01-2012 23:59:59");
            var periods = "";
            var insights = new BusinessEntities.FacebookInsights();
            var objectInsights = new List<KeyValuePair<string, string>>();

            var worklist = InsightStorageDL.GetActiveWorkList(2);
            Debug.WriteLine("GetActiveWorkList Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, worklist.Count);
            
            foreach (var item in worklist)
            {
                insights = FaceBook.GetFacebookInsights(item.LocationId, item.UniqueId, item.Token, startDate.ToString(), endDate.ToString(), periods);
                Debug.WriteLine("GetFacebookInsights Processing Time: {0}ms", sw.ElapsedMilliseconds);

                if (String.IsNullOrEmpty(insights.facebookResponse.code))
                {
                    objectInsights = FaceBook.GetFacebookInsightsObject(insights);
                    Debug.WriteLine("GetFacebookInsightsObject Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, objectInsights.Count);
                }
                else
                {
                    Debug.WriteLine("GetFacebookInsightsObject Processing Error: {0} for LocationId: {1}", insights.facebookResponse.message, item.LocationId);
                }
            }

            Assert.IsTrue(insights.insights.data.Count > 0);
        }

        [TestMethod]
        public void GetInsightsTest()
        {
            //Method Signature = public static List<Dictionary<string, string>> GetInsights(DateTime queryDate)

            var queryDate = DateTime.Today.AddDays(-5);
            var insights = PublishManager.GetInsights(queryDate);
            
            Assert.IsTrue(insights.Count > 0);
        }

        [TestMethod]
        public void spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result_Test()
        {
            //Method Signature = public spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result GetDealerLocationSocialNetworkPostDetailsByPostQueueID(long dealerLocationSocialNetworkPostQueueID)

            var _PublishManager = new PublishManager();

            var postId = 89952;
            var postInfo = _PublishManager.GetDealerLocationSocialNetworkPostDetailsByPostQueueID(postId);

            Assert.IsTrue(postInfo.UniqueID.Length > 0);
        }


        [TestMethod]
        public void PostByQueueId_Test()
        {
            //Method Signature = public List<spGetDealerLocationSocialNetworkPostTimeZone_New_Result> GetDealerLocationSocialNetworkPostTimeZone()

            var _PublishManager = new PublishManager();

            var postQueueId = 89952;
            var postListing = _PublishManager.GetDealerLocationSocialNetworkPostTimeZone();

            var postItem = (from i in postListing
                            where i.PostQueueID == postQueueId
                            select i).FirstOrDefault();


            //Assert.IsTrue(postInfo.UniqueID.Length > 0);
        }
    }
}
