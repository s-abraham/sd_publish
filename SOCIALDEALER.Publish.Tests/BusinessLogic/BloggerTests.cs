﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;

namespace SOCIALDEALER.Publish.Tests.BusinessLogic
{
    [TestClass]
    public class BloggerTests
    {
        
        #region Authentication Tests

        [TestMethod]
        public void GetBloggerAuthCodeURL_Test()
        {
            // Signature: public static string GetBloggerAuthCodeURL(string client_id, string redirect_URL)
            var result = Blogger.GetBloggerAuthCodeURL("469042947424-tlmkc5rmi94ubh0urf747614crlpjk5n.apps.googleusercontent.com", "https://localhost/dealers/SDSettings/SocialNetworksBlogger.aspx");
        }

        [TestMethod]
        public void GetBloggerRefreshToken_Test()
        {
            // Signature: public static BloggerRefreshTokenRequestResponse GetBloggerRefreshToken(string authCode, string client_id, string client_secret, string redirect_URL, string grant_type)
            var result = Blogger.GetBloggerRefreshToken("4/TNnY2vYhHzdWEKq1Qur3PogNwvWo.Qs08XiZGmN8QmmS0T3UFEsMsCpdZgAI", "469042947424-tlmkc5rmi94ubh0urf747614crlpjk5n.apps.googleusercontent.com", "LevkPg_MxxPcyrcmo51R4gaK", "https://localhost/dealers/SDSettings/SocialNetworksBlogger.aspx", "authorization_code");
        }

        [TestMethod]
        public void GetBloggerAccessToken_Test()
        {
            // Signature: public static BloggerAccessTokenRequestResponse GetBloggerAccessToken(string client_id, string client_secret, string refresh_token, string grant_type)
            var result = Blogger.GetBloggerAccessToken("469042947424-tlmkc5rmi94ubh0urf747614crlpjk5n.apps.googleusercontent.com", "LevkPg_MxxPcyrcmo51R4gaK", "", "Implicit");
        }
        
        [TestMethod]
        public void GetStatusOfBloggerAccessToken_Test()
        {
            // Signature: public static BloggerTokenStatusResponse GetStatusOfBloggerAccessToken(string accessToken)
            var result = Blogger.GetStatusOfBloggerAccessToken("ya29.AHES6ZTjAFoDNi_kQXkxkQ5bYhcE-slqFtSKwOoox7wIkQ");
        }

        
        #endregion

        #region BLOG TESTS

        [TestMethod]
        public void GetBlogInfobyAccessToken_Test()
        {
            // Signature: public static BloggerGetBlogInfoByAccessToken GetBlogInfobyAccessToken(string access_token)
            var result = Blogger.GetBlogInfobyAccessToken("4/vYsx1XAu8gKkeyUvprocTXsLcxBd.Enz7FV_A7-0cmmS0T3UFEsP4uYtYgAI");
        }

        [TestMethod]
        public void GetBlogInfobyBlogID_Test()
        {
            // Signature: public static BloggerGetBlogInfoByURLorIDResponse GetBlogInfobyBlogID(string blogID)
            var result = Blogger.GetBlogInfobyBlogID ("611342858858402100");
        }

        [TestMethod]
        public void GetBlogInfobyBlogURL_Test()
        {
            // Signature:  public static BloggerGetBlogInfoByURLorIDResponse GetBlogInfobyBlogURL(string blogURL)
            var result = Blogger.GetBlogInfobyBlogURL("http://developmentmotors.blogspot.com");
        }

        #endregion
        
        #region POST TESTS

        //[TestMethod]

        //public void AddBloggerPost_Test()
        //{
        //    // Signature:  public static BloggerPostResponse AddBloggerPost(string access_token, string blogID, string postTitle, string postContent)
        //    var result = Blogger.AddBloggerPost("ya29.AHES6ZQl3TTKKw_aBvBfJdUdd1Azahvtncj8Tjrg2QXBKw", "611342858858402100" , "Welcome To Development Motors Blog" ,  "Hello");
        //}
      
        //[TestMethod]
        //public void ListBloggerPostsByBlog_Test()
        //{
        //    // Signature: public static BloggerGetListResponse ListBloggerPostsByBlog(string blogID)
        //    var result = Blogger.ListBloggerPostsByBlog("611342858858402100");
        //}

        //[TestMethod]
        //public void SearchForBloggerPost_Test()
        //{
        //    // Signature:   public static BloggerGetListResponse SearchForBloggerPost(string blogID, string searchKeyword)
        //    var result = Blogger.SearchForBloggerPost("611342858858402100", "test");
        //}

        //[TestMethod]
        //public void BloggerGetSpecificPost_Test()
        //{
        //    // Signature:  public static BloggerPostResponse GetBloggerPostbyPostID(string blogID, string postID)
        //    var result = Blogger.GetBloggerPostbyPostID("611342858858402100", "8303119176276199487");
        //}

        //[TestMethod]
        //public void UpdateBloggerPost_Test()
        //{
        //    // Signature:  public static BloggerPostResponse UpdateBloggerPost(string access_token, string blogID, string postID, string postTitle, string postContent)
        //    var result = Blogger.UpdateBloggerPost("ya29.AHES6ZQl3TTKKw_aBvBfJdUdd1Azahvtncj8Tjrg2QXBKw", "611342858858402100", "8449871707773275225", "Updating a post title", "Updating post Content");
        //}

        //[TestMethod]
        //public void BloggerDeletePost_Test()
        //{
        //    // Signature:  public static BloggerResponse DeleteBloggerPost(string access_token,string blogID, string postID)
        //    var result = Blogger.DeleteBloggerPost("ya29.AHES6ZQl3TTKKw_aBvBfJdUdd1Azahvtncj8Tjrg2QXBKw", "611342858858402100", "858179147544016741");
        //}

        //#endregion

        //#region COMMENT TESTS

        //[TestMethod]
        //public void BloggerGetCommentsListByPost_Test()
        //{
        //    // Signature: public static BloggerGetCommentListResponse GetBloggerCommentsListByPost(string blogID, string postID)
        //    var result = Blogger.GetBloggerCommentsListByPost("611342858858402100", "8449871707773275225");
        //}

        //[TestMethod]
        //public void BloggerGetCommentByCommentID_Test()
        //{
        //    // Signature: public static BloggerGetSpecificCommentResponse GetBloggerCommentByCommentID(string blogID, string postID, string commentID)
        //    var result = Blogger.GetBloggerCommentByCommentID("611342858858402100", "8449871707773275225", "4746565986320807018");
        //}


        //#endregion

        //#region PAGE TESTS

        //[TestMethod]
        //public void BloggerGetPageListByBlog_Test()
        //{
        //    // Signature:  public static BloggerGetPageListResponse GetBloggerPageListByBlog(string blogID)
        //    var result = Blogger.GetBloggerPageListByBlog("611342858858402100");
        //}

        //[TestMethod]
        //public void BloggerGetPageByPageID_Test()
        //{
        //    // Signature: public static BloggerGetSpecificPageResponse GetBloggerPageByPageID(string blogID, string pageID)
        //    var result = Blogger.GetBloggerPageByPageID("611342858858402100", "5287806608023658425");
        //}

        #endregion
    }
}
    




