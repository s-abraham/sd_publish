﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;

namespace SOCIALDEALER.Publish.Tests.BusinessLogic
{
    [TestClass]
    public class GooglePlusTests
    {
        #region AUTHENTICATION TESTS

        [TestMethod]
        public void GooglePlusGetAuthCode_Test()
        {
            // Signature: public static string GooglePlusGetAuthCode(string client_id, string redirect_URL)
            var result = GooglePlus.GooglePlusGetAuthCode("469042947424-phgupikgj9qfvkaka91ep4q19q81slmn.apps.googleusercontent.com", "https://socialdealer.com/oauth2callback");
        }

        [TestMethod]
        public void GooglePlusGetRefreshToken_Test()
        {
            // Signature:  public static GooglePlusRefreshTokenRequestResponse GooglePlusGetRefreshToken(string authCode, string client_id, string client_secret, string redirect_URL, string grant_type)
           // for blogger var result = GooglePlus.GetGooglePlusRefreshToken("4/Ne2lE_H-XgnsuRNbZNlMmPq2Es_y.4gHycQGK2CIUuJJVnL49Cc9YgvhqdwI", "469042947424-4b7ai2pbvi16oi4giiv59tm917m0a3ba.apps.googleusercontent.com", "rJDumq2m5LFQX0EAokJwuVKD", "http://localhost/dealers/SDSettings/SocialNetworksBlogger.aspx", "authorization_code");
            var result = GooglePlus.GetGooglePlusRefreshToken("4/uM0bQmRMPqGlqNMhqzgSOfgGlhb7.QjTXnImaCwsYuJJVnL49Cc-qyRlfdwI", "469042947424-4b7ai2pbvi16oi4giiv59tm917m0a3ba.apps.googleusercontent.com", "rJDumq2m5LFQX0EAokJwuVKD", "https://localhost/dealers/SDSettings/SocialNetworksGooglePlus.aspx", "authorization_code");
        }

        [TestMethod]
        public void GooglePlusGetAccessToken_Test()
        {
            // Signature: public static GooglePlusAccessTokenRequestResponse GooglePlusGetAccessToken(string client_id, string client_secret, string refresh_token, string grant_type)
            var result = GooglePlus.GetGooglePlusAccessToken("469042947424-phgupikgj9qfvkaka91ep4q19q81slmn.apps.googleusercontent.com", "pis-4zCw3XLgPKsMXTQybzYd", "1/0egVYJ2h_-15ByKeO5QOm6UxkqBBmqTPZaTBdIC36eo", "refresh_token");
        }

        [TestMethod]
        public void GooglePlusTokenStatus_Test()
        {
            // Signature: public static GooglePlusTokenStatusResponse GooglePlusTokenStatus(string accessToken)
            var result = GooglePlus.GooglePlusAccessTokenStatus("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw");
        }

        #endregion

        #region USER/PAGES TESTS

        [TestMethod]
        public void GooglePlusGetUserFromToken_Test()
        {
            // Signature: public static GooglePlusGetUserResponse GooglePlusGetUserFromToken(string accessToken)
            var result = GooglePlus.GetGooglePlusUserFromAccessToken("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw");
        }

        [TestMethod]
        public void GooglePlusGetPagesForAnUser_Test()
        {
            // Signature: public static GooglePlusGetPageListResponse GooglePlusGetPagesForAnUser(string userID)
            var result = GooglePlus.GetGooglePlusPagesForAnUser("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "100648571705457313815");
        }

        [TestMethod]
        public void GooglePlusGetPageByPageID_Test()
        {
            // Signature: public static GooglePlusGetSpecificPageResponse GooglePlusGetPagebyPageID(string pageID)
            var result = GooglePlus.GetGooglePlusPagebyPageID("103035118708843740598");
        }

        [TestMethod]
        public void GooglePlusSearchForPage_Test()
        {
            // Signature: public static GooglePlusGetPageListResponse GooglePlusSearchForPage(string searchName)
            var result = GooglePlus.SearchForGooglePlusPage("Castle Chevrolet Villa Park");
        }


        #endregion

        #region ACTIVITIES/POSTS TESTS

        [TestMethod]
        public void GooglePlusAddActivity_Test()
        {
            // Signature: public static GooglePlusAddActivityResponse GooglePlusAddActivity(string pageID, string accessType, string objectType, string activityType, string postContent, string attachmentID, string postURL, string circleID, string userID)

            var result = GooglePlus.AddGooglePlusActivity("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "", "", "", "rstlned", "", "", "", "");
        }

        [TestMethod]
        public void GooglePlusGetActivitiesListForAPage_Test()
        {
            // Signature: public static GooglePlusGetActivityListByPageResponse GooglePlusGetActivitiesListForAPage(string pageID)
            var result = GooglePlus.GetGooglePlusActivitiesListForAPage("103035118708843740598");
        }

        [TestMethod]
        public void GooglePlusGetActivitiesListForACircle_Test()
        {
            // Signature: public static GooglePlusGetActivityListByCircleResponse GooglePlusGetActivitiesListForACircle(string pageID, string circleID)
            var result = GooglePlus.GetGooglePlusActivitiesListForACircle("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "48f1d4778b56c10f");
        }

        [TestMethod]
        public void GooglePlusGetActivityByActivityID_Test()
        {
            // Signature: public static GooglePlusGetSpecificActivityResponse GooglePlusGetActivityByActivityID(string activityID)
            var result = GooglePlus.GetGooglePlusActivityByActivityID("z13wivcyoky0jtj0w22nflf43pustxthl04");
        }

        [TestMethod]
        public void GooglePlusDeleteActivity_Test()
        {
            // Signature: public static GooglePlusResponse GooglePlusDeleteActivity(string pageID, string activityID)
            var result = GooglePlus.DeleteGooglePlusActivity("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "z133sji53zzpuzb5h04cehi5gkirf3br5q40k");
        }

        [TestMethod]
        public void GooglePlusUpdateActivity_Test()
        {
            // Signature:  public static GooglePlusUpdateActivityResponse GooglePlusUpdateActivity(string pageID,string activityID, string objectType, string activityType, string postContent, string postURL)
            var result = GooglePlus.UpdateGooglePlusActivity("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "z133sji53zzpuzb5h04cehi5gkirf3br5q40k", "", "", "test-update", "");
        }
             
        #endregion

        #region COMMENTS TESTS

        [TestMethod]
        public void AddComment_Test()
        {
            // Signature: public static GooglePlusAddActivityResponse GooglePlusAddActivity(string pageID, string accessType, string objectType, string activityType, string postContent, string attachmentID, string postURL, string circleID, string userID)
            var result = GooglePlus.AddGooglePlusComment("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "z13fhfm5smjwzds0z22nflf43pustxthl04", "Test comment");
        }  

        [TestMethod]
        public void GooglePlusGetCommentsListForAnActivity_Test()
        {
            // Signature:  public static GooglePlusGetCommentsListResponse GooglePlusGetCommentsListForAnActivity(string ActivityID)

            var result = GooglePlus.GetGooglePlusCommentsListForAnActivity("z12mgzvxavfyh5chg04cehi5gkirf3br5q40k");
        }

        [TestMethod]
        public void GooglePlusGetCommentByCommentID_Test()
        {
            // Signature:  public static GooglePlusGetSpecificCommentResponse GooglePlusGetCommentByCommentID(string commentID)
            var result = GooglePlus.GetGooglePlusCommentByCommentID("z12mgzvxavfyh5chg04cehi5gkirf3br5q40k.1353949177041000");
        }

        [TestMethod]
        public void GooglePlusDeleteComment_Test()
        {
            // Signature:  public static GooglePlusResponse GooglePlusDeleteComment(string pageID, string commentID)
            var result = GooglePlus.DeleteGooglePlusComment("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "z12mgzvxavfyh5chg04cehi5gkirf3br5q40k.1353949177041000");
        }

        [TestMethod]
        public void GooglePlusUpdateComment_Test()
        {
            // Signature:  public static GooglePlusUpdateCommentsResponse GooglePlusUpdateComment(string pageID, string commentID, string commentContent)
            var result = GooglePlus.UpdateGooglePlusComment("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "z12mgzvxavfyh5chg04cehi5gkirf3br5q40k.1353949177041000", "Comment-Update Test");
        }

        #endregion

        #region CIRCLES TESTS

        [TestMethod]
        public void GooglePlusCreateCircle_Test()
        {
            // Signature: public static GooglePLusCreateCircleResponse GooglePlusCreateCircle(string pageID, string circleName)
            var result = GooglePlus.CreateGooglePlusCircle("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "Centrino");
        }

        [TestMethod]
        public void GooglePlusGetCirclesListForAPage_Test()
        {
            // Signature: public static GooglePlusGetCirclesListResponse GooglePlusGetCirclesListForAPage(string pageID)
            var result = GooglePlus.GetGooglePlusCirclesListForAPage("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598");
        }

        [TestMethod]
        public void GooglePlusGetCircleByCircleID_Test()
        {
            // Signature: public static GooglePlusGetSpecificCircleResponse GooglePlusGetCircleByCircleID(string pageID, string circleID)
            var result = GooglePlus.GetGooglePlusCircleByCircleID("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "48f1d4778b56c10f");
        }

        [TestMethod]
        public void GooglePlusDeleteCircle_Test()
        {
            // Signature: public static GooglePlusResponse GooglePlusDeleteCircle(string pageID, string circleID)
            var result = GooglePlus.DeleteGooglePlusCircle("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "521c54670a18401d");
        }

        [TestMethod]
        public void GooglePlusUpdateCircle_Test()
        {
            // Signature:  public static GooglePLusCreateOrUpdateCircleResponse GooglePlusUpdateCircle(string pageID, string circleID, string circleName)
            var result = GooglePlus.UpdateGooglePlusCircle("","103035118708843740598", "306fa8f40b42761e", "Optimal");
        }

        [TestMethod]
        public void GooglePlusGetMembersListForACircle_Test()
        {
            // Signature: public static GooglePlusGetMembersListResponse GooglePlusGetMembersListForACircle(string pageID, string circleID)
            var result = GooglePlus.GetGooglePlusMembersListForACircle("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "48f1d4778b56c10f");
        }

        [TestMethod]
        public void GooglePlusGetAddPeopleToACircle_Test()
        {
            // Signature: public static GooglePlusCreateOrUpdateOrGetSpecificCircleResponse GooglePlusGetAddPeopleToACircle(string pageID, string circleID, string userID)
            var result = GooglePlus.AddGooglePlusPeopleToACircle("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "48f1d4778b56c10f", "105235061584657296760");
        }

        [TestMethod]
        public void GooglePlusGetRemovePeopleFromACircle_Test()
        {
            // Signature: public static GooglePlusCreateOrUpdateOrGetSpecificCircleResponse GooglePlusGetAddPeopleToACircle(string pageID,string circleID, string userID)
            var result = GooglePlus.RemoveGooglePlusPersonFromACircle("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "48f1d4778b56c10f", "105235061584657296760");
        }

        #endregion

        #region MEDIA TESTS

        [TestMethod]
        public void GooglePlusUploadPhotoToTempServer_Test()
        {
            // Signature: public static string GooglePlusUploadPhotoToTempServer(string pageID, string pathToImage)
            var result = GooglePlus.UploadGooglePlusPhotoToTempServer("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "http://www.carversation.com/wp-content/uploads/2008/06/bentley.jpg");
        }

        [TestMethod]
        public void GooglePlusPostPhoto_Test()
        {
            // Signature: public static GooglePlusAddActivityResponse GooglePlusPostPhoto(string pageID, string accessType, string activityType, string postContent, string postURL, string pathToImage, string circleID, string userID)
            var result = GooglePlus.PostGooglePlusPhoto("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "", "", "test", "", "http://upload.wikimedia.org/wikipedia/commons/f/f2/2007-Toyota-Camry-SE.jpg", "", "");
        }

        [TestMethod]
        public void GooglePlusGetMediabyMediaID_Test()
        {
            // Signature: public static GooglePlusGetSpecificMediaResponse GooglePlusGetMediabyMediaID(string mediaID, string pageID)
            var result = GooglePlus.GetGooglePlusMediabyMediaID("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598.5803698330603030658", "103035118708843740598");
        }

        #endregion

        #region PLACES/EVENTS TESTS

        [TestMethod]
        public void GooglePlusPlacesSearch_Test()
        {
            // Signature: public static GooglePlusPlaceSearchResponse GooglePlusPlacesSearch(string searchlocation, string searchRadius, string searchKeyWord)
            var result = GooglePlus.PlacesSearchGooglePlus("41.861030,-87.952363", "5000", "Castle Chevrolet");
        }

        [TestMethod]
        public void GooglePlusPlacesDetail_Test()
        {
            // Signature:  public static GooglePlusPlaceDetailResponse GooglePlusPlacesDetail(string reference)
            var result = GooglePlus.PlacesDetailGooglePlus("CqQBlAAAAAbVQnIso6BcwuXf76cv5pz9KGrouSplfZElRtn5ZnoC_VMUDwaffxobSJPOeJyFk1e2R_p3D2HrhkRx8T0FEaEOP5OpLNpSGpxp3IVTi0QFI9sSYu-vrRXeRTEF2QXnQKHG4dqMGqRlHMt4YE-S8yKEAbYPhGIdxEeemFaK8dS28Md6QUOd_mEfXwhI1gZ_P0IZVz1EXmfv8Vumjgw0H4cSEPWNk1jr-ZMKZKi3KAVCp98aFF_bM9GZXcJJn3e4On-ULElIjNf_");
        }

        [TestMethod]
        public void GooglePlusAddPlace_Test()
        {
            // Signature: public static GooglePlusAddPlaceResponse GooglePlusAddPlace(double latitude, double longtitude, int accuracy, string searchName, string types)
            var result = GooglePlus.AddGooglePlusPlace(41.876754, -87.627682, 5000, "CeltronEnterprises", "establishment");
        }

        [TestMethod]
        public void GooglePlusDeletePlace_Test()
        {
            // Signature: public static GooglePlusDeletePlaceResponse GooglePlusDeletePlace(string reference)
            var result = GooglePlus.DeleteGooglePlusPlace("CkQxAAAA8qriqYHIHtPnP7MuJ2l_L1UqC7lVpow93YVlU8MYuA5Op01iE6Ja1eSJhnZEbWwWfmT919wfq-4wlEGAvRruShIQIj9j27jrf9CroE92EVODvRoUlR1hTd7-lmxWiIxM2DJfv1WUPOY");
        }

        [TestMethod]
        public void GooglePlusAddEvent_Test()
        {
            // Signature: public static GooglePlusAddEventResponse GooglePlusAddEvent(int duration, string reference, string summary, string postURL)
            var result = GooglePlus.AddGooglePlusEvent(4500, "CkQxAAAA8qriqYHIHtPnP7MuJ2l_L1UqC7lVpow93YVlU8MYuA5Op01iE6Ja1eSJhnZEbWwWfmT919wfq-4wlEGAvRruShIQIj9j27jrf9CroE92EVODvRoUlR1hTd7-lmxWiIxM2DJfv1WUPOY", "Rock n Roll Dreams: The band fully electric in my kitchen and drinking all my beer.", "http://www.socialdealer.com");
        }

        [TestMethod]
        public void GooglePlusDeleteEvent_Test()
        {
            // Signature: public static GooglePlusEventDeleteResponse GooglePlusDeleteEvent(string reference, string event_id)
            var result = GooglePlus.DeleteGooglePlusEvent("CkQxAAAA8qriqYHIHtPnP7MuJ2l_L1UqC7lVpow93YVlU8MYuA5Op01iE6Ja1eSJhnZEbWwWfmT919wfq-4wlEGAvRruShIQIj9j27jrf9CroE92EVODvRoUlR1hTd7-lmxWiIxM2DJfv1WUPOY", "AS5NzgkzKmg");
        }

        [TestMethod]
        public void GooglePlusEventDetail_Test()
        {
            // Signature: public static GooglePlusEventDetailResponse GooglePlusEventDetail(string reference, string event_id)
            var result = GooglePlus.EventDetailGooglePlus("CkQxAAAA8qriqYHIHtPnP7MuJ2l_L1UqC7lVpow93YVlU8MYuA5Op01iE6Ja1eSJhnZEbWwWfmT919wfq-4wlEGAvRruShIQIj9j27jrf9CroE92EVODvRoUlR1hTd7-lmxWiIxM2DJfv1WUPOY", "AS5NzgkzKmg");
        }

        #endregion

        #region STATISTICS TESTS

        [TestMethod]
        public void GooglePlusGetStatisticsPlusoners_Test()
        {
            // Signature: public static PlusonsersResponse GooglePlusGetStatisticsPlusoners(string pageID, string activityID)
            var result = GooglePlus.GetGooglePlusStatisticsPlusoners("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "z12suvfazsfqtvrgx04cehi5gkirf3br5q4k");
        }

        [TestMethod]
        public void GooglePlusGetStatisticsReSharers_Test()
        {
            // Signature: public static ResharersResponse GooglePlusGetStatisticsReSharers(string pageID, string activityID)
            var result = GooglePlus.GetGooglePlusStatisticsReSharers("ya29.AHES6ZSyGQ9AOd1dN8wzWzIxA9uasjkRzcZMr6r-tARZQbPuXw", "103035118708843740598", "z12suvfazsfqtvrgx04cehi5gkirf3br5q40k");
        }

        #endregion
    }
}