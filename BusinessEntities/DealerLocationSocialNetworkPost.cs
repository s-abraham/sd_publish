﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BusinessEntities
{
    public class DealerLocationSocialNetworkPost
    {
        public DealerLocationSocialNetworkPost()
        {
            socialNetworkIDs = new List<string>();
            imagePath = new List<string>();
            fBOptions = new List<string>();
            scheduleDate = Convert.ToDateTime("01/01/1990");
            postExpirationDate = Convert.ToDateTime("01/01/1990");
            eventStartTime = Convert.ToDateTime("01/01/1990");
            eventEndTime = Convert.ToDateTime("01/01/1990");
            dealerLocationSocialNetworkPostId = 0;
        }

        [DataMember(Order = 1)]
        public int dealerID { get; set; }
        [DataMember(Order = 2)]
        public List<string> socialNetworkIDs { get; set; }
        [DataMember(Order = 3)]
        public string userID { get; set; }
        [DataMember(Order = 4)]
        public string postTitle { get; set; }
        [DataMember(Order = 5)]
        public string postText { get; set; }
        [DataMember(Order = 6)]
        public string link { get; set; }
        [DataMember(Order = 7)]
        public List<string> imagePath { get; set; }
        [DataMember(Order = 8)]
        public string videoSource { get; set; }
        [DataMember(Order = 9)]
        public bool postToAll { get; set; }
        [DataMember(Order = 10)]
        public int postSourceID { get; set; }
        [DataMember(Order = 11)]
        public DateTime scheduleDate { get; set; }
        [DataMember(Order = 12)]
        public int scheduledPostID { get; set; }
        [DataMember(Order = 13)]
        public long pkvalue { get; set; }
        [DataMember(Order = 14)]
        public int dealerLocationID { get; set; }
        [DataMember(Order = 15)]
        public int departmentID { get; set; }
        [DataMember(Order = 16)]
        public string countriesIDs { get; set; }
        [DataMember(Order = 17)]
        public string citiesID { get; set; }
        [DataMember(Order = 18)]
        public string regionsIDs { get; set; }
        [DataMember(Order = 19)]
        public bool isSyndicated { get; set; }
        [DataMember(Order = 20)]
        public bool isUploadedImage { get; set; }
        [DataMember(Order = 21)]
        public bool isScheduledPost { get; set; }
        [DataMember(Order = 22)]        
        public bool isVideoPrivate { get; set; }
        [DataMember(Order = 23)]
        public string videoPath { get; set; }
        [DataMember(Order = 24)]
        public string eventName { get; set; }
        [DataMember(Order = 25)]
        public DateTime eventStartTime { get; set; }
        [DataMember(Order = 26)]
        public DateTime eventEndTime { get; set; }
        [DataMember(Order = 27)]
        public string fBLocation { get; set; }
        [DataMember(Order = 28)]
        public string privacy { get; set; }
        [DataMember(Order = 29)]
        public string fBAlbumId { get; set; }
        [DataMember(Order = 30)]
        public string fBAlbumName { get; set; }
        [DataMember(Order = 31)]
        public string fBQuestion { get; set; }
        [DataMember(Order = 32)]
        public List<string> fBOptions { get; set; }
        [DataMember(Order = 33)]
        public bool fBAllowNewOptions { get; set; }
        [DataMember(Order = 34)]
        public DateTime postExpirationDate { get; set; }
        [DataMember(Order = 35)]
        public int postTypeId { get; set; }
        [DataMember(Order = 36)]
        public long dealerLocationSocialNetworkPostId { get; set; }
        [DataMember(Order = 37)]
        public string fBAlbumDescription { get; set; }
        [DataMember(Order = 38)]
        public string description { get; set; }
        [DataMember(Order = 39)]
        public string hashValue { get; set; }
        
    }
}