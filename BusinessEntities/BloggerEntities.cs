﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class BloggerEntities
    {


    } 

    #region General/Authentication

    #region BLOGGER REQUEST
    public class BloggerRequest
    {
        public BloggerRequest()
        {
            RequestURL = string.Empty;
            Parameters = string.Empty;
            Method = string.Empty;
            TimeOut = 0;
            Filename = string.Empty;
            Message = string.Empty;
            Status = string.Empty;
        }
        public string RequestURL { get; set; }
        public string Parameters { get; set; }
        public string Method { get; set; }
        public int TimeOut { get; set; }
        public byte[] bytes { get; set; }
        public string Filename { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    }

    #endregion

    #region BLOGGER RESPONSE
    public class BloggerResponse
    {
        public BloggerResponse()
        {
            error = new BloggerError();
            tokenError = new BloggerErrorResponse();
        }
        public BloggerError error { get; set; }
        public BloggerErrorResponse tokenError { get; set; } // Use this to get error from TokenResponse     
    }

    public class BloggerErrorResponse
    {
        public BloggerErrorResponse()
        {
            error = string.Empty;
            error_description = string.Empty;
            message = string.Empty;

        }
        public string error { get; set; }
        public string error_description { get; set; }
        public string message { get; set; }
    }

    public class BloggerError
    {
        public BloggerError()
        {
            errors = new List<BloggerMoreDetail>();
            code = 0;
            message = string.Empty;
        }
        public List<BloggerMoreDetail> errors { get; set; }
        public int code { get; set; }
        public string message { get; set; }

        public override string ToString()
        {
            if (code != 0 || !string.IsNullOrEmpty(message) || errors.Count > 0)
            {
                string str = "code : " + code + " , Message :" + message + " , errors" + errors.ToString();
                return str;
            }
            else
            {
                return string.Empty;
            }
        }
    }

    public class BloggerMoreDetail
    {
        public BloggerMoreDetail()
        {
            domain = string.Empty;
            reason = string.Empty;
            message = string.Empty;
        }
        public string domain { get; set; }
        public string reason { get; set; }
        public string message { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(domain) || !string.IsNullOrEmpty(reason) || !string.IsNullOrEmpty(message))
            {
                string str = "domain : " + domain + " , reason :" + reason + " , message :" + message;
                return str;
            }
            else
            {
                return string.Empty;
            }
        }
    }   

    #endregion

    #region BLOGGER AUTH PARAMETERS
    public class BloggerAuthParams
    {
        public BloggerAuthParams()
        {
            client_id = string.Empty;
            client_secret = string.Empty;
            refresh_token = string.Empty;
            grant_type = string.Empty;
            redirect_URL = string.Empty;
        }        
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public string refresh_token { get; set; }
        public string grant_type { get; set; }
        public string redirect_URL { get; set; }
    }

    #endregion

    #region BLOGGER GET REFRESH TOKEN (& TEMP ACCESS TOKEN) RESPONSE
    public class BloggerRefreshTokenRequestResponse
    {
        public BloggerRefreshTokenRequestResponse()
        {
            errorResponse = new BloggerErrorResponse();
            token_type = string.Empty;
            expires_in = 0;
            refresh_token = string.Empty;
            access_token = string.Empty;
        }
        public BloggerErrorResponse errorResponse;
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string access_token { get; set; }
    }

    #endregion

    #region BLOGGER ACCESS TOKEN RESPONSE
    public class BloggerAccessTokenRequestResponse
    {
        public BloggerAccessTokenRequestResponse()
        {
            errorResponse = new BloggerErrorResponse();
            access_token = string.Empty;
            token_type = string.Empty;
            expires_in = 0;
            id_token = string.Empty;
        }
        public BloggerErrorResponse errorResponse;
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string id_token { get; set; }
    }

    #endregion

    #region BLOGGER CHECK TOKEN STATUS RESPONSE
    public class BloggerTokenStatusResponse
    {
        public BloggerTokenStatusResponse()
        {
            errorResponse = new BloggerErrorResponse ();
            issued_to = string.Empty;
            audience = string.Empty;
            user_id = string.Empty;
            scope = string.Empty;
            expires_in = 0;
            email = string.Empty;
            access_type = string.Empty;
            verified_email = false;
        }
        public BloggerErrorResponse errorResponse { get; set; }
        public string issued_to { get; set; }
        public string audience { get; set; }
        public string user_id { get; set; }
        public string scope { get; set; }
        public int expires_in { get; set; }
        public string email { get; set; }
        public bool verified_email { get; set; }
        public string access_type { get; set; }
    }

    #endregion

    #endregion

    #region Blogs

    #region BLOGGER GET BLOG INFO BY URL OR ID RESPONSE
    public class BloggerGetBlogInfoByURLorIDResponse
    {
        public BloggerGetBlogInfoByURLorIDResponse()
        {
            posts = new Posts();
            pages = new Pages();
            locale = new Locale();
            error = new BloggerResponse();

        }
        public string kind { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string url { get; set; }
        public string selfLink { get; set; }
        public Posts posts { get; set; }
        public Pages pages { get; set; }
        public Locale locale { get; set; }
        public BloggerResponse error { get; set; }
    }

    public class Posts
    {
        public Posts()
        {

        }
        public int totalItems { get; set; }
        public string selfLink { get; set; }
    }

    public class Pages
    {
        public Pages()
        {

        }
        public int totalItems { get; set; }
        public string selfLink { get; set; }
    }

    public class Locale
    {
        public Locale()
        {

        }
        public string language { get; set; }
        public string country { get; set; }
        public string variant { get; set; }
    }

    #endregion

    #region BLOGGER GET BLOG INFO BY ACCESS TOKEN RESPONSE
    public class BloggerGetBlogInfoByAccessToken
    {
        public BloggerGetBlogInfoByAccessToken()
        {
            items = new List<BloggerGetBlogInfoByURLorIDResponse>();
            error = new BloggerResponse();
        }
        public string kind { get; set; }
        public List<BloggerGetBlogInfoByURLorIDResponse> items { get; set; }
        public BloggerResponse error { get; set; }
    }

    #endregion

    #endregion

    #region Posts

    #region BLOGGER ADD POST REQUEST
    public class BloggerAddPostRequest
    {
        public BloggerAddPostRequest()
        {

        }
        public string title { get; set; }
        public string content { get; set; }
    }

    #endregion

    #region BLOGGER ADD POST RESPONSE
    public class BloggerPostResponse
    {
        public BloggerPostResponse()
        {
            blog = new Blog();
            author = new Author();
            replies = new Replies();
            error = new BloggerResponse();
            IsSuccess = false;
        }
        public string kind { get; set; }
        public string id { get; set; }
        public Blog blog { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string url { get; set; }
        public string selfLink { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public Author author { get; set; }
        public Replies replies { get; set; }
        public BloggerResponse error { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class Blog
    {
        public Blog()
        {

        }
        public string id { get; set; }
    }

    #endregion

    #region BLOGGER GET LIST OF POSTS RESPONSE
    public class BloggerGetListResponse
    {
        public BloggerGetListResponse()
        {
            items = new List<BloggerPostResponse>();
            error = new BloggerResponse();
        }
        public string kind { get; set; }
        public List<BloggerPostResponse> items { get; set; }
        public BloggerResponse error { get; set; }
    }

    #endregion

    #region BLOGGER UPDATEPOST REQUEST
    public class BloggerUpdatePostRequest
    {
        public BloggerUpdatePostRequest()
        {
            error = new BloggerResponse();
        }
        public string title { get; set; }
        public string content { get; set; }
        public string id { get; set; }
        public BloggerResponse error { get; set; }
    }

    #endregion

    #endregion
    
    #region Comments

    #region BLOGGER GET COMMENTS LIST RESPONSE
    public class BloggerGetCommentListResponse
    {
        public BloggerGetCommentListResponse()
        {
            items = new List<BloggerGetSpecificCommentResponse>();
            error = new BloggerResponse();
        }
        public string kind { get; set; }
        public string nextPageToken { get; set; }
        public List<BloggerGetSpecificCommentResponse> items { get; set; }
        public BloggerResponse error { get; set; }
    }

    public class Post
    {
        public Post()
        {

        }
        public string id { get; set; }
    }

    #endregion

    #region BLOGGER GET SPECIFIC COMMENT RESPONSE
    public class BloggerGetSpecificCommentResponse
    {
        public BloggerGetSpecificCommentResponse()
        {
            post = new Post();
            blog = new Blog();
            author = new Author();
            error = new BloggerResponse();
        }
        public string kind { get; set; }
        public string id { get; set; }
        public Post post { get; set; }
        public Blog blog { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string selfLink { get; set; }
        public string content { get; set; }
        public Author author { get; set; }
        public BloggerResponse error { get; set; }

    }

    #endregion

    #endregion

    #region Pages

    #region BLOGGER GET LIST OF PAGES RESPONSE
    public class BloggerGetPageListResponse
    {
        public BloggerGetPageListResponse()
        {
            items = new List<BloggerGetSpecificPageResponse>();
            error = new BloggerResponse();
        }
        public string kind { get; set; }
        public List<BloggerGetSpecificPageResponse> items { get; set; }
        public BloggerResponse error { get; set; }
    }

    #endregion

    #region BLOGGER GET SPECIFIC PAGE RESPONSE
    public class BloggerGetSpecificPageResponse
    {
        public BloggerGetSpecificPageResponse()
        {
            blog = new Blog();
            author = new Author();
            error = new BloggerResponse();
        }
        public string kind { get; set; }
        public string id { get; set; }
        public Blog blog { get; set; }
        public string published { get; set; }
        public string updated { get; set; }
        public string url { get; set; }
        public string selfLink { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public Author author { get; set; }
        public BloggerResponse error { get; set; }
    }

    #endregion

    #endregion
    
}
