﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BusinessEntities
{
    public class YoutubeEntities
    {

    }

    #region YOUTUBE REQUEST
    public class YoutubeRequest
    {
        public YoutubeRequest()
        {
            RequestURL = string.Empty;
            Parameters = string.Empty;
            Method = string.Empty;
            Status = string.Empty;
            link = string.Empty;
            oauth_token = string.Empty;
            oauth_token_secret = string.Empty;
            DealerLocationSocialNetworkPostID = 0;
            DealerLocationSocialNetworkPostQueueID = 0;
        }
        public string RequestURL { get; set; }
        public string Parameters { get; set; }
        public string Method { get; set; }
        public int TimeOut { get; set; }
        public string link { get; set; }
        public string Status { get; set; }
        public string oauth_token { get; set; }
        public string oauth_token_secret { get; set; }
        public string oauth_consumer_key { get; set; }
        public string oauth_consumer_secret { get; set; }
        public int DealerLocationSocialNetworkPostID { get; set; }
        public int DealerLocationSocialNetworkPostQueueID { get; set; }
    }

    #endregion

    #region YOUTUBE PAGE INSIGHTS RESPONSE

    public class YoutubePageInsightsResponse
    {
        public YoutubePageInsightsResponse()
        {
            entry = new YoutubePageInsightsEntry();
            errorMessage = string.Empty;
        }
        public YoutubePageInsightsEntry entry { get; set; }
        public string errorMessage { get; set; } 
    }

    
    public class YoutubePageInsightsEntry
    {
        public YoutubePageInsightsEntry()
        {
            title = new Title();
            gd_feedLink = new List<GdFeedLink>();
            yt_statistics = new YtStatistics();
        }
        public Title title { get; set; }
        public List<GdFeedLink> gd_feedLink { get; set; }
        public YtStatistics yt_statistics { get; set; }
    }
    
    public class Title
    {
        public Title()
        {
            _t = string.Empty;
        }
        public string _t { get; set; }
    }
    
    public class GdFeedLink
    {
        public GdFeedLink()
        {
            rel = string.Empty;
            href = string.Empty;
            countHint = 0;
        }
        public string rel { get; set; }
        public string href { get; set; }
        public int countHint { get; set; }
    }

    public class YtStatistics
    {
        public YtStatistics()
        {
            subscriberCount = 0;
            channelViewsCount = 0;
            totalUploadViews = 0;
            videosCount = 0;
            subscriptionsCount = 0;
            favoritesCount = 0;
            contactsCount = 0;
        }
        public int? subscriberCount { get; set; }
        public int? channelViewsCount { get; set; }
        public int? totalUploadViews { get; set; }
        public int? videosCount { get; set; }
        public int? subscriptionsCount { get; set; }
        public int? favoritesCount { get; set; }
        public int? contactsCount { get; set; }
    }

    #endregion
}


