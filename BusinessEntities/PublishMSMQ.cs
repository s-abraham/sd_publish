﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class PublishMSMQ
    {
        public PublishMSMQ()
        {
            DealerLocationSocialNetworkPostID = string.Empty;
            DealerLocationSocialNetworkPostQueueID = "-1";
        }
        public string DealerLocationSocialNetworkPostID { get; set; }
        public string DealerLocationSocialNetworkPostQueueID { get; set; }        
        public bool isRetry { get; set; }
        public DateTime LastExecution { get; set; }
        public string LastExecutionResponse { get; set; }
    }
}
