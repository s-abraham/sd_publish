﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ProcessorStats
    {
        public ProcessorStats()
        {
            ErrorList = new List<StatError>();
        }

        public DateTime ProcessDate { get; set; }

        public int RecordsAttempted { get; set; }

        public int RecordsProcessedSuccess { get; set; }

        public int RecordsProcessedError { get; set; }

        public List<StatError> ErrorList { get; set; } 
    }

    public class StatError
    {
        public int LocationId { get; set; }

        public SocialNetworks SocialNetwork { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}
