﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public static class Statistics
    {
        private static ProcessorStats _processorStats;
        private static DateTime _processDate;

        static Statistics()
        {
            _processorStats = new ProcessorStats();
            _processDate = DateTime.Now;
        }

        public static void AddAttempt()
        {
            _processorStats.RecordsAttempted++;
        }

        public static void AddFail(int locationId, SocialNetworks socialNetwork, string errorCode, string message)
        {
            _processorStats.RecordsProcessedError++;
            _processorStats.ErrorList.Add(new StatError() { LocationId = locationId, ErrorCode = errorCode, ErrorMessage = message, SocialNetwork = socialNetwork });
        }

        public static void AddSuccess()
        {
            _processorStats.RecordsProcessedSuccess++;
        }

        public static string ShowStats()
        {
            var sbOutput = new StringBuilder();

            sbOutput.AppendLine("Total Processed:" + _processorStats.RecordsAttempted);
            sbOutput.AppendLine("Total Fails:" + _processorStats.RecordsProcessedError);
            sbOutput.AppendLine("Total Successes:" + _processorStats.RecordsProcessedSuccess);

            foreach(var item in _processorStats.ErrorList)
            {
                Debug.WriteLine(item.ErrorCode);
            }

            var groupedErrors = _processorStats.ErrorList.GroupBy(x => x.ErrorCode).OrderBy(y => y.Key);

            foreach (var errorCode in groupedErrors)
            {
                Debug.WriteLine("Error: " + errorCode.Key);
                foreach (var statError in errorCode)
                {
                    Debug.WriteLine("DL: " + statError.LocationId + " SNID: " + statError.SocialNetwork + " Message: " + statError.ErrorMessage);
                }
            }

            return sbOutput.ToString();
        }

    }
}
