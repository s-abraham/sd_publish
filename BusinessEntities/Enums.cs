﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public enum URlTrackType
    {
        WeMissYouEmail = 1,
        NotificationEmail = 2,
        FacebookInventory = 3,
        TwitterInventory = 4,
        FacebookLandingPage = 5,
        FacebookCustomerEmail = 6,
        TwitterCustomerEmail = 7,
        FacebookPublish = 8,
        TwitterPublish = 9,
        PrintLandingPage = 10

    }
    public enum PostType
    {
        FacebookStatus = 1,
        FacebookPhoto = 2,
        FacebookLink = 3,
        FacebookEvent = 4,
        FacebookQuestion = 5,
        FacebookMultiplePhoto = 6,
        TwitterStatus = 7
        

    }
    public enum SocialNetworks
    {
        All = 0,
        FacebookOnly = 2,
        TwitterOnly = 3,
        LinkedInOnly = 4,
        YouTubeOnly = 5
    }

    public enum DailyType
    {
        Insight = 0,
        Page = 1,
        PostFeed = 2
    }

    public enum LOOKUPSSTATUS
    {
        PENDING = 1,
        SUCCESS = 2,
        FAILURE = 3,
        CANCELLED = 4,
        HOLD = 5
    }

    public enum FacebookActionType
    {
        Like = 1,
        Comment = 2,
        Share = 3,
        NewPost = 4,
        StatusLike = 5,
        PageLike = 6,
        CommentonUserWallPost = 7
    }

}
