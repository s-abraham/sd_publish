﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using BusinessLogic;
using BusinessEntities;
using System.Configuration;
using System.Web.Script.Serialization;

namespace SOCIALDEALER.Publish.Facebook.Page
{
    class Program
    {
        private static int SocialNetworkID = Convert.ToInt32(ConfigurationManager.AppSettings["SocialNetworkID"]);
        private static string DealerLocationIDs = Convert.ToString(ConfigurationManager.AppSettings["DealerLocationIDs"]);
        private static string ServiceName = Convert.ToString(ConfigurationManager.AppSettings["ServiceName"]);        
        private static string startDate = string.Empty;
        private static string endDate = string.Empty;
        
        static void Main(string[] args)
        {
          
            
            //FaceBook.GetFacebookAlbums(0, "255547016773", "AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe");

            GetSetPageInformation();
        }

        private static void GetSetPageInformation()
        {
            InsightStorageRTManager objInsightStorageRTManager = new InsightStorageRTManager();
            try
            {
               int iCounter = 1;
                
                //Console.WriteLine("Getting Worklist...");
                
                List<spGetDealerLocationSocialNetworkList_Result> listspGetDealerLocationSocialNetworkList_Result = objInsightStorageRTManager.GetDealerLocationSocialNetworkList(SocialNetworkID, DealerLocationIDs);
                
                //Console.WriteLine("Total Worklist : " + listspGetDealerLocationSocialNetworkList_Result.Count());
                //Console.WriteLine("");

                string message = string.Empty;
                message = "ID to Process : " + listspGetDealerLocationSocialNetworkList_Result.Count;
                objInsightStorageRTManager.InsertServiceLog(ServiceName, message);

                foreach (var spGetDealerLocationSocialNetworkList_Result in listspGetDealerLocationSocialNetworkList_Result)
                {
                    try
                    {
                        #region FACEBOOK
                        if (spGetDealerLocationSocialNetworkList_Result.SocialNetworkID == 2)
                        {
                            if (iCounter == 1)
                            {
                                SetDateForFacebookInsights();
                            }

                            #region FACEBOOK BASIC

                            FacebookPage objFacebookPage = FaceBook.GetFacebookPageInformation(spGetDealerLocationSocialNetworkList_Result.DealerLocationID
                                                                                                , spGetDealerLocationSocialNetworkList_Result.UniqueID
                                                                                                , spGetDealerLocationSocialNetworkList_Result.Token);


                            string strxml = Utility.SerializeObject<FacebookPage>(objFacebookPage);
                            if (!string.IsNullOrEmpty(strxml))
                            {
                                objInsightStorageRTManager.SaveFBPageDataXML(Utility.Trim(strxml), spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID, spGetDealerLocationSocialNetworkList_Result.UniqueID);
                            }
                            else
                            {
                                #region ERROR HANDLING

                                ExceptionLog ObjExceptionLog = new ExceptionLog();
                                ObjExceptionLog.DealerLocationSocialNetworkID = spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID;
                                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                                ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML";
                                ObjExceptionLog.InnerException = string.Empty;
                                ObjExceptionLog.PostResponse = string.Empty;
                                ObjExceptionLog.StackTrace = string.Empty;
                                ObjExceptionLog.MethodName = "Program\\GetSetPageInformation\\FACEBOOK\\SerializeObject";
                                ObjExceptionLog.ExtraInfo = "UniqueID : " + spGetDealerLocationSocialNetworkList_Result.UniqueID + ", Access Token : " + spGetDealerLocationSocialNetworkList_Result.Token;

                                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                                #endregion
                            }

                            #endregion

                            #region FACEBOOK DETAILS
                            var objFacebookInsights = FaceBook.GetFacebookInsights(spGetDealerLocationSocialNetworkList_Result.DealerLocationID,
                                                                                    spGetDealerLocationSocialNetworkList_Result.UniqueID,
                                                                                    spGetDealerLocationSocialNetworkList_Result.Token,
                                                                                    startDate,
                                                                                    endDate,
                                                                                    "day,lifetime");
                                                                                    //"day,week,days_28,lifetime");
                            
                            //FacebookInsights objFacebookInsights_db = new FacebookInsights();
                            
                            try
                            {
                                int sum = 0;
                                foreach (var item in objFacebookInsights.insights.data)
                                {
                                    Console.WriteLine(item.name);
                                    sum = 0;
                                    var itemval = item.values.FirstOrDefault();
                                    
                                    if (itemval != null)
                                    {
                                        if (itemval._value == "[]" || string.IsNullOrEmpty(itemval._value))
                                        {
                                            sum = 0;
                                        }
                                        else if (Utility.IsAllDigits(itemval._value))
                                        {
                                            sum += Convert.ToInt32(itemval._value);
                                        }
                                        else
                                        {
                                            var jss = new JavaScriptSerializer();
                                            dynamic data = jss.Deserialize<dynamic>(itemval._value);

                                            foreach (KeyValuePair<string, object> subitem in data)
                                            {
                                                sum += Convert.ToInt32(subitem.Value);
                                            }                                            
                                        }
                                        item.sum = sum;                                     
                                    }
                                }
                                
                            }
                            catch (Exception ex)
                            {
                                #region ERROR HANDLING

                                #endregion
                            }

                            strxml = Utility.SerializeObject<FacebookInsights>(objFacebookInsights);
                            strxml = Utility.Trim(strxml).Replace(@"<value xsi:type=""xsd:string"">","<value>");
                            
                            if (!string.IsNullOrEmpty(strxml))
                            {
                                objInsightStorageRTManager.SaveFBPageInsightXML(strxml, spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID, spGetDealerLocationSocialNetworkList_Result.UniqueID);
                            }
                            else
                            {
                                #region ERROR HANDLING

                                ExceptionLog ObjExceptionLog = new ExceptionLog();
                                ObjExceptionLog.DealerLocationSocialNetworkID = spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID;
                                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                                ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML";
                                ObjExceptionLog.InnerException = string.Empty;
                                ObjExceptionLog.PostResponse = string.Empty;
                                ObjExceptionLog.StackTrace = string.Empty;
                                ObjExceptionLog.MethodName = "Program\\GetSetPageInformation\\FACEBOOKINSIGHTS\\SerializeObject";
                                ObjExceptionLog.ExtraInfo = "UniqueID : " + spGetDealerLocationSocialNetworkList_Result.UniqueID + ", Access Token : " + spGetDealerLocationSocialNetworkList_Result.Token;

                                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                                #endregion
                            }

                            #endregion

                            Console.WriteLine(iCounter + " FaceBook : " + spGetDealerLocationSocialNetworkList_Result.UniqueID);
                        }

                        #endregion

                        #region GOOGLE PLUS
                        if (spGetDealerLocationSocialNetworkList_Result.SocialNetworkID == 29)
                        {
                            #region PostCount

                            GooglePlusGetActivityListByPageResponse objGooglePlusGetActivityListByPageResponse = GooglePlus.GetGooglePlusActivitiesListForAPage(spGetDealerLocationSocialNetworkList_Result.UniqueID);
                            
                            #endregion

                            #region PlusOneCount & FollowerCount
                            
                            GooglePlusGetSpecificPageResponse objGooglePlusGetSpecificPageResponse = GooglePlus.GetGooglePlusPagebyPageID(spGetDealerLocationSocialNetworkList_Result.UniqueID);
                            
                            #endregion

                            string Post_Error = string.Empty;
                            string PlusOne_Follower_Error = string.Empty;

                            int PostCount = objGooglePlusGetActivityListByPageResponse.items.Count;
                            if (!string.IsNullOrEmpty(objGooglePlusGetActivityListByPageResponse.error.message))
                            {
                                Post_Error = objGooglePlusGetActivityListByPageResponse.error.message;
                            }

                            int PlusOneCount = objGooglePlusGetSpecificPageResponse.plusOneCount;
                            int FollowerCount = objGooglePlusGetSpecificPageResponse.circledByCount;                           
                            
                            if (!string.IsNullOrEmpty(objGooglePlusGetSpecificPageResponse.error.message))
                            {
                                PlusOne_Follower_Error = objGooglePlusGetSpecificPageResponse.error.message;
                            }

                            objInsightStorageRTManager.SaveGooglePlusPage(0, spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID, spGetDealerLocationSocialNetworkList_Result.UniqueID
                                                                          , spGetDealerLocationSocialNetworkList_Result.PageFriendlyName, PlusOneCount, FollowerCount, PostCount, Post_Error, PlusOne_Follower_Error);
                            
                            Console.WriteLine(iCounter + " Google Plus : " + spGetDealerLocationSocialNetworkList_Result.UniqueID);
                        }
                        #endregion

                        #region TWITTER
                        if (spGetDealerLocationSocialNetworkList_Result.SocialNetworkID == 3)
                        {
                            TwitterPageInsightsResponse objTwitterPageInsightsResponse = Twitter.GetTwitterPageInsights(spGetDealerLocationSocialNetworkList_Result.UniqueID
                                                                                                                        , spGetDealerLocationSocialNetworkList_Result.Token
                                                                                                                        , spGetDealerLocationSocialNetworkList_Result.TokenSecret
                                                                                                                        , spGetDealerLocationSocialNetworkList_Result.AppKey
                                                                                                                        , spGetDealerLocationSocialNetworkList_Result.AppSecret);

                            string Error = string.Empty;

                            if (!string.IsNullOrEmpty(objTwitterPageInsightsResponse.errors.error))
                            {
                                Error = objTwitterPageInsightsResponse.errors.error;
                            }                            
                                                        
                            objInsightStorageRTManager.SaveTwitterPageInsight(0, spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID
                                                                                , spGetDealerLocationSocialNetworkList_Result.UniqueID
                                                                                , spGetDealerLocationSocialNetworkList_Result.TwitterScreenName
                                                                                , objTwitterPageInsightsResponse.statuses_count
                                                                                , objTwitterPageInsightsResponse.friends_count
                                                                                , objTwitterPageInsightsResponse.followers_count
                                                                                , objTwitterPageInsightsResponse.listed_count
                                                                                , objTwitterPageInsightsResponse.favourites_count
                                                                                , objTwitterPageInsightsResponse.friends_count
                                                                                , objTwitterPageInsightsResponse.status.retweet_count
                                                                                , Error);
                            
                            Console.WriteLine(iCounter + " Twitter : " + spGetDealerLocationSocialNetworkList_Result.UniqueID);
                            
                        }
                        #endregion

                        #region YOUTUBE

                        if (spGetDealerLocationSocialNetworkList_Result.SocialNetworkID == 5)
                        {
                            YoutubePageInsightsResponse objYoutubePageInsightsResponse = Youtube.GetYoutubePageInsights(spGetDealerLocationSocialNetworkList_Result.UniqueID);

                            if (objYoutubePageInsightsResponse.entry.yt_statistics.subscriberCount == null)
                            {
                                objYoutubePageInsightsResponse.entry.yt_statistics.subscriberCount = 0;
                            }

                            if (objYoutubePageInsightsResponse.entry.yt_statistics.channelViewsCount == null)
                            {
                                objYoutubePageInsightsResponse.entry.yt_statistics.channelViewsCount = 0;
                            }

                            if (objYoutubePageInsightsResponse.entry.yt_statistics.totalUploadViews == null)
                            {
                                objYoutubePageInsightsResponse.entry.yt_statistics.totalUploadViews = 0;
                            }

                            if (objYoutubePageInsightsResponse.entry.yt_statistics.videosCount == null)
                            {
                                objYoutubePageInsightsResponse.entry.yt_statistics.videosCount = 0;
                            }

                            if (objYoutubePageInsightsResponse.entry.yt_statistics.subscriptionsCount == null)
                            {
                                objYoutubePageInsightsResponse.entry.yt_statistics.subscriptionsCount = 0;
                            }

                            if (objYoutubePageInsightsResponse.entry.yt_statistics.favoritesCount == null)
                            {
                                objYoutubePageInsightsResponse.entry.yt_statistics.favoritesCount = 0;
                            }

                            if (objYoutubePageInsightsResponse.entry.yt_statistics.contactsCount == null)
                            {
                                objYoutubePageInsightsResponse.entry.yt_statistics.contactsCount = 0;
                            }

                            objInsightStorageRTManager.SaveYouTubePageInsight(0,spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID
                                                                                ,spGetDealerLocationSocialNetworkList_Result.UniqueID
                                                                                ,Convert.ToInt32(objYoutubePageInsightsResponse.entry.yt_statistics.subscriberCount)
                                                                                ,Convert.ToInt32(objYoutubePageInsightsResponse.entry.yt_statistics.channelViewsCount)
                                                                                ,Convert.ToInt32(objYoutubePageInsightsResponse.entry.yt_statistics.totalUploadViews)
                                                                                ,Convert.ToInt32(objYoutubePageInsightsResponse.entry.yt_statistics.videosCount)
                                                                                ,Convert.ToInt32(objYoutubePageInsightsResponse.entry.yt_statistics.subscriptionsCount)
                                                                                ,Convert.ToInt32(objYoutubePageInsightsResponse.entry.yt_statistics.favoritesCount)
                                                                                ,Convert.ToInt32(objYoutubePageInsightsResponse.entry.yt_statistics.contactsCount)
                                                                                ,objYoutubePageInsightsResponse.errorMessage);

                            Console.WriteLine(iCounter + " Youtube : " + spGetDealerLocationSocialNetworkList_Result.UniqueID);
                        }

                        #endregion

                        iCounter++;
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        ExceptionLog ObjExceptionLog = new ExceptionLog();
                        ObjExceptionLog.DealerLocationSocialNetworkID = spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID;
                        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjExceptionLog.ExceptionMessage = ex.Message;
                        ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjExceptionLog.PostResponse = string.Empty;
                        ObjExceptionLog.StackTrace = ex.StackTrace;
                        ObjExceptionLog.MethodName = "Program\\GetSetPageInformation\\forEach";
                        ObjExceptionLog.ExtraInfo = "UniqueID : " + spGetDealerLocationSocialNetworkList_Result.UniqueID + ", Access Token : " + spGetDealerLocationSocialNetworkList_Result.Token;

                        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                        #endregion
                    }
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "Program\\GetSetPageInformation";
                ObjExceptionLog.ExtraInfo = string.Empty;

                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }

        private static void SetDateForFacebookInsights()
        {
            try
            {
                InsightStorageRTManager objInsightStorageRTManager = new InsightStorageRTManager();

                var worklist_temp = objInsightStorageRTManager.GetDealerLocationSocialNetworkList(2, "2");

                var DealerLocationID = worklist_temp.Select(d => d.DealerLocationID).FirstOrDefault();
                var UniqueID = worklist_temp.Select(d => d.UniqueID).FirstOrDefault();
                var Token = worklist_temp.Select(d => d.Token).FirstOrDefault();

                

                //Get queryDate From Facebook
                //var fb_startDate = DateTime.Now.AddDays(-7).ToShortDateString() + " 00:00:00";
                //var fb_endDate = DateTime.Now.ToShortDateString() + " 23:59:59";
                                
                //var objFacebookInsights_temp = FaceBook.GetFacebookInsights(DealerLocationID, UniqueID, Token, fb_startDate, fb_endDate, "day,lifetime");

                //if (string.IsNullOrEmpty(objFacebookInsights_temp.facebookResponse.code))
                {
                    //var obj = objFacebookInsights_temp.insights.data.FirstOrDefault();

                    //DateTime _date = Convert.ToDateTime(obj.values.LastOrDefault().end_time.ToString().Substring(0,10));
                    DateTime _date = DateTime.Now.AddDays(-2);
                    //startDate = _date.ToShortDateString() + " 00:00:00";
                    //endDate = _date.ToShortDateString() + " 23:59:59";

                    Dictionary<DateTime, int> dictionary = new Dictionary<DateTime, int>();
                    for (int iCounter = 0; iCounter > -15; iCounter--)
                    {

                        DateTime TempDate = _date.AddDays(iCounter);

                        //DateTime statdate = Convert.ToDateTime(TempDate.ToShortDateString());
                        //DateTime enddate = Convert.ToDateTime(TempDate.ToShortDateString());
                        var fb_startDate = TempDate.ToShortDateString() + " 00:00:00";
                        var fb_endDate = TempDate.ToShortDateString() + " 23:59:59";

                        var objFacebookInsights_temp1 = FaceBook.GetFacebookInsights(DealerLocationID, UniqueID, Token, fb_startDate, fb_endDate, "day,lifetime,week,days_28");

                        if (string.IsNullOrEmpty(objFacebookInsights_temp1.facebookResponse.code))
                        {
                            try
                            {
                                //var result = objFacebookInsights_temp1.insights.data.Where(x => x.name == "page_impressions_by_city_unique").SingleOrDefault();

                                //if (result != null)
                                if (objFacebookInsights_temp1.insights.data.Count >= 180)
                                {
                                    //dictionary.Add(TempDate, objFacebookInsights_temp1.insights.data.Count);

                                    DateTime date = Convert.ToDateTime(TempDate);
                                    startDate = date.ToShortDateString() + " 00:00:00";
                                    endDate = date.ToShortDateString() + " 23:59:59";
                                    break;
                                }
                            }
                            catch
                            {

                            }
                            //dictionary.Add(TempDate,objFacebookInsights_temp1.insights.data.Count);
                            //if (objFacebookInsights_temp1.insights.data.Count > 199) break;                            
                        }

                       
                    }
                    if (dictionary.Count > 0)
                    {
                        var result = dictionary.Where(x => x.Value >= 199).FirstOrDefault();

                        DateTime date = Convert.ToDateTime(result.Key);
                        startDate = date.ToShortDateString() + " 00:00:00";
                        endDate = date.ToShortDateString() + " 23:59:59";
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}
