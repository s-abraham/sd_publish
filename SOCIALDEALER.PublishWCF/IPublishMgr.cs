﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using BusinessEntities;

namespace SOCIALDEALER.PublishWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPublishMgr" in both code and config file together.
    [ServiceContract]
    public interface IPublishMgr
    {

        ///// <summary>
        ///// Create new Wall Post
        ///// </summary>
        ///// <param name="">xml request</param>
        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //           ResponseFormat = WebMessageFormat.Xml,
        //           RequestFormat = WebMessageFormat.Xml,
        //           UriTemplate = "CreateWallPost",
        //           BodyStyle = WebMessageBodyStyle.Bare)]
        //string CreateWallPost(DealerLocationSocialNetworkPost objDealerLocationSocialNetworkPost);


        ///// <summary>
        ///// Create new Wall Post
        ///// </summary>
        ///// <param name="">xml request</param>
        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //           ResponseFormat = WebMessageFormat.Xml,
        //           RequestFormat = WebMessageFormat.Xml,
        //           UriTemplate = "CreateFacebookWallPost",
        //           BodyStyle = WebMessageBodyStyle.Bare)]
        //FacebookResponse CreateFacebookWallPost(string UniqueID, string Token, string message, string caption, string description, string name, string picture, string link, string dealerLocationSocialNetworkId, string source, string targeting);
        

        /// <summary>
        /// Create new Wall Post
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "CreateFacebookWallPost",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookResponse CreateFacebookWallPost(FacebookPost facebookPost);
        
        /// <summary>
        /// Get Facebook Album List
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookAlbumList/{DealerLocationID}/{UniqueID}?access_token={Token}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookAlbum GetFacebookAlbumList(string DealerLocationID, string UniqueID, string Token);

        /// <summary>
        /// Create new Facebook Album
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "CreateFacebookAlbum/{UniqueID}/{albumName}/{albumMessage}?access_token={Token}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookResponse CreateFacebookAlbum(string UniqueID, string albumName, string albumMessage, string Token);

        /// <summary>
        /// Create new Facebook Event
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "CreateFacebookEvent",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookResponse CreateFacebookEvent(string UniqueID, string Token, string eventname, string startTime, string endTime, string location, string description, string privacy);

        /// <summary>
        /// Create Facebook Question
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "CreateFacebookQuestion",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookResponse CreateFacebookQuestion(string UniqueID, string Token, string Question, string options, string AllowNewOptions);

        /// <summary>
        /// Upload Photos to Facebook 
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "UploadFacebookPhotos",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        List<FacebookResponse> UploadFacebookPhotos(string Token, string Message, string filenames, string Album_id);

        /// <summary>
        /// Get Question from Facebook
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookQuestion",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookQuestion GetFacebookQuestions(string UniqueID, string Token, string FromDate, string ToDate);

        /// <summary>
        /// Get Question from Facebook
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookEvents",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookEvent GetFacebookEvents(string UniqueID, string Token, string FromDate, string ToDate);

        /// <summary>
        /// Get Facebook Post Statistics
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookPostStatistics",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookPostStatistics GetFacebookPostStatistics(string ResultID, string Token,int PostTypeID);

        /// <summary>
        /// Get Facebook Photo Statistics
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookPhotoStatistics",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookPhotoStatistics GetFacebookPhotoStatistics(string ResultID, string Token);


        /// <summary>
        /// Delete new Facebook Event
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "DELETE",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "DeleteFacebookEvent",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookResponse DeleteFacebookEvent(string ResultID, string Token);

        /// <summary>
        /// Delete new Facebook WallPost
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "DELETE",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "DeleteFacebookWallPost",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookResponse DeleteFacebookWallPost(string ResultID, string Token);


        /// <summary>
        /// Delete new Facebook Question
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "DELETE",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "DeleteFacebookQuestion",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookResponse DeleteFacebookQuestion(string ResultID, string Token);


        /// <summary>
        /// TwitterStatusUpdate
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "TwitterStatusUpdate",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        TwitterResponse TwitterStatusUpdate(string status, string link, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret);

        /// <summary>
        /// Get Facebook Insights
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookInsights",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookInsights GetFacebookInsights(int DealerLocationID, string UserID, string Token, string FromDate, string ToDate, string Periods);

        /// <summary>
        /// Get Facebook Insights
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Json,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookInsights_string",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        string GetFacebookInsights_string(int DealerLocationID, string UserID, string Token, string FromDate, string ToDate, string Periods);

        /// <summary>
        /// Get Facebook Insights
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookInsights",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookPage GetFacebookPageInformation(int DealerLocationID, string UserID, string Token);

        /// <summary>
        /// Get Facebook Feed
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookFeed",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookFeed GetFacebookFeed(string UniqueID, string Token, string FromDate, string ToDate, string limit);
        

        /// <summary>
        /// Get Facebook Album List
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookPermissions/{Token}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        List<string> GetFacebookPermissions(string Token);


        /// <summary>
        /// Get Facebook Album List
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "CheckFacebookPermissions/{DealerLocationID}/{Token}/{PermissionName}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookPermissions CheckFacebookPermissions(string DealerLocationID, string Token, string PermissionName);

        
        /// <summary>
        /// Get Facebook Permissions
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "ValidateTokenDealerLocationID/{DealerLocationID}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        TokenValidation ValidateTokenDealerLocationID(string DealerLocationID);

        

        /// <summary>
        /// Get Facebook Event Statistics
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookEventStatistics/{ResultID}?access_token={Token}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookEventStatistics GetFacebookEventStatistics(string ResultID, string Token);

        /// <summary>
        /// Get Facebook Question Statistics
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetFacebookQuestionStatistics/{ResultID}?access_token={Token}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        FacebookQuestionStatistics GetFacebookQuestionStatistics(string ResultID, string Token);

        /// <summary>
        /// Get GooglePlus Refresh Token 
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetGooglePlusRefreshToken/{authCode}/{client_id}/{client_secret}/{redirect_URL}?grant_type={grant_type}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusRefreshTokenRequestResponse GetGooglePlusRefreshToken(string authCode, string client_id, string client_secret, string redirect_URL, string grant_type);

        /// <summary>
        /// Get GooglePlus AccessToken From refresh_token
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GooglePlusGetAccessToken/{authCode}/{client_id}/{client_secret}/{redirect_URL}?grant_type={grant_type}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusAccessTokenRequestResponse GetGooglePlusAccessToken(string client_id, string client_secret, string refresh_token, string grant_type);

        /// <summary>
        /// Get GooglePlus User From Access token
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetGooglePlusUserFromAccesstoken?access_token={access_token}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusGetUserResponse GetGooglePlusUserFromAccesstoken(string access_token);


        /// <summary>
        /// Get GooglePlus Pages For An User
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetGooglePlusPagesForAnUser/{userID}?access_token={access_token}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusGetPageListResponse GetGooglePlusPagesForAnUser(string userID, string access_token);
    }
}
