﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BusinessEntities;
using BusinessLogic;
using DataLayer;

namespace SOCIALDEALER.PublishWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PublishMgr" in code, svc and config file together.
    public class PublishMgr : IPublishMgr
    {
        public void DoWork()
        {
        }

        //public string CreateWallPost(DealerLocationSocialNetworkPost objDealerLocationSocialNetworkPost)
        //{
        //    try
        //    {
        //        PublishToSocialNetwork.SaveToSocialNetworksTable(objDealerLocationSocialNetworkPost);
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //        PublishManager objPublishManager = new PublishManager();

        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        ObjPublishExceptionLog.PostResponse = string.Empty;
        //        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //        ObjPublishExceptionLog.MethodName = "PublishMgr\\CreateWallPost";
        //        ObjPublishExceptionLog.ExtraInfo = string.Empty;
        //        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

        //        #endregion
        //    }

        //    return objDealerLocationSocialNetworkPost.postText;
        //}

        //public FacebookResponse CreateFacebookWallPost(string UniqueID, string Token, string message, string caption, string description, string name, string picture, string link, string dealerLocationSocialNetworkId, string source, string targeting)
        //{
        //    FacebookResponse objFacebookResponse = new FacebookResponse();
        //    try
        //    {
        //        objFacebookResponse = FaceBook.CreateFacebookWallPost(UniqueID, Token, message, caption, description, name, picture, link,Convert.ToInt32(dealerLocationSocialNetworkId), source, targeting);
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //        PublishManager objPublishManager = new PublishManager();

        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        ObjPublishExceptionLog.PostResponse = string.Empty;
        //        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //        ObjPublishExceptionLog.MethodName = "PublishMgr\\CreateFacebookWallPost";
        //        ObjPublishExceptionLog.ExtraInfo = string.Empty;
        //        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

        //        #endregion
        //    }

        //    return objFacebookResponse;
        //}

        public FacebookResponse CreateFacebookWallPost(FacebookPost facebookPost)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            try
            {
                objFacebookResponse = FaceBook.CreateFacebookWallPost(facebookPost.UniqueID, facebookPost.Token, facebookPost.message, facebookPost.caption, facebookPost.description, facebookPost.name, facebookPost.picture, facebookPost.link, Convert.ToInt32(facebookPost.dealerLocationSocialNetworkId), facebookPost.source, facebookPost.targeting);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\CreateFacebookWallPost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookResponse;
        }

        public FacebookAlbum GetFacebookAlbumList(string DealerLocationID, string UniqueID, string Token)
        {
            FacebookAlbum objFacebookAlbum = new FacebookAlbum();
            try
            {
                objFacebookAlbum = FaceBook.GetFacebookAlbums(Convert.ToInt32(DealerLocationID),UniqueID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookAlbumList";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            
            return objFacebookAlbum;
        }

        public FacebookResponse CreateFacebookAlbum(string UniqueID, string albumName, string albumMessage, string Token)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            try
            {
                objFacebookResponse = FaceBook.CreateFacebookAlbum(UniqueID, Token, albumName, albumMessage);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\CreateFacebookAlbum";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookResponse;
        }

        public FacebookResponse CreateFacebookEvent(string UniqueID, string Token, string eventname, string startTime, string endTime, string location, string description, string privacy)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            try
            {
                objFacebookResponse = FaceBook.CreateFacebookEvent(UniqueID, Token, eventname, startTime, endTime, location, description, privacy);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\CreateFacebookEvent";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookResponse;
        }

        public FacebookResponse CreateFacebookQuestion(string UniqueID, string Token, string Question, string options, string AllowNewOptions)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            try
            {
                objFacebookResponse = FaceBook.CreateFacebookQuestion(UniqueID, Token, Question, options, AllowNewOptions);
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\CreateFacebookQuestion";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookResponse;
        }

        public List<FacebookResponse> UploadFacebookPhotos(string Token, string Message, string filenames, string Album_id)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            List<FacebookResponse> listFacebookResponse = new List<FacebookResponse>();
            try
            {
                DealerLocationSocialNetwork dlSocialNetwork = new DealerLocationSocialNetwork();
                spGetDealerLocationSocialNetworkPostDetails_New_Result obj = new spGetDealerLocationSocialNetworkPostDetails_New_Result();

                string[] filename = filenames.Split(',');

                foreach (string _PhotoURL in filename)
                {
                    objFacebookResponse = FaceBook.UploadFacebookPhoto(Token, Message, _PhotoURL, Album_id, dlSocialNetwork, obj);
                    listFacebookResponse.Add(objFacebookResponse);
                }
                
                
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\UploadFacebookPhotos";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return listFacebookResponse;
        }
        
        public FacebookQuestion GetFacebookQuestions(string UniqueID, string Token, string FromDate, string ToDate)
        {
            FacebookQuestion objFacebookQuestion = new FacebookQuestion();
            try
            {
                DateTime _FromDate = DateTime.Now;
                DateTime _ToDate = DateTime.Now;
                bool isFilter = false;
                objFacebookQuestion = FaceBook.GetFacebookQuestion(UniqueID, Token, FromDate, ToDate);

                if (FromDate == "<undefined value>")
                {
                    FromDate = "-1";
                }
                if (ToDate == "<undefined value>")
                {
                    ToDate = "-1";
                }
                if (FromDate != "-1")
                {
                    try
                    {
                        _FromDate = Convert.ToDateTime(FromDate);
                        isFilter = true;
                    }
                    catch 
                    {
                        objFacebookQuestion.facebookResponse.message = "Invalid From Date. ('-1' will not Filter Date)";
                        objFacebookQuestion.question = null;
                        isFilter = false;
                    }
                }

                if (ToDate != "-1" && isFilter)
                {
                    try
                    {
                        _ToDate = Convert.ToDateTime(ToDate);
                        isFilter = true;
                    }
                    catch
                    {
                        objFacebookQuestion.facebookResponse.message = "Invalid To Date.('-1' will not Filter Date)";
                        objFacebookQuestion.question = null;
                        isFilter = false;
                    }
                }
                else
                {

                }
                               
                if (isFilter)
                {
                    if (_FromDate == _ToDate)
                    {
                        _ToDate = _ToDate.AddDays(1);
                    }
                    if (objFacebookQuestion.question != null)
                    {
                        List<Question> objQuestions = objFacebookQuestion.question.Where(f => f.created_time >= _FromDate && f.created_time <= _ToDate).ToList();
                        objFacebookQuestion.question = objQuestions;
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookQuestions";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookQuestion;
        }

        public FacebookEvent GetFacebookEvents(string UniqueID, string Token, string FromDate, string ToDate)
        {
            FacebookEvent objFacebookEvent = new FacebookEvent();
            try
            {
                DateTime _FromDate = DateTime.Now;
                DateTime _ToDate = DateTime.Now;
                bool isFilter = false;
                objFacebookEvent = FaceBook.GetFacebookEvent(UniqueID, Token, FromDate, ToDate);

                if (FromDate == "<undefined value>")
                {
                    FromDate = "-1";
                }
                if (ToDate == "<undefined value>")
                {
                    ToDate = "-1";
                }
                if (FromDate != "-1")
                {
                    try
                    {
                        _FromDate = Convert.ToDateTime(FromDate);
                        isFilter = true;
                    }
                    catch
                    {
                        objFacebookEvent.facebookResponse.message = "Invalid From Date. ('-1' will not Filter Date)";
                        objFacebookEvent.events = null;
                        isFilter = false;
                    }
                }

                if (ToDate != "-1" && isFilter)
                {
                    try
                    {
                        _ToDate = Convert.ToDateTime(ToDate);
                        isFilter = true;
                    }
                    catch
                    {
                        objFacebookEvent.facebookResponse.message = "Invalid To Date.('-1' will not Filter Date)";
                        objFacebookEvent.events = null;
                        isFilter = false;
                    }
                }
                else
                {

                }

                if (isFilter)
                {
                    if (_FromDate == _ToDate)
                    {
                        _ToDate = _ToDate.AddDays(1);
                    }
                    if (objFacebookEvent.events != null)
                    {
                        List<Event> objQuestions = objFacebookEvent.events.Where(f => f.start_time >= _FromDate && f.start_time <= _ToDate).ToList();
                        objFacebookEvent.events = objQuestions;
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookEvents";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookEvent;
        }

        public FacebookPostStatistics GetFacebookPostStatistics(string ResultID, string Token,int PostTypeID)
        {
            FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();
            try
            {
                objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(ResultID, Token, PostTypeID);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookPostStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookPostStatistics;
        }

        public FacebookPhotoStatistics GetFacebookPhotoStatistics(string ResultID, string Token)
        {
            FacebookPhotoStatistics objFacebookPhotoStatistics = new FacebookPhotoStatistics();
            try
            {
                objFacebookPhotoStatistics = FaceBook.GetFacebookPhotoStatistics(ResultID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookPostStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookPhotoStatistics;
        }

        public FacebookResponse DeleteFacebookEvent(string ResultID, string Token)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            try
            {
                objFacebookResponse = FaceBook.DeleteFacebookEvent(ResultID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\DeleteFacebookEvent";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookResponse;
        }

        public FacebookResponse DeleteFacebookWallPost(string ResultID, string Token)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            try
            {
                objFacebookResponse = FaceBook.DeleteFacebookWallPost(ResultID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\DeleteFacebookWallPost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookResponse;
        }

        public FacebookResponse DeleteFacebookQuestion(string ResultID, string Token)
        {
            FacebookResponse objFacebookResponse = new FacebookResponse();
            try
            {
                objFacebookResponse = FaceBook.DeleteFacebookQuestion(ResultID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\DeleteFacebookQuestion";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookResponse;
        }

        public TwitterResponse TwitterStatusUpdate(string status, string link, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            TwitterResponse objTwitterResponse = new TwitterResponse();
            try
            {
                objTwitterResponse = Twitter.TwitterStatusUpdate2(status, link, oauth_token, oauth_token_secret, oauth_consumer_key, oauth_consumer_secret, 0, 0);
                
                //PublishToSocialNetwork.PostToTwitter(status, link, oauth_token, oauth_token_secret, oauth_consumer_key, oauth_consumer_secret, 0, 0, 0, DateTime.Now);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\TwitterStatusUpdate";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objTwitterResponse;
        }

        public FacebookInsights GetFacebookInsights(int DealerLocationID, string UserID, string Token, string FromDate, string ToDate, string Periods)
        {
            FacebookInsights objFacebookInsights = new FacebookInsights();
            try
            {
                objFacebookInsights = FaceBook.GetFacebookInsights(DealerLocationID, UserID, Token, FromDate,ToDate,Periods);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookInsights";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookInsights;
        }

        public string GetFacebookInsights_string(int DealerLocationID, string UserID, string Token, string FromDate, string ToDate, string Periods)
        {
            FacebookInsights objFacebookInsights = new FacebookInsights();
            try
            {
                objFacebookInsights = FaceBook.GetFacebookInsights(DealerLocationID, UserID, Token, FromDate, ToDate, Periods);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookInsights";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            string str = Utility.SerializeObject<FacebookInsights>(objFacebookInsights);
            return str;
        }

        public FacebookPage GetFacebookPageInformation(int DealerLocationID, string UserID, string Token)
        {
            FacebookPage objFacebookPage = new FacebookPage();
            try
            {
                objFacebookPage = FaceBook.GetFacebookPageInformation(DealerLocationID, UserID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookPageInformation";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookPage;
        }

        public FacebookFeed GetFacebookFeed(string UniqueID, string Token, string FromDate, string ToDate,string limit)
        {
            FacebookFeed objFacebookFeed = new FacebookFeed();
            try
            {
                objFacebookFeed = FaceBook.GetFacebookFeedID(UniqueID, Token, FromDate, ToDate, limit);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookFeed";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookFeed;
        }

        public List<string> GetFacebookPermissions(string Token)
        {
            List<string> listPermissions = new List<string>();
            try
            {
                listPermissions = FaceBook.GetFacebookPermissions(Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookPermissions";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return listPermissions;
        }

        public FacebookPermissions CheckFacebookPermissions(string DealerLocationID, string Token, string PermissionName)
        {
            FacebookPermissions objFacebookPermissions = new FacebookPermissions();
            try
            {
                objFacebookPermissions = FaceBook.CheckFacebookPermissions(Convert.ToInt32(DealerLocationID), Token, PermissionName);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\CheckFacebookPermissions";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookPermissions;
        }

        public TokenValidation ValidateTokenDealerLocationID(string DealerLocationID)
        {
            TokenValidation objTokenValidation = new TokenValidation();
            try
            {
                objTokenValidation = FaceBook.ValidateTokenDealerLocationID(Convert.ToInt32(DealerLocationID));
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\ValidateTokenDealerLocationID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objTokenValidation;
        }

        public FacebookEventStatistics GetFacebookEventStatistics(string ResultID, string Token)
        {
            FacebookEventStatistics objFacebookEventStatistics = new FacebookEventStatistics();
            try
            {
                objFacebookEventStatistics = FaceBook.GetFacebookEventStatistics(ResultID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookEventStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookEventStatistics;
        }

        public FacebookQuestionStatistics GetFacebookQuestionStatistics(string ResultID, string Token)
        {
            FacebookQuestionStatistics objFacebookQuestionStatistics = new FacebookQuestionStatistics();
            try
            {
                objFacebookQuestionStatistics = FaceBook.GetFacebookQuestionStatistics(ResultID, Token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetFacebookQuestionStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookQuestionStatistics;
        }

        public GooglePlusRefreshTokenRequestResponse GetGooglePlusRefreshToken(string authCode, string client_id, string client_secret, string redirect_URL, string grant_type)
        {
            GooglePlusRefreshTokenRequestResponse objGooglePlusRefreshTokenRequestResponse = new GooglePlusRefreshTokenRequestResponse();
            try
            {
                objGooglePlusRefreshTokenRequestResponse = GooglePlus.GetGooglePlusRefreshToken(authCode, client_id, client_secret, redirect_URL, grant_type);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GooglePlusGetRefreshToken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusRefreshTokenRequestResponse;
        }

        public GooglePlusAccessTokenRequestResponse GetGooglePlusAccessToken(string client_id, string client_secret, string refresh_token, string grant_type)
        {
            GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = new GooglePlusAccessTokenRequestResponse();
            try
            {
                objGooglePlusAccessTokenRequestResponse = GooglePlus.GetGooglePlusAccessToken(client_id, client_secret, refresh_token, grant_type);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetGooglePlusAccessToken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusAccessTokenRequestResponse;
        }

        public GooglePlusGetUserResponse GetGooglePlusUserFromAccesstoken(string access_token)
        {
            GooglePlusGetUserResponse objGooglePlusGetUserResponse = new GooglePlusGetUserResponse();
            try
            {
                objGooglePlusGetUserResponse = GooglePlus.GetGooglePlusUserFromAccessToken(access_token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetGooglePlusUserFromAccesstoken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusGetUserResponse;
        }

        public GooglePlusGetPageListResponse GetGooglePlusPagesForAnUser(string userID, string access_token)
        {
            GooglePlusGetPageListResponse objGetGooglePlusPagesForAnUser = new GooglePlusGetPageListResponse();
            try
            {
                objGetGooglePlusPagesForAnUser = GooglePlus.GetGooglePlusPagesForAnUser(userID, access_token);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishMgr\\GetGooglePlusPagesForAnUser";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGetGooglePlusPagesForAnUser;
        }
        
    }
}
