﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BusinessEntities;
using DataLayer;
using BusinessLogic;

namespace SOCIALDEALER.PublishWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PublishDataBaseMgr" in code, svc and config file together.
    public class PublishDataBaseMgr : IPublishDataBaseMgr
    {

        public string SavePost(DealerLocationSocialNetworkPost objDealerLocationSocialNetworkPost)
        {
            string result = string.Empty;
            PublishManager objPublishManager = new PublishManager();
            try
            {
                result = PublishToSocialNetwork.SaveToSocialNetworksTable(objDealerLocationSocialNetworkPost);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(objDealerLocationSocialNetworkPost.dealerLocationSocialNetworkPostId);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishDataBaseMgr\\SavePost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return result;
        }

        public string SavePostQueue(int DealerLocationID, int DealerLocationSocialNetworkPostID, string Type)
        {
            string result = string.Empty;
            PublishManager objPublishManager = new PublishManager();
            try
            {
                result = PublishToSocialNetwork.SaveToSocialNetworksQueueTable(DealerLocationID, DealerLocationSocialNetworkPostID, Type);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishDataBaseMgr\\SavePost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return result;
        }
    }
}
