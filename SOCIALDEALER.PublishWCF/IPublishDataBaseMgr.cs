﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using BusinessEntities;

namespace SOCIALDEALER.PublishWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPublishDataBaseMgr" in both code and config file together.
    [ServiceContract]
    public interface IPublishDataBaseMgr
    {
        /// <summary>
        /// Create new Wall Post
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "SavePost",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        string SavePost(DealerLocationSocialNetworkPost objDealerLocationSocialNetworkPost);

        /// <summary>
        /// Create new Wall Post Queue
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "SavePostQueue",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        string SavePostQueue(int DealerLocationID, int DealerLocationSocialNetworkPostID, string Type);
        
    }
}
