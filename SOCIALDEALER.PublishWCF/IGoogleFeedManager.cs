﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using BusinessEntities;

namespace SOCIALDEALER.PublishWCF
{
    [ServiceContract]
    public interface IGoogleFeedManager
    {
        /// <summary>
        /// Get GooglePlus Page Feed
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetGooglePlusPageFeeds?{pageId}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusGetActivityListByPageResponse GetGooglePlusPageFeeds(string pageId);

        /// <summary>
        /// Get GooglePlus Page Feed Comments
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetGooglePlusFeedComments?{activityID}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusGetCommentsListResponse GetGooglePlusFeedComments(string activityID);

        /// <summary>
        /// Post GooglePlus Page Comments
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "PostGooglePlusComment",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusPostResponse PostGooglePlusComment(string refresh_token, string appKey, string appSecret, string pageID, string activityID, string commentContent);
        //UriTemplate = "PostGooglePlusComment/{activityID}/{appKey}/{appSecret}/{pageID}/{commentContent}?refresh_token={refresh_token}",


        /// <summary>
        /// Delete a GooglePlus Comment
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "DELETE",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "DeleteGooglePlusComment",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusResponse DeleteGooglePlusComment(string refresh_token, string appKey, string appSecret, string pageID, string commentID);

        /// <summary>
        /// Delete a GooglePlus Wall Post
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "DELETE",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "DeleteGooglePlusPost",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        GooglePlusResponse DeleteGooglePlusPost(string refresh_token, string appKey, string appSecret, string pageID, string activityID);


    }
}
