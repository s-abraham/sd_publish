﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using BusinessEntities;
using BusinessLogic;
using DataLayer;

namespace SOCIALDEALER.PublishWCF
{

    public class GoogleFeedManager : IGoogleFeedManager
    {
        PublishManager objPublishManager = new PublishManager();

        public GooglePlusGetActivityListByPageResponse GetGooglePlusPageFeeds(string pageId)
        {
            var googlePlusGetActivityListByPageResponse = new GooglePlusGetActivityListByPageResponse();

            try
            {
                googlePlusGetActivityListByPageResponse = BusinessLogic.GooglePlus.GetGooglePlusActivitiesListForAPage(pageId);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.ExceptionMessage = "";
                ObjPublishExceptionLog.InnerException = string.Empty;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = string.Empty;
                ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\GetGooglePlusPageFeeds";
                ObjPublishExceptionLog.ExtraInfo = "pageId : " + pageId;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            
            return googlePlusGetActivityListByPageResponse;
        }

        public GooglePlusGetCommentsListResponse GetGooglePlusFeedComments(string activityID)
        {
            var googlePlusGetCommentsListResponse = new GooglePlusGetCommentsListResponse();

            try
            {
                googlePlusGetCommentsListResponse = BusinessLogic.GooglePlus.GetGooglePlusCommentsListForAnActivity(activityID);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.ExceptionMessage = "";
                ObjPublishExceptionLog.InnerException = string.Empty;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = string.Empty;
                ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\GetGooglePlusFeedComments";
                ObjPublishExceptionLog.ExtraInfo = "activityID : " + activityID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            
            return googlePlusGetCommentsListResponse;
        }

        public GooglePlusPostResponse PostGooglePlusComment(string refresh_token, string appKey, string appSecret, string pageID, string activityID, string commentContent)
        {
            var googlePlusPostResponse = new GooglePlusPostResponse();

            try
            {
                // Get Access Token
                string grant_type = "refresh_token";
                string access_token = string.Empty;
                access_token = PublishToSocialNetwork.GetGooglePlusAccessTokenbyRefreshToken(appKey, appSecret, refresh_token, grant_type);

                if (!string.IsNullOrEmpty(access_token))
                {
                    googlePlusPostResponse = BusinessLogic.GooglePlus.AddGooglePlusComment(access_token, pageID, activityID, commentContent);
                }
                else
                {
                    #region ERROR HANDLING

                    var ObjPublishExceptionLog = new PublishExceptionLog();
                    ObjPublishExceptionLog.ExceptionMessage = "Failed to generate new access_token by  PublishToSocialNetwork.GetGooglePlusAccessTokenbyRefreshToken(";
                    ObjPublishExceptionLog.InnerException = string.Empty;
                    ObjPublishExceptionLog.PostResponse = string.Empty;
                    ObjPublishExceptionLog.StackTrace = string.Empty;
                    ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\PostGooglePlusComment";
                    ObjPublishExceptionLog.ExtraInfo = "refresh_token : " + refresh_token;
                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = string.Empty;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = string.Empty;
                ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\PostGooglePlusComment";
                ObjPublishExceptionLog.ExtraInfo = "refresh_token : " + refresh_token;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return googlePlusPostResponse;
        }

        public GooglePlusResponse DeleteGooglePlusComment(string refresh_token, string appKey, string appSecret, string pageID, string commentID)
        {
            var googlePlusResponse = new GooglePlusResponse();

            try
            {
                // Get Access Token
                string grant_type = "refresh_token";
                string access_token = string.Empty;
                access_token = PublishToSocialNetwork.GetGooglePlusAccessTokenbyRefreshToken(appKey, appSecret, refresh_token, grant_type);

                if (!string.IsNullOrEmpty(access_token))
                {
                    googlePlusResponse = BusinessLogic.GooglePlus.DeleteGooglePlusComment(access_token, pageID, commentID);
                }
                else
                {
                    #region ERROR HANDLING

                    var ObjPublishExceptionLog = new PublishExceptionLog();
                    ObjPublishExceptionLog.ExceptionMessage = "Failed to generate new access_token by  PublishToSocialNetwork.GetGooglePlusAccessTokenbyRefreshToken(";
                    ObjPublishExceptionLog.InnerException = string.Empty;
                    ObjPublishExceptionLog.PostResponse = string.Empty;
                    ObjPublishExceptionLog.StackTrace = string.Empty;
                    ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\DeleteGooglePlusComment";
                    ObjPublishExceptionLog.ExtraInfo = "refresh_token : " + refresh_token;
                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = string.Empty;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = string.Empty;
                ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\DeleteGooglePlusComment";
                ObjPublishExceptionLog.ExtraInfo = "refresh_token : " + refresh_token;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return googlePlusResponse;
        }

        public GooglePlusResponse DeleteGooglePlusPost(string refresh_token, string appKey, string appSecret, string pageID, string activityID)
        {
            var googlePlusResponse = new GooglePlusResponse();

            try
            {
                // Get Access Token
                string grant_type = "refresh_token";
                string access_token = string.Empty;
                access_token = PublishToSocialNetwork.GetGooglePlusAccessTokenbyRefreshToken(appKey, appSecret, refresh_token, grant_type);

                if (!string.IsNullOrEmpty(access_token))
                {
                    googlePlusResponse = BusinessLogic.GooglePlus.DeleteGooglePlusActivity(access_token, pageID, activityID);
                }
                else
                {
                    #region ERROR HANDLING

                    var ObjPublishExceptionLog = new PublishExceptionLog();
                    ObjPublishExceptionLog.ExceptionMessage = "Failed to generate new access_token by  PublishToSocialNetwork.GetGooglePlusAccessTokenbyRefreshToken(";
                    ObjPublishExceptionLog.InnerException = string.Empty;
                    ObjPublishExceptionLog.PostResponse = string.Empty;
                    ObjPublishExceptionLog.StackTrace = string.Empty;
                    ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\DeleteGooglePlusPost";
                    ObjPublishExceptionLog.ExtraInfo = "refresh_token : " + refresh_token;
                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = string.Empty;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = string.Empty;
                ObjPublishExceptionLog.MethodName = "GoogleFeedManager\\DeleteGooglePlusPost";
                ObjPublishExceptionLog.ExtraInfo = "refresh_token : " + refresh_token;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return googlePlusResponse;
        }
    }
}
