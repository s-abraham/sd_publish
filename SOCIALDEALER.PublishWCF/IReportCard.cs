﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace SOCIALDEALER.PublishWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReportCard" in both code and config file together.
    [ServiceContract]
    public interface IReportCard
    {
        
        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetSocialReportCardByLocation/{DealerLocationID}/{Month}/{Year}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        string GetSocialReportCardByLocation(int DealerLocationID, int Month, int Year);

        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetReputationReportCardByLocation/{DealerLocationID}/{Month}/{Year}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        string GetReputationReportCardByLocation(int DealerLocationID, int Month, int Year);
        

        [OperationContract]
        [WebInvoke(Method = "GET",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "GetCombinedReportCardByLocation/{DealerLocationID}/{Month}/{Year}",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        string GetCombinedReportCardByLocation(int DealerLocationID, int Month, int Year);
        
    }
}
