﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataLayer;
using System.Data.Objects;
using DataLayerInsights;

namespace BusinessLogic
{
    public class PublishManager
    {
        private SocialDealerEntities dbContext = new SocialDealerEntities();

        public PublishManager()
        {
        }

        public spSaveDealerLocationSocialNetworkPostDetails_New_Result SavePostToDealerLocationSocialNetworkPost(string postDetailsXML, long DealerLocationSocialNetworkPostID)
        {
            spSaveDealerLocationSocialNetworkPostDetails_New_Result result = new spSaveDealerLocationSocialNetworkPostDetails_New_Result();

            try
            {
                ObjectResult<spSaveDealerLocationSocialNetworkPostDetails_New_Result> spSaveDealerLocationSocialNetworkPostDetails_New_Result;

                spSaveDealerLocationSocialNetworkPostDetails_New_Result = dbContext.spSaveDealerLocationSocialNetworkPostDetails_New(postDetailsXML);

                //result = Convert.ToString(spSaveDealerLocationSocialNetworkPostDetails_New_Result.Select(o => o.RESULT).SingleOrDefault());
                result = spSaveDealerLocationSocialNetworkPostDetails_New_Result.SingleOrDefault();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID =Convert.ToInt32(DealerLocationSocialNetworkPostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\SavePostToDealerLocationSocialNetworkPost";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return result;
        }
        public spSaveDealerLocationSocialNetworkPostQueueDetails_Result SaveDealerLocationSocialNetworkPostQueueDetails(int DealerLocationID, int DealerLocationSocialNetworkPostID, string Type)
        {
            spSaveDealerLocationSocialNetworkPostQueueDetails_Result result = new spSaveDealerLocationSocialNetworkPostQueueDetails_Result();

            try
            {
                ObjectResult<spSaveDealerLocationSocialNetworkPostQueueDetails_Result> spSaveDealerLocationSocialNetworkPostQueueDetails_Result;

                spSaveDealerLocationSocialNetworkPostQueueDetails_Result = dbContext.spSaveDealerLocationSocialNetworkPostQueueDetails(DealerLocationID, DealerLocationSocialNetworkPostID, Type);

                //result = Convert.ToString(spSaveDealerLocationSocialNetworkPostQueueDetails_Result.Select(o => o.RESULT).SingleOrDefault());
                result = spSaveDealerLocationSocialNetworkPostQueueDetails_Result.SingleOrDefault();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\SaveDealerLocationSocialNetworkPostQueueDetails";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return result;
        }

        
        public bool GetNeedsApprovalFromDealerId(int dealerID)
        {
            var NeedsApproval = false;
            try
            {
                try
                {
                    var spNeedsApprovalFromDealerId_Result = dbContext.spNeedsApprovalFromDealerId(dealerID).SingleOrDefault();
                    //spNeedsApprovalFromDealerId_Result objspNeedsApprovalFromDealerId_Result = spNeedsApprovalFromDealerId_Result.SingleOrDefault();

                    if (spNeedsApprovalFromDealerId_Result != null)
                    {
                        NeedsApproval = Convert.ToBoolean(spNeedsApprovalFromDealerId_Result.NeedsApproval);
                    }
                }
                catch (Exception ex)
                {
                    NeedsApproval = false;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\GetNeedsApprovalFromDealerId";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return NeedsApproval;
        }

        public void AddDealerLocationSocialNetworkPostQueueDetails(bool postToAll,int dealerLocationSocialNetworkPostID,int dealerID,string dealerLocationSocialNetworkID,bool needsApproval)
        {
            try
            {
                if (postToAll)
                {
                    dbContext.spAddDealerLocationSocialNetworkPostQueueDetails(dealerLocationSocialNetworkPostID, dealerID, null, needsApproval);
                }
                else
                {
                    dbContext.spAddDealerLocationSocialNetworkPostQueueDetails(dealerLocationSocialNetworkPostID, dealerID, dealerLocationSocialNetworkID, needsApproval);
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = dealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\AddDealerLocationSocialNetworkPostQueueDetails";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

        public void AddPublishExceptionLog(PublishExceptionLog entity)
        {
            try
            {
                entity.CreatedDate = DateTime.Now;
                
                PublishExceptionLog _PublishExceptionLog = new PublishExceptionLog();
                _PublishExceptionLog.CreatedDate = entity.CreatedDate;
                _PublishExceptionLog.DealerLocationSocialNetworkPostID = entity.DealerLocationSocialNetworkPostID;
                _PublishExceptionLog.DealerLocationSocialNetworkPostQueueID = entity.DealerLocationSocialNetworkPostQueueID;
                _PublishExceptionLog.ExceptionMessage = entity.ExceptionMessage;
                _PublishExceptionLog.InnerException = entity.InnerException;
                _PublishExceptionLog.PostResponse = entity.PostResponse;
                _PublishExceptionLog.StackTrace = entity.StackTrace;
                _PublishExceptionLog.MethodName = entity.MethodName;
                _PublishExceptionLog.ExtraInfo = entity.ExtraInfo;

                dbContext.AddToPublishExceptionLogs(_PublishExceptionLog);
                dbContext.SaveChanges();

                // _dc.clientalert_errors.InsertOnSubmit(entity);
            }
            catch(Exception ex)
            {

            }
        }

        public void AddPublishServiceLog(PublishServiceLog entity)
        {
            try
            {
                entity.CreatedDate = DateTime.Now;

                PublishServiceLog _PublishServiceLog = new PublishServiceLog();
                _PublishServiceLog.CreatedDate = entity.CreatedDate;
                _PublishServiceLog.Message = entity.Message;
                _PublishServiceLog.MethodName = entity.MethodName;                
                _PublishServiceLog.Exception = entity.Exception;
                
                dbContext.AddToPublishServiceLogs(_PublishServiceLog);
                dbContext.SaveChanges();

                // _dc.clientalert_errors.InsertOnSubmit(entity);
            }
            catch (Exception ex)
            {

            }
        }

        //public List<spGetDealerLocationSocialNetworkPostDetails_Result> GetDealerLocationSocialNetworkPostDetails(long DealerLocationSocialNetworkPostID, DateTime ScheduleDate, int DealerLocationID, int IsSent)
        //{
        //    List<spGetDealerLocationSocialNetworkPostDetails_Result> listspGetDealerLocationSocialNetworkPostDetails_Result = new List<spGetDealerLocationSocialNetworkPostDetails_Result>();
        //    try
        //    {
        //        ObjectResult<spGetDealerLocationSocialNetworkPostDetails_Result> spGetDealerLocationSocialNetworkPostDetails_Result = dbContext.spGetDealerLocationSocialNetworkPostDetails(DealerLocationSocialNetworkPostID, ScheduleDate, DealerLocationID, IsSent);

        //        listspGetDealerLocationSocialNetworkPostDetails_Result = spGetDealerLocationSocialNetworkPostDetails_Result.ToList();
        //    }
        //    catch(Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        ObjPublishExceptionLog.PostResponse = string.Empty;
        //        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //        ObjPublishExceptionLog.MethodName = "PublishManager\\GetDealerLocationSocialNetworkPostDetails";
        //        ObjPublishExceptionLog.ExtraInfo = string.Empty;

        //        AddPublishExceptionLog(ObjPublishExceptionLog);

        //        #endregion
        //    }

        //    return listspGetDealerLocationSocialNetworkPostDetails_Result ;
        //}

        public List<spGetDealerLocationSocialNetworkPostDetails_New_Result> GetDealerLocationSocialNetworkPostDetails(string DealerLocationSocialNetworkPostID,long dealerLocationSocialNetworkPostQueueID, DateTime ScheduleDate, int DealerLocationID, int IsSent)
        {
            List<spGetDealerLocationSocialNetworkPostDetails_New_Result> listspGetDealerLocationSocialNetworkPostDetails_New_Result = new List<spGetDealerLocationSocialNetworkPostDetails_New_Result>();
            try
            {
                ObjectResult<spGetDealerLocationSocialNetworkPostDetails_New_Result> spGetDealerLocationSocialNetworkPostDetails_New_Result = dbContext.spGetDealerLocationSocialNetworkPostDetails_New(DealerLocationSocialNetworkPostID,dealerLocationSocialNetworkPostQueueID, ScheduleDate, DealerLocationID, IsSent);

                listspGetDealerLocationSocialNetworkPostDetails_New_Result = spGetDealerLocationSocialNetworkPostDetails_New_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\GetDealerLocationSocialNetworkPostDetails";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return listspGetDealerLocationSocialNetworkPostDetails_New_Result;
        }

        public DealerLocationSocialNetwork GetDealerLocationSocialNetwork(int DealerLocationSocialNetworkID)
        {
            DealerLocationSocialNetwork objDealerLocationSocialNetwork = null;
            try
            {
                objDealerLocationSocialNetwork = dbContext.DealerLocationSocialNetworks.Where(d => d.ID == DealerLocationSocialNetworkID).SingleOrDefault();
            }
            catch(Exception ex)
            {
                 #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\GetDealerLocationSocialNetwork";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objDealerLocationSocialNetwork;
        }

        public DealerLocationSocialNetwork GetDealerLocationSocialNetworkbyDealerLocationID(int DealerLocationID,int SocialNetworkID)
        {
            DealerLocationSocialNetwork objDealerLocationSocialNetwork = null;
            try
            {
                objDealerLocationSocialNetwork = dbContext.DealerLocationSocialNetworks.Where(d => d.LocationID == DealerLocationID && d.SocialNetworkID == SocialNetworkID && d.MainPage == true).SingleOrDefault();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\GetDealerLocationSocialNetwork";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objDealerLocationSocialNetwork;
        }

        public DealerLocation GetDealerLocationByDealerLocationID(int DealerLocationID)
        {
            DealerLocation objDealerLocation = null;
            try
            {
                objDealerLocation = dbContext.DealerLocations.Where(d => d.ID == DealerLocationID).SingleOrDefault();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\GetDealerLocationByDealerLocationID";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objDealerLocation;
        }

        public void PostDealerLocationSocialNetworkPostQueueDetails(long dealerLocationSocialNetworkPostQueueID, string errMsg, string resultID, string detailedError,bool retry)
        {
            try
            {
                dbContext.spPostDealerLocationSocialNetworkPostQueueDetails_New(dealerLocationSocialNetworkPostQueueID, errMsg, resultID, detailedError, retry);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\PostDealerLocationSocialNetworkPostQueueDetails";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

        public static List<Dictionary<string, string>> GetInsights(DateTime queryDate)
        {
            var results = new List<Dictionary<string, string>>();

            var sw = Stopwatch.StartNew();
            var startDate = queryDate.ToShortDateString() + " 00:00:00";
            var endDate = queryDate.ToShortDateString() + " 23:59:59";
            var periods = "";
            var socialNetworkId = 2;
            var insights = new BusinessEntities.FacebookInsights();
            var insightsDictionary = new Dictionary<string, string>();
            var counter = 0;
            try
            {
                var worklist = InsightStorageDL.GetActiveWorkList(socialNetworkId);
                Debug.WriteLine("GetActiveWorkList Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, worklist.Count);

                foreach (var item in worklist)
                {
                    counter++;
                    Statistics.AddAttempt();
                    Debug.WriteLine("Proessing Record: {0}, LocationId: {1}", counter, item.LocationId);

                    insights = FaceBook.GetFacebookInsights(item.LocationId, item.UniqueId, item.Token, startDate, endDate, periods);
                    Debug.WriteLine("GetFacebookInsights Processing Time: {0}ms", sw.ElapsedMilliseconds);

                    if (String.IsNullOrEmpty(insights.facebookResponse.code))
                    {
                        Statistics.AddSuccess();
                        insightsDictionary = FaceBook.GetFacebookInsightsDictionary(insights);
                        results.Add(insightsDictionary);

                        //if (insightsDictionary.Count >= 95)
                        //{
                        //    InsightsFormatter(insightsDictionary);
                        //}

                        Debug.WriteLine("GetFacebookInsightsDictionary Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, insightsDictionary.Count);
                    }
                    else
                    {
                        Statistics.AddFail(item.LocationId, (SocialNetworks)item.SocialNetworkId, insights.facebookResponse.code, insights.facebookResponse.message);
                        Debug.WriteLine("GetFacebookInsightsDictionary Processing Error: {0} for LocationId: {1}", insights.facebookResponse.message, item.LocationId);
                    }

                    Debug.WriteLine("****************************************************************************");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            Debug.WriteLine(Statistics.ShowStats());

            return results;
        }

        public static void InsightsFormatter(Dictionary<string, string> facebookInsightsDictionary)
        {
            foreach (var item in facebookInsightsDictionary)
            {
                Debug.WriteLine("public string " + item.Key + " {get;set;}");
            }
        }

        public void UpdateDealerLocationSocialNetworkPostDetails(long dealerLocationSocialNetworkPostID, string fBAlbumId)
        {            
            try
            {
                dbContext.spUpdateDealerLocationSocialNetworkPostDetails(dealerLocationSocialNetworkPostID, fBAlbumId);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\UpdateDealerLocationSocialNetworkPostDetails";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

        public spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result GetDealerLocationSocialNetworkPostDetailsByPostQueueID(long dealerLocationSocialNetworkPostQueueID)
        {
            spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result spgetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result = new spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result();
            try
            {
                ObjectResult<spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result> objspGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result = dbContext.spGetDealerLocationSocialNetworkPostDetailsByPostQueueID(dealerLocationSocialNetworkPostQueueID);

                spgetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result = objspGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result.SingleOrDefault();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\GetDealerLocationSocialNetworkPostDetailsByPostQueueID";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return spgetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result;
        }

        public List<spGetDealerLocationSocialNetworkPostTimeZone_New_Result> GetDealerLocationSocialNetworkPostTimeZone()
        {
            List<spGetDealerLocationSocialNetworkPostTimeZone_New_Result> listspGetDealerLocationSocialNetworkPostTimeZone_New_Result = new List<spGetDealerLocationSocialNetworkPostTimeZone_New_Result>();
            try
            {
                ObjectResult<spGetDealerLocationSocialNetworkPostTimeZone_New_Result> objspGetDealerLocationSocialNetworkPostTimeZone_New_Result = dbContext.spGetDealerLocationSocialNetworkPostTimeZone_New();

                listspGetDealerLocationSocialNetworkPostTimeZone_New_Result = objspGetDealerLocationSocialNetworkPostTimeZone_New_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\GetDealerLocationSocialNetworkPostTimeZone";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return listspGetDealerLocationSocialNetworkPostTimeZone_New_Result;
        }

        public void UpdateDealerLocationSocialNetworkPostQueue_FeedID(long dealerLocationSocialNetworkPostQueueID, string feedID)
        {            
            try
            {
                dbContext.spUpdateDealerLocationSocialNetworkPostQueue_FeedID(dealerLocationSocialNetworkPostQueueID, feedID);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = String.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishManager\\UpdateDealerLocationSocialNetworkPostQueue_FeedID";
                ObjPublishExceptionLog.ExtraInfo = String.Empty;

                AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }
        
        
    }
}
