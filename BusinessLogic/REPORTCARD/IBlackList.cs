﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.REPORTCARD
{
    public interface IBlacklist : IEnumerable<string>
    {
        bool Countains(string word);
        int Count { get; }
        void UnionWith(IBlacklist other);
    }
}
