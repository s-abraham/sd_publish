﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.REPORTCARD.EXTRACTOR
{
    public class TextExtractor : BaseExtractor
    {
        private readonly string m_File;

        public TextExtractor() : base()
        {

        }

        public TextExtractor(string file) : base()
        {
            m_File = file;
        }


        public override List<string> GetWords(string parseableText)
        {
            var words = parseableText.Replace(".", " ").Replace("!", " ").Replace("-", "").Replace("?", " ").Replace("(", " ").Replace(")", " ").Replace(",", " ").Replace("*", " ").Replace("%", " ").Replace("$", " ").Replace("&amp;", " ").Replace("&quot;", " ").Replace("&#39;", "'").Replace("quot;", " ").Replace("\"", " ").Trim().ToLower().Split(' ').ToList();
                
            foreach (var word in words.Where(word => string.IsNullOrEmpty(word)).ToList())
            {
                words.Remove(word);
            }

            return words;
        }

        //public override List<string> GetWords()
        //{
        //    using (StreamReader reader = File.OpenText(m_File))
        //    {
        //        var words = GetWords(reader);

        //        foreach (var word in words.Where(word => string.IsNullOrEmpty(word)))
        //        {
        //            words.Remove(word);
        //        }

        //        return words;
        //    }
        //}

        //public virtual List<string> GetWords(StreamReader reader)
        //{
        //    var line = reader.ReadLine();

        //    var wordsInLine = new List<string>();

        //    while (line != null)
        //    {
        //        if (CanSkipFile(line))
        //        {
        //            break;
        //        }

        //        wordsInLine = GetWordsInLine(line);

        //        foreach (var word in wordsInLine.Where(word => string.IsNullOrEmpty(word)))
        //        {
        //            wordsInLine.Remove(word);
        //        }

        //        line = reader.ReadLine();
        //    }

        //    return wordsInLine;
        //}

        //protected override List<string> GetWordsInLine(string line)
        //{
        //    var word = new StringBuilder();

        //    foreach (char ch in line)
        //    {
        //        if (char.IsLetter(ch))
        //        {
        //            word.Append(ch);
        //        }
        //        else
        //        {
        //            if (word.Length > 1)
        //            {
        //                yield return word.ToString();
        //            }
        //            word.Clear();
        //        }
        //    }
        //}

        protected virtual bool CanSkipFile(string line)
        {
            return false;
        }
    }
}
