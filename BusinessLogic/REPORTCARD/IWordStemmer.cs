﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.REPORTCARD
{
    public interface IWordStemmer
    {
        string GetStem(string word);
    }
}
