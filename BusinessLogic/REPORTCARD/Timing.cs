﻿namespace BusinessLogic.REPORTCARD
{
    public class Timing
    {
        public long BlackListRetrieved { get; set; }
        public long ReviewsRetrieved { get; set; }
        public long ReviewsGrouped { get; set; }
        public long ExtractedReviews { get; set; }
        public long CountedWords { get; set; }
        public long BlackListProcessed { get; set; }

    }
}