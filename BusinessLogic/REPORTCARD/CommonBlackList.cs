﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLogic.REPORTCARD
{
    public class CommonBlacklist : IBlacklist
    {
        private readonly HashSet<string> m_ExcludedWordsHashSet;

        public CommonBlacklist()
            : this(new string[] { })
        {
        }

        public CommonBlacklist(IEnumerable<string> excludedWords)
            : this(excludedWords, StringComparer.InvariantCultureIgnoreCase)
        {
        }


        public static IBlacklist CreateFromTextFile(string fileName, int dealerLocationId)
        {
            return
                !File.Exists(fileName)
                    ? new NullBlacklist()
                    : CreateFromStremReader(new FileInfo(fileName).OpenText(), dealerLocationId);
        }

        public static IBlacklist CreateFromStremReader(TextReader reader, int dealerLocationId)
        {
            CommonBlacklist commonBlacklist = new CommonBlacklist();

            try
            {
                if (reader == null) throw new ArgumentNullException("reader");
                //var locationNameList = ReviewManager.GetLocationName(dealerLocationId).Trim().ToLower().Split(' ').ToList();
                
                using (reader)
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        line.Trim();
                        commonBlacklist.Add(line);
                        line = reader.ReadLine();
                    }
                }

                //foreach (var word in locationNameList)
                //{
                //    commonBlacklist.Add(word);
                //}
            }
            catch (Exception)
            {
                
                throw;
            }
            return commonBlacklist;
        }

        public CommonBlacklist(IEnumerable<string> excludedWords, StringComparer comparer)
        {
            m_ExcludedWordsHashSet = new HashSet<string>(excludedWords, comparer);
        }

        public bool Countains(string word)
        {
            return m_ExcludedWordsHashSet.Contains(word);
        }

        public void Add(string word)
        {
            m_ExcludedWordsHashSet.Add(word);
        }

        public int Count
        {
            get { return m_ExcludedWordsHashSet.Count; }
        }

        public void UnionWith(IBlacklist other)
        {
            this.m_ExcludedWordsHashSet.UnionWith(other);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return this.m_ExcludedWordsHashSet.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
