﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.REPORTCARD
{
    public static class WordExtensions
    {
        public static List<WordGroup> GroupByStem(this List<IWord> words, IWordStemmer stemmer)
        {
            return
                words.GroupBy(
                    word => stemmer.GetStem(word.Text),
                    (stam, sameStamWords) => new WordGroup(stam, sameStamWords)).ToList();

        }

        public static List<T> SortByOccurences<T>(this List<T> words) where T : IWord
        {
            return
                words.OrderByDescending(
                    word => word.Occurrences).ToList();
        }

        public static List<IWord> CountOccurences(this List<string> terms)
        {
            return
                terms.GroupBy(
                    term => term,
                    (term, equivalentTerms) => new Word(term, equivalentTerms.Count()),
                    StringComparer.InvariantCultureIgnoreCase)
                    .Cast<IWord>().ToList();
        }

        public static List<string> Filter(this List<string> terms, IBlacklist blacklist)
        {
            return
                terms.Where(
                    term => !blacklist.Countains(term)).ToList();
        }
    }
}
