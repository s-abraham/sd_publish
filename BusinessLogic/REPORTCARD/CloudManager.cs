﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.REPORTCARD.EXTRACTOR;

namespace BusinessLogic.REPORTCARD
{
    public class CloudManager
    {
        TextExtractor _textExtractor;
        
        public CloudManager()
        {
            _textExtractor = new TextExtractor();
        }

       
        
        public List<string> GenerateList(int dealerLocationId, List<string> ReviewList, List<string> locationNameList)
        {
            var finalResult = new List<string>();
            var sw = new Stopwatch();
            var timing = new Timing();
            sw.Start();
            var blacklist = GetBlacklist(dealerLocationId);
            timing.BlackListRetrieved = sw.ElapsedMilliseconds;
            IEnumerable<IWord> wordList = new List<IWord>();
            var reviewSet = new StringBuilder();           
            timing.ReviewsRetrieved = sw.ElapsedMilliseconds;
            var counter = 0;
            var mostCommonNames = new CommonFirstNames();

            foreach (var review in ReviewList)
            {
                counter++;
                reviewSet.Append(review + " " );
                //Debug.WriteLine("#" + counter);
            }

            var EnglishWordsOnly = new EnglishCommonWords();
            var words = _textExtractor.GetWords(reviewSet.ToString());

            //Debug.WriteLine("Extracted:" + counter);
            var freshList = WordExtensions.CountOccurences(words);

            int totalCount = 0;        

            //Debug.WriteLine("WordList Count before Backlist removal:" + freshList.Count);

            bool isANumber;
            double number2 = 0;

            foreach (IWord word in freshList.ToList())
            {
                isANumber = double.TryParse(word.Text, out number2);
                if (isANumber == true)
                {
                    freshList.Remove(word);
                }

                if (EnglishWordsOnly.Contains(word.Text.ToLower()) || locationNameList.Contains(word.Text.ToLower()) || word.Text.Length <=2)
                {
                    //Debug.WriteLine("Blacklisted Word Found:" + word.Text + ": " + word.Occurrences);
                    freshList.Remove(word);
                }
            }

            //Debug.WriteLine("WordList Count after Backlist removal:" + freshList.Count);
            
            freshList = SortandFilterWordList(freshList);

            //DisplayResults(freshList);
            Debug.WriteLine("Extracted Cloud tags in " + sw.ElapsedMilliseconds + "ms");

            foreach (var word in freshList)
            {
                totalCount += word.Occurrences;
            
            }

            foreach (var word in freshList)
            {
                finalResult.Add(word.Text + " " + Math.Round(((word.Occurrences * 100.00 )/ totalCount),1).ToString());
            }

            return finalResult;
        }

        private List<IWord> SortandFilterWordList(List<IWord> wordList)
        {
            var results = new List<IWord>();
            var moreResults = new List<IWord>();
            var otherWords = new EnglishCommonWords();
            

            results = wordList.OrderByDescending(x => x.Occurrences).Take(40).ToList();
            
            return results;
        }

        private void DisplayResults(List<IWord> wordList)
        {
            foreach (var word in wordList)
            {
                Debug.WriteLine(word.Text + " \t" + word.Occurrences);
            }
        }

        private static IBlacklist GetBlacklist(int dealerLocationId)
        {
            IBlacklist result;
            try
            {
                var blacklistPath = AppDomain.CurrentDomain.BaseDirectory + "BlackList.txt";
                result = CommonBlacklist.CreateFromTextFile(blacklistPath, dealerLocationId);
                result.UnionWith(new EnglishCommonWords());
            }
            catch (Exception)
            {
                
                throw;
            }
            
            return result;
        }

        //private List<string> GetReviews(int dealerLocationId, int ratingPolarity = -1, int reviewSourceId = -1)
        //{
        //    var results = new List<string>();

        //    try
        //    {
        //        results = reportManager.GetAllReviewsByLocationId(dealerLocationId, ratingPolarity, reviewSourceId);
        //    }
        //    catch (Exception)
        //    {
                
        //    }

        //    return results;
        //}

        
    }
}
