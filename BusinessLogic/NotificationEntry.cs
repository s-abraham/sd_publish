﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    public class NotificationEntry
    {
        public NotificationEntry()
        {
            NotificationDetail = new DataLayer.Notification();
            NotificationTransmissionDetail = new DataLayer.NotificationTransmission();
        }

        public DataLayer.Notification NotificationDetail { get; set; }

        public DataLayer.NotificationTransmission NotificationTransmissionDetail { get; set; }
    }
}
