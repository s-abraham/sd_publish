﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using DataLayer;
using System.Dynamic;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Net;
using System.IO;
using BusinessEntities;
using Newtonsoft.Json;
using DataLayerInsights;
using HtmlAgilityPack;

namespace BusinessLogic
{
    public static class FaceBook
    {
        #region variable

        private static string FacebookGraphAPIURLWallPostCreate = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.WallPost.Create"]);
        private static string FacebookGraphAPIURLUploadPhoto = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.UploadPhoto"]);
        private static string FacebookGraphAPIURLAlbumGet = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Album.Get"]);
        private static string FacebookGraphAPIURLAlbumCreate = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Album.Create"]);
        private static string FacebookGraphAPIURLEventCreate = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Event.Create"]);
        private static string FacebookGraphAPIURLQuestionCreate = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Question.Create"]);
        private static string FacebookGraphAPIURLQuestionGet = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Question.Get"]);
        private static string FacebookGraphAPIURLEventGet = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Event.Get"]);
        private static string FacebookGraphAPIURLPostStatistics = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.PostStatistics"]);
        private static string FacebookGraphAPIURLPhotoStatistics = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.PhotoStatistics"]);
        private static string FacebookGraphAPIURLEventDelete = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Event.Delete"]);
        private static string FacebookGraphAPIURLWallPostDelete = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.WallPost.Delete"]);
        private static string FacebookGraphAPIURLQuestionDelete = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Question.Delete"]);        
        private static string FacebookGraphAPIURLInsights = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Insights"]);
        private static string FacebookGraphAPIURLPage = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Page"]);
        private static string FacebookGraphAPIURLPlace = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Place"]);
        private static string FacebookGraphAPIURLFeed = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Feed"]);
        private static string FacebookGraphAPIURLObjectIDGetResultID = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.ObjectID.Get.ResultID"]);
        private static string FacebookGraphAPIURLEventStatistics = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.Event.Statistics"]);
        private static string FacebookGraphAPIURLQuestionStatistics = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.QuestionStatistics"]);
        private static string FacebookGraphAPIURLPostRegStatistics = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.PostRegStatistics"]);
        private static string FacebookGraphAPIURLPostInsightStatistics = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.PostInsightStatistics"]);
        private static string FacebookGraphAPIURLPhotoRegStatistics = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.PhotoRegStatistics"]);        
        private static string FacebookAPITimeOut = Convert.ToString(ConfigurationManager.AppSettings["FacebookAPITimeOut"]);
        private static string FacebookGraphAPIURLPostStatisticsByFeedID = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.PostStatisticsByFeedID"]);
        private static string FacebookGraphAPIURLUserPicture = Convert.ToString(ConfigurationManager.AppSettings["FacebookGraphAPIURL.UserPicture"]);

        #endregion

        #region Functions

        #region Public

        public static string GetTargetingInfo(spGetDealerLocationSocialNetworkPostDetails_New_Result post)
        {
            string targeting = "";
            if (string.IsNullOrEmpty(post.Region) && string.IsNullOrEmpty(post.City))
            {
                targeting = "{'countries':'" + post.Country.Trim() + "'}";
            }
            else
            {
                if (!string.IsNullOrEmpty(post.Region))
                {
                    targeting = "{'countries':'" + post.Country.Trim() + "','regions':[";
                    foreach (string region in post.Region.Split(','))
                    {
                        targeting += "{'name': '" + region + "'},";
                    }
                    targeting = targeting.Substring(0, targeting.Length - 1) + "]}";
                }
                else if (!string.IsNullOrEmpty(post.City))
                {
                    targeting = "{'cities':[";
                    foreach (string city in post.City.Split(','))
                    {
                        targeting += "{'name': '" + city + "'},";
                    }
                    targeting = targeting.Substring(0, targeting.Length - 1) + "]}";
                }
            }
            return targeting;
        }
        
        #region Wall Post

        public static FacebookPostStatistics GetFacebookPostStatistics(string ResultID, string Token, int PostTypeID, bool GetUserPicture = false)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();
            //string fields = "id,from,message,actions,privacy,type,status_type,created_time,updated_time,comments.limit(5000).fields(id,from,message,created_time,user_likes,like_count,likes.fields(id,name)),likes.limit(50000).fields(id,name),shares,application";
            string fields = string.Empty;
            if (PostTypeID == Convert.ToInt32(PostType.FacebookStatus) || PostTypeID == Convert.ToInt32(PostType.FacebookLink))
            {
                fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),message,privacy,type,application,is_published,object_id,shares";
                if (GetUserPicture)
                {
                    fields = "id,from,name,link,created_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),message,privacy,application,shares";
                }
            }
            else if (PostTypeID == Convert.ToInt32(PostType.FacebookPhoto) || PostTypeID == Convert.ToInt32(PostType.FacebookMultiplePhoto))
            {
                fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),sharedposts.limit(5000).fields(created_time,from,id,name,message)";
            }

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PostStatistics objPostStatistics = new PostStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPostStatistics))
                    {
                        FacebookGraphAPIURLPostStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLPostStatistics, ResultID, fields, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);
                        

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objPostStatistics = jss.Deserialize<PostStatistics>(rawResponse);

                                if (PostTypeID == Convert.ToInt32(PostType.FacebookPhoto) || PostTypeID == Convert.ToInt32(PostType.FacebookMultiplePhoto))
                                {
                                    objPostStatistics.message = objPostStatistics.name;
                                }

                                #region GetUserPicture
                                if (GetUserPicture)
                                {
                                    if (objPostStatistics.comments != null)
                                    {
                                        foreach (var item in objPostStatistics.comments.data)
                                        {
                                            //Get User Picture
                                            FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(item.from.id);
                                            if (objFaceBookUserPicture != null)
                                            {
                                                if (objFaceBookUserPicture.picture.data != null)
                                                {
                                                    item.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                                }
                                                else
                                                {
                                                    item.from.pictureurl = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                item.from.pictureurl = string.Empty;
                                            }

                                        }
                                    }

                                    if (objPostStatistics != null)
                                    {
                                        //Get User Picture
                                        FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(objPostStatistics.from.id);
                                        if (objFaceBookUserPicture != null)
                                        {
                                            if (objFaceBookUserPicture.picture.data != null)
                                            {
                                                objPostStatistics.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                            }
                                            else
                                            {
                                                objPostStatistics.from.pictureurl = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            objPostStatistics.from.pictureurl = string.Empty;
                                        }
                                    }
                                }
                                #endregion

                                if (rawResponse == @"{""id"":""" + "" + ResultID + "" + @"""}")
                                {
                                    objPostStatistics.is_Deleted = true;
                                }
                            }

                            objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatistics.postStatistics = objPostStatistics;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";

                            objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatistics.postStatistics = objPostStatistics;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatistics";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPostStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookPostStatistics.postStatistics = objPostStatistics;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            
            return objFacebookPostStatistics;
        }

        public static FacebookPostStatisticsFeedID GetFacebookPostStatisticsByFeedId(string FeedID, string Token, bool GetUserPicture = false)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();
            
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PostStatisticsByFeedID objPostStatisticsByFeedID = new PostStatisticsByFeedID();
            try
            {
                if (!string.IsNullOrEmpty(FeedID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPostStatisticsByFeedID))
                    {
                        FacebookGraphAPIURLPostStatisticsByFeedID = "https://graph.facebook.com/{0}?access_token={1}";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "25000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLPostStatisticsByFeedID, FeedID, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);


                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objPostStatisticsByFeedID = jss.Deserialize<PostStatisticsByFeedID>(rawResponse);

                                #region GetUserPicture
                                if (GetUserPicture)
                                {
                                    if (objPostStatisticsByFeedID.comments != null)
                                    {
                                        foreach (var item in objPostStatisticsByFeedID.comments.data)
                                        {
                                            //Get User Picture
                                            FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(item.from.id);
                                            if (objFaceBookUserPicture != null)
                                            {
                                                if (objFaceBookUserPicture.picture.data != null)
                                                {
                                                    item.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                                }
                                                else
                                                {
                                                    item.from.pictureurl = string.Empty;
                                                }
                                            }
                                            else
                                            {
                                                item.from.pictureurl = string.Empty;
                                            }

                                        }
                                    }

                                    if (objPostStatisticsByFeedID != null)
                                    {
                                        //Get User Picture
                                        FaceBookUserPicture objFaceBookUserPicture = FaceBookUserPicture(objPostStatisticsByFeedID.from.id);
                                        if (objFaceBookUserPicture != null)
                                        {
                                            if (objFaceBookUserPicture.picture.data != null)
                                            {
                                                objPostStatisticsByFeedID.from.pictureurl = objFaceBookUserPicture.picture.data.url;
                                            }
                                            else
                                            {
                                                objPostStatisticsByFeedID.from.pictureurl = string.Empty;
                                            }
                                        }
                                        else
                                        {
                                            objPostStatisticsByFeedID.from.pictureurl = string.Empty;
                                        }
                                    }
                                }
                                #endregion

                                if (rawResponse == @"{""id"":""" + "" + FeedID + "" + @"""}")
                                {
                                    objPostStatisticsByFeedID.is_Deleted = true;
                                }
                            }

                            objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying PostStatistics By FeedID from FaceBook.";

                            objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                            objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatisticsByFeedId";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPostStatisticsFeedID.facebookResponse = ObjFacebookResponse;
                    objFacebookPostStatisticsFeedID.postStatisticsbyfeedid = objPostStatisticsByFeedID;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPostStatisticsByFeedId";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return objFacebookPostStatisticsFeedID;
        }

        public static FacebookResponse CreateFacebookWallPost(string message, string caption, string description, string name, string picture, string link, int dealerLocationSocialNetworkId, string source, string targeting, DealerLocationSocialNetwork dlSocialNetwork, spGetDealerLocationSocialNetworkPostDetails_New_Result obj)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (dlSocialNetwork != null)
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLWallPostCreate))
                    {
                        FacebookGraphAPIURLWallPostCreate = "https://graph.facebook.com/{0}/feed?access_token={1}";
                    }
                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLWallPostCreate, dlSocialNetwork.UniqueID, dlSocialNetwork.Token);

                    List<KeyValuePair<string, string>> listparameters = new List<KeyValuePair<string, string>>();
                    string parameters = string.Empty;

                    if (!string.IsNullOrEmpty(message))
                        listparameters.Add(new KeyValuePair<String, String>("message", message));
                    if (!string.IsNullOrEmpty(caption))
                        listparameters.Add(new KeyValuePair<String, String>("caption", caption));
                    if (!string.IsNullOrEmpty(description))
                        listparameters.Add(new KeyValuePair<String, String>("description", description));
                    if (!string.IsNullOrEmpty(name))
                        listparameters.Add(new KeyValuePair<String, String>("name", name));
                    if (!string.IsNullOrEmpty(picture))
                        listparameters.Add(new KeyValuePair<String, String>("picture", picture));
                    else
                        listparameters.Add(new KeyValuePair<String, String>("picture", null));
                    if (!string.IsNullOrEmpty(source))
                        listparameters.Add(new KeyValuePair<String, String>("source", source));
                    if (!string.IsNullOrEmpty(targeting))
                        listparameters.Add(new KeyValuePair<String, String>("targeting", targeting));

                    if (!string.IsNullOrEmpty(link))
                    {
                        listparameters.Add(new KeyValuePair<String, String>("link", link));
                        //string actions = "{ 'name': 'Share', 'link':'http://www.facebook.com/sharer.php?u=" + Uri.EscapeDataString(link) + "&t=" + Uri.EscapeDataString("SOCIALDEALER") + "'}";
                        //listparameters.Add(new KeyValuePair<String, String>("actions", actions));
                    }

                    foreach (var val in listparameters)
                    {
                        if (string.IsNullOrEmpty(parameters))
                        {
                            parameters = val.Key + "=" + val.Value;
                        }
                        else
                        {
                            parameters = parameters + "&" + val.Key + "=" + val.Value;
                        }
                    }
                                        
                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "POST";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
                        objFacebookRequest.Parameters = parameters;

                        rawResponse = ExecuteCommand_WallPost(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        }
                        else
                        {
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookWallPost";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.ID = string.Empty;
                    ObjFacebookResponse.message = "DealerLocationSocialNetwork Object is NULL";
                    ObjFacebookResponse.type = "Validation";
                    ObjFacebookResponse.code = string.Empty;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookWallPost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }
            return ObjFacebookResponse;
        }

        public static FacebookResponse CreateFacebookWallPost(string UniqueID,string Token, string message, string caption, string description, string name, string picture, string link, int dealerLocationSocialNetworkId, string source, string targeting)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLWallPostCreate))
                    {
                        FacebookGraphAPIURLWallPostCreate = "https://graph.facebook.com/{0}/feed?access_token={1}";
                    }
                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLWallPostCreate, UniqueID, Token);

                    List<KeyValuePair<string, string>> listparameters = new List<KeyValuePair<string, string>>();
                    string parameters = string.Empty;

                    if (!string.IsNullOrEmpty(message))
                        listparameters.Add(new KeyValuePair<String, String>("message", message));
                    if (!string.IsNullOrEmpty(caption))
                        listparameters.Add(new KeyValuePair<String, String>("caption", caption));
                    if (!string.IsNullOrEmpty(description))
                        listparameters.Add(new KeyValuePair<String, String>("description", description));
                    if (!string.IsNullOrEmpty(name))
                        listparameters.Add(new KeyValuePair<String, String>("name", name));
                    if (!string.IsNullOrEmpty(picture))
                        listparameters.Add(new KeyValuePair<String, String>("picture", picture));
                    else
                        listparameters.Add(new KeyValuePair<String, String>("picture", null));
                    if (!string.IsNullOrEmpty(source))
                        listparameters.Add(new KeyValuePair<String, String>("source", source));
                    if (!string.IsNullOrEmpty(targeting))
                        listparameters.Add(new KeyValuePair<String, String>("targeting", targeting));

                    if (!string.IsNullOrEmpty(link))
                    {
                        listparameters.Add(new KeyValuePair<String, String>("link", link));
                        //string actions = "{ 'name': 'Share', 'link':'http://www.facebook.com/sharer.php?u=" + Uri.EscapeDataString(link) + "&t=" + Uri.EscapeDataString("SOCIALDEALER") + "'}";
                        //listparameters.Add(new KeyValuePair<String, String>("actions", actions));
                    }

                    foreach (var val in listparameters)
                    {
                        if (string.IsNullOrEmpty(parameters))
                        {
                            parameters = val.Key + "=" + val.Value;
                        }
                        else
                        {
                            parameters = parameters + "&" + val.Key + "=" + val.Value;
                        }
                    }

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "POST";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
                        objFacebookRequest.Parameters = parameters;

                        rawResponse = ExecuteCommand_WallPost(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        }
                        else
                        {
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookWallPost";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.ID = string.Empty;
                    ObjFacebookResponse.message = "DealerLocationSocialNetwork Object is NULL";
                    ObjFacebookResponse.type = "Validation";
                    ObjFacebookResponse.code = string.Empty;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookWallPost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookWallPost";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }
            return ObjFacebookResponse;
        }
        
        public static FacebookResponse DeleteFacebookWallPost(string ResultID, string Token)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLWallPostDelete))
                    {
                        FacebookGraphAPIURLWallPostDelete = "https://graph.facebook.com/{0}?access_token={1}";
                    }
                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }



                    string URL = string.Format(FacebookGraphAPIURLWallPostDelete, ResultID, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "DELETE";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_WallPost(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else if (rawResponse == "true")
                            {
                                ObjFacebookResponse.message = "WallPost Deleted Successfully.";
                            }
                            else if (rawResponse == "false")
                            {
                                ObjFacebookResponse.message = "WallPost Deleted Successfully.";
                            }
                            else
                            {
                                ObjFacebookResponse.message = rawResponse;
                            }
                        }
                        else
                        {
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\DeleteFacebookWallPost";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception\\DeleteFacebookWallPost";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\DeleteFacebookWallPost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\DeleteFacebookWallPost";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        #endregion

        #region Photo
        
        public static FacebookResponse UploadFacebookPhoto(string Token,string message, string filename, string Album_id, DealerLocationSocialNetwork dlSocialNetwork, spGetDealerLocationSocialNetworkPostDetails_New_Result obj)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (!string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLUploadPhoto))
                    {
                        FacebookGraphAPIURLUploadPhoto = "https://graph.facebook.com/";
                    }
                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }
                    FacebookAPITimeOut = Convert.ToString(Convert.ToInt32(FacebookAPITimeOut) + 10000);

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = FacebookGraphAPIURLUploadPhoto;
                        objFacebookRequest.Method = "POST";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
                        objFacebookRequest.Message = message;
                        objFacebookRequest.Filename = Guid.NewGuid().ToString();
                        objFacebookRequest.Album_id = Album_id;
                        objFacebookRequest.bytes = DownloadImage(filename);

                        rawResponse = ExecuteCommand_Photo(objFacebookRequest, Token);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        }
                        else
                        {
                        }
                        ObjFacebookResponse.PhotoURL = filename;
                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\UploadFacebookPhoto";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception\\UploadFacebookPhoto";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }
                    
                    
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\UploadFacebookPhoto";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\UploadFacebookPhoto";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        public static FacebookPhotoStatistics GetFacebookPhotoStatistics(string ResultID, string Token)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookPhotoStatistics objFacebookPhotoStatistics = new FacebookPhotoStatistics();
            //string fields = "id,from,name,picture,source,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),tags.limit(500).fields(id,name,created_time,x,y),sharedposts.limit(5000).fields(created_time,from,id,name,message)";
            //string fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),message,privacy,type,application,is_published,object_id,shares";
            string fields = "id,from,name,link,created_time,updated_time,likes.limit(50000).fields(id,name),comments.limit(50000).fields(id,from,like_count,message,created_time,likes.limit(50000).fields(id,name),user_likes),shares";

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PhotoStatistics objPhotoStatistics = new PhotoStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLPhotoStatistics))
                    {
                        FacebookGraphAPIURLPhotoStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLPhotoStatistics, ResultID, fields, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objPhotoStatistics = jss.Deserialize<PhotoStatistics>(rawResponse);

                                if (rawResponse == @"{""id"":""" + "" + ResultID + "" + @"""}")
                                {
                                    objPhotoStatistics.is_Deleted = true;
                                }
                            }

                            objFacebookPhotoStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookPhotoStatistics.photoStatistics = objPhotoStatistics;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying PostStatistics from FaceBook.";

                            objFacebookPhotoStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookPhotoStatistics.photoStatistics = objPhotoStatistics;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPhotoStatistics";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPhotoStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookPhotoStatistics.photoStatistics = objPhotoStatistics;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPhotoStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return objFacebookPhotoStatistics;
        }

        public static FaceBookUserPicture FaceBookUserPicture(string FacebookID)
        {
            FaceBookUserPicture objFaceBookUserPicture = new FaceBookUserPicture();
            try
            {
                if (!string.IsNullOrEmpty(FacebookID))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLUserPicture))
                    {
                        FacebookGraphAPIURLPhotoStatistics = "https://graph.facebook.com/{0}?fields=picture.type%28small%29";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLUserPicture, FacebookID);

                    string rawResponse = string.Empty;
                    try
                    {
                         FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                //rawResponse = rawResponse.Replace(@"{""error"":", "");
                                //rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            else
                            {
                                objFaceBookUserPicture = jss.Deserialize<FaceBookUserPicture>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
            }

            return objFaceBookUserPicture;
        }
        #endregion

        #region Album

        public static FacebookAlbum GetFacebookAlbums(int DealerLocationID, string UniqueID, string Token)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookAlbum FacebookAlbums = new FacebookAlbum();

            string fields = "id,name,can_upload,count";
            string limit = "1000";
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Album albums = new Album();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLAlbumGet))
                {
                    FacebookGraphAPIURLAlbumGet = "https://graph.facebook.com/{0}/albums?fields={1}&limit={2}&access_token={3}";
                }
                if (string.IsNullOrEmpty(FacebookAPITimeOut))
                {
                    FacebookAPITimeOut = "20000";
                }

                if (DealerLocationID != 0 && (string.IsNullOrEmpty(UniqueID) && string.IsNullOrEmpty(Token)))
                {
                    DealerLocationSocialNetwork objDealerLocationSocialNetwork = objPublishManager.GetDealerLocationSocialNetworkbyDealerLocationID(DealerLocationID,2);

                    if (objDealerLocationSocialNetwork != null)
                    {
                        UniqueID = objDealerLocationSocialNetwork.UniqueID;
                        Token = objDealerLocationSocialNetwork.Token;
                    }
                }

                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    string URL = string.Format(FacebookGraphAPIURLAlbumGet, UniqueID, fields,limit, Token);

                    string rawResponse = string.Empty;
                    
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();
                        
                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else //if (rawResponse.StartsWith(@"{""data"":"))
                            {
                                //rawResponse = rawResponse.Replace(@"{""data"":", "");
                                //rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                albums = jss.Deserialize<Album>(rawResponse);
                            }

                            FacebookAlbums.facebookResponse = ObjFacebookResponse;
                            FacebookAlbums.albums = albums;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Album List from FaceBook.";

                            FacebookAlbums.facebookResponse = ObjFacebookResponse;
                            FacebookAlbums.albums = albums;
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookAlbums";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }               
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    FacebookAlbums.facebookResponse = ObjFacebookResponse;
                    FacebookAlbums.albums = albums;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookAlbums";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return FacebookAlbums;
        }

        public static FacebookResponse CreateFacebookAlbum(string UniqueID, string Token, string albumName, string albumMessage)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                //https://graph.facebook.com/142257429131489/albums?name=Photo Album 2&message=Test Photo Album from Post Method 2&access_token=AAAAANqD36fQBAK0dzaCrQbZCGhXnxRTL2abdGBtKxUoWRdhCddQQlXqV2lVlX3ZBBqU3sOO28i3pPYsDUR2ZBm0pNXXDrZAHe4IflcqI5UIEvnXeNPuG
                if (string.IsNullOrEmpty(FacebookGraphAPIURLAlbumCreate))
                {
                    FacebookGraphAPIURLAlbumCreate = "https://graph.facebook.com/{0}/albums?access_token={1}";
                }
                if (string.IsNullOrEmpty(FacebookAPITimeOut))
                {
                    FacebookAPITimeOut = "20000";
                }

                if (!string.IsNullOrEmpty(albumName) && !string.IsNullOrEmpty(albumMessage) && !string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    string URL = string.Format(FacebookGraphAPIURLAlbumCreate, UniqueID, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        List<KeyValuePair<string, string>> listparameters = new List<KeyValuePair<string, string>>();
                        string parameters = string.Empty;

                        if (!string.IsNullOrEmpty(albumName))
                            listparameters.Add(new KeyValuePair<String, String>("name", albumName));
                        if (!string.IsNullOrEmpty(albumMessage))
                            listparameters.Add(new KeyValuePair<String, String>("message", albumMessage));

                        foreach (var val in listparameters)
                        {
                            if (string.IsNullOrEmpty(parameters))
                            {
                                parameters = val.Key + "=" + val.Value;
                            }
                            else
                            {
                                parameters = parameters + "&" + val.Key + "=" + val.Value;
                            }
                        }

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "POST";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
                        objFacebookRequest.Parameters = parameters;
                        
                        rawResponse = ExecuteCommand_CreateAlbum(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                            }
                            ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                        }
                        else
                        {
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookAlbum";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(0);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(0);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookAlbum";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookAlbum";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }
        
        #endregion

        #region Event

        public static FacebookEvent GetFacebookEvent(string UniqueID, string Token, string FromDate, string ToDate)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookEvent objFacebookEvent = new FacebookEvent();
            string fields = "id,name,start_time,end_time,is_date_only,description,rsvp_status,location";
            string limit = "1000";

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            List<Event> events = new List<Event>();
            try
            {
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLEventGet))
                    {
                        FacebookGraphAPIURLEventGet = "https://graph.facebook.com/{0}/events?fields={1}&limit={2}&access_token={3}";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLEventGet, UniqueID, fields, limit, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else if (rawResponse.StartsWith(@"{""data"":"))
                            {
                                rawResponse = rawResponse.Substring(8, rawResponse.Length - 8);
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                int lastindex = rawResponse.LastIndexOf(']');
                                rawResponse = rawResponse.Substring(0, lastindex + 1);

                                events = jss.Deserialize<List<Event>>(rawResponse);
                            }

                            objFacebookEvent.facebookResponse = ObjFacebookResponse;
                            objFacebookEvent.events = events;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Question List from FaceBook.";

                            objFacebookEvent.facebookResponse = ObjFacebookResponse;
                            objFacebookEvent.events = events;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookEvent";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookEvent.facebookResponse = ObjFacebookResponse;
                    objFacebookEvent.events = events;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookEvent";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return objFacebookEvent;
        }

        public static FacebookResponse CreateFacebookEvent(string UniqueID, string Token, string eventname, string startTime, string endTime, string location, string description, string privacy)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLEventCreate))
                {
                    FacebookGraphAPIURLEventCreate = "https://graph.facebook.com/{0}/events?access_token={1}";
                }
                if (string.IsNullOrEmpty(FacebookAPITimeOut))
                {
                    FacebookAPITimeOut = "20000";
                }

                string URL = string.Format(FacebookGraphAPIURLEventCreate, UniqueID, Token);

                List<KeyValuePair<string, string>> listparameters = new List<KeyValuePair<string, string>>();
                string parameters = string.Empty;
                
                if (!string.IsNullOrEmpty(eventname))
                    listparameters.Add(new KeyValuePair<String, String>("name", eventname));
                if (!string.IsNullOrEmpty(startTime))
                    listparameters.Add(new KeyValuePair<String, String>("start_time", Convert.ToDateTime(startTime).ToString("o")));
                if (!string.IsNullOrEmpty(endTime))
                    listparameters.Add(new KeyValuePair<String, String>("end_time", Convert.ToDateTime(endTime).ToString("o")));
                if (!string.IsNullOrEmpty(location))
                    listparameters.Add(new KeyValuePair<String, String>("location", location));
                if (!string.IsNullOrEmpty(description))
                    listparameters.Add(new KeyValuePair<String, String>("description", description));
                if (!string.IsNullOrEmpty(privacy))
                    listparameters.Add(new KeyValuePair<String, String>("privacy", privacy.ToUpper()));

                foreach (var val in listparameters)
                {
                    if (string.IsNullOrEmpty(parameters))
                    {
                        parameters = "&" + val.Key + "=" + val.Value;
                    }
                    else
                    {
                        parameters = parameters + "&" + val.Key + "=" + val.Value;
                    }
                }

                string rawResponse = string.Empty;
                try
                {
                    FacebookRequest objFacebookRequest = new FacebookRequest();

                    objFacebookRequest.RequestURL = URL;
                    objFacebookRequest.Method = "POST";
                    objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
                    objFacebookRequest.Parameters = parameters;

                    rawResponse = ExecuteCommand_Event(objFacebookRequest);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.Contains("error"))
                        {
                            rawResponse = rawResponse.Replace(@"{""error"":", "");
                            rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                        }
                        ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                    }
                    else
                    {
                    }

                    //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                }
                catch (Exception ex)
                {
                    #region ERROR HANDLING

                    PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                    ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                    ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                    ObjPublishExceptionLog.PostResponse = string.Empty;
                    ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                    ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookEvent";
                    ObjPublishExceptionLog.ExtraInfo = rawResponse;
                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        ObjFacebookResponse.message = ex.Message;
                        ObjFacebookResponse.code = string.Empty;
                        ObjFacebookResponse.type = "Exception";
                        ObjFacebookResponse.ID = string.Empty;
                    }

                    #endregion
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookEvent";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookEvent";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        public static FacebookResponse DeleteFacebookEvent(string ResultID, string Token)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLEventDelete))
                    {
                        FacebookGraphAPIURLEventDelete = "https://graph.facebook.com/{0}?access_token={1}";
                    }
                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }



                    string URL = string.Format(FacebookGraphAPIURLEventDelete, ResultID, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "DELETE";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Event(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else if (rawResponse == "true")
                            {
                                ObjFacebookResponse.message = "Event Deleted Successfully.";
                            }
                            else if (rawResponse == "false")
                            {
                                ObjFacebookResponse.message = "Event Deleted Successfully.";
                            }
                            else
                            {
                                ObjFacebookResponse.message = rawResponse;
                            }
                        }
                        else
                        {
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\DeleteFacebookEvent";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\DeleteFacebookEvent";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\DeleteFacebookEvent";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        public static FacebookEventStatistics GetFacebookEventStatistics(string ResultID, string Token)
        {
            PublishManager objPublishManager = new PublishManager();
            
            string fields = "name,id,rsvp_status";
            string limit = "50000";
            string summary = "1";
            EventStatistics objEventStatistics = new EventStatistics();
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookEventStatistics objFacebookEventStatistics = new FacebookEventStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLEventStatistics))
                    {
                        FacebookGraphAPIURLEventStatistics = "https://graph.facebook.com/{0}/invited?fields={1}&limit={2}&summary={3}&access_token={4}";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLEventStatistics, ResultID, fields, limit,summary, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else 
                            {
                                objEventStatistics = jss.Deserialize<EventStatistics>(rawResponse);
                            }

                            objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookEventStatistics.eventStatistics = objEventStatistics;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Event Statistics from FaceBook.";

                            objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookEventStatistics.eventStatistics = objEventStatistics;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookEventStatistics";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookEventStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookEventStatistics.eventStatistics = objEventStatistics;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookEventStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookEventStatistics;
        }

        #endregion
        
        #region Question

        public static FacebookResponse CreateFacebookQuestion(string UniqueID, string Token, string Question, string options, string AllowNewOptions)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                //https://graph.facebook.com/142257429131489/questions&options=['IBM','EDS','MICROSOFT']&allow_new_options=False&question=which is your fav. Cell Phone 11?
                if (string.IsNullOrEmpty(FacebookGraphAPIURLQuestionCreate))
                {
                    FacebookGraphAPIURLQuestionCreate = "https://graph.facebook.com/{0}/questions?access_token={1}";
                }
                if (string.IsNullOrEmpty(FacebookAPITimeOut))
                {
                    FacebookAPITimeOut = "20000";
                }

                string URL = string.Format(FacebookGraphAPIURLQuestionCreate, UniqueID, Token);
                int options_Count = 0;
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token) && !string.IsNullOrEmpty(Question))
                {  
                    List<KeyValuePair<string, string>> listparameters = new List<KeyValuePair<string, string>>();
                    string parameters = string.Empty;

                    if (!string.IsNullOrEmpty(Question))
                        listparameters.Add(new KeyValuePair<String, String>("question", Question));

                    if (!string.IsNullOrEmpty(options))
                    {
                        string[] words = options.Split('|');
                        options_Count = words.Count();
                        string options_Temp = string.Empty;
                        foreach (var word in words)
                        {
                            string word_temp = System.Web.HttpUtility.UrlEncode(word);
                            options_Temp = options_Temp + "'" + word_temp + "',";
                        }
                        options_Temp = "[" + options_Temp.Substring(0, options_Temp.Length - 1) + "]";

                        listparameters.Add(new KeyValuePair<String, String>("options", options_Temp));                        
                    }
                    if (options_Count <= 10)
                    {
                        if (!string.IsNullOrEmpty(AllowNewOptions))
                            listparameters.Add(new KeyValuePair<String, String>("allow_new_options", FirstCharToUpper(AllowNewOptions)));

                        foreach (var val in listparameters)
                        {
                            if (string.IsNullOrEmpty(parameters))
                            {
                                parameters = "&" + val.Key + "=" + val.Value;
                            }
                            else
                            {
                                parameters = parameters + "&" + val.Key + "=" + val.Value;
                            }
                        }

                        string rawResponse = string.Empty;
                        try
                        {
                            FacebookRequest objFacebookRequest = new FacebookRequest();

                            objFacebookRequest.RequestURL = URL;
                            objFacebookRequest.Method = "POST";
                            objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
                            objFacebookRequest.Parameters = parameters;

                            rawResponse = ExecuteCommand_Question(objFacebookRequest);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();
                                if (rawResponse.Contains("error"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);
                                }
                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                            }

                            //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookQuestion";
                            ObjPublishExceptionLog.ExtraInfo = rawResponse;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                ObjFacebookResponse.message = ex.Message;
                                ObjFacebookResponse.code = string.Empty;
                                ObjFacebookResponse.type = "Exception";
                                ObjFacebookResponse.ID = string.Empty;
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        ObjFacebookResponse.ID = string.Empty;
                        ObjFacebookResponse.message = "Exception";
                        ObjFacebookResponse.type = "The poll you tried to create has too few or too many options. Please try again.";
                        ObjFacebookResponse.code = "1525008";
                    }

                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\CreateFacebookQuestion";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\CreateFacebookQuestion";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        public static FacebookQuestion GetFacebookQuestion(string UniqueID, string Token, string FromDate, string ToDate)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookQuestion objFacebookQuestion = new FacebookQuestion();
            string fields = "created_time,from,id,is_published,question,updated_time,options.limit(10).fields(id,name,vote_count)";
            string limit = "1000";

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            List<Question> questions = new List<Question>();
            try
            {
                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLQuestionGet))
                    {
                        FacebookGraphAPIURLQuestionGet = "https://graph.facebook.com/{0}/questions?fields={1}&limit={2}&access_token={3}";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLQuestionGet, UniqueID, fields, limit, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else if (rawResponse.StartsWith(@"{""data"":"))
                            {
                                rawResponse = rawResponse.Substring(8, rawResponse.Length - 8);
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                int lastindex = rawResponse.LastIndexOf(']');
                                rawResponse = rawResponse.Substring(0, lastindex + 1);

                                questions = jss.Deserialize<List<Question>>(rawResponse);
                            }

                            objFacebookQuestion.facebookResponse = ObjFacebookResponse;
                            objFacebookQuestion.question = questions;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Question List from FaceBook.";

                            objFacebookQuestion.facebookResponse = ObjFacebookResponse;
                            objFacebookQuestion.question = questions;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookQuestion";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookQuestion.facebookResponse = ObjFacebookResponse;
                    objFacebookQuestion.question = questions;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookQuestion";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return objFacebookQuestion;
        }

        public static FacebookResponse DeleteFacebookQuestion(string ResultID, string Token)
        {
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLQuestionDelete))
                    {
                        FacebookGraphAPIURLQuestionDelete = "https://graph.facebook.com/{0}?access_token={1}";
                    }
                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }



                    string URL = string.Format(FacebookGraphAPIURLQuestionDelete, ResultID, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "DELETE";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Question(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.Contains("error"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else if (rawResponse == "true")
                            {
                                ObjFacebookResponse.message = "Question Deleted Successfully.";
                            }
                            else if (rawResponse == "false")
                            {
                                ObjFacebookResponse.message = "Question Deleted Successfully.";
                            }
                            else
                            {
                                ObjFacebookResponse.message = rawResponse;
                            }
                        }
                        else
                        {
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\DeleteFacebookQuestion";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            ObjFacebookResponse.message = ex.Message;
                            ObjFacebookResponse.code = string.Empty;
                            ObjFacebookResponse.type = "Exception\\DeleteFacebookQuestion";
                            ObjFacebookResponse.ID = string.Empty;
                        }

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\DeleteFacebookQuestion";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                ObjFacebookResponse.ID = string.Empty;
                ObjFacebookResponse.message = ex.Message;
                ObjFacebookResponse.type = "Exception\\DeleteFacebookQuestion";
                ObjFacebookResponse.code = string.Empty;

                #endregion
            }

            return ObjFacebookResponse;
        }

        public static FacebookQuestionStatistics GetFacebookQuestionStatistics(string ResultID, string Token)
        {
            PublishManager objPublishManager = new PublishManager();

            string fields = "created_time,id,from,is_published,question,updated_time,options.limit(300).fields(created_time,id,from,name,vote_count,votes.limit(50000).fields(id,name,gender))";

            QuestionStatistics objQuestionStatistics = new QuestionStatistics();
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookQuestionStatistics objFacebookQuestionStatistics = new FacebookQuestionStatistics();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLQuestionStatistics))
                    {
                        FacebookGraphAPIURLQuestionStatistics = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";                        
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLQuestionStatistics, ResultID, fields, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objQuestionStatistics = jss.Deserialize<QuestionStatistics>(rawResponse);
                            }

                            objFacebookQuestionStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookQuestionStatistics.questionStatistics = objQuestionStatistics;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Event Statistics from FaceBook.";

                            objFacebookQuestionStatistics.facebookResponse = ObjFacebookResponse;
                            objFacebookQuestionStatistics.questionStatistics = objQuestionStatistics;
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookQuestionStatistics";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookQuestionStatistics.facebookResponse = ObjFacebookResponse;
                    objFacebookQuestionStatistics.questionStatistics = objQuestionStatistics;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookQuestionStatistics";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookQuestionStatistics;
        }

        #endregion

        #region INSIGHTS
        
        public static FacebookInsights GetFacebookInsights(int dealerLocationId, string uniqueId, string token, string startDate, string endDate, string periods, string FeedID = "")
        {
            var objFacebookResponse = new FacebookResponse();
            var objFacebookInsights = new FacebookInsights();
            var objInsights = new Insights();
            var objPublishManager = new PublishManager();
            try
            {
                var isValid = true;
                var errorMessage = string.Empty;

                var fromDate = Convert.ToDateTime(startDate);
                var toDate = Convert.ToDateTime(endDate);
                var toTime = string.Empty;
                if (fromDate > toDate)
                {
                    isValid = false;
                    errorMessage = "To Date Cannot be Greater than From Date.";
                }

                if (isValid)
                {
                    if (toDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                    {
                        toTime = "T" + DateTime.Now.Hour.ToString() + ":00:00";
                    }
                    else
                    {
                        toTime = "T" + "23:59:00";
                    }
                    var since = fromDate.ToString("yyyy-MM-dd") + "T00:00:00";
                    var until = toDate.ToString("yyyy-MM-dd") + toTime;

                    if (string.IsNullOrEmpty(FacebookGraphAPIURLInsights))
                    {
                        FacebookGraphAPIURLInsights = "https://graph.facebook.com/{0}/insights?format=json&since={1}&until={2}&access_token={3}";
                        //https://graph.facebook.com/{0}/insights?format=json&since={1}&until={2}&access_token={3}
                    }
                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }
                    var url = string.Empty;
                    if (string.IsNullOrEmpty(FeedID))
                    {
                        url = string.Format(FacebookGraphAPIURLInsights, uniqueId, since, until, token);
                    }
                    else
                    {
                        FacebookGraphAPIURLInsights = "https://graph.facebook.com/{0}/insights?format=json&access_token={1}";
                        url = string.Format(FacebookGraphAPIURLInsights, FeedID, token);
                    }

                    string rawResponse = string.Empty;
                    try
                    {
                        var objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = url;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                objFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                objFacebookInsights.facebookResponse = objFacebookResponse;
                                objFacebookInsights.responseJson = rawResponse;
                                objFacebookInsights.insights = objInsights;
                            }
                            else
                            {
                                objInsights = JsonConvert.DeserializeObject<Insights>(rawResponse);

                                if (objInsights.data.Count > 0)
                                {

                                    IQueryable<DataInsights> IQDataInsights;
                                    IQDataInsights = objInsights.data.AsQueryable();

                                    IQueryable<DataInsights> iqDataInsights = IQDataInsights;


                                    if (string.IsNullOrEmpty(periods))
                                    {
                                        periods = "day,lifetime";
                                    }

                                    var words = periods.Split(',');
                                    string[] fbPeriod = { "day", "week", "days_28", "lifetime" };

                                    if (words.Count() == 1)
                                    {
                                        iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0]);
                                    }
                                    else if (words.Count() == 2)
                                    {
                                        iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0] || o.period.ToLower() == words[1]);
                                    }
                                    else if (words.Count() == 3)
                                    {
                                        iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0] || o.period.ToLower() == words[1] || o.period.ToLower() == words[2]);
                                    }
                                    else if (words.Count() == 4)
                                    {
                                        iqDataInsights = iqDataInsights.Where(o => o.period.ToLower() == words[0] || o.period.ToLower() == words[1] || o.period.ToLower() == words[2] || o.period.ToLower() == words[3]);
                                    }

                                    //foreach (string Period in words)
                                    //{
                                    //    if (FBPeriod.Contains(Period))
                                    //    {
                                    //        _IQDataInsights = _IQDataInsights.Where(o => Periods.ToLower() == o.period.ToLower());
                                    //    }
                                    //    else
                                    //    {
                                    //        ErrorMessage = "Some of the period is invalid.!";
                                    //        _IQDataInsights = _IQDataInsights.Where(o => o.period.ToLower() == Period.ToLower());
                                    //        ObjFacebookResponse.message = ObjFacebookResponse.message + ErrorMessage;
                                    //    }
                                    //}

                                    var data = iqDataInsights.ToList();
                                    if (data.Count == 0)
                                    {
                                        errorMessage = "Some of the period is invalid.";
                                        objFacebookResponse.message = objFacebookResponse.message + errorMessage;
                                    }

                                    objFacebookInsights.facebookResponse = objFacebookResponse;
                                    objFacebookInsights.responseJson = rawResponse;
                                    objFacebookInsights.insights.data = new List<DataInsights>();
                                    objFacebookInsights.insights.data = data;
                                }
                                else
                                {
                                    errorMessage = "No Insights Record Found.";
                                    objFacebookResponse.message = objFacebookResponse.message + errorMessage;

                                    objFacebookInsights.facebookResponse = objFacebookResponse;
                                    objFacebookInsights.responseJson = rawResponse;
                                    objFacebookInsights.insights.data = new List<DataInsights>();                                    
                                }
                            }
                     
                        }
                        else
                        {
                            objFacebookResponse.message = "Problem in Retrying FeceBook Insights.";

                            objFacebookInsights.facebookResponse = objFacebookResponse;
                            objFacebookInsights.responseJson = rawResponse;
                            objFacebookInsights.insights = objInsights;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        var objPublishExceptionLog = new PublishExceptionLog();
                        objPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        objPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        objPublishExceptionLog.ExceptionMessage = ex.Message;
                        objPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        objPublishExceptionLog.PostResponse = string.Empty;
                        objPublishExceptionLog.StackTrace = ex.StackTrace;
                        objPublishExceptionLog.MethodName = "FaceBook\\GetFacebookInsights";
                        objPublishExceptionLog.ExtraInfo = "DealerLocationID : " + dealerLocationId + "rawResponse : " + rawResponse;
                        objPublishManager.AddPublishExceptionLog(objPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            objFacebookResponse.message = ex.Message;
                            objFacebookResponse.code = string.Empty;
                            objFacebookResponse.type = "Exception\\GetFacebookInsights";
                            objFacebookResponse.ID = string.Empty;

                            objFacebookInsights.responseJson = rawResponse;
                        }

                        #endregion
                    }
                }
                else
                {
                    objFacebookResponse.message = errorMessage;

                    objFacebookInsights.facebookResponse = objFacebookResponse;
                    objFacebookInsights.responseJson = string.Empty;
                    objFacebookInsights.insights = objInsights;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var objPublishExceptionLog = new PublishExceptionLog();
                objPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                objPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                objPublishExceptionLog.ExceptionMessage = ex.Message;
                objPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                objPublishExceptionLog.PostResponse = string.Empty;
                objPublishExceptionLog.StackTrace = ex.StackTrace;
                objPublishExceptionLog.MethodName = "FaceBook\\GetFacebookInsights";
                objPublishExceptionLog.ExtraInfo = "DealerLocationID : " + dealerLocationId;
                objPublishManager.AddPublishExceptionLog(objPublishExceptionLog);

                objFacebookResponse.ID = string.Empty;
                objFacebookResponse.message = ex.Message;
                objFacebookResponse.type = "Exception\\GetFacebookInsights";
                objFacebookResponse.code = string.Empty;

                #endregion
            }

            return objFacebookInsights;
        }
        
        public static List<KeyValuePair<string, string>> GetFacebookInsightsObject(FacebookInsights facebookInsights)
        {
            var result = new List<KeyValuePair<string, string>>();
            var keyval = new KeyValuePair<string, string>();

            try
            {
                foreach (var item in facebookInsights.insights.data)
                {
                    var itemval = item.values.FirstOrDefault();
                    if (itemval != null)
                    {
                        keyval = new KeyValuePair<string, string>(item.name, CleanValue(itemval._value));
                    }
                    result.Add(keyval);
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                Debug.WriteLine(ex);
                
                #endregion
            }

            return result;
        }

        public static Dictionary<string, string> GetFacebookInsightsDictionary(FacebookInsights facebookInsights)
        {
            var result = new Dictionary<string, string>();

            try
            {
                foreach (var item in facebookInsights.insights.data)
                {
                    var itemval = item.values.FirstOrDefault();
                    if (itemval != null)
                    {
                        result.Add(item.name, CleanValue(itemval._value));
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                Debug.WriteLine(ex);

                #endregion
            }

            return result;
        }

        private static string ExtractDictionaryValue(Dictionary<string, string> insightsDictionary, string key)
        {
            var result = string.Empty;

            try
            {
                insightsDictionary.TryGetValue(key, out result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return result;
        }

        public static InsightModel ConvertDictionary2Object(Dictionary<string, string> insightsDictionary)
        {
            var result = new InsightModel();

            //result.DateofQuery,
            //result.LocationId,
            //result.DateUpdated,
            //result.UniqueId,
            result.page_fan_adds_unique = ExtractDictionaryValue(insightsDictionary, "page_fan_adds_unique");
            result.page_fan_adds = ExtractDictionaryValue(insightsDictionary, "page_fan_adds");
            result.page_fan_removes_unique = ExtractDictionaryValue(insightsDictionary, "page_fan_removes_unique");
            result.page_fan_removes = ExtractDictionaryValue(insightsDictionary, "page_fan_removes");
            result.page_subscriber_adds_unique = ExtractDictionaryValue(insightsDictionary, "page_subscriber_adds_unique");
            result.page_subscriber_adds = ExtractDictionaryValue(insightsDictionary, "page_subscriber_adds");
            result.page_subscriber_removes_unique = ExtractDictionaryValue(insightsDictionary, "page_subscriber_removes_unique");
            result.page_subscriber_removes = ExtractDictionaryValue(insightsDictionary, "page_subscriber_removes");
            result.page_views_login_unique = ExtractDictionaryValue(insightsDictionary, "page_views_login_unique");
            result.page_views_login = ExtractDictionaryValue(insightsDictionary, "page_views_login");
            result.page_views_logout = ExtractDictionaryValue(insightsDictionary, "page_views_logout");
            result.page_views = ExtractDictionaryValue(insightsDictionary, "page_views");
            result.page_tab_views_login_top_unique = ExtractDictionaryValue(insightsDictionary, "page_tab_views_login_top_unique");
            result.page_tab_views_login_top = ExtractDictionaryValue(insightsDictionary, "page_tab_views_login_top");
            result.page_tab_views_logout_top = ExtractDictionaryValue(insightsDictionary, "page_tab_views_logout_top");
            result.page_views_internal_referrals = ExtractDictionaryValue(insightsDictionary, "page_views_internal_referrals");
            result.page_views_external_referrals = ExtractDictionaryValue(insightsDictionary, "page_views_external_referrals");
            result.page_story_adds_unique = ExtractDictionaryValue(insightsDictionary, "page_story_adds_unique");
            result.page_story_adds = ExtractDictionaryValue(insightsDictionary, "page_story_adds");
            result.page_story_adds_by_story_type_unique = ExtractDictionaryValue(insightsDictionary, "page_story_adds_by_story_type_unique");
            result.page_story_adds_by_story_type = ExtractDictionaryValue(insightsDictionary, "page_story_adds_by_story_type");
            result.page_impressions_by_age_gender_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_by_age_gender_unique");
            result.page_impressions_by_country_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_by_country_unique");
            result.page_impressions_by_locale_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_by_locale_unique");
            result.page_impressions_by_city_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_by_city_unique");
            result.page_story_adds_by_age_gender_unique = ExtractDictionaryValue(insightsDictionary, "page_story_adds_by_age_gender_unique");
            result.page_story_adds_by_country_unique = ExtractDictionaryValue(insightsDictionary, "page_story_adds_by_country_unique");
            result.page_story_adds_by_city_unique = ExtractDictionaryValue(insightsDictionary, "page_story_adds_by_city_unique");
            result.page_story_adds_by_locale_unique = ExtractDictionaryValue(insightsDictionary, "page_story_adds_by_locale_unique");
            result.page_impressions_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_unique");
            result.page_impressions = ExtractDictionaryValue(insightsDictionary, "page_impressions");
            result.page_impressions_paid_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_paid_unique");
            result.page_impressions_paid = ExtractDictionaryValue(insightsDictionary, "page_impressions_paid");
            result.page_impressions_organic_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_organic_unique");
            result.page_impressions_organic = ExtractDictionaryValue(insightsDictionary, "page_impressions_organic");
            result.page_impressions_viral_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_viral_unique");
            result.page_impressions_viral = ExtractDictionaryValue(insightsDictionary, "page_impressions_viral");
            result.page_impressions_by_story_type_unique = ExtractDictionaryValue(insightsDictionary, "page_impressions_by_story_type_unique");
            result.page_impressions_by_story_type = ExtractDictionaryValue(insightsDictionary, "page_impressions_by_story_type");
            result.page_places_checkin_total = ExtractDictionaryValue(insightsDictionary, "page_places_checkin_total");
            result.page_places_checkin_total_unique = ExtractDictionaryValue(insightsDictionary, "page_places_checkin_total_unique");
            result.page_places_checkin_mobile = ExtractDictionaryValue(insightsDictionary, "page_places_checkin_mobile");
            result.page_places_checkin_mobile_unique = ExtractDictionaryValue(insightsDictionary, "page_places_checkin_mobile_unique");
            result.page_places_checkins_by_age_gender = ExtractDictionaryValue(insightsDictionary, "page_places_checkins_by_age_gender");
            result.page_places_checkins_by_country = ExtractDictionaryValue(insightsDictionary, "page_places_checkins_by_country");
            result.page_places_checkins_by_city = ExtractDictionaryValue(insightsDictionary, "page_places_checkins_by_city");
            result.page_places_checkins_by_locale = ExtractDictionaryValue(insightsDictionary, "page_places_checkins_by_locale");
            result.page_posts_impressions_unique = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions_unique");
            result.page_posts_impressions = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions");
            result.page_posts_impressions_paid_unique = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions_paid_unique");
            result.page_posts_impressions_paid = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions_paid");
            result.page_posts_impressions_organic_unique = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions_organic_unique");
            result.page_posts_impressions_organic = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions_organic");

            result.page_posts_impressions_viral_unique = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions_viral_unique");
            result.page_posts_impressions_viral = ExtractDictionaryValue(insightsDictionary, "page_posts_impressions_viral");
            result.page_consumptions_unique = ExtractDictionaryValue(insightsDictionary, "page_consumptions_unique");
            result.page_consumptions = ExtractDictionaryValue(insightsDictionary, "page_consumptions");
            result.page_consumptions_by_consumption_type_unique = ExtractDictionaryValue(insightsDictionary, "page_consumptions_by_consumption_type_unique");
            result.page_consumptions_by_consumption_type = ExtractDictionaryValue(insightsDictionary, "page_consumptions_by_consumption_type");
            result.page_fans_by_like_source_unique = ExtractDictionaryValue(insightsDictionary, "page_fans_by_like_source_unique");

            result.page_fans_by_like_source = ExtractDictionaryValue(insightsDictionary, "page_fans_by_like_source");
            result.page_subscribers_by_subscribe_source_unique = ExtractDictionaryValue(insightsDictionary, "page_subscribers_by_subscribe_source_unique");
            result.page_subscribers_by_subscribe_source = ExtractDictionaryValue(insightsDictionary, "page_subscribers_by_subscribe_source");
            result.page_negative_feedback_unique = ExtractDictionaryValue(insightsDictionary, "page_negative_feedback_unique");
            result.page_negative_feedback = ExtractDictionaryValue(insightsDictionary, "page_negative_feedback");
            result.page_negative_feedback_by_type_unique = ExtractDictionaryValue(insightsDictionary, "page_negative_feedback_by_type_unique");
            result.page_negative_feedback_by_type = ExtractDictionaryValue(insightsDictionary, "page_negative_feedback_by_type");

            result.page_fans = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_fans_locale = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_fans_city = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_fans_country = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_fans_gender = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_fans_age = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_fans_gender_age = ExtractDictionaryValue(insightsDictionary, "XXXX");

            result.page_friends_of_fans = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_subscribers = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_subscribers_locale = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_subscribers_city = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_subscribers_country = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_subscribers_gender_age = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_storytellers = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_storytellers_by_story_type = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_storytellers_by_age_gender = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_storytellers_by_country = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_storytellers_by_city = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_storytellers_by_locale = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_engaged_users = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_impressions_frequency_distribution = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_impressions_viral_frequency_distribution = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_posts_impressions_frequency_distribution = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_views_unique = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_stories = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_stories_by_story_type = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_admin_num_posts = ExtractDictionaryValue(insightsDictionary, "XXXX");
            result.page_admin_num_posts_by_type = ExtractDictionaryValue(insightsDictionary, "XXXX");

            //http://stackoverflow.com/questions/9210428/how-to-convert-class-into-dictionarystring-string
            //PropertyInfo[] infos = local.GetType().GetProperties();


            return result;
        }
        
        private static string CleanValue(string inputVal)
        {
            var result = string.Empty;

            try
            {
                if (inputVal == null || inputVal == "[]")
                {
                    result = "0";
                }
                else
                {
                    result = inputVal.Replace("\r", "").Replace("\n", "");
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                Debug.WriteLine(ex);

                #endregion
            }

            return result;
        }

        #endregion

        #region PAGE INFORMATION

        public static FacebookPage GetFacebookPageInformation(int DealerLocationID,string UniqueID, string Token)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookPage objFacebookPage = new FacebookPage();

            string fields = "name,is_published,website,username,about,location,phone,can_post,checkins,were_here_count,talking_about_count,unread_notif_count,new_like_count,offer_eligible,category,id,link,likes,influences,keywords,members,parent_page,unread_message_count,unseen_message_count,network";
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Page page = new Page();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLPage))
                {
                    FacebookGraphAPIURLPage = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                }
                if (string.IsNullOrEmpty(FacebookAPITimeOut))
                {
                    FacebookAPITimeOut = "20000";
                }

                if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                {
                    string URL = string.Format(FacebookGraphAPIURLPage, UniqueID, fields, Token);

                    string rawResponse = string.Empty;

                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                                
                            }
                            else 
                            {
                                page = jss.Deserialize<Page>(rawResponse);
                            }

                            objFacebookPage.facebookResponse = ObjFacebookResponse;
                            objFacebookPage.page = page;
                            objFacebookPage.responseJson = rawResponse;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Page Information from FaceBook.";

                            objFacebookPage.facebookResponse = ObjFacebookResponse;
                            objFacebookPage.page = page;
                            objFacebookPage.responseJson = rawResponse;
                        }

                        //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPageInformation";
                        ObjPublishExceptionLog.ExtraInfo = "DealerLocationID : " + DealerLocationID + "rawResponse : " + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookPage.facebookResponse = ObjFacebookResponse;
                    objFacebookPage.page = page;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPageInformation";
                ObjPublishExceptionLog.ExtraInfo = "DealerLocationID : " + DealerLocationID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookPage;
        }

        #endregion

        #region FEED INFORMATION

        public static FacebookFeed GetFacebookFeedID(string UniqueID, string Token, string FromDate, string ToDate,string limit)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookFeed objFacebookFeed = new FacebookFeed();
            string fields = "created_time,id,object_id,type,application,from,message";            
            string format = "json";
            
            bool isValid = true;
            string ErrorMessage = string.Empty;

            //created_time,id,object_id,type,application&until=2012-10-22T00:00:00.0000000-06:00&since=2012-01-01T00:00:00.0000000-06:00&format=json&limit=1000
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            Feed feed = new Feed();
            try
            {
                if (string.IsNullOrEmpty(FacebookGraphAPIURLFeed))
                {
                    FacebookGraphAPIURLFeed = "https://graph.facebook.com/{0}/feed?fields={1}&since={2}&until={3}&format={4}&limit={5}&access_token={6}";                    
                    //https://graph.facebook.com/104832512988489/feed?fields=created_time,id,object_id,type,application&until=2012-10-22T00:00:00.0000000-06:00&since=2012-01-01T00:00:00.0000000-06:00&format=json&limit=1000
                }
                if (string.IsNullOrEmpty(FacebookAPITimeOut))
                {
                    FacebookAPITimeOut = "30000";
                }

                DateTime _FromDate = Convert.ToDateTime(FromDate);
                DateTime _ToDate = Convert.ToDateTime(ToDate).AddDays(1);
                string toTime = string.Empty;
                if (_FromDate > _ToDate)
                {
                    isValid = false;
                    ErrorMessage = "To Date Cannot be Greater than From Date.";
                }

                if (isValid)
                {
                    if (!string.IsNullOrEmpty(UniqueID) && !string.IsNullOrEmpty(Token))
                    {
                        toTime = "T" + "23:59:00";

                        //if (_ToDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                        //{
                        //    toTime = "T" + DateTime.Now.Hour.ToString() + ":00:00";
                        //}
                        //else
                        //{
                        //    toTime = "T" + "23:59:00";
                        //}
                        var since = _FromDate.ToString("yyyy-MM-dd") + "T00:00:00";
                        var until = _ToDate.ToString("yyyy-MM-dd") + toTime;

                        if(string.IsNullOrEmpty(limit))
                        {
                            limit = "10000";
                        }
                        string URL = string.Format(FacebookGraphAPIURLFeed, UniqueID, fields, since, until, format, limit, Token);

                        string rawResponse = string.Empty;

                        try
                        {
                            FacebookRequest objFacebookRequest = new FacebookRequest();

                            objFacebookRequest.RequestURL = URL;
                            objFacebookRequest.Method = "GET";
                            objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                            rawResponse = ExecuteCommand_Get(objFacebookRequest);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                }
                                else
                                {
                                    feed = jss.Deserialize<Feed>(rawResponse);                                    
                                }

                                objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                objFacebookFeed.feed = feed;
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Problem to Retrying feed Information from FaceBook.";

                                objFacebookFeed.facebookResponse = ObjFacebookResponse;
                                objFacebookFeed.feed = feed;

                            }

                            //result = app.Post(dlSocialNetwork.UniqueID + "/feed", parameters);
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookFeedID";
                            ObjPublishExceptionLog.ExtraInfo = "URL : " + URL;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            #endregion
                        }
                    }
                    else
                    {

                        ObjFacebookResponse.message = "Invalid Parameters UniqueID or Token.";

                        objFacebookFeed.facebookResponse = ObjFacebookResponse;
                        objFacebookFeed.feed = feed;
                    }
                }
                else
                {
                    ObjFacebookResponse.message = ErrorMessage;

                    objFacebookFeed.facebookResponse = ObjFacebookResponse;                    
                    objFacebookFeed.feed = feed;
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookFeedID";
                ObjPublishExceptionLog.ExtraInfo = "UniqueID : " + UniqueID + ", Token :" + Token + ", FromDate : " + FromDate + ", ToDate : " + ToDate;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookFeed;
        }

        #endregion

        public static List<string> GetFacebookPermissions(string Token)
        {
            PublishManager objPublishManager = new PublishManager();
            string message = string.Empty;
            string Xpath = string.Empty;
            string ParentnodePermissionsDetailsXPath = "//table[contains(@class,'uiInfoTable')]";
            string listnodePermissionsDetailsXPath = ".//tr";
            string td_text = string.Empty;
            List<string> listPermissions = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(Token))
                {
                    string URL = (string.Format("https://developers.facebook.com/tools/debug/access_token?q={0}",Token));
                    
                    string responseHtml = GetResponse(URL);

                    var doc = new HtmlDocument();
                    doc.OptionFixNestedTags = true;
                    doc.LoadHtml(responseHtml);

                    string Error = string.Empty;
                    try
                    {
                      Error = doc.DocumentNode.SelectSingleNode("//div[contains(@class,'pam uiBoxRed')]").InnerText;
                    }
                    catch 
                    {
                        Error = string.Empty;
                    }

                    if (string.IsNullOrEmpty(Error))
                    {
                        Xpath = ParentnodePermissionsDetailsXPath;
                        HtmlNode reviewNode = doc.DocumentNode.SelectSingleNode(ParentnodePermissionsDetailsXPath);

                        Xpath = listnodePermissionsDetailsXPath;
                        var links = reviewNode.SelectNodes(listnodePermissionsDetailsXPath);

                        if (links != null && links.Count > 0)
                        {
                            string th_text = string.Empty;
                            bool isvalid = false;
                            foreach (var html in links)
                            {
                                Xpath = ".//th";
                                th_text = html.SelectSingleNode(".//th").InnerText;

                                if (th_text.ToLower().Contains("scopes"))
                                {
                                    td_text = html.SelectSingleNode(".//td/a").InnerText;
                                    isvalid = true;
                                    break;
                                }
                                else if (th_text.ToLower().Contains("error parsing url"))
                                {
                                    //Xpath = ".//td";
                                    td_text = html.SelectSingleNode(".//td").InnerText;
                                    isvalid = false;
                                    break;
                                }
                            }

                            if (!string.IsNullOrEmpty(td_text) && isvalid)
                            {
                                string[] Permissions = td_text.Split(' ');
                                foreach (string Permission in Permissions)
                                {
                                    listPermissions.Add(Permission);
                                }
                            }
                            else
                            {
                                listPermissions.Add(td_text);
                            }
                        }
                    }
                    else
                    {
                        message = "The access token could not be decrypted";
                        listPermissions.Add(message);
                    }
                }
                else
                {
                    message = "Invalid Parameters UniqueID or Token.";
                    listPermissions.Add(message);
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookPermissions";
                ObjPublishExceptionLog.ExtraInfo = "Token : " + Token ;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return listPermissions;
        }

        public static FacebookObjectIDByResultID GetFacebookObjectIDbyResultID(string ResultID, string Token)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookEvent objFacebookEvent = new FacebookEvent();
            string fields = "object_id,actions";

            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            FacebookObjectIDByResultID objFacebookObjectIDByResultID = new FacebookObjectIDByResultID();
            try
            {
                if (!string.IsNullOrEmpty(ResultID) && !string.IsNullOrEmpty(Token))
                {
                    if (string.IsNullOrEmpty(FacebookGraphAPIURLObjectIDGetResultID))
                    {
                        FacebookGraphAPIURLObjectIDGetResultID = "https://graph.facebook.com/{0}?fields={1}&access_token={2}";
                    }

                    if (string.IsNullOrEmpty(FacebookAPITimeOut))
                    {
                        FacebookAPITimeOut = "20000";
                    }

                    string URL = string.Format(FacebookGraphAPIURLObjectIDGetResultID, ResultID, fields, Token);

                    string rawResponse = string.Empty;
                    try
                    {
                        FacebookRequest objFacebookRequest = new FacebookRequest();

                        objFacebookRequest.RequestURL = URL;
                        objFacebookRequest.Method = "GET";
                        objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objFacebookRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();
                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                rawResponse = rawResponse.Replace(@"{""error"":", "");
                                rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                ObjFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);
                            }
                            else
                            {
                                objFacebookObjectIDByResultID = jss.Deserialize<FacebookObjectIDByResultID>(rawResponse);
                            }

                            objFacebookEvent.facebookResponse = ObjFacebookResponse;
                        }
                        else
                        {
                            ObjFacebookResponse.message = "Problem to Retrying Event By Result ID from FaceBook.";

                            objFacebookEvent.facebookResponse = ObjFacebookResponse;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookObjectIDbyResultID";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    ObjFacebookResponse.message = "Invalid Parameters";

                    objFacebookEvent.facebookResponse = ObjFacebookResponse;
                    //objFacebookEvent.events = events;
                }
                //473216489376933_368874216533892?fields=
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetFacebookEventbyResultID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objFacebookObjectIDByResultID;
        }

        public static FacebookPermissions CheckFacebookPermissions(int DealerLocationID, string Token, string PermissionName)
        {
            FacebookPermissions objFacebookPermissions = new FacebookPermissions();
            Permissions objPermissions;
            PublishManager objPublishManager = new PublishManager();

            string FacebookCreateEvent = Convert.ToString(ConfigurationManager.AppSettings["Facebook.CreateEvent"]);
            string FacebookCreateLink = Convert.ToString(ConfigurationManager.AppSettings["Facebook.CreateLink"]);
            string FacebookUploadPhoto = Convert.ToString(ConfigurationManager.AppSettings["Facebook.UploadPhoto"]);
            string FacebookQuestion = Convert.ToString(ConfigurationManager.AppSettings["Facebook.Question"]);
            string FacebookStatus = Convert.ToString(ConfigurationManager.AppSettings["Facebook.Status"]);
            //string FacebookVideo = Convert.ToString(ConfigurationManager.AppSettings["Facebook.Video"]);

            if (DealerLocationID != 0  && string.IsNullOrEmpty(Token))
            {
                DealerLocationSocialNetwork objDealerLocationSocialNetwork = objPublishManager.GetDealerLocationSocialNetworkbyDealerLocationID(DealerLocationID, 2);

                if (objDealerLocationSocialNetwork != null)
                {                    
                    Token = objDealerLocationSocialNetwork.Token;
                }
            }
            string message = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(Token))
                {
                    List<string> listPermissions = GetFacebookPermissions(Token);

                    bool CreateEvent = true;
                    bool CreateLink = true;
                    bool UploadPhoto = true;
                    bool Question = true;
                    bool Status = true;
                    //bool Video;

                    foreach (string permissions in listPermissions)
                    {

                        if (permissions.ToLower() == FacebookCreateEvent && CreateEvent && (PermissionName.ToLower() == "event" || string.IsNullOrEmpty(PermissionName)))
                        {
                            objPermissions = new Permissions();
                            objPermissions.name = "Event";
                            objPermissions.permission = true;
                            objFacebookPermissions.permissions.Add(objPermissions);
                            CreateEvent = false;
                        }
                        if (permissions.ToLower() == FacebookCreateLink && CreateLink && (PermissionName.ToLower() == "link" || string.IsNullOrEmpty(PermissionName)))
                        {
                            objPermissions = new Permissions();
                            objPermissions.name = "Link";
                            objPermissions.permission = true;
                            objFacebookPermissions.permissions.Add(objPermissions);
                            CreateLink = false;
                        }
                        if (permissions.ToLower() == FacebookUploadPhoto && UploadPhoto && (PermissionName.ToLower() == "uploadphoto" || string.IsNullOrEmpty(PermissionName)))
                        {
                            objPermissions = new Permissions();
                            objPermissions.name = "UploadPhoto";
                            objPermissions.permission = true;
                            objFacebookPermissions.permissions.Add(objPermissions);
                            UploadPhoto = false;
                        }
                        if (permissions.ToLower() == FacebookCreateLink && Question && (PermissionName.ToLower() == "question" || string.IsNullOrEmpty(PermissionName)))
                        {
                            objPermissions = new Permissions();
                            objPermissions.name = "Question";
                            objPermissions.permission = true;
                            objFacebookPermissions.permissions.Add(objPermissions);
                            Question = false;
                        }
                        if (permissions.ToLower() == FacebookCreateLink && Status && (PermissionName.ToLower() == "status" || string.IsNullOrEmpty(PermissionName)))
                        {
                            objPermissions = new Permissions();
                            objPermissions.name = "Status";
                            objPermissions.permission = true;
                            objFacebookPermissions.permissions.Add(objPermissions);
                            Status = false;
                        }
                        //if (permissions.ToLower() == FacebookVideo && Video && (PermissionName.ToLower() == "video" ||  string.IsNullOrEmpty(PermissionName)))
                        //{
                        //    objPermissions.name = "Upload Video";
                        //    objPermissions.permission = true;
                        //    Video = false;
                        //}

                    }
                    if (CreateEvent && (PermissionName.ToLower() == "event" || string.IsNullOrEmpty(PermissionName)))
                    {
                        objPermissions = new Permissions();
                        objPermissions.name = "Event";
                        objPermissions.permission = false;
                        objFacebookPermissions.permissions.Add(objPermissions);
                        CreateEvent = false;
                    }
                    if (CreateLink && (PermissionName.ToLower() == "link" || string.IsNullOrEmpty(PermissionName)))
                    {
                        objPermissions = new Permissions();
                        objPermissions.name = "Link";
                        objPermissions.permission = false;
                        objFacebookPermissions.permissions.Add(objPermissions);
                        CreateEvent = false;
                    }
                    if (UploadPhoto && (PermissionName.ToLower() == "uploadphoto" || string.IsNullOrEmpty(PermissionName)))
                    {
                        objPermissions = new Permissions();
                        objPermissions.name = "UploadPhoto";
                        objPermissions.permission = false;
                        objFacebookPermissions.permissions.Add(objPermissions);
                        CreateEvent = false;
                    }
                    if (Question && (PermissionName.ToLower() == "question" || string.IsNullOrEmpty(PermissionName)))
                    {
                        objPermissions = new Permissions();
                        objPermissions.name = "Question";
                        objPermissions.permission = false;
                        objFacebookPermissions.permissions.Add(objPermissions);
                        CreateEvent = false;
                    }
                    if (Status && (PermissionName.ToLower() == "status" || string.IsNullOrEmpty(PermissionName)))
                    {
                        objPermissions = new Permissions();
                        objPermissions.name = "Status";
                        objPermissions.permission = false;
                        objFacebookPermissions.permissions.Add(objPermissions);
                        CreateEvent = false;
                    }
                    //Create Event

                }
                else
                {
                    message = "Invalid Parameters UniqueID or Token.";
                    objFacebookPermissions.message = message;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\CheckFacebookPermissions";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                objFacebookPermissions.message = ex.Message;
                #endregion
            }

            return objFacebookPermissions;
        }

        public static TokenValidation ValidateTokenDealerLocationID(int DealerLocationID)
        {
            TokenValidation objTokenValidation = new TokenValidation();
            List<TokenDetail> listTokenDetail = new List<TokenDetail>();
            TokenDetail objTokenDetail;
            PublishManager objPublishManager = new PublishManager();

            try
            {
                if (DealerLocationID != 0)
                {
                    

                    // Facebook
                    DealerLocationSocialNetwork objDealerLocationSocialNetwork = objPublishManager.GetDealerLocationSocialNetworkbyDealerLocationID(DealerLocationID, 2);
                    
                    if (objDealerLocationSocialNetwork != null)
                    {
                       FacebookPermissions objFacebookPermissions = CheckFacebookPermissions(0, objDealerLocationSocialNetwork.Token, "uploadphoto");
                       if (string.IsNullOrEmpty(objFacebookPermissions.message))
                       {
                           Permissions objPermissions = objFacebookPermissions.permissions.FirstOrDefault();

                           if (objPermissions.permission == true)
                           {
                               objTokenDetail = new TokenDetail();
                               objTokenDetail.DealerLocationID = objDealerLocationSocialNetwork.LocationID;
                               objTokenDetail.DealerLocationSocialNetWorkID = objDealerLocationSocialNetwork.ID;
                               objTokenDetail.SocialNetWorkID = objDealerLocationSocialNetwork.SocialNetworkID;
                               objTokenDetail.SocialNetWorkName = "Facebook";

                               DealerLocation objDealerLocation = objPublishManager.GetDealerLocationByDealerLocationID(DealerLocationID);
                               objTokenDetail.DealerLocationName = objDealerLocation.Name;

                               objTokenDetail.isValidToken = true;

                               listTokenDetail.Add(objTokenDetail);

                           }
                           else
                           {
                               objTokenDetail = new TokenDetail();
                               objTokenDetail.DealerLocationID = objDealerLocationSocialNetwork.LocationID;
                               objTokenDetail.DealerLocationSocialNetWorkID = objDealerLocationSocialNetwork.ID;
                               objTokenDetail.SocialNetWorkID = objDealerLocationSocialNetwork.SocialNetworkID;

                               objTokenDetail.SocialNetWorkName = "Facebook";

                               DealerLocation objDealerLocation = objPublishManager.GetDealerLocationByDealerLocationID(DealerLocationID);
                               objTokenDetail.DealerLocationName = objDealerLocation.Name;

                               objTokenDetail.isValidToken = false;

                               listTokenDetail.Add(objTokenDetail);
                           }
                       }
                    }
                    else
                    {
                        objTokenValidation.message = "Error: Could not Get DealerLocation SocialNetwork Information.";
                    }
                    
                    // Twitter
                    //objDealerLocationSocialNetwork = new DealerLocationSocialNetwork();
                    //objDealerLocationSocialNetwork = objPublishManager.GetDealerLocationSocialNetworkbyDealerLocationID(DealerLocationID, 3);

                    //if (objDealerLocationSocialNetwork != null)
                    //{
                    //    bool result = Twitter.VerifyTwitterToken(objDealerLocationSocialNetwork.Token, objDealerLocationSocialNetwork.TokenSecret, objDealerLocationSocialNetwork.AppKey, objDealerLocationSocialNetwork.AppSecret);

                    //    objTokenDetail = new TokenDetail();
                    //    objTokenDetail.DealerLocationID = objDealerLocationSocialNetwork.LocationID;
                    //    objTokenDetail.DealerLocationSocialNetWorkID = objDealerLocationSocialNetwork.ID;
                    //    objTokenDetail.SocialNetWorkID = objDealerLocationSocialNetwork.SocialNetworkID;
                    //    objTokenDetail.isValidToken = result;

                    //    listTokenDetail.Add(objTokenDetail);
                    //}

                    objTokenValidation.tokenDetail = listTokenDetail;
                }
                else
                {
                    objTokenValidation.message = "Error: Could not Get DealerLocation SocialNetwork Information.";
                    objTokenValidation.tokenDetail = listTokenDetail;
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\ValidateTokenDealerLocationID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objTokenValidation;
        }

        public static void UpdateAppKeysAndValidateTokensForALLFBAccounts(int DealerLocationID)
        {
            PublishManager objPublishManager = new PublishManager();
            SocialDealerManager objSocialDealerManager = new SocialDealerManager();
            //var fbTokenResults = new List<spGetFBTokenInfoByLocation_Result>();
            FacebookTokenInfo objFacebookTokenInfo = new FacebookTokenInfo();
            DebugToken objDebugToken = new DebugToken();
            FacebookResponse objFacebookResponse = new FacebookResponse();
            
            try
            {
                var fbTokenResults = objSocialDealerManager.GetSocialNetWorkByDealerLocationID(DealerLocationID, 2);
                int accountFBCounter = 0;
                int activeFBCounter = 0;
                int inActiveFBCounter = 0;

                foreach (var fbTokenResult in fbTokenResults)
                {
                    accountFBCounter++;
                    string appKey = string.Empty;
                    try
                    {
                        if (!string.IsNullOrEmpty(fbTokenResult.Token))
                        {
                            string rawMessage = string.Empty;
                            //string URL = (string.Format("https://developers.facebook.com/tools/debug/access_token?q={0}", fbTokenResult.Token));
                            string URL = (string.Format("https://graph.facebook.com/debug_token?input_token={0}&access_token={1}", fbTokenResult.Token,fbTokenResult.BaseToken));
                            string rawResponse = GetResponse(URL);
                            string responseHtml = string.Empty;

                            if (!string.IsNullOrEmpty(rawResponse))
                            {

                                var jss = new JavaScriptSerializer();
                                if (rawResponse.StartsWith(@"{""error"":"))
                                {
                                    rawResponse = rawResponse.Replace(@"{""error"":", "");
                                    rawResponse = rawResponse.Substring(0, rawResponse.Length - 1);

                                    objFacebookResponse = jss.Deserialize<FacebookResponse>(rawResponse);

                                    inActiveFBCounter++;
                                    objSocialDealerManager.UpdateTokenStatus(false, "InvalidTokenForApp", fbTokenResult.ID);
                                }
                                else
                                {
                                    objDebugToken = jss.Deserialize<DebugToken>(rawResponse);

                                    if (objDebugToken.data.is_valid == true)
                                    {
                                        activeFBCounter++;
                                        objSocialDealerManager.UpdateTokenStatus(true, objDebugToken.data.app_id, fbTokenResult.ID);
                                    }
                                    else
                                    {
                                        inActiveFBCounter++;
                                        objSocialDealerManager.UpdateTokenStatus(false, "InvalidTokenForApp", fbTokenResult.ID);
                                    }
                                }

                                objFacebookTokenInfo.facebookResponse = objFacebookResponse;
                                objFacebookTokenInfo.debugToken = objDebugToken;
                                

                                //appKey = "InvalidTokenForApp";
                                //string STR = responseHtml;
                                //string STRFirst = @"App ID:</th><td class=""data""><div>";
                                //////table//tbody//tr[1]//td[1]
                                //string STRLast = @" : <a href=""http://www.facebook.com/apps/application.php";

                                //int Pos1 = STR.IndexOf(STRFirst) + STRFirst.Length;
                                //int Pos2 = STR.IndexOf(STRLast);

                                //if (Pos2 > Pos1)
                                //{
                                //    rawMessage = STR.Substring(Pos1, Pos2 - Pos1);
                                //}

                                //if (!(responseHtml.ToLower().Contains("error</div></h2")))
                                //{
                                //    appKey = rawMessage;
                                //    //Doing an additional Check for Carfax Appkeys to make sure photo_upload permission is enabled
                                //    // If not, correct Appkey is stored in DB, BUT IsValidToken will be set to False
                                //    //if (appKey == "100453413444810" || appKey == "134230910079767")
                                //    //{
                                //    //    TokenValidation objTokenValidation = new TokenValidation();
                                //    //    TokenDetail objTokenDetail = new TokenDetail();
                                //    //    objTokenValidation = ValidateTokenDealerLocationID(fbTokenResult.DealerLocationID);
                                //    //    if (objTokenValidation.tokenDetail.FirstOrDefault().isValidToken == true)
                                //    //    {
                                //    //        activeFBCounter++;
                                //    //        objSocialDealerManager.UpdateTokenStatus(true, appKey, fbTokenResult.ID);
                                //    //    }
                                //    //    else
                                //    //    {
                                //    //        inActiveFBCounter++;
                                //    //        objSocialDealerManager.UpdateTokenStatus(false, appKey, fbTokenResult.ID);
                                //    //        //context.spValidateTokenStatus(false, fbTokenResult.ID);
                                //    //    }
                                //    //}
                                //    //else
                                //    //{
                                //        activeFBCounter++;
                                //        objSocialDealerManager.UpdateTokenStatus(true, appKey, fbTokenResult.ID);
                                //        //context.spValidateTokenStatus(true, fbTokenResult.ID);
                                //    //}
                                //}
                                //else
                                //{
                                //    inActiveFBCounter++;
                                //    objSocialDealerManager.UpdateTokenStatus(false, appKey, fbTokenResult.ID);
                                //    //context.spValidateTokenStatus(false, fbTokenResult.ID);
                                //}

                                //context.spUpdateDLSNAppkey(appKey, fbTokenResult.ID);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\UpdateFBAppKeyAndValidateToken";
                        ObjPublishExceptionLog.ExtraInfo = string.Empty;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\UpdateFBAppKeyAndValidateToken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }
		

        #endregion

        #region Private

        private static byte[] DownloadImage(string ImageUrl)
        {
            PublishManager objPublishManager = new PublishManager();
            try
            {
                var httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(ImageUrl);
                httpWebRequest.AllowWriteStreamBuffering = true;
                httpWebRequest.Timeout = 20000;
                var webResponse = httpWebRequest.GetResponse();
                var webStream = webResponse.GetResponseStream();

                using (var ms = new MemoryStream())
                {
                    webStream.CopyTo(ms);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\DownloadImage";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                byte[] bytes = null;
                return bytes;

                #endregion
            }


        }

        private static string ExecuteCommand_Photo(FacebookRequest _facebookRequest, string Token)
        {
            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {

                // Create Boundary
                string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");

                // Create Path                
                if (!String.IsNullOrEmpty(_facebookRequest.Album_id))
                {
                    _facebookRequest.RequestURL += _facebookRequest.Album_id + "/";
                }
                _facebookRequest.RequestURL += "photos";

                // Create HttpWebRequest
                HttpWebRequest httpRequest;
                httpRequest = (HttpWebRequest)HttpWebRequest.Create(_facebookRequest.RequestURL);
                httpRequest.ServicePoint.Expect100Continue = false;
                httpRequest.Method = _facebookRequest.Method;
                httpRequest.UserAgent = "Mozilla/4.0 (compatible; Windows NT)";
                httpRequest.ContentType = "multipart/form-data; boundary=" + boundary;
                httpRequest.KeepAlive = false;
                httpRequest.Timeout = _facebookRequest.TimeOut;

                // New String Builder
                StringBuilder sb = new StringBuilder();

                // Add Form Data
                string formdataTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}\r\n";

                // Access Token
                sb.AppendFormat(formdataTemplate, boundary, "access_token", Token);

                // Message
                sb.AppendFormat(formdataTemplate, boundary, "message", _facebookRequest.Message);

                // Header
                string headerTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";

                sb.AppendFormat(headerTemplate, boundary, "source", _facebookRequest.Filename, @"application/octet-stream");

                // File
                string formString = sb.ToString();
                byte[] formBytes = Encoding.UTF8.GetBytes(formString);
                byte[] trailingBytes = Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
                byte[] image;
                image = _facebookRequest.bytes;

                // Memory Stream
                MemoryStream imageMemoryStream = new MemoryStream();
                imageMemoryStream.Write(image, 0, image.Length);

                // Set Content Length
                long imageLength = imageMemoryStream.Length;
                long contentLength = formBytes.Length + imageLength + trailingBytes.Length;
                httpRequest.ContentLength = contentLength;

                // Get Request Stream
                httpRequest.AllowWriteStreamBuffering = false;
                Stream strm_out = httpRequest.GetRequestStream();

                // Write to Stream
                strm_out.Write(formBytes, 0, formBytes.Length);
                byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)imageLength))];
                              

                int bytesRead = 0;
                int bytesTotal = 0;
                imageMemoryStream.Seek(0, SeekOrigin.Begin);
                while ((bytesRead = imageMemoryStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    strm_out.Write(buffer, 0, bytesRead); bytesTotal += bytesRead;
                }
                strm_out.Write(trailingBytes, 0, trailingBytes.Length);

                // Close Stream
                strm_out.Close();

                rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Post");

                #region old code
                //try
                //{
                //    //Get Response
                //    using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                //    {
                //        using (Stream responseStream = svcResponse.GetResponseStream())
                //        {
                //            try
                //            {
                //                using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                //                {
                //                    rawResponse = readr.ReadToEnd();
                //                    rawResponse = rawResponse.Trim();
                //                }
                //            }
                //            catch (Exception ex)
                //            {
                //                #region ERROR HANDLING

                //                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                //                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                //                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //                ObjPublishExceptionLog.PostResponse = string.Empty;
                //                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //                ObjPublishExceptionLog.MethodName = "FaceBook\\ExecuteCommand_Photo\\UploadPhoto\\GetResponseStream";
                //                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                //                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                //                #endregion
                //            }
                //        }

                //    }
                //}
                //catch (WebException webex)
                //{
                //    using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                //    {
                //        var responseText = reader.ReadToEnd();
                //        rawResponse = responseText;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    #region ERROR HANDLING

                //    PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //    ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                //    ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                //    ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //    ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //    ObjPublishExceptionLog.PostResponse = string.Empty;
                //    ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //    ObjPublishExceptionLog.MethodName = "FaceBook\\ExecuteCommand_Photo\\GetResponse";
                //    ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                //    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                //    #endregion
                //}
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\ExecuteCommand_Photo\\UploadPhoto";
                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }





            // Return
            return rawResponse;
        }

        private static string ExecuteCommand_WallPost(FacebookRequest _facebookRequest)
        {

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST/DELETE"
                UTF8Encoding encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_facebookRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_facebookRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_facebookRequest.Method))
                        httpRequest.Method = _facebookRequest.Method;

                    byte[] postBytes;
                    if (httpRequest.Method == "POST")
                    {
                        httpRequest.ContentType = "application/x-www-form-urlencoded";
                        postBytes = encoding.GetBytes(_facebookRequest.Parameters);
                        using (Stream reqStream = httpRequest.GetRequestStream())
                        {
                            reqStream.Write(postBytes, 0, postBytes.Length);
                            reqStream.Flush();
                            reqStream.Close();
                        }
                    }
                    httpRequest.Timeout = _facebookRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_WallPost");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\ExecuteCommand_WallPost";
                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Get(FacebookRequest _facebookRequest)
        {

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "GET"
                UTF8Encoding encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_facebookRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_facebookRequest.RequestURL);
                    httpRequest.UserAgent = "Mozilla/4.0 (compatible; Windows NT)";

                    if (!String.IsNullOrEmpty(_facebookRequest.Method))
                        httpRequest.Method = _facebookRequest.Method;

                    httpRequest.Timeout = _facebookRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Get");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Utility\\ExecuteCommand_Get";
                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_CreateAlbum(FacebookRequest _facebookRequest)
        {

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST"
                UTF8Encoding encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_facebookRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_facebookRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_facebookRequest.Method))
                        httpRequest.Method = _facebookRequest.Method;

                    byte[] postBytes;
                    if (httpRequest.Method == "POST")
                    {
                        httpRequest.ContentType = "application/x-www-form-urlencoded";
                        postBytes = encoding.GetBytes(_facebookRequest.Parameters);
                        using (Stream reqStream = httpRequest.GetRequestStream())
                        {
                            reqStream.Write(postBytes, 0, postBytes.Length);
                            reqStream.Flush();
                            reqStream.Close();
                        }
                    }
                    httpRequest.Timeout = _facebookRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_CreateAlbum");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\ExecuteCommand_CreateAlbum";
                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Event(FacebookRequest _facebookRequest)
        {

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST/DELETE"
                UTF8Encoding encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_facebookRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_facebookRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_facebookRequest.Method))
                        httpRequest.Method = _facebookRequest.Method;

                    byte[] postBytes;
                    if (httpRequest.Method == "POST")
                    {
                        httpRequest.ContentType = "application/x-www-form-urlencoded";
                        postBytes = encoding.GetBytes(_facebookRequest.Parameters);
                        using (Stream reqStream = httpRequest.GetRequestStream())
                        {
                            reqStream.Write(postBytes, 0, postBytes.Length);
                            reqStream.Flush();
                            reqStream.Close();
                        }
                    }
                    httpRequest.Timeout = _facebookRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Event");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\ExecuteCommand_Event";
                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Question(FacebookRequest _facebookRequest)
        {
            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST/DELETE"
                UTF8Encoding encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_facebookRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_facebookRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_facebookRequest.Method))
                        httpRequest.Method = _facebookRequest.Method;

                    byte[] postBytes;
                    if (httpRequest.Method == "POST")
                    {
                        httpRequest.ContentType = "application/x-www-form-urlencoded";
                        postBytes = encoding.GetBytes(_facebookRequest.Parameters);
                        using (Stream reqStream = httpRequest.GetRequestStream())
                        {
                            reqStream.Write(postBytes, 0, postBytes.Length);
                            reqStream.Flush();
                            reqStream.Close();
                        }
                    }
                    httpRequest.Timeout = _facebookRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Question");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\ExecuteCommand_Question";
                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }
        
        private static string WebRequest(HttpWebRequest httpRequest, int DealerLocationSocialNetworkPostID, int DealerLocationSocialNetworkPostQueueID, string MethodName)
        {
            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "FaceBook\\" + MethodName + "\\WebRequest\\GetResponseStream";
                            ObjPublishExceptionLog.ExtraInfo = string.Empty;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";

                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\" + MethodName + "\\WebRequest\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                #endregion
            }

            return rawResponse;
        }

        private static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("ARGH!");
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }

        private static string GetResponse(string requestURL)
        {
            FacebookRequest _facebookRequest = new FacebookRequest();
            PublishManager objPublishManager = new PublishManager();
            string Response = string.Empty;
            try
            {
                _facebookRequest.RequestURL = requestURL;
                _facebookRequest.Method = "GET";
                _facebookRequest.TimeOut = 20000;

                Response = ExecuteCommand_Get(_facebookRequest);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = "requestURL : " + requestURL;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return Response;
        }

        //private static 
        #endregion

        #endregion
    }
}
