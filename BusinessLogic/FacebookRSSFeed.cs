﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using DataLayer;
using BusinessEntities;
using System.IO;
using System.Web.Script.Serialization;

namespace BusinessLogic
{
    public static class FacebookRSSFeed
    {
        #region variable

        //private static string FacebookRSSFeedURL = Convert.ToString(ConfigurationManager.AppSettings["FacebookRSSFeedURL"]);
        private static string FacebookAPITimeOut = Convert.ToString(ConfigurationManager.AppSettings["FacebookAPITimeOut"]);

        #endregion

        #region Functions

        #region Public

        public static FacebookPageRSSFeed GetRSSFeedInformation(string FacebookRSSFeedURL)
        {
            PublishManager objPublishManager = new PublishManager();
            FacebookPageRSSFeed objFacebookPageRSSFeed = new FacebookPageRSSFeed();
            try
            {
                if (string.IsNullOrEmpty(FacebookRSSFeedURL))
                {
                    FacebookRSSFeedURL = "https://www.facebook.com/feeds/notifications.php?id=255547016773&viewer=100002916188585&key=AWif3BKtbYxO61cY&format=json";
                }

                if (string.IsNullOrEmpty(FacebookAPITimeOut))
                {
                    FacebookAPITimeOut = "20000";
                }

                string URL = string.Format(FacebookRSSFeedURL);

                string rawResponse = string.Empty;

                try
                {
                    FacebookRequest objFacebookRequest = new FacebookRequest();
                    objFacebookRequest.RequestURL = URL;
                    objFacebookRequest.Method = "GET";
                    objFacebookRequest.TimeOut = Convert.ToInt32(FacebookAPITimeOut);
                    
                    rawResponse = ExecuteCommand_Get(objFacebookRequest);
                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        objFacebookPageRSSFeed = jss.Deserialize<FacebookPageRSSFeed>(rawResponse);

                    }
                }
                catch (Exception ex)
                {

                }


            }
            catch (Exception ex)
            {
            }

            return objFacebookPageRSSFeed;
        }

        public static List<FaceBookPostIdType> ProcessRSSFeed(FacebookPageRSSFeed objFacebookPageRSSFeed)
        {
            List<FaceBookPostIdType> listFaceBookPostIdType = new List<FaceBookPostIdType>();            
            try
            {
                if (objFacebookPageRSSFeed.entries != null)
                {
                    string PageTitle = objFacebookPageRSSFeed.title.Trim().ToLower();
                    if (!string.IsNullOrEmpty(PageTitle))
                    {
                        PageTitle = PageTitle.Substring(0, PageTitle.IndexOf("'s"));
                    }
                    foreach (var item in objFacebookPageRSSFeed.entries)
                    {
                        try
                        {
                            string title = item.title.Trim().ToLower();

                            if (!string.IsNullOrEmpty(title))
                            {
                                string alternate = string.Empty;
                                string ActionType = string.Empty;
                                string PostID = string.Empty;
                                string Level = string.Empty;
                                string postType = string.Empty;
                                string RSSFeedMessage = string.Empty;

                                if (title.Contains("wall post") && title.Contains("commented"))
                                {
                                    //Comment on Some one Wall Post
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "post";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.CommentonUserWallPost);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains("photo") && title.Contains("commented") && title.Contains(PageTitle))
                                {
                                    //Comment on Photo
                                    Level = "post";
                                    postType = "photo";
                                    //ActionType = "comment";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.Comment);
                                    PostID = alternate.Substring(alternate.IndexOf("fbid=") + 5, alternate.IndexOf("&set") - (alternate.IndexOf("fbid=") + 5));

                                }
                                else if (title.Contains("photo") && title.Contains("like") && title.Contains(PageTitle))
                                {
                                    //Photo Like
                                    Level = "post";
                                    postType = "photo";
                                    //ActionType = "like";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.Like);
                                    //PostID = alternate.Substring(alternate.IndexOf("fbid=") + 5, alternate.IndexOf("&set") - (alternate.IndexOf("fbid=") + 5));
                                }
                                else if (title.Contains("link") && title.Contains("commented") && title.Contains(PageTitle))
                                {
                                    //Comment on Link
                                    Level = "post";
                                    postType = "link";
                                    //ActionType = "comment";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.Comment);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains("link") && title.Contains("like") && title.Contains(PageTitle))
                                {
                                    //Likes on Link
                                    Level = "post";
                                    postType = "link";
                                    //ActionType = "comment";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.Like);
                                    //PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&aref") - (alternate.IndexOf("posts/") + 6));

                                }
                                else if (title.Contains("status") && title.Contains("like") && title.Contains(PageTitle))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "status";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.StatusLike);
                                    //PostID = alternate.Substring(alternate.IndexOf("fbid=") + 5, alternate.IndexOf("&id") - (alternate.IndexOf("fbid=") + 5));
                                }
                                else if (title.Contains("status") && title.Contains("commented") && title.Contains(PageTitle))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "status";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.Comment);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains("status") && title.Contains("commented") && !title.Contains(PageTitle))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "post";
                                    postType = "status";
                                    //ActionType = "status";

                                    alternate = item.alternate.Trim().ToLower().Replace("%2f", "/");
                                    ActionType = Convert.ToString(FacebookActionType.CommentonUserWallPost);
                                    PostID = alternate.Substring(alternate.IndexOf("posts/") + 6, alternate.IndexOf("&comment") - (alternate.IndexOf("posts/") + 6));
                                }
                                else if (title.Contains(PageTitle) && title.Contains("like"))
                                {
                                    //Post on TimeLine (New Post)
                                    Level = "page";
                                    postType = "page";
                                    //ActionType = "pagelike";

                                    alternate = item.alternate.Trim().ToLower();
                                    ActionType = Convert.ToString(FacebookActionType.StatusLike);
                                }

                                if (!string.IsNullOrEmpty(PostID))
                                {
                                    var result = listFaceBookPostIdType.Where(x => x.PostID == PostID).SingleOrDefault();
                                    if (result == null)
                                    {
                                        listFaceBookPostIdType.Add(new FaceBookPostIdType { PostID = PostID, PostType = postType, ActionType = ActionType, RSSFeedMessage = item.title });
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {

            }

            return listFaceBookPostIdType;
        }

        #endregion

        #region Private

        private static string ExecuteCommand_Get(FacebookRequest _facebookRequest)
        {

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "GET"
                UTF8Encoding encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_facebookRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_facebookRequest.RequestURL);
                    httpRequest.UserAgent = "Mozilla/5.0 (compatible; Windows NT)";

                    if (!String.IsNullOrEmpty(_facebookRequest.Method))
                        httpRequest.Method = _facebookRequest.Method;

                    httpRequest.Timeout = _facebookRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest,"ExecuteCommand_Get");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FacebookRSSFeed\\ExecuteCommand_Get";
                ObjPublishExceptionLog.ExtraInfo = _facebookRequest.RequestURL + "," + _facebookRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string WebRequest(HttpWebRequest httpRequest,string MethodName)
        {
            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "FacebookRSSFeed\\" + MethodName + "\\WebRequest\\GetResponseStream";
                            ObjPublishExceptionLog.ExtraInfo = string.Empty;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";

                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FacebookRSSFeed\\" + MethodName + "\\WebRequest\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                #endregion
            }

            return rawResponse;
        }

        #endregion

        #endregion
    }
}
