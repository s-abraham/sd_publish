﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using BusinessEntities;
using System.Web.Script.Serialization;
using DataLayer;
using Newtonsoft.Json;
using System.Configuration;


namespace BusinessLogic
{
    public static class Youtube
    {
        #region VARIABLES

        private static string YoutubeAPITimeOut = Convert.ToString(ConfigurationManager.AppSettings["YoutubeAPI.TimeOut"]);
        private static string YoutubeAPIKey = Convert.ToString(ConfigurationManager.AppSettings["YoutubeAPI.Key"]);
        private static string YoutubeAPIURLGetPageInsights = Convert.ToString(ConfigurationManager.AppSettings["YoutubeAPIURL.GetPageInsights"]);

        #endregion

        #region METHODS

        #region Public

        public static YoutubePageInsightsResponse GetYoutubePageInsights(string uniqueID)
        {
            PublishManager objPublishManager = new PublishManager();
            YoutubePageInsightsResponse objYoutubePageInsightsResponse = new YoutubePageInsightsResponse();

            try
            {
                if (!string.IsNullOrEmpty(uniqueID))
                {
                    if (string.IsNullOrEmpty(YoutubeAPIURLGetPageInsights))
                    {
                        YoutubeAPIURLGetPageInsights = "https://gdata.youtube.com/feeds/api/users/{0}?key={1}&alt=json&v=2";
                    }

                    if (string.IsNullOrEmpty(YoutubeAPIKey))
                    {
                        YoutubeAPIKey = "AI39si7atM2H5_Rw1Do73BpP8yGiHyWlBvgrZuf4UOwY-A6heOVd3-34auL036_iwcx220N03KypsEyYMGRNmNenkD9G3dTIig";
                    }

                    if (string.IsNullOrEmpty(YoutubeAPITimeOut))
                    {
                        YoutubeAPITimeOut = "20000";
                    }

                    string url = string.Format(YoutubeAPIURLGetPageInsights, uniqueID, YoutubeAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        YoutubeRequest objYoutubeRequest = new YoutubeRequest();
                        objYoutubeRequest.RequestURL = url;
                        objYoutubeRequest.Method = "GET";
                        objYoutubeRequest.TimeOut = Convert.ToInt32(YoutubeAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objYoutubeRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (!rawResponse.ToLower().Contains("\"title\""))
                            {
                                objYoutubePageInsightsResponse.errorMessage = "Invalid Request URL";
                            }
                            else
                            {
                                rawResponse = rawResponse.Replace("\"viewCount\":", "\"channelViewsCount\":");
                                objYoutubePageInsightsResponse = jss.Deserialize<YoutubePageInsightsResponse>(rawResponse.Replace("$", "_"));

                                objYoutubePageInsightsResponse.entry.yt_statistics.subscriptionsCount = objYoutubePageInsightsResponse.entry.gd_feedLink.Any(f => f.rel.ToLower().Contains("subscriptions")) ?
                                   (objYoutubePageInsightsResponse.entry.gd_feedLink.First(f => f.rel.ToLower().Contains("subscriptions"))).countHint : 0;
                                objYoutubePageInsightsResponse.entry.yt_statistics.contactsCount = objYoutubePageInsightsResponse.entry.gd_feedLink.Any(f => f.rel.ToLower().Contains("contacts")) ?
                                   (objYoutubePageInsightsResponse.entry.gd_feedLink.First(f => f.rel.ToLower().Contains("contacts"))).countHint : 0;
                                objYoutubePageInsightsResponse.entry.yt_statistics.videosCount = objYoutubePageInsightsResponse.entry.gd_feedLink.Any(f => f.rel.ToLower().Contains("uploads")) ?
                                   (objYoutubePageInsightsResponse.entry.gd_feedLink.First(f => f.rel.ToLower().Contains("uploads"))).countHint : 0;
                                objYoutubePageInsightsResponse.entry.yt_statistics.favoritesCount = objYoutubePageInsightsResponse.entry.gd_feedLink.Any(f => f.rel.ToLower().Contains("favorites")) ?
                                  (objYoutubePageInsightsResponse.entry.gd_feedLink.First(f => f.rel.ToLower().Contains("favorites"))).countHint : 0;
                            }
                        }

                        else
                        {
                            objYoutubePageInsightsResponse.errorMessage = "rawResponse is empty";
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Youtube\\GetYoutubePageInsights";
                        ObjPublishExceptionLog.ExtraInfo = "uniqueID : " + uniqueID + ", " + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
                        objYoutubePageInsightsResponse.errorMessage = ex.Message;
                        #endregion
                    }
                }
                else
                {
                   objYoutubePageInsightsResponse.errorMessage = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Youtube\\GetYoutubePageInsights";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objYoutubePageInsightsResponse;
        }

        public static void ValidateALLYoutubeTokens(int DealerLocationID)
        {
            PublishManager objPublishManager = new PublishManager();
            SocialDealerManager objSocialDealerManager = new SocialDealerManager();
            
            try
            {
                int accountYTCounter = 0;
                int activeYTCounter = 0;
                int inActiveYTCounter = 0;

                var ytTokenResults = objSocialDealerManager.GetSocialNetWorkByDealerLocationID(DealerLocationID, 5);

                foreach (var ytTokenResult in ytTokenResults)
                {
                    accountYTCounter++;
                    try
                    {
                        if (!string.IsNullOrEmpty(ytTokenResult.Token))
                        {
                            string URL = (string.Format("http://gdata.youtube.com/feeds/api/users/default?v=2&access_token={0}", ytTokenResult.Token));

                            string responseHtml = GetResponse(URL);

                            if (!string.IsNullOrEmpty(responseHtml))
                            {
                                if (!(responseHtml.ToLower().Contains("error")))
                                {
                                    activeYTCounter++;
                                    objSocialDealerManager.UpdateTokenStatus(true, string.Empty, ytTokenResult.ID);
                                    //context.spValidateTokenStatus(true, ytTokenResult.ID);
                                }
                                else
                                {
                                    inActiveYTCounter++;
                                    objSocialDealerManager.UpdateTokenStatus(false, string.Empty, ytTokenResult.ID);
                                    //context.spValidateTokenStatus(false, ytTokenResult.ID);
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Youtube\\ValidateALLYoutubeTokens";
                        ObjPublishExceptionLog.ExtraInfo = "token" + ytTokenResult.Token;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                //PublishServiceLog ObjPublishServiceLog = new PublishServiceLog();
                //ObjPublishServiceLog.Message = "Validating Youtube Tokens: " + activeYTCounter + " Valid & " + inActiveYTCounter + " Invalid";
                //ObjPublishServiceLog.Exception = "Total YT Accounts Validated: " + accountYTCounter;
                //ObjPublishServiceLog.MethodName = "Youtube\\ValidateAllYoutubeTokens";
                //objPublishManager.AddPublishServiceLog(ObjPublishServiceLog);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Youtube\\ValidateALLYoutubeTokens";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

        #endregion

        #region Private

        private static string WebRequest(HttpWebRequest httpRequest, int DealerLocationSocialNetworkPostID, int DealerLocationSocialNetworkPostQueueID, string MethodName)
        {
            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "Youtube\\" + MethodName + "\\WebRequest\\GetResponseStream";
                            ObjPublishExceptionLog.ExtraInfo = string.Empty;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            rawResponse = @"{""error"":" + "" + ex.Message + "" + @", ""request"":""\/1\/statuses\/update.json""}";

                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Youtube\\" + MethodName + "\\WebRequest\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                rawResponse = @"{""error"":" + "" + ex.Message + "" + @", ""request"":""\/1\/statuses\/update.json""}";

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Get(YoutubeRequest _youtubeRequest)
        {

            var objPublishManager = new PublishManager();
            var rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "GET"
                var encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_youtubeRequest.RequestURL))
                {
                    var httpRequest = (HttpWebRequest)HttpWebRequest.Create(_youtubeRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_youtubeRequest.Method))
                        httpRequest.Method = _youtubeRequest.Method;

                    httpRequest.Timeout = _youtubeRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Get");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Youtube\\ExecuteCommand_Get";
                ObjPublishExceptionLog.ExtraInfo = _youtubeRequest.RequestURL + "," + _youtubeRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }
        
        private static string GetResponse(string requestURL)
        {
            YoutubeRequest _youtubeRequest = new YoutubeRequest();
            PublishManager objPublishManager = new PublishManager();
            string Response = string.Empty;
            try
            {
                _youtubeRequest.RequestURL = requestURL;
                _youtubeRequest.Method = "GET";
                _youtubeRequest.TimeOut = 20000;

                Response = ExecuteCommand_Get(_youtubeRequest);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Youtube\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = "requestURL : " + requestURL;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return Response;
        }
        #endregion

        #endregion
    }
}
