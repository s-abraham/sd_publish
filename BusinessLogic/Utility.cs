﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using DataLayer;
using System.Net;

namespace BusinessLogic
{
    public static class Utility
    {   
        public static void SendMessageToQueue(string XMLMessage, string QueueName)
        {
            MessageQueue msMq = null;
            DefaultPropertiesToSend myDefaultProperties = new DefaultPropertiesToSend();
            try
            {
                // check if queue exists, if not create it
                myDefaultProperties.Label = "SocialNetworkPost";
                myDefaultProperties.Recoverable = true;

                if (!MessageQueue.Exists(QueueName))
                {
                    msMq = MessageQueue.Create(QueueName);
                }
                else
                {
                    msMq = new MessageQueue(QueueName);
                }
                msMq.DefaultPropertiesToSend = myDefaultProperties;
                msMq.Send(Trim(XMLMessage), "SocialNetworkPost");
            }
            catch (MessageQueueException ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = Convert.ToString(ex.InnerException);
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\SendMessageToQueue";
                ObjPublishExceptionLog.ExtraInfo = XMLMessage;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = Convert.ToString(ex.InnerException);
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\SendMessageToQueue";
                ObjPublishExceptionLog.ExtraInfo = XMLMessage;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            finally
            {
                msMq.Close();
            }
        }

        public static string SerializeObject<T>(T obj)
        {
            XmlSerializer xmls = new XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                using (XmlWriter writer = new XmlTextWriter(ms, new UTF8Encoding()))
                {
                    xmls.Serialize(writer, obj, ns);
                }
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        public static string Trim(string xml)
        {
            //xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
            //<?xml version="1.0" encoding="utf-8"?>
            //xmlns:xsd="http://www.w3.org/2001/XMLSchema"

            string[] trimCommentandNamespaces = new string[] { "<?xml version=\"1.0\" encoding=\"utf-8\"?>", " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" };
            foreach (string s in trimCommentandNamespaces)
            {
                xml = xml.Replace(s, "");
            }

            return xml;
        }

        public static T FromXml<T>(String xml)
        {
            T returnedXmlClass = default(T);

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass = (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        // String passed is not XML, simply return defaultXmlClass
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = Convert.ToString(ex.InnerException);
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\FromXml";
                ObjPublishExceptionLog.ExtraInfo = xml;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return returnedXmlClass;
        }

        public static string MakesdTinyUrl(string Url)
        {
            string text = string.Empty;
            try
            {
                var request = WebRequest.Create(ConfigurationManager.AppSettings["tinyurldomain"] + "?url=" + Url);
                var res = request.GetResponse();

                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                if (!text.Contains("http://"))
                    text = "http://" + text;

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = Convert.ToString(ex.InnerException);
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\MakesdTinyUrl";
                ObjPublishExceptionLog.ExtraInfo = Url;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return text;
        }

        public static bool IsAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }
               
    }
}
