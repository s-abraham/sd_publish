﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data.Objects;
using BusinessEntities;
using System.Configuration;
using System.Web;
using SocialDealer.SocialNetworks.Twitter;
using System.Data;
using System.Net;
using System.IO;
using LinqToTwitter;

namespace BusinessLogic
{
    public class PublishToSocialNetwork
    {
        private static string QueueName = Convert.ToString(ConfigurationManager.AppSettings["QueueName"]);
        private static int PostExpirationDate = Convert.ToInt32(ConfigurationManager.AppSettings["PostExpirationDate"]);
        private static readonly int twitterMaxLength = Convert.ToInt32(ConfigurationManager.AppSettings["TwitterMaxLength"]);
        private static readonly int facebookMaxLength = Convert.ToInt32(ConfigurationManager.AppSettings["FacebookMaxLength"]);
        private static string FacebookErrorsXMLFilePath = Convert.ToString(ConfigurationManager.AppSettings["FacebookErrorsXMLFilePath"]);
        private static int MaxReTryNumber = Convert.ToInt32(ConfigurationManager.AppSettings["MaxReTryNumber"]);
        private static string ApprovalEmailFilePath = Convert.ToString(ConfigurationManager.AppSettings["ApprovalEmailFilePath"]);
        private static string SendApprovalEmail = Convert.ToString(ConfigurationManager.AppSettings["SendApprovalEmail"]);
        private static int MessageTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["MessageTypeId"]);
        
        private static SDNotificationEntities sdNotificationEntities = new SDNotificationEntities();
        private static string _FBAlbumId = string.Empty;


        public PublishToSocialNetwork()
        {
            sdNotificationEntities.CommandTimeout = 180;
        }

        //string dealerID, string socialNetworkIDs, Guid userID, string postTitle, string postText, string link, string imagePath, string videoSource, bool postToAll, int postSourceID, string scheduleDate, int scheduledPostID, long pkvalue, int dealerLocationID, int departmentID, string countriesIDs, string citiesID, string regionsIDs, bool isSyndicated, string isUploadedImage)
        public static string SaveToSocialNetworksTable(DealerLocationSocialNetworkPost objDealerLocationSocialNetworkPost)
        {
            int DealerLocationSocialNetworkPostID = 0;
            PublishManager objPublishManager = new PublishManager();
            spSaveDealerLocationSocialNetworkPostDetails_New_Result objspSaveDealerLocationSocialNetworkPostDetails_New_Result = new spSaveDealerLocationSocialNetworkPostDetails_New_Result();
            var body = string.Empty;
            try
            {   
                //if (string.IsNullOrEmpty(objDealerLocationSocialNetworkPost.scheduleDate))
                //{
                //    objDealerLocationSocialNetworkPost.scheduleDate = Convert.ToString(DateTime.Now);
                //}

                objDealerLocationSocialNetworkPost.postExpirationDate = Convert.ToDateTime(objDealerLocationSocialNetworkPost.scheduleDate).AddMinutes(PostExpirationDate);

                if (objDealerLocationSocialNetworkPost.dealerID == 0)
                {
                    objDealerLocationSocialNetworkPost.dealerID = -1;
                }
                
                var needsApproval = false;
                                
                if (objDealerLocationSocialNetworkPost.postText.Contains("\n"))
                {
                    objDealerLocationSocialNetworkPost.postText = objDealerLocationSocialNetworkPost.postText.Replace("\n", " ").Replace("\r", "");
                }
                
                bool NeedsApproval = objPublishManager.GetNeedsApprovalFromDealerId(objDealerLocationSocialNetworkPost.dealerID);

                if(string.IsNullOrEmpty(SendApprovalEmail))
                {
                    SendApprovalEmail = "true";
                }
                if (NeedsApproval && SendApprovalEmail.ToLower() == "true")
                {
                    //if the postSourceID is anything other than 7 then it needs approval. If it is 7 (YouTube Scheduler) then no approval needed.
                    needsApproval = objDealerLocationSocialNetworkPost.postSourceID != 7;

                    #region Create Approval Notification

                    DataSet ds = new DataSet();
                    try
                    {
                        ds.ReadXml(ApprovalEmailFilePath);
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = string.Empty;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = "";
                        ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToFacebook\\Reading ApprovalEmail XML File";
                        ObjPublishExceptionLog.ExtraInfo = string.Empty;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                    List<spGetTemplates_Result1> templatesList = new List<spGetTemplates_Result1>();
                    templatesList = GetTemplates();

                    foreach (DataRow row in ds.Tables[0].Rows)                            
                    {
                        string[] test_arr;
                        test_arr = Convert.ToString(row["DealerLocationIds"]).Split(',');
                        Boolean Condition = ((IList<string>)test_arr).Contains(Convert.ToString(objDealerLocationSocialNetworkPost.dealerLocationID));

                        if (Condition)
                        {
                            // Create Notification
                            string emailDestinations = Convert.ToString(row["EmailAddress"]);
                            
                            if (!string.IsNullOrEmpty(emailDestinations))
                            {
                                
                                var messageTemplate = templatesList.SingleOrDefault(x => x.MessageTypeId == MessageTypeId);
                                if (messageTemplate != null)
                                {
                                    int lookupsStatus;
                                    lookupsStatus = Convert.ToInt32(LOOKUPSSTATUS.PENDING);
                                                                        
                                    ContactDetails ObjcontactDetails = new ContactDetails();
                                    ObjcontactDetails.SendCC.Add(emailDestinations);
                                    string contactDetails = Utility.SerializeObject(ObjcontactDetails);

                                    var notificationEntry = new NotificationEntry();
                                    notificationEntry.NotificationDetail.ProcessedDate = DateTime.Now;
                                    notificationEntry.NotificationDetail.MessageTypeId = MessageTypeId; // New Review Email from SDNotification.lookups.MessageType
                                    notificationEntry.NotificationDetail.CreatedDate = DateTime.Now;
                                    notificationEntry.NotificationDetail.CustomerId = 0;
                                    notificationEntry.NotificationDetail.DealerLocationId = objDealerLocationSocialNetworkPost.dealerLocationID;
                                    notificationEntry.NotificationDetail.MessageCategoryId = 1; // Reputation Activity from SDNotification.[lookups].[MessageCategory]

                                    body = GenerateEmailBody_ReviewText(messageTemplate, objDealerLocationSocialNetworkPost);
                                    notificationEntry.NotificationDetail.Body = body;


                                    notificationEntry.NotificationDetail.Subject = "Pending Approval";

                                    notificationEntry.NotificationTransmissionDetail.TransmissionTypeId = 1; //Email from SDNotification.[lookups].[TransmissionType]
                                    notificationEntry.NotificationTransmissionDetail.DateUpdated = DateTime.Now;
                                    notificationEntry.NotificationTransmissionDetail.ScheduledDate = DateTime.Now;
                                    notificationEntry.NotificationTransmissionDetail.StatusId = lookupsStatus; //Pending from SDNotification.[lookups].[Status]
                                    notificationEntry.NotificationTransmissionDetail.ServiceNumber = 1;
                                    notificationEntry.NotificationTransmissionDetail.ContactDetails = contactDetails;
                                    
                                    NotificationManager notificationManager = new NotificationManager();
                                    var rawResult = notificationManager.SaveNotificationEntry(notificationEntry);
                                }
                            }                            
                        }
                    }

                    #endregion
                }
                
                string strXML = Utility.SerializeObject<DealerLocationSocialNetworkPost>(objDealerLocationSocialNetworkPost);
                strXML = Utility.Trim(strXML);

                if (!string.IsNullOrEmpty(strXML))
                {
                    objspSaveDealerLocationSocialNetworkPostDetails_New_Result = objPublishManager.SavePostToDealerLocationSocialNetworkPost(strXML, objDealerLocationSocialNetworkPost.dealerLocationSocialNetworkPostId);
                }
                else
                {
                    #region ERROR HANDLING

                    PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                    ObjPublishExceptionLog.ExceptionMessage = "Error in Serializing objDealerLocationSocialNetworkPost to XML";
                    ObjPublishExceptionLog.InnerException = string.Empty;
                    ObjPublishExceptionLog.PostResponse = string.Empty;
                    ObjPublishExceptionLog.StackTrace = string.Empty;
                    ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\SaveToSocialNetworksTable\\SerializeObject";
                    ObjPublishExceptionLog.ExtraInfo = string.Empty;
                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                    #endregion
                }

                if (!objspSaveDealerLocationSocialNetworkPostDetails_New_Result.RESULT.ToLower().Contains("error"))
                {
                    // IF Request is Not scheduledPost then Send Post to MSMQ Else Windows Service will pick from DB
                    if (!objDealerLocationSocialNetworkPost.isScheduledPost && !objDealerLocationSocialNetworkPost.isSyndicated && needsApproval == false) // ask what is Use of objDealerLocationSocialNetworkPost.isSyndicated
                    {
                        PublishMSMQ objPublishMSMQ = new PublishMSMQ();
                        objPublishMSMQ.DealerLocationSocialNetworkPostID = objspSaveDealerLocationSocialNetworkPostDetails_New_Result.DealerLocationSocialNetworkPostIDs;
                        objPublishMSMQ.isRetry = false;
                        objPublishMSMQ.LastExecution = DateTime.Now;
                        objPublishMSMQ.LastExecutionResponse = string.Empty;

                        string XMLMessage = Utility.SerializeObject<PublishMSMQ>(objPublishMSMQ);

                        if (!string.IsNullOrEmpty(XMLMessage))
                        {
                            Utility.SendMessageToQueue(XMLMessage, QueueName);
                        }
                    }
                }

                #region old code
                //if (objDealerLocationSocialNetworkPost.scheduledPostID == -1)
                //{
                //    //save Post to PostToDealerLocationSocialNetworkPost
                //    DealerLocationSocialNetworkPostID = objPublishManager.SavePostToDealerLocationSocialNetworkPost(objDealerLocationSocialNetworkPost);

                //    //IF postId is not Null then save Post to DealerLocationSocialNetworkPostQueue
                //    if (DealerLocationSocialNetworkPostID > 0)
                //    {
                //        //objPublishManager.AddDealerLocationSocialNetworkPostQueueDetails(objDealerLocationSocialNetworkPost.postToAll, DealerLocationSocialNetworkPostID, objDealerLocationSocialNetworkPost.dealerID, objDealerLocationSocialNetworkPost.socialNetworkIDs, needsApproval);
                //    }
                    
                //    // IF Request is Not scheduledPost then Send Post to MSMQ Else Windows Service will pick from DB
                //    if (!objDealerLocationSocialNetworkPost.isScheduledPost && !objDealerLocationSocialNetworkPost.isSyndicated) // ask what is Use of objDealerLocationSocialNetworkPost.isSyndicated
                //    {
                //        PublishMSMQ objPublishMSMQ = new PublishMSMQ();
                //        objPublishMSMQ.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                //        objPublishMSMQ.isRetry = false;
                //        objPublishMSMQ.LastExecution = DateTime.Now;
                //        objPublishMSMQ.LastExecutionResponse = string.Empty;

                //        string XMLMessage = Utility.SerializeObject<PublishMSMQ>(objPublishMSMQ);

                //        if (!string.IsNullOrEmpty(XMLMessage))
                //        {
                //            Utility.SendMessageToQueue(XMLMessage, QueueName);
                //        }
                //    }
                //    //if (!isSchedule && !objDealerLocationSocialNetworkPost.isSyndicated)
                //    //{
                //    //    List<SocialNetworkPost> posts;
                //    //    posts = PublishManager.GetDealerLocationSocialNetworkPostDetails(postId, schedule, -1, -1);
                //    //    foreach (var post in posts)
                //    //    {
                //    //        PostQueue(post);
                //    //        System.Threading.Thread.Sleep(500);
                //    //    }
                //    //}
                //}
                //else
                //{
                //    //PublishManager.UpdatePostInformation(scheduledPostID, postText, postTitle, link, imagePath, videoSource, postToAll, int.Parse(dealerID), socialNetworkIDs, schedule, departmentID, userID, dealerLocationID, pkvalue, postSourceID, countriesIDs, citiesID, regionsIDs, isSyndicated, uploadedImage, needsApproval);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\SaveToSocialNetworksTable";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
                
                #endregion
            }

            return objspSaveDealerLocationSocialNetworkPostDetails_New_Result.RESULT;            
        }

        public static string SaveToSocialNetworksQueueTable(int DealerLocationID, int DealerLocationSocialNetworkPostID, string Type)
        {
            var result = string.Empty;
            PublishManager objPublishManager = new PublishManager();
            try
            {
                spSaveDealerLocationSocialNetworkPostQueueDetails_Result objspSaveDealerLocationSocialNetworkPostQueueDetailsresult = objPublishManager.SaveDealerLocationSocialNetworkPostQueueDetails(DealerLocationID, DealerLocationSocialNetworkPostID, Type);
                result = objspSaveDealerLocationSocialNetworkPostQueueDetailsresult.RESULT;
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                #endregion
            }

            return result;

        }
        private static string GenerateEmailBody_ReviewText(spGetTemplates_Result1 messageTemplate, DealerLocationSocialNetworkPost objDealerLocationSocialNetworkPost)
        {
            var result = string.Empty;

            try
            {
                SocialDealerEntities objSocialDealerEntities = new SocialDealerEntities();
                var messageTemplateMain = messageTemplate.MessageTemplate;
                string imageblock = @"<span><a href=""[$ImageURL$]"" target=""_blank"">[$ImageURL$]</a></span>";

                Guid userID = new Guid(objDealerLocationSocialNetworkPost.userID);

                var UserName = (from l in objSocialDealerEntities.aspnet_Users
                                where l.UserId == userID
                                select l.UserName).SingleOrDefault();

                //var DealerLocationName = (from l in objSocialDealerEntities.DealerLocations
                //                           where l.ID == objDealerLocationSocialNetworkPost.dealerLocationID
                //                            select l.Name).SingleOrDefault();
                                
                
                messageTemplateMain = messageTemplateMain.Replace("[$PostTitle$]", objDealerLocationSocialNetworkPost.postTitle);
                                
                if (objDealerLocationSocialNetworkPost.postTypeId == Convert.ToInt32(PostType.FacebookStatus))
                {
                    messageTemplateMain = messageTemplateMain.Replace("[$Imageblock$]", "&nbsp;");
                    messageTemplateMain = messageTemplateMain.Replace("[$PostMessage$]", objDealerLocationSocialNetworkPost.postText);
                }

                if (objDealerLocationSocialNetworkPost.postTypeId == Convert.ToInt32(PostType.FacebookLink))
                {
                    imageblock = imageblock.Replace("[$ImageURL$]", objDealerLocationSocialNetworkPost.link);
                    messageTemplateMain = messageTemplateMain.Replace("[$Imageblock$]", imageblock);
                    messageTemplateMain = messageTemplateMain.Replace("[$PostMessage$]", objDealerLocationSocialNetworkPost.postText);
                }                

                if(objDealerLocationSocialNetworkPost.postTypeId == Convert.ToInt32(PostType.FacebookMultiplePhoto))
                {
                    //string imageList = string.Join(" <br />", objDealerLocationSocialNetworkPost.imagePath.ToArray());
                    string imageList = string.Empty;

                    string imageblocktemp = string.Empty;
                    foreach (string image in objDealerLocationSocialNetworkPost.imagePath)
                    {
                        imageblocktemp = imageblock;
                        imageblocktemp = imageblocktemp.Replace("[$ImageURL$]", image);
                        
                        imageList = imageList + imageblocktemp + "<br />";
                    }
                    messageTemplateMain = messageTemplateMain.Replace("[$Imageblock$]", imageList);
                    messageTemplateMain = messageTemplateMain.Replace("[$PostMessage$]", "&nbsp;");
                }

                if (objDealerLocationSocialNetworkPost.postTypeId == Convert.ToInt32(PostType.FacebookPhoto))
                {
                    string imageList = string.Join("", objDealerLocationSocialNetworkPost.imagePath.ToArray());
                    imageblock = imageblock.Replace("[$ImageURL$]", imageList);

                    messageTemplateMain = messageTemplateMain.Replace("[$Imageblock$]", imageblock);
                    messageTemplateMain = messageTemplateMain.Replace("[$PostMessage$]", objDealerLocationSocialNetworkPost.postText);
                }
                
                if (objDealerLocationSocialNetworkPost.postTypeId == Convert.ToInt32(PostType.FacebookEvent))
                {
                    string EventPostMessage = string.Empty;
                    EventPostMessage = "Event Name : " + objDealerLocationSocialNetworkPost.eventName + "<br />";
                    EventPostMessage += "Details: " + objDealerLocationSocialNetworkPost.description + "<br />";
                    EventPostMessage += "Where: " + objDealerLocationSocialNetworkPost.fBLocation + "<br />";
                    EventPostMessage += "From: " + objDealerLocationSocialNetworkPost.eventStartTime.ToString() + "<br />";
                    EventPostMessage += "To: " + objDealerLocationSocialNetworkPost.eventEndTime.ToString() ;

                    messageTemplateMain = messageTemplateMain.Replace("[$Imageblock$]", "&nbsp;");
                    messageTemplateMain = messageTemplateMain.Replace("[$PostMessage$]", EventPostMessage);
                }                

                if (objDealerLocationSocialNetworkPost.postTypeId == Convert.ToInt32(PostType.FacebookQuestion))
                {
                    string fBQuestion_Option = "Question: " + objDealerLocationSocialNetworkPost.fBQuestion + "<br />" + "Options: " + string.Join(", ", objDealerLocationSocialNetworkPost.fBOptions.ToArray());

                    messageTemplateMain = messageTemplateMain.Replace("[$Imageblock$]", "&nbsp;");
                    messageTemplateMain = messageTemplateMain.Replace("[$PostMessage$]", fBQuestion_Option);
                }

                string SocialNetworkblock = @"<img alt=""[$SocialNetworkName$]"" border=""0"" src=""[$SocialNetworkImgURL$]"" />";
                string SocialNetwork_temp = string.Empty;
                string SocialNetwork = string.Empty;

                var DealerLocationName = string.Empty;
                string oldDealerLocationID = string.Empty;

                foreach (string id in objDealerLocationSocialNetworkPost.socialNetworkIDs)
                {
                    if (!string.IsNullOrEmpty(id))
                    {
                        int NewID = Convert.ToInt32(id);

                        //int SocialNetworkID = objSocialDealerEntities.DealerLocationSocialNetworks.Where(d => d.ID == Convert.ToInt32(id)).Select(o => o.SocialNetworkID).SingleOrDefault();
                        var SocialNetworkID = (from l in objSocialDealerEntities.DealerLocationSocialNetworks
                                               where l.ID == NewID
                                               select l.SocialNetworkID).SingleOrDefault();

                        var DealerLocationID = (from l in objSocialDealerEntities.DealerLocationSocialNetworks
                                                where l.ID == NewID
                                                select l.LocationID).SingleOrDefault();

                        if (string.IsNullOrEmpty(DealerLocationName))
                        {
                            DealerLocationName = (from l in objSocialDealerEntities.DealerLocations
                                                  where l.ID == DealerLocationID
                                                  select l.Name).SingleOrDefault();

                            messageTemplateMain = messageTemplateMain.Replace("[$DealerLocationName$]", DealerLocationName);
                        }

                        SocialNetwork_temp = SocialNetworkblock;

                        //Facebook
                        if (SocialNetworkID == 2)
                        {
                            SocialNetwork_temp = SocialNetwork_temp.Replace("[$SocialNetworkImgURL$]", "http://dealers.socialdealer.com/Images/socialnetworks/facebook-20.gif");
                            SocialNetwork_temp = SocialNetwork_temp.Replace("[$SocialNetworkName$]", "Facebook");
                        }

                        //Twitter
                        if (SocialNetworkID == 3)
                        {
                            SocialNetwork_temp = SocialNetwork_temp.Replace("[$SocialNetworkImgURL$]", "http://dealers.socialdealer.com/Images/socialnetworks/twitter-20.gif");
                            SocialNetwork_temp = SocialNetwork_temp.Replace("[$SocialNetworkName$]", "Twitter");
                        }

                        //Google+
                        if (SocialNetworkID == 29)
                        {
                            SocialNetwork_temp = SocialNetwork_temp.Replace("[$SocialNetworkImgURL$]", "http://dealers.socialdealer.com/Images/socialnetworks/googleplus-20.png");
                            SocialNetwork_temp = SocialNetwork_temp.Replace("[$SocialNetworkName$]", "GooglePlus");
                        }

                        SocialNetwork = SocialNetwork + " " + SocialNetwork_temp;
                    }
                }

                messageTemplateMain = messageTemplateMain.Replace("[$SocialNetwork$]", SocialNetwork);
                messageTemplateMain = messageTemplateMain.Replace("[$ScheduleDate$]", objDealerLocationSocialNetworkPost.scheduleDate.ToString());
                messageTemplateMain = messageTemplateMain.Replace("[$UserName$]", UserName);

                result = messageTemplateMain;
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                #endregion
            }
            
            return result;
        }

        private static List<spGetTemplates_Result1> GetTemplates()
        {
            var results = new List<spGetTemplates_Result1>();

            try
            {
                results = sdNotificationEntities.spGetTemplates().ToList();
            }
            catch (EntitySqlException ex)
            {
                #region ErrorHandler
                
                #endregion
            }
            catch (EntityCommandExecutionException ex)
            {
                #region ErrorHandler
                #endregion
            }
            catch (System.Data.EntityException ex)
            {
                #region ErrorHandler
                #endregion
            }
            catch (System.InvalidOperationException ex)
            {
                #region ErrorHandler
                #endregion
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                #endregion
            }

            return results;
        }

        
        public static void PostToSocialNetworks(spGetDealerLocationSocialNetworkPostDetails_New_Result obj)
        {
            PublishManager objPublishManager = new PublishManager();
            try
            {
                switch (obj.SocialNetworkID)
                {
                       
                    case 2:
                        {
                            #region FACEBOOK
                            
                            try
                            {
                                PostToFacebook(obj);
                            }
                            catch (Exception ex)
                            {
                                #region ERROR HANDLING

                                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                                ObjPublishExceptionLog.PostResponse = string.Empty;
                                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToSocialNetworks\\FACEBOOK";
                                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                #endregion
                            }

                            break;
                            
                            #endregion
                        }
                    case 3:
                        {
                            #region TWITTER

                            try
                            {
                                string _Errormsg  = string.Empty;
                                string _detailedError = string.Empty;

                                FacebookObjectIDByResultID objFacebookObjectIDByResultID = new FacebookObjectIDByResultID();
                                spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result spgetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result = new spGetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result();
                                string object_id = string.Empty;
                                if (obj.ParentDealerLocationSocialNetworkPostQueueId != 0 && obj.ParentDealerLocationSocialNetworkPostQueueId != null)
                                {
                                    string ResultID = string.Empty;
                                    string Token = string.Empty;
                                    spgetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result = objPublishManager.GetDealerLocationSocialNetworkPostDetailsByPostQueueID(Convert.ToInt64(obj.ParentDealerLocationSocialNetworkPostQueueId));
                                    Token = spgetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result.Token;
                                    ResultID = spgetDealerLocationSocialNetworkPostDetailsByPostQueueID_Result.ResultID;
                                    if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookQuestion))
                                    {
                                        if (!string.IsNullOrEmpty(ResultID))
                                        {

                                            if (ResultID.Contains('_'))
                                            {
                                                objFacebookObjectIDByResultID = FaceBook.GetFacebookObjectIDbyResultID(ResultID, Token);
                                                object_id = objFacebookObjectIDByResultID.object_id;
                                            }
                                            else
                                            {
                                                object_id = ResultID;
                                            }

                                            if (!string.IsNullOrEmpty(object_id))
                                            {
                                                obj.Comment = obj.Title + "  ";
                                                obj.Link = "http://www.facebook.com/questions/" + object_id + "/";

                                                PostToTwitter(obj.Comment, obj.Link, obj.Token, obj.TokenSecret, obj.AppKey, obj.AppSecret, obj.PostID, obj.PostQueueID,Convert.ToInt32(obj.ReTryNumber),Convert.ToDateTime(obj.PostExpirationDate),Convert.ToBoolean(obj.isScheduledPost));
                                                //PostToTwitter(obj.Comment, obj.Link, "874247418-6pnvzlPmGu69Xc1PKx1m7B4Xh40urftadrWVFQxL", "9wK4wtGZCX9um6Gd09S4ujaY0VpFgrrhLddZOUPmOos", obj.PostID, obj.PostQueueID);
                                            }
                                        }
                                        else
                                        {
                                            _Errormsg = "Result ID is NULL (Can not get Post URL from Facebook.)";
                                            _detailedError = _Errormsg + "ParentDealerLocationSocialNetworkPostQueueId : " + obj.ParentDealerLocationSocialNetworkPostQueueId + ", DealerLocationSocialNetworkPostQueueId : " + obj.PostQueueID;

                                            #region ERROR HANDLING
                                           
                                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                            ObjPublishExceptionLog.ExceptionMessage = _Errormsg;
                                            ObjPublishExceptionLog.InnerException = string.Empty;
                                            ObjPublishExceptionLog.PostResponse = string.Empty;
                                            ObjPublishExceptionLog.StackTrace = string.Empty;
                                            ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToSocialNetworks\\TWITTER";
                                            ObjPublishExceptionLog.ExtraInfo = "ParentDealerLocationSocialNetworkPostQueueId : " + obj.ParentDealerLocationSocialNetworkPostQueueId + ", DealerLocationSocialNetworkPostQueueId : " + obj.PostQueueID;
                                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                            #endregion

                                            objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(Convert.ToInt64(obj.PostQueueID), _Errormsg, string.Empty, _detailedError, false);
                                        }
                                       
                                    }
                                    else if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookEvent))
                                    {
                                        if (!string.IsNullOrEmpty(ResultID))
                                        {

                                            if (ResultID.Contains('_'))
                                            {
                                                objFacebookObjectIDByResultID = FaceBook.GetFacebookObjectIDbyResultID(ResultID, Token);
                                                object_id = objFacebookObjectIDByResultID.object_id;
                                            }
                                            else
                                            {
                                                object_id = ResultID;
                                            }

                                            if (!string.IsNullOrEmpty(object_id))
                                            {
                                                obj.Comment = obj.Title + "  ";
                                                obj.Link = "http://www.facebook.com/events/" + object_id + "/";

                                                PostToTwitter(obj.Comment, obj.Link, obj.Token, obj.TokenSecret, obj.AppKey, obj.AppSecret, obj.PostID, obj.PostQueueID, Convert.ToInt32(obj.ReTryNumber), Convert.ToDateTime(obj.PostExpirationDate), Convert.ToBoolean(obj.isScheduledPost));                                                
                                                //PostToTwitter(obj.Comment, obj.Link, "874247418-6pnvzlPmGu69Xc1PKx1m7B4Xh40urftadrWVFQxL", "9wK4wtGZCX9um6Gd09S4ujaY0VpFgrrhLddZOUPmOos", obj.PostID, obj.PostQueueID);
                                            }
                                        }
                                        else
                                        {
                                            _Errormsg = "Result ID is NULL (Can not get Post URL from Facebook.)";
                                            _detailedError = _Errormsg + "ParentDealerLocationSocialNetworkPostQueueId : " + obj.ParentDealerLocationSocialNetworkPostQueueId + ", DealerLocationSocialNetworkPostQueueId : " + obj.PostQueueID;


                                            #region ERROR HANDLING
                                            
                                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                            ObjPublishExceptionLog.ExceptionMessage = "Result ID is NULL (Can not get Post URL from Facebook.)";
                                            ObjPublishExceptionLog.InnerException = string.Empty;
                                            ObjPublishExceptionLog.PostResponse = string.Empty;
                                            ObjPublishExceptionLog.StackTrace = string.Empty;
                                            ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToSocialNetworks\\TWITTER";
                                            ObjPublishExceptionLog.ExtraInfo = "ParentDealerLocationSocialNetworkPostQueueId : " + obj.ParentDealerLocationSocialNetworkPostQueueId + ", DealerLocationSocialNetworkPostQueueId : " + obj.PostQueueID;
                                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                            #endregion

                                            objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(Convert.ToInt64(obj.PostQueueID), _Errormsg, string.Empty, _detailedError, false);
                                        }
                                    }
                                    else
                                    {
                                        PostToTwitter(obj.Comment, obj.Link, obj.Token, obj.TokenSecret, obj.AppKey, obj.AppSecret, obj.PostID, obj.PostQueueID, Convert.ToInt32(obj.ReTryNumber), Convert.ToDateTime(obj.PostExpirationDate), Convert.ToBoolean(obj.isScheduledPost));
                                    }
                                }
                                else
                                {
                                    var linkUrl = string.Empty;
                                    var comment = string.Empty;

                                    if (!string.IsNullOrEmpty(obj.Link))
                                    {                                        
                                        linkUrl = " " + MakeTinyUrl(obj.Link);                                     
                                    }
                                    else if (!string.IsNullOrEmpty(obj.Image))
                                    {                                        
                                        linkUrl = " " + MakeTinyUrl(obj.Image);                                        
                                    }

                                    #region LOG REQUEST

                                    PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                    ObjPublishExceptionLog.ExceptionMessage = obj.Comment + " , " + linkUrl + " , " + obj.Token + " , " + obj.TokenSecret + " , " + obj.AppKey + " , " + obj.AppSecret + " , " + Convert.ToString(obj.PostID) + " , " + Convert.ToString(obj.PostQueueID) + " , " + Convert.ToString(obj.ReTryNumber) + " , " + Convert.ToString(Convert.ToDateTime(obj.PostExpirationDate));
                                    ObjPublishExceptionLog.InnerException = string.Empty;
                                    ObjPublishExceptionLog.PostResponse = string.Empty;
                                    ObjPublishExceptionLog.StackTrace = string.Empty;
                                    ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToSocialNetworks\\TWITTER";
                                    ObjPublishExceptionLog.ExtraInfo = string.Empty;
                                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                    #endregion

                                    PostToTwitter(obj.Comment, linkUrl, obj.Token, obj.TokenSecret, obj.AppKey, obj.AppSecret, obj.PostID, obj.PostQueueID, Convert.ToInt32(obj.ReTryNumber), Convert.ToDateTime(obj.PostExpirationDate), Convert.ToBoolean(obj.isScheduledPost));
                                }
                                
                            }
                            catch (Exception ex)
                            {
                                #region ERROR HANDLING

                                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                                ObjPublishExceptionLog.PostResponse = string.Empty;
                                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToSocialNetworks\\TWITTER";
                                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                #endregion
                            }

                            break;

                            #endregion
                        }
                    case 29:
                        {
                            #region GOOGLEPLUS
                            try
                            {
                                PostToGooglePlus(obj);
                            }
                            catch (Exception ex)
                            {
                                #region ERROR HANDLING

                                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                                ObjPublishExceptionLog.PostResponse = string.Empty;
                                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToSocialNetworks\\GooglePlus";
                                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                #endregion
                            }
                            break;
                            #endregion
                        }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToSocialNetworks";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }
        
        private static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 30)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + ex.StackTrace);
            }
        }

        //private static string MakeTinyUrl(string url, Guid userid, long postid, int clickTrackType, int dealerlocationid)
        //{
        //    PublishManager objPublishManager = new PublishManager();
        //    string text = string.Empty;

        //    try
        //    {
        //        //if (!url.ToLower().StartsWith("https") && !url.ToLower().StartsWith("http") && !url.ToLower().StartsWith("ftp"))
        //        //{
        //        //    url = "http://" + url;
        //        //}
        //        //url = HttpUtility.UrlEncode(url) + "&objectid=" + postid.ToString() + "&urlTracktype=" + clickTrackType.ToString() + "&userid=" + userid.ToString() + "&dealerlocationid=" + dealerlocationid.ToString();

        //        //text = Utility.MakesdTinyUrl(url);
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(postid);
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        ObjPublishExceptionLog.PostResponse = string.Empty;
        //        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //        ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\MakeTinyUrl";
        //        ObjPublishExceptionLog.ExtraInfo = "URL : " + Convert.ToString(url) + " , USERID : " + Convert.ToString(userid) + " , POSTID : " + Convert.ToString(postid) + " , DEALERLOCATIONID : " + Convert.ToString(dealerlocationid);
        //        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

        //        #endregion
        //    }
        //    //return text;
        //    return url;
        //}

        public static string GetGooglePlusAccessTokenbyRefreshToken(string client_id, string client_secret, string refresh_token, string grant_type)
        {
            PublishManager objPublishManager = new PublishManager();
            string access_token = string.Empty;
            try
            {                
                int MaxRetrytogetAccessToken = 3;
                GooglePlusAccessTokenRequestResponse objGooglePlusAccessTokenRequestResponse = new GooglePlusAccessTokenRequestResponse();
                for (int iCounter = 0; iCounter <= MaxRetrytogetAccessToken; iCounter++)
                {
                    objGooglePlusAccessTokenRequestResponse = GooglePlus.GetGooglePlusAccessToken(client_id, client_secret, refresh_token, grant_type);

                    if (objGooglePlusAccessTokenRequestResponse != null)
                    {
                        if (!string.IsNullOrEmpty(objGooglePlusAccessTokenRequestResponse.access_token))
                        {
                            access_token = objGooglePlusAccessTokenRequestResponse.access_token;
                            break;
                        }
                        else
                        {
                            // Log Retry to get Access Token
                            #region LOG RETRY TO GET ACCESS TOKEN

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            ObjPublishExceptionLog.ExceptionMessage = "Retry Number : " + iCounter + " , Retry to get Access Token from RefreshToken";
                            ObjPublishExceptionLog.InnerException = string.Empty;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = string.Empty;
                            ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\GetGooglePlusAccessTokenbyRefreshToken//forLoop";
                            ObjPublishExceptionLog.ExtraInfo = "client_id : " + client_id + " ,client_secret : " + client_secret + " ,refresh_token : " + refresh_token + " ,grant_type : " + grant_type;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            #endregion
                        }

                    }
                    else
                    {
                        #region LOG RETRY TO GET ACCESS TOKEN

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = "Retry Number : " + iCounter + " , Retry to get Access Token from RefreshToken and Response from GetGooglePlusAccessToken Method is null";
                        ObjPublishExceptionLog.InnerException = string.Empty;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = string.Empty;
                        ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\GetGooglePlusAccessTokenbyRefreshToken//forLoop";
                        ObjPublishExceptionLog.ExtraInfo = "client_id : " + client_id + " ,client_secret : " + client_secret + " ,refresh_token : " + refresh_token + " ,grant_type : " + grant_type;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\GetGooglePlusAccessTokenbyRefreshToken";
                ObjPublishExceptionLog.ExtraInfo = "client_id : " + client_id + " ,client_secret : " + client_secret + " ,refresh_token : " + refresh_token + " ,grant_type : " + grant_type;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return access_token;
        }

        public static void PostToGooglePlus(spGetDealerLocationSocialNetworkPostDetails_New_Result obj)
        {
            PublishManager objPublishManager = new PublishManager();            
            string returnedMsg = string.Empty;
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            try
            {                
                if (obj.DealerLocationSocialNetworkID != 0)
                {
                    #region GET ACCESS TOKEN
                    
                    // Get Access Token
                    string grant_type = "refresh_token";
                    string access_token = string.Empty;

                    access_token = GetGooglePlusAccessTokenbyRefreshToken(obj.AppKey, obj.AppSecret, obj.Token, grant_type);
                    #endregion

                    if (!string.IsNullOrEmpty(access_token))
                    {
                        string accessType = "public";
                        string activityType = string.Empty;
                        string attachmentID = string.Empty;
                        string circleID = string.Empty;
                        string userID = string.Empty;
                        string objectType = string.Empty;

                        if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookStatus))
                        {   
                            objGooglePlusPostResponse = GooglePlus.AddGooglePlusActivity(access_token, obj.UniqueID, accessType, objectType, activityType, obj.Comment, attachmentID, obj.Link, circleID, userID);
                        }
                        if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookLink))
                        {
                            objectType = "article";
                            objGooglePlusPostResponse = GooglePlus.AddGooglePlusActivity(access_token, obj.UniqueID, accessType, objectType, activityType, obj.Comment, attachmentID, obj.Link, circleID, userID);
                        }
                        if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookPhoto))
                        {                            
                            objGooglePlusPostResponse = GooglePlus.PostGooglePlusPhoto(access_token, obj.UniqueID, accessType, activityType, obj.Comment, obj.Link, obj.Image, circleID, userID);
                        }
                    }
                    else
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                        ObjPublishExceptionLog.ExceptionMessage = "Not Able to Get Access Token for Google Plus";
                        ObjPublishExceptionLog.InnerException = string.Empty;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = string.Empty;
                        ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToGooglePlus";
                        ObjPublishExceptionLog.ExtraInfo = "refresh_token : " + obj.Token;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                    if (objGooglePlusPostResponse.IsSuccess)
                    {
                        //Convert.ToString(objPostRequestResult.Finalresponse)
                        objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, objGooglePlusPostResponse.error.ToString(), Convert.ToString(objGooglePlusPostResponse.postURL), objGooglePlusPostResponse.error.ToString(), false);
                    }
                    else // Log the Error and Retry
                    {

                    }
                }

                

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToGooglePlus";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

        public static void PostToFacebook(spGetDealerLocationSocialNetworkPostDetails_New_Result obj)
        {
            PublishManager objPublishManager = new PublishManager();            
            string returnedMsg = string.Empty;
            FacebookResponse ObjFacebookResponse = new FacebookResponse();
            PublishExceptionLog ObjPublishExceptionLog;
            try
            {                
                obj.Link = System.Web.HttpUtility.UrlEncode(obj.Link);
                obj.FBAlbumDescription = System.Web.HttpUtility.UrlEncode(obj.FBAlbumDescription);
                obj.FBAlbumName = System.Web.HttpUtility.UrlEncode(obj.FBAlbumName);

                if (obj.DealerLocationSocialNetworkID != 0)
                {
                    //var comment = obj.Comment;
                    string link = string.Empty;
                    //string image = string.Empty;
                    int targetDealerLocationId;
                    //comment = comment.Substring(0, facebookMaxLength > comment.Length ? comment.Length : facebookMaxLength);

                    DealerLocationSocialNetwork dlSocialNetwork = objPublishManager.GetDealerLocationSocialNetwork(Convert.ToInt32(obj.DealerLocationSocialNetworkID));

                    targetDealerLocationId = Convert.ToInt32(obj.TargetDealerLocationID);

                    if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookPhoto) || obj.PostTypeId == Convert.ToInt32(PostType.FacebookMultiplePhoto))
                    {
                                                  
                        if (dlSocialNetwork != null)
                        {
                            if (!string.IsNullOrEmpty(obj.FBAlbumName) && string.IsNullOrEmpty(obj.FBAlbumId))
                            {
                                if (string.IsNullOrEmpty(_FBAlbumId))
                                {
                                    //Create Album
                                    ObjFacebookResponse = FaceBook.CreateFacebookAlbum(obj.UniqueID, obj.Token, obj.FBAlbumName, obj.FBAlbumDescription);
                                    _FBAlbumId = ObjFacebookResponse.ID;
                                    objPublishManager.UpdateDealerLocationSocialNetworkPostDetails(obj.PostID, _FBAlbumId);
                                }
                               
                                if (!string.IsNullOrEmpty(_FBAlbumId))
                                {
                                    obj.FBAlbumId = _FBAlbumId;
                                    ObjFacebookResponse = FaceBook.UploadFacebookPhoto(dlSocialNetwork.Token, obj.Comment, obj.Image, obj.FBAlbumId, dlSocialNetwork, obj);                                    
                                }
                                else
                                {
                                    #region ERROR HANDLING

                                    ObjPublishExceptionLog = new PublishExceptionLog();
                                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                    ObjPublishExceptionLog.ExceptionMessage = ObjFacebookResponse.message;
                                    ObjPublishExceptionLog.InnerException = string.Empty;
                                    ObjPublishExceptionLog.PostResponse = ObjFacebookResponse.ToString();
                                    ObjPublishExceptionLog.StackTrace = "";
                                    ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToFacebook\\Creating New Album";
                                    ObjPublishExceptionLog.ExtraInfo = ObjFacebookResponse.ToString();
                                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                    #endregion
                                }
                            }
                            else
                            {
                                ObjFacebookResponse = FaceBook.UploadFacebookPhoto(dlSocialNetwork.Token, obj.Comment, obj.Image, obj.FBAlbumId, dlSocialNetwork, obj);
                            }
                        }                        
                            //else
                            //{
                            //    link = MakeTinyUrl(obj.Image, obj.UserID, obj.PostID, (int)URlTrackType.FacebookPublish, targetDealerLocationId);
                            //    obj.Image = " " + MakeTinyUrl(obj.Image, obj.UserID, obj.PostID, (int)URlTrackType.FacebookPublish, targetDealerLocationId);
                            //}
                    }

                    if (String.IsNullOrEmpty(ObjFacebookResponse.message))
                    {
                        var targeting = FaceBook.GetTargetingInfo(obj);


                        if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookStatus))
                        {
                            //if (!obj.IsUploadedImage)
                            //{
                            //    link = MakeTinyUrl(obj.Image, obj.UserID, obj.PostID, (int)URlTrackType.FacebookPublish, targetDealerLocationId);                                
                            //}
                            obj.Comment = System.Web.HttpUtility.UrlEncode(obj.Comment);
                            obj.Title = System.Web.HttpUtility.UrlEncode(obj.Title);

                            ObjFacebookResponse = FaceBook.CreateFacebookWallPost(obj.Comment, DateTime.UtcNow.ToString(), null, obj.Title, obj.Image, link, Convert.ToInt32(obj.DealerLocationSocialNetworkID), obj.VideoSource, targeting, dlSocialNetwork, obj);
                        }
                        else if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookEvent))
                        {

                            obj.EventName = System.Web.HttpUtility.UrlEncode(obj.EventName);
                            obj.Description = System.Web.HttpUtility.UrlEncode(obj.Description);
                            obj.FBLocation = System.Web.HttpUtility.UrlEncode(obj.FBLocation);

                            ObjFacebookResponse = FaceBook.CreateFacebookEvent(obj.UniqueID, obj.Token, obj.EventName, Convert.ToString(obj.EventStartTime), Convert.ToString(obj.EventEndTime), obj.FBLocation, obj.Description, obj.Privacy);

                        }
                        else if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookQuestion))
                        {
                            obj.FBQuestion = System.Web.HttpUtility.UrlEncode(obj.FBQuestion);

                            ObjFacebookResponse = FaceBook.CreateFacebookQuestion(obj.UniqueID, obj.Token, obj.FBQuestion, obj.FBOptions, Convert.ToString(obj.FBAllowNewOptions));
                        }
                        else if (obj.PostTypeId == Convert.ToInt32(PostType.FacebookLink))
                        {
                            link = obj.Link;
                            //MakeTinyUrl(obj.Link, obj.UserID, obj.PostID, (int)URlTrackType.FacebookPublish, targetDealerLocationId);
                            //if (!string.IsNullOrEmpty(obj.Image))
                            //{
                            //    obj.Image = " " + MakeTinyUrl(obj.Image, obj.UserID, obj.PostID, (int)URlTrackType.FacebookPublish, targetDealerLocationId);
                            //}
                            obj.Comment = System.Web.HttpUtility.UrlEncode(obj.Comment);
                            obj.Title = System.Web.HttpUtility.UrlEncode(obj.Title);

                            ObjFacebookResponse = FaceBook.CreateFacebookWallPost(obj.Comment, DateTime.UtcNow.ToString(), null, obj.Title, obj.Image, link, Convert.ToInt32(obj.DealerLocationSocialNetworkID), obj.VideoSource, targeting, dlSocialNetwork, obj);
                        }

                        if (!string.IsNullOrEmpty(ObjFacebookResponse.ID))
                        {
                            objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, ObjFacebookResponse.message, ObjFacebookResponse.ID, string.Empty, false);
                        }
                        else // Log the Error 
                        {

                            string Facebook_Error_Message = ObjFacebookResponse.message;
                            string Facebook_Error_Code = ObjFacebookResponse.code;

                            #region RETRY

                            //Retry
                            bool sendforRetry = true;

                            DataSet ds = new DataSet();
                            try
                            {
                                ds.ReadXml(FacebookErrorsXMLFilePath);
                            }
                            catch (Exception ex)
                            {
                                #region ERROR HANDLING

                                ObjPublishExceptionLog = new PublishExceptionLog();
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                                ObjPublishExceptionLog.InnerException = string.Empty;
                                ObjPublishExceptionLog.PostResponse = ObjFacebookResponse.ToString();
                                ObjPublishExceptionLog.StackTrace = "";
                                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToFacebook\\Reading Facebook Errors XML File";
                                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                #endregion
                            }

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                // If Error Message from List then Do not Retry 
                                // Else Retry (Send To MSMQ)
                                if (Facebook_Error_Code.ToLower().Contains(Convert.ToString(row["code"])))
                                {
                                    sendforRetry = false;
                                    break;
                                }
                            }
                            try
                            {
                                if (string.IsNullOrEmpty(Convert.ToString(obj.PostExpirationDate)))
                                {
                                    obj.PostExpirationDate = DateTime.Now;
                                }
                            }
                            catch (Exception ex)
                            {
                                obj.PostExpirationDate = DateTime.Now;
                            }

                            if (sendforRetry)
                            {
                                //if (obj.PostExpirationDate >= DateTime.Now)
                                {
                                    if (obj.ReTryNumber < 5)
                                    {
                                        PublishMSMQ objPublishMSMQ = new PublishMSMQ();

                                        objPublishMSMQ.DealerLocationSocialNetworkPostID = "-1";
                                        objPublishMSMQ.DealerLocationSocialNetworkPostQueueID = Convert.ToString(obj.PostQueueID);

                                        objPublishMSMQ.isRetry = true;
                                        objPublishMSMQ.LastExecution = DateTime.Now;
                                        objPublishMSMQ.LastExecutionResponse = "Error:" + Facebook_Error_Message + "| Code:" + Facebook_Error_Code;

                                        objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, ObjFacebookResponse.message, ObjFacebookResponse.ID, ObjFacebookResponse.ToString(), true);

                                        string XMLMessage = Utility.SerializeObject<PublishMSMQ>(objPublishMSMQ);

                                        if (!string.IsNullOrEmpty(XMLMessage))
                                        {
                                            Utility.SendMessageToQueue(XMLMessage, QueueName);
                                        }
                                    }
                                    else
                                    {
                                        ObjFacebookResponse.message = "End of Retry due to Maximum Retry Limit.  " + ObjFacebookResponse.message;
                                        objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, ObjFacebookResponse.message, ObjFacebookResponse.ID, ObjFacebookResponse.ToString(), false);
                                    }

                                }
                                //else
                                //{
                                //    ObjFacebookResponse.message = "End of Retry due to Post Expiration Date for Retry Limit.  " + ObjFacebookResponse.message;
                                //    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, ObjFacebookResponse.message, ObjFacebookResponse.ID, ObjFacebookResponse.ToString(), false);
                                //}
                            }
                            else
                            {
                                ObjFacebookResponse.message = "Will not Retry beacuse Facebook Response Error is in the List of Error Not to Retry.  " + ObjFacebookResponse.message;
                                objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, ObjFacebookResponse.message, ObjFacebookResponse.ID, ObjFacebookResponse.ToString(), false);
                            }

                            #endregion

                            #region ERROR HANDLING

                            ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                            ObjPublishExceptionLog.ExceptionMessage = ObjFacebookResponse.message;
                            ObjPublishExceptionLog.InnerException = string.Empty;
                            ObjPublishExceptionLog.PostResponse = ObjFacebookResponse.ToString();
                            ObjPublishExceptionLog.StackTrace = "";
                            ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToFacebook\\Facebook";
                            ObjPublishExceptionLog.ExtraInfo = ObjFacebookResponse.ToString();
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            #endregion
                        }
                    }
                    else
                    {
                        // There was a facebook Error
                        objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, "FACEBOOK EXCEPTION : " + ObjFacebookResponse.message, string.Empty, ObjFacebookResponse.message, false);
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(obj.PostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(obj.PostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToFacebook";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion

                if (obj.ReTryNumber < 5)
                {
                    PublishMSMQ objPublishMSMQ = new PublishMSMQ();
                    objPublishMSMQ.DealerLocationSocialNetworkPostID = "-1";
                    objPublishMSMQ.DealerLocationSocialNetworkPostQueueID = Convert.ToString(obj.PostQueueID);

                    objPublishMSMQ.isRetry = true;
                    objPublishMSMQ.LastExecution = DateTime.Now;
                    //objPublishMSMQ.LastExecutionResponse = "Error:" + Facebook_Error_Message + "| Code:" + Facebook_Error_Code;

                    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, "RETRY FACEBOOK EXCEPTION : " + ex.Message, string.Empty, ex.StackTrace, true);

                    string XMLMessage = Utility.SerializeObject<PublishMSMQ>(objPublishMSMQ);

                    if (!string.IsNullOrEmpty(XMLMessage))
                    {
                        Utility.SendMessageToQueue(XMLMessage, QueueName);
                    }
                }
                else
                {
                    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(obj.PostQueueID, "FACEBOOK EXCEPTION : " + ex.Message, string.Empty, ex.StackTrace, false);
                }
                
            }
        }

        public static void ProcessPosts(PublishMSMQ objPublishMSMQ)
        {
            PublishManager objPublishManager = new PublishManager();
            try
            {
                if (!string.IsNullOrEmpty(objPublishMSMQ.DealerLocationSocialNetworkPostID) || !string.IsNullOrEmpty(objPublishMSMQ.DealerLocationSocialNetworkPostQueueID))
                {

                    List<spGetDealerLocationSocialNetworkPostDetails_New_Result> listspGetDealerLocationSocialNetworkPostDetails_New_Result = objPublishManager.GetDealerLocationSocialNetworkPostDetails(objPublishMSMQ.DealerLocationSocialNetworkPostID,Convert.ToInt64(objPublishMSMQ.DealerLocationSocialNetworkPostQueueID), DateTime.Now, -1, 0);

                    //List<spGetDealerLocationSocialNetworkPostDetails_New_Result> listspGetDealerLocationSocialNetworkPostDetails_New_Result_Temp = listspGetDealerLocationSocialNetworkPostDetails_New_Result.Where(o => string.IsNullOrEmpty(Convert.ToString(o.ParentDealerLocationSocialNetworkPostQueueId))).ToList();
                    _FBAlbumId = string.Empty;
                    foreach (var spGetDealerLocationSocialNetworkPostDetails_New_Result in listspGetDealerLocationSocialNetworkPostDetails_New_Result)
                    {
                        try
                        {
                            PostToSocialNetworks(spGetDealerLocationSocialNetworkPostDetails_New_Result);
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(spGetDealerLocationSocialNetworkPostDetails_New_Result.PostID);
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetDealerLocationSocialNetworkPostDetails_New_Result.PostQueueID);
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\ProcessPosts\\foreach Loop";
                            ObjPublishExceptionLog.ExtraInfo = "DealerLocationSocialNetworkPostIDs : " + objPublishMSMQ.DealerLocationSocialNetworkPostID;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            #endregion
                        }
                    }
                }

                //listspGetDealerLocationSocialNetworkPostDetails_New_Result = objPublishManager.GetDealerLocationSocialNetworkPostDetails(objPublishMSMQ.DealerLocationSocialNetworkPostID, DateTime.Now, -1, 0);

                //foreach (var spGetDealerLocationSocialNetworkPostDetails_New_Result in listspGetDealerLocationSocialNetworkPostDetails_New_Result)
                //{
                //    try
                //    {
                //        PostToSocialNetworks(spGetDealerLocationSocialNetworkPostDetails_New_Result);
                //    }
                //    catch (Exception ex)
                //    {
                //        #region ERROR HANDLING

                //        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(spGetDealerLocationSocialNetworkPostDetails_New_Result.PostID);
                //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetDealerLocationSocialNetworkPostDetails_New_Result.PostQueueID);
                //        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                //        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                //        ObjPublishExceptionLog.PostResponse = string.Empty;
                //        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                //        ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\ProcessPosts\\foreach Loop";
                //        ObjPublishExceptionLog.ExtraInfo = "DealerLocationSocialNetworkPostIDs : " + objPublishMSMQ.DealerLocationSocialNetworkPostID;
                //        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                //        #endregion
                //    }
                //}
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\ProcessPosts";
                ObjPublishExceptionLog.ExtraInfo = "DealerLocationSocialNetworkPostIDs : " + objPublishMSMQ.DealerLocationSocialNetworkPostID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

        public static void PostToTwitter(string status, string link, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, long DealerLocationSocialNetworkPostID, long DealerLocationSocialNetworkPostQueueID, int ReTryNumber, DateTime PostExpirationDate, bool isScheduledPost)
        {
            PublishManager objPublishManager = new PublishManager();            
            string returnedMsg = string.Empty;            
            PublishExceptionLog ObjPublishExceptionLog;
            TwitterResponse objTwitterResponse = new TwitterResponse();
            TwitterResponse objTwitterResponseLinq = new TwitterResponse();
            try
            {
                objTwitterResponse = Twitter.TwitterStatusUpdate2(status, link, oauth_token, oauth_token_secret,oauth_consumer_key,oauth_consumer_secret, DealerLocationSocialNetworkPostID, DealerLocationSocialNetworkPostQueueID);

                if (!string.IsNullOrEmpty(objTwitterResponse.ID))
                {
                    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, string.Empty, objTwitterResponse.ID, string.Empty, false);
                }
                else // Log the Error 
                {
                    //ID :  , Message : , Type :  , Code : , error : Could not authenticate with OAuth., request : /1/statuses/update.json
                    string Twitter_Error_Message = objTwitterResponse.error;
                    string Twitter_Error_Code = objTwitterResponse.code;

                    #region RETRY

                    //Retry
                    bool sendforRetry = false;


                        // If Error Message from List then Do not Retry 
                        // Else Retry (Send To MSMQ)
                    if (Twitter_Error_Message.ToLower().Contains(Convert.ToString("could not authenticate with oauth")))
                    {
                        //Log the Retry with LinqToTwitter

                        //objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, objTwitterResponse.message + " Retry with LinqToTwitter Method." , objTwitterResponse.ID, objTwitterResponse.ToString(), true);

                        //objTwitterResponseLinq = PostToTwitter_LinqToTwitter(status, link, oauth_token, oauth_token_secret, oauth_consumer_key, oauth_consumer_secret, DealerLocationSocialNetworkPostID, DealerLocationSocialNetworkPostQueueID, ReTryNumber, PostExpirationDate);

                        //if (!string.IsNullOrEmpty(objTwitterResponseLinq.ID))
                        //{
                        //    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, string.Empty, objTwitterResponseLinq.ID, "LinqToTwitter", false); 
                        //    sendforRetry = false;                            
                        //}
                        //else
                        //{
                        //    sendforRetry = true;
                        //    Twitter_Error_Message = Twitter_Error_Message + objTwitterResponseLinq.error;
                        //}

                        sendforRetry = true;
                    }

                    try
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(PostExpirationDate)))
                        {
                            PostExpirationDate = DateTime.Now;
                        }
                    }
                    catch (Exception ex)
                    {
                        PostExpirationDate = DateTime.Now;
                    }


                    if (sendforRetry)
                    {
                        //if (PostExpirationDate >= DateTime.Now)                        
                        {
                            if (ReTryNumber < 5)
                            {                                

                                PublishMSMQ objPublishMSMQ = new PublishMSMQ();
                                
                                objPublishMSMQ.DealerLocationSocialNetworkPostID = "-1";
                                objPublishMSMQ.DealerLocationSocialNetworkPostQueueID = Convert.ToString(DealerLocationSocialNetworkPostQueueID);
                               
                                objPublishMSMQ.isRetry = true;
                                objPublishMSMQ.LastExecution = DateTime.Now;
                                objPublishMSMQ.LastExecutionResponse = "Error:" + Twitter_Error_Message + "| Code:" + Twitter_Error_Code;

                                objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, objTwitterResponse.message, objTwitterResponse.ID, objTwitterResponse.ToString(), true);

                                string XMLMessage = Utility.SerializeObject<PublishMSMQ>(objPublishMSMQ);

                                if (!string.IsNullOrEmpty(XMLMessage))
                                {
                                    Utility.SendMessageToQueue(XMLMessage, QueueName);
                                }
                            }
                            else
                            {
                                objTwitterResponse.message = "End of Retry due to Maximum Retry Limit.  " + " ,Message : " + objTwitterResponse.message + " ,Error : " + objTwitterResponse.error;
                                objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, objTwitterResponse.message, objTwitterResponse.ID, objTwitterResponse.ToString(), false);
                            }

                        }
                        //else
                        //{
                        //    objTwitterResponse.message = "End of Retry due to Post Expiration Date for Retry Limit.  " + " ,Message : " + objTwitterResponse.message + " ,Error : " + objTwitterResponse.error;
                        //    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, objTwitterResponse.message, objTwitterResponse.ID, objTwitterResponse.ToString(), false);
                        //}
                    }
                    else
                    {
                        objTwitterResponse.message = "Will not Retry beacuse Twitter Response Error is in the List of Error Not to Retry.  " + " ,Message : " + objTwitterResponse.message + " ,Error : " + objTwitterResponse.error;
                        objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, objTwitterResponse.message, objTwitterResponse.ID, objTwitterResponse.ToString(), false);
                    }

                    #endregion


                    #region ERROR HANDLING

                    ObjPublishExceptionLog = new PublishExceptionLog();
                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                    ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);
                    ObjPublishExceptionLog.ExceptionMessage = objTwitterResponse.message;
                    ObjPublishExceptionLog.InnerException = string.Empty;
                    ObjPublishExceptionLog.PostResponse = objTwitterResponse.ToString();
                    ObjPublishExceptionLog.StackTrace = "";
                    ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToTwitter";
                    ObjPublishExceptionLog.ExtraInfo = objTwitterResponse.ToString();
                    objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                    #endregion


                    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, objTwitterResponse.error, objTwitterResponse.ID, objTwitterResponse.ToString(), false);
                }
            }  
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToTwitter";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion

                if (ReTryNumber < 5)
                {
                    PublishMSMQ objPublishMSMQ = new PublishMSMQ();
                    objPublishMSMQ.DealerLocationSocialNetworkPostID = "-1";
                    objPublishMSMQ.DealerLocationSocialNetworkPostQueueID = Convert.ToString(DealerLocationSocialNetworkPostQueueID);

                    objPublishMSMQ.isRetry = true;
                    objPublishMSMQ.LastExecution = DateTime.Now;
                    //objPublishMSMQ.LastExecutionResponse = "Error:" + Facebook_Error_Message + "| Code:" + Facebook_Error_Code;

                    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, "RETRY TWITTER EXCEPTION : " + ex.Message , string.Empty, ex.StackTrace, true);

                    string XMLMessage = Utility.SerializeObject<PublishMSMQ>(objPublishMSMQ);

                    if (!string.IsNullOrEmpty(XMLMessage))
                    {
                        Utility.SendMessageToQueue(XMLMessage, QueueName);
                    }
                }
                else
                {
                    objPublishManager.PostDealerLocationSocialNetworkPostQueueDetails(DealerLocationSocialNetworkPostQueueID, "TWITTER EXCEPTION : " + ex.Message, string.Empty, ex.StackTrace, false);
                }
            }
        }

        private static TwitterResponse PostToTwitter_LinqToTwitter(string status, string link, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, long DealerLocationSocialNetworkPostID, long DealerLocationSocialNetworkPostQueueID, int ReTryNumber, DateTime PostExpirationDate)
        {
            PublishManager objPublishManager = new PublishManager();
            PublishExceptionLog ObjPublishExceptionLog;
            TwitterResponse objTwitterResponse = new TwitterResponse();
            string ErrMsg = "Error!";
            string statusid = string.Empty;

            try
            {
                WebAuthorizer auth;
                TwitterContext twitterCtx;

                string twitterConsumerkey = oauth_consumer_key;
                string twitterConsumerSecret = oauth_consumer_secret;
                
                DateTime TwittrerStatusDate = DateTime.Now;
                string inReplytoScreenName = string.Empty;
                string InReplyToStatusID = string.Empty;

                

                auth = new WebAuthorizer
                {
                    Credentials = new InMemoryCredentials
                    {
                        ConsumerKey = twitterConsumerkey,
                        ConsumerSecret = twitterConsumerSecret,
                        AccessToken = oauth_token_secret,
                        OAuthToken = oauth_token
                    }
                };
                Status updateone;


                twitterCtx = twitterCtx = auth.IsAuthorized ? new TwitterContext(auth) : new TwitterContext();
                var accounts = from acct in twitterCtx.Account
                               where acct.Type == AccountType.RateLimitStatus
                               select acct;

                int RemainingHits = 0;

                foreach (var account in accounts)
                {
                    RemainingHits = account.RateLimitStatus.RemainingHits;
                    break;
                }
                if (RemainingHits > 0)
                {
                    if (!string.IsNullOrEmpty(InReplyToStatusID))
                    {
                        updateone = twitterCtx.UpdateStatus(status, InReplyToStatusID);
                    }
                    else
                    {
                        updateone = twitterCtx.UpdateStatus(status);
                    }
                    statusid = updateone.StatusID;
                    inReplytoScreenName = updateone.InReplyToScreenName;
                    TwittrerStatusDate = updateone.CreatedAt;                    
                    ErrMsg = string.Empty;
                }
                else
                {
                    ErrMsg = "limit reached";
                }


            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToTwitter_LinqToTwitter";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objTwitterResponse.error = ErrMsg;
            objTwitterResponse.ID = statusid;

            return objTwitterResponse;
        }

        public static void PostToMSMQ()
        {
            PublishManager objPublishManager = new PublishManager();
            PublishServiceLog ObjPublishServiceLog;
            PublishExceptionLog ObjPublishExceptionLog;
            try
            {
                List<spGetDealerLocationSocialNetworkPostTimeZone_New_Result> listspGetDealerLocationSocialNetworkPostTimeZone_New_Result = new List<spGetDealerLocationSocialNetworkPostTimeZone_New_Result>();
                listspGetDealerLocationSocialNetworkPostTimeZone_New_Result = objPublishManager.GetDealerLocationSocialNetworkPostTimeZone();

                #region SERVICE LOG

                ObjPublishServiceLog = new PublishServiceLog();

                ObjPublishServiceLog.Message = "SOCIALDEALER.Publish.PostingToMSMQ Service " + DateTime.Now;
                ObjPublishServiceLog.Exception = "Total Number of Post to Process : " + listspGetDealerLocationSocialNetworkPostTimeZone_New_Result.Count;
                ObjPublishServiceLog.MethodName = "PublishToSocialNetwork\\PostToMSMQ";
                objPublishManager.AddPublishServiceLog(ObjPublishServiceLog);

                #endregion

                foreach (var spGetDealerLocationSocialNetworkPostTimeZone_New_Result in listspGetDealerLocationSocialNetworkPostTimeZone_New_Result)
                {
                    try
                    {
                        PublishMSMQ objPublishMSMQ = new PublishMSMQ();
                        objPublishMSMQ.DealerLocationSocialNetworkPostID = "-1";
                        objPublishMSMQ.DealerLocationSocialNetworkPostQueueID = Convert.ToString(spGetDealerLocationSocialNetworkPostTimeZone_New_Result.PostQueueID);
                        objPublishMSMQ.isRetry = false;
                        objPublishMSMQ.LastExecution = DateTime.Now;
                        objPublishMSMQ.LastExecutionResponse = string.Empty;

                        string XMLMessage = Utility.SerializeObject<PublishMSMQ>(objPublishMSMQ);

                        if (!string.IsNullOrEmpty(XMLMessage))
                        {
                            Utility.SendMessageToQueue(XMLMessage, QueueName);
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(spGetDealerLocationSocialNetworkPostTimeZone_New_Result.PostID);
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetDealerLocationSocialNetworkPostTimeZone_New_Result.PostQueueID);
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToMSMQ\\Foreach";
                        ObjPublishExceptionLog.ExtraInfo = string.Empty;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\PostToMSMQ";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }
    }
}
