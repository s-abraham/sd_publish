﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;

namespace BusinessLogic
{
    public class NotificationManager
    {
        public NotificationManager()
        {            
        }

        public int SaveNotificationEntry(NotificationEntry notificationEntry)
        {
            var result = 0;
            var notification_id = 0;

            try
            {
                try
                {
                    notification_id = SaveNotification(notificationEntry.NotificationDetail);
                }
                catch (Exception ex)
                {
                    #region ErrorHandler                    
                    #endregion
                }

                if (notification_id > 0)
                {
                    try
                    {
                        notificationEntry.NotificationTransmissionDetail.NotificationId = notification_id;
                        int notificationTransmission_id = SaveNotificationTransmission(notificationEntry.NotificationTransmissionDetail);

                        if (notificationTransmission_id > 0)
                        {
                            result = notificationTransmission_id;
                        }
                        else
                        {
                            throw new Exception("UnabletoSaveNotificationTransmissionId");
                        }

                    }
                    catch (Exception ex)
                    {
                        #region ErrorHandler                       
                        #endregion
                    }
                }
                else
                {
                    throw new Exception("UnabletoSaveNotificationId");
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                #endregion
            }

            return result;
        }

        private int SaveNotification(DataLayer.Notification notification)
        {
            var result = 0;

            try
            {
                var context = new SDNotificationEntities();

                var activityResult = context.spNotificationInsert(
                    notification.MessageTypeId,
                    notification.MessageCategoryId,
                    notification.Subject,
                    notification.Body,
                    notification.CreatedDate,
                    notification.DealerLocationId,
                    notification.ProcessedDate,
                    notification.CustomerId,
                    notification.DealerGroupId).SingleOrDefault() ?? 0;

                result = Convert.ToInt32(activityResult);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                #endregion
            }

            return result;
        }

        private int SaveNotificationTransmission(DataLayer.NotificationTransmission notificationTransmission)
        {
            var result = 0;

            try
            {
                var context = new SDNotificationEntities();

                var activityResult = context.spNotificationTransmissionInsert(
                    notificationTransmission.NotificationId,
                    notificationTransmission.TransmissionTypeId,
                    notificationTransmission.DateUpdated,
                    notificationTransmission.ScheduledDate,
                    notificationTransmission.StatusId,
                    notificationTransmission.ServiceNumber,
                    Utility.Trim(notificationTransmission.ContactDetails)
                    ).SingleOrDefault() ?? 0;

                result = Convert.ToInt32(activityResult);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                #endregion
            }

            return result;
        }
    }
}
