﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using BusinessEntities;
using System.Web.Script.Serialization;
using DataLayer;
using Newtonsoft.Json;
using System.Configuration;

namespace BusinessLogic
{
    public static class GooglePlus
    {
        #region VARIABLES


        
        // General global variables
        private static string GooglePlusAPITimeOut = Convert.ToString(ConfigurationManager.AppSettings["GooglePlus.TimeOut"]);
        private static string GooglePlusAPIKey = Convert.ToString(ConfigurationManager.AppSettings["GooglePlus.ApiKey"]);
        private static int GooglePlusAPIMaxResults = Convert.ToInt32(ConfigurationManager.AppSettings["GooglePlusAPI.MaxResults"]);

        // Authentication URL's
        private static string GooglePlusURLAuthenticationHeader = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Authentication.Header"]);
        private static string GooglePlusURLGetAuthCode = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Authentication.GetAuthCode"]);
        private static string GooglePlusURLGetTokenStatus = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Authentication.GetTokenStatus"]);

        //User/Pages
        private static string GooglePlusURLGetUserFromAccessToken = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.User.FromAccessToken"]);
        private static string GooglePlusURLGetListOfPagesForAUser = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Page.GetListforAUser"]);
        private static string GooglePlusURLGetSpecificPage = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.GetSpecific.Page"]);
        private static string GooglePlusURLSearchForAPage = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.SearchForA.Page"]);

        // Places/Events
        private static string GooglePlusURLPlaceSearch = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Place.Search"]);
        private static string GooglePlusURLPlaceDetail = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Place.Detail"]);
        private static string GooglePlusURLPlaceCreate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Place.Create"]);
        private static string GooglePlusURLPlaceDelete = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Place.Delete"]);
        private static string GooglePlusURLEventCreate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Event.Create"]);
        private static string GooglePlusURLEventDelete = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Event.Delete"]);
        private static string GooglePlusURLEventDetails = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Event.Detail"]);

        // Activity/Posts
        private static string GooglePlusAPIURLActivityCreate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Activity.Create"]);
        private static string GooglePlusAPIURLActivityGetListForAPage = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Activity.GetListForAPage"]);
        private static string GooglePlusAPIURLActivityGetListForACircle = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Activity.GetListForACircle"]);
        private static string GooglePlusAPIURLActivityGetSpecific = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Activity.GetSpecific"]);
        private static string GooglePlusAPIURLActivityDelete = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Activity.Delete"]);
        private static string GooglePlusAPIURLActivityUpdate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Activity.Update"]);

        // Comments
        private static string GooglePlusAPIURLCommentCreate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Comment.Create"]);
        private static string GooglePlusAPIURLCommentGetForAnActivity = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Comment.GetListForAnActivity"]);
        private static string GooglePlusAPIURLCommentGetSpecific = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Comment.GetSpecific"]);
        private static string GooglePlusAPIURLCommentDelete = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Comment.Delete"]);
        private static string GooglePlusAPIURLCommentUpdate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Comment.Update"]);

        //Circles
        private static string GooglePlusAPIURLCirclesCreate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.Create"]);
        private static string GooglePlusAPIURLCirclesGetListForAPage = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.GetListForAPage"]);
        private static string GooglePlusAPIURLCirclesGetSpecific = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.GetSpecific"]);
        private static string GooglePlusAPIURLCirclesDelete = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.Delete"]);
        private static string GooglePlusAPIURLCirclesUpdate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.Update"]);
        private static string GooglePlusAPIURLCirclesGetMembersList = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.GetMembersList"]);
        private static string GooglePlusAPIURLCirclesAddTo = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.AddTo"]);
        private static string GooglePlusAPIURLCirclesRemoveFrom = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Circles.RemoveFrom"]);

        //Media
        private static string GooglePlusAPIURLMediaCreate = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Media.Create"]);
        private static string GooglePlusAPIURLMediaGetSpecific = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Media.GetSpecific"]);

        //Statistics
        private static string GooglePlusAPIURLStatsGetPlusoners = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Stats.GetPlusoners"]);
        private static string GooglePlusAPIURLStatsResharers = Convert.ToString(ConfigurationManager.AppSettings["GooglePlusAPIURL.Stats.GetResharers"]);

        #endregion

        #region METHODS

        #region Public Methods

        #region AUTHENTICATION

        public static string GooglePlusGetAuthCode(string client_id, string redirect_URL)
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusErrorResponse objGooglePlusResponse = new GooglePlusErrorResponse();
            GooglePlusParams googlePlusGetAuthCodeParams = new GooglePlusParams();
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(redirect_URL))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLGetAuthCode))
                    {
                        GooglePlusURLGetAuthCode = "https://accounts.google.com/o/oauth2/auth?response_type=code&amp;client_id={0}&amp;redirect_uri={1}&amp;scope=https://www.googleapis.com/auth/userinfo.profile%20https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/plus.pages.manage%20https://www.googleapis.com/auth/plus.circles.read%20https://www.googleapis.com/auth/plus.circles.write%20https://www.googleapis.com/auth/plus.stream.read%20https://www.googleapis.com/auth/plus.stream.write%20https://www.googleapis.com/auth/plus.media.readwrite%20https://www.googleapis.com/auth/plus.media.writeonly%20https://www.googleapis.com/auth/plus.media.upload&amp;access_type=offline&amp;approval_prompt=force";
                    }

                    googlePlusGetAuthCodeParams.client_id = client_id;
                    googlePlusGetAuthCodeParams.redirect_URL = redirect_URL;

                    string url = string.Format(GooglePlusURLGetAuthCode, client_id, redirect_URL);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {

                                objGooglePlusResponse = jss.Deserialize<GooglePlusErrorResponse>(rawResponse);
                            }

                            else
                            {

                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusGetAuthCode";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    // objGooglePlusResponse.error.message = "You have entered invalid paramaters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusGetAuthCode";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return result;
        }

        public static GooglePlusRefreshTokenRequestResponse GetGooglePlusRefreshToken(string authCode, string client_id, string client_secret, string redirect_URL, string grant_type)
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusErrorResponse objGooglePlusResponse = new GooglePlusErrorResponse();
            GooglePlusParams googlePlusGetTokenParams = new GooglePlusParams();
            GooglePlusRefreshTokenRequestResponse result = new GooglePlusRefreshTokenRequestResponse();

            try
            {
                if (!string.IsNullOrEmpty(authCode) && !string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(client_secret) && !string.IsNullOrEmpty(redirect_URL) && !string.IsNullOrEmpty(grant_type))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLAuthenticationHeader))
                    {
                        GooglePlusURLAuthenticationHeader = "https://accounts.google.com/o/oauth2/token";
                    }

                    var postParamBuilder = new StringBuilder();

                    googlePlusGetTokenParams.client_id = client_id;
                    googlePlusGetTokenParams.client_secret = client_secret;
                    googlePlusGetTokenParams.redirect_URL = redirect_URL;
                    googlePlusGetTokenParams.grant_type = grant_type;

                    postParamBuilder.Append("code=" + System.Web.HttpUtility.UrlEncode(authCode));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("client_id") + "=" + System.Web.HttpUtility.UrlEncode(googlePlusGetTokenParams.client_id));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("client_secret") + "=" + System.Web.HttpUtility.UrlEncode(googlePlusGetTokenParams.client_secret));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("redirect_uri") + "=" + System.Web.HttpUtility.UrlEncode(googlePlusGetTokenParams.redirect_URL));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("grant_type") + "=" + System.Web.HttpUtility.UrlEncode(googlePlusGetTokenParams.grant_type));

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = postParamBuilder.ToString();

                        objGooglePlusRequest.RequestURL = GooglePlusURLAuthenticationHeader;
                        objGooglePlusRequest.Method = "POST";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(Convert.ToInt32(GooglePlusAPITimeOut));

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {

                                objGooglePlusResponse = jss.Deserialize<GooglePlusErrorResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusRefreshTokenRequestResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusRefreshToken";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusRefreshToken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.errorResponse = objGooglePlusResponse;
            return result;
        }

        public static GooglePlusAccessTokenRequestResponse GetGooglePlusAccessToken(string client_id, string client_secret, string refresh_token, string grant_type)
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusErrorResponse objGooglePlusResponse = new GooglePlusErrorResponse();
            GooglePlusParams googlePlusGetTokenParams = new GooglePlusParams();
            GooglePlusAccessTokenRequestResponse result = new GooglePlusAccessTokenRequestResponse();

            try
            {
                if (!string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(client_secret) && !string.IsNullOrEmpty(refresh_token) && !string.IsNullOrEmpty(grant_type))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLAuthenticationHeader))
                    {
                        GooglePlusURLAuthenticationHeader = "https://accounts.google.com/o/oauth2/token";
                    }

                    var postParamBuilder = new StringBuilder();

                    googlePlusGetTokenParams.client_id = client_id;
                    googlePlusGetTokenParams.client_secret = client_secret;
                    googlePlusGetTokenParams.refresh_token = refresh_token;
                    googlePlusGetTokenParams.grant_type = grant_type;

                    postParamBuilder.Append("client_id=" + googlePlusGetTokenParams.client_id);
                    postParamBuilder.Append("&client_secret=" + googlePlusGetTokenParams.client_secret);
                    postParamBuilder.Append("&refresh_token=" + googlePlusGetTokenParams.refresh_token);
                    postParamBuilder.Append("&grant_type=" + googlePlusGetTokenParams.grant_type);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = postParamBuilder.ToString();

                        objGooglePlusRequest.RequestURL = GooglePlusURLAuthenticationHeader;
                        objGooglePlusRequest.Method = "POST";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusErrorResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusAccessTokenRequestResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusAccessToken";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusAccessToken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.errorResponse = objGooglePlusResponse;
            return result;
        }

        public static GooglePlusTokenStatusResponse GooglePlusAccessTokenStatus(string accessToken)
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusErrorResponse objGooglePlusResponse = new GooglePlusErrorResponse();
            GooglePlusTokenStatusResponse result = new GooglePlusTokenStatusResponse();

            try
            {
                if (!string.IsNullOrEmpty(accessToken))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLGetTokenStatus))
                    {
                        GooglePlusURLGetTokenStatus = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}";
                    }

                    string url = string.Format(GooglePlusURLGetTokenStatus, accessToken);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusErrorResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusTokenStatusResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusAccessTokenStatus";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusAccessTokenStatus";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.errorResponse = objGooglePlusResponse;
            return result;
        }

        #endregion

        #region USER/PAGES

        public static GooglePlusGetUserResponse GetGooglePlusUserFromAccessToken(string access_token, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetUserResponse result = new GooglePlusGetUserResponse();

            try
            {

                if (!string.IsNullOrEmpty(access_token))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLGetUserFromAccessToken))
                    {
                        GooglePlusURLGetUserFromAccessToken = "https://www.googleapis.com/oauth2/v1/userinfo?access_token={0}&quotaUser={1}";
                    }

                    string url = string.Format(GooglePlusURLGetUserFromAccessToken, access_token, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetUserResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusURLGetUserFromAccessToken";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusGetUserFromAccessToken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusGetPageListResponse GetGooglePlusPagesForAnUser(string access_token, string userID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetPageListResponse result = new GooglePlusGetPageListResponse();

            try
            {

                if (!string.IsNullOrEmpty(userID) && !string.IsNullOrEmpty(access_token))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLGetListOfPagesForAUser))
                    {
                        GooglePlusURLGetListOfPagesForAUser = "https://www.googleapis.com/plusPages/v2/people/{0}/people/pages?access_token={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusURLGetListOfPagesForAUser, userID, access_token, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetPageListResponse>(rawResponse);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusPagesForAnUser";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "user_id" + userID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }
                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusPagesForAnUser";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusGetSpecificPageResponse GetGooglePlusPagebyPageID(string pageID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetSpecificPageResponse result = new GooglePlusGetSpecificPageResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLGetSpecificPage))
                    {
                        GooglePlusURLGetSpecificPage = "https://www.googleapis.com/plus/v1/people/{0}?key={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusURLGetSpecificPage, pageID, GooglePlusAPIKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetSpecificPageResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusPagebyPageID";
                        ObjPublishExceptionLog.ExtraInfo = "page_id : " + pageID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusPagebyPageID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusGetPageListResponse SearchForGooglePlusPage(string searchName, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetPageListResponse result = new GooglePlusGetPageListResponse();

            try
            {
                if (!string.IsNullOrEmpty(searchName))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLSearchForAPage))
                    {
                        GooglePlusURLSearchForAPage = "https://www.googleapis.com/plus/v1/people?query={0}&key={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusURLSearchForAPage, searchName, GooglePlusAPIKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetPageListResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\SearchForGooglePlusPage";
                        ObjPublishExceptionLog.ExtraInfo = "search_Name : " + searchName + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\SearchForGooglePlusPage";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        #endregion

        #region ACTIVITIES/POSTS

        public static GooglePlusPostResponse AddGooglePlusActivity(string access_token, string pageID, string accessType, string objectType, string activityType, string postContent, string attachmentID, string postURL, string circleID, string userID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddActivityRequest postParams = new GooglePlusAddActivityRequest();
            GooglePlusAddorUpdateorGetSpecificActivityResponse rawresult = new GooglePlusAddorUpdateorGetSpecificActivityResponse();
            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(postContent))
                {
                    // below if statement checks if certain invalid conditions are met. if they are, the method skips down to an error message.. If not, the method continues its sequence
                    if (!((!string.IsNullOrEmpty(attachmentID) && objectType != "photo" && objectType != "video") // An attachment id is entered, but objectType is not set to "photo" or "video" 
                    || ((objectType == "photo") && string.IsNullOrEmpty(attachmentID)) // The objectType is set to "photo"  but an attachment id isn't entered
                    || (objectType != "" && objectType != "video" && objectType != "photo" && objectType != "article"))) // objectType is something other than null, video,photo or article
                    {
                        if (string.IsNullOrEmpty(GooglePlusAPIURLActivityCreate))
                        {
                            GooglePlusAPIURLActivityCreate = "https://www.googleapis.com/plusPages/v2/people/{0}/activities?access_token={1}&quotaUser={2}";
                        }

                        string url = string.Format(GooglePlusAPIURLActivityCreate, pageID, access_token, sdUserId);


                        activityType = "post";

                        postParams.obj.originalContent = postContent;
                        postParams.verb = activityType;

                        var objGooglePlusAddActivityItem = new GooglePlusAddActivityItem();

                        if (accessType == "circle")
                        {
                            objGooglePlusAddActivityItem.id = circleID;
                            objGooglePlusAddActivityItem.type = accessType;

                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }
                        else if (accessType == "person")
                        {
                            objGooglePlusAddActivityItem.type = accessType;
                            objGooglePlusAddActivityItem.id = userID;

                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }
                        else if (accessType == "myCircles" || accessType == "extendedCircles")
                        {
                            objGooglePlusAddActivityItem.type = accessType;
                            objGooglePlusAddActivityItem.id = string.Empty;

                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }
                        else
                        {
                            objGooglePlusAddActivityItem.type = "public";
                            objGooglePlusAddActivityItem.id = string.Empty;
                            postParams.access.items.Add(objGooglePlusAddActivityItem);
                        }

                        var objGooglePlusAddActivityAttachment = new GooglePlusAddActivityAttachment();

                        objGooglePlusAddActivityAttachment.objectType = objectType;
                        objGooglePlusAddActivityAttachment.url = postURL;
                        objGooglePlusAddActivityAttachment.id = attachmentID;

                        postParams.obj.attachments.Add(objGooglePlusAddActivityAttachment);

                        string json = JsonConvert.SerializeObject(postParams);

                        string rawResponse = string.Empty;

                        try
                        {
                            GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                            objGooglePlusRequest.Parameters = json;

                            objGooglePlusRequest.RequestURL = url;
                            objGooglePlusRequest.Method = "POSTviaJSON";
                            objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                            rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();

                                if (rawResponse.Contains("error"))
                                {
                                    objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                                }

                                else
                                {
                                    rawresult = jss.Deserialize<GooglePlusAddorUpdateorGetSpecificActivityResponse>(rawResponse);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusActivity";
                            ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "access_type" + accessType + "object_type" + objectType + "activity_type" + activityType + "post_content" + postContent + "atttachment_id" + attachmentID + "post_URL" + postURL + "circle_id" + circleID + "user_id" + userID + rawResponse;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            #endregion
                        }
                    }

                    else
                    {
                        objGooglePlusResponse.error.message = "Please specify a valid objectType (photo, album, video or an article). If posting a photo/album, please specify an attachment ID";
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }


                objGooglePlusPostResponse.error = objGooglePlusResponse.error;
                objGooglePlusPostResponse.postURL = rawresult.url;

                if (!string.IsNullOrEmpty(rawresult.published))
                {
                    objGooglePlusPostResponse.IsSuccess = true;
                }

                else { objGooglePlusPostResponse.IsSuccess = false; }

                objGooglePlusPostResponse.finalActivityresponse = rawresult;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusActivity";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusPostResponse;
        }

        public static GooglePlusGetActivityListByPageResponse GetGooglePlusActivitiesListForAPage(string pageID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusGetActivityListByPageResponse rawResult = new GooglePlusGetActivityListByPageResponse();
            GooglePlusGetActivityListByPageResponse result = new GooglePlusGetActivityListByPageResponse();

            try
            {
                rawResult = GetGooglePlusActivitiesPartialList(pageID, string.Empty);

                // Getting Count of the Results
                int resCount = rawResult.items.Count;

                // When the count of results is 100 or more, looping through each of those 100 items and adding to Final Result List
                while (resCount >= 100)
                {
                    foreach (var rawitem in rawResult.items)
                    {
                        result.items.Add(rawitem);
                    }
                    string nextToken = rawResult.nextPageToken;
                    rawResult = GetGooglePlusActivitiesPartialList(pageID, nextToken);
                    resCount = rawResult.items.Count;
                }

                // When the count of results is less than 100, looping through each of those items and adding to Final Result List
                foreach (var rawitem in rawResult.items)
                {
                    result.items.Add(rawitem);
                }

                result.kind = rawResult.kind;
                result.etag = rawResult.etag;
                result.nextPageToken = rawResult.nextPageToken;
                result.title = rawResult.title;
                result.error = rawResult.error;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusActivitiesListForAPage";
                ObjPublishExceptionLog.ExtraInfo = "page_id : " + pageID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            
            return result;
        }

        public static GooglePlusGetActivityListByCircleResponse GetGooglePlusActivitiesListForACircle(string access_token, string pageID, string circleID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetActivityListByCircleResponse result = new GooglePlusGetActivityListByCircleResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLActivityGetListForACircle))
                    {
                        GooglePlusAPIURLActivityGetListForACircle = "https://www.googleapis.com/plusPages/v2/circles/{0}/activities?access_token={1}&onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLActivityGetListForACircle, circleID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetActivityListByCircleResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusGetActivitiesListByCircle";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                result.error = objGooglePlusResponse.error;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GooglePlusGetActivitiesListByCircle";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return result;
        }

        public static GooglePlusAddorUpdateorGetSpecificActivityResponse GetGooglePlusActivityByActivityID(string activityID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddorUpdateorGetSpecificActivityResponse result = new GooglePlusAddorUpdateorGetSpecificActivityResponse();

            try
            {
                if (!string.IsNullOrEmpty(activityID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLActivityGetSpecific))
                    {
                        GooglePlusAPIURLActivityGetSpecific = "https://www.googleapis.com/plus/v1/activities/{0}?key={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusAPIURLActivityGetSpecific, activityID, GooglePlusAPIKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusAddorUpdateorGetSpecificActivityResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusActivityByActivityID";
                        ObjPublishExceptionLog.ExtraInfo = "activity_id : " + activityID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusActivityByActivityID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusResponse DeleteGooglePlusActivity(string access_token, string pageID, string activityID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(activityID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLActivityDelete))
                    {
                        GooglePlusAPIURLActivityDelete = "https://www.googleapis.com/plusPages/v2/activities/{0}?access_token={1}&onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLActivityDelete, activityID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        var objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "DELETE";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                        }

                        else
                        {
                            objGooglePlusResponse.error.message = "You have succesfully deleted the activity";
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusActivity";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "activity_id" + activityID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }


            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusActivity";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusResponse;
        }

        public static GooglePlusPostResponse UpdateGooglePlusActivity(string access_token, string pageID, string activityID, string objectType, string activityType, string postContent, string postURL, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddActivityRequest postParams = new GooglePlusAddActivityRequest();
            GooglePlusAddorUpdateorGetSpecificActivityResponse rawresult = new GooglePlusAddorUpdateorGetSpecificActivityResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(activityID))
                {
                    if (!((!string.IsNullOrEmpty(postURL) && objectType != "article" && objectType != "video")
                        || (objectType != "" && objectType != "video" && objectType != "article"))) // objectType is something other than null, video or article)
                    {
                        if (string.IsNullOrEmpty(GooglePlusAPIURLActivityUpdate))
                        {
                            GooglePlusAPIURLActivityUpdate = "https://www.googleapis.com/plusPages/v2/activities/{0}?access_token={1}&onBehalfOf={2}&quotaUser={3}";
                        }

                        string url = string.Format(GooglePlusAPIURLActivityUpdate, activityID, access_token, pageID, sdUserId);

                        if (string.IsNullOrEmpty(activityType) || activityType != "share")
                        {
                            activityType = "post";
                        }

                        postParams.obj.originalContent = postContent;
                        postParams.verb = activityType;

                        var objGooglePlusAddActivityItem = new GooglePlusAddActivityItem();

                        objGooglePlusAddActivityItem.type = "public";

                        postParams.access.items.Add(objGooglePlusAddActivityItem);


                        var objGooglePlusAddActivityAttachment = new GooglePlusAddActivityAttachment();

                        objGooglePlusAddActivityAttachment.objectType = objectType;
                        objGooglePlusAddActivityAttachment.url = postURL;

                        postParams.obj.attachments.Add(objGooglePlusAddActivityAttachment);


                        string json = JsonConvert.SerializeObject(postParams);

                        string rawResponse = string.Empty;

                        try
                        {
                            GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                            objGooglePlusRequest.Parameters = json;

                            objGooglePlusRequest.RequestURL = url;
                            objGooglePlusRequest.Method = "PUT";
                            objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                            rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {
                                var jss = new JavaScriptSerializer();

                                if (rawResponse.Contains("error"))
                                {



                                    objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                                }

                                else
                                {
                                    rawresult = jss.Deserialize<GooglePlusAddorUpdateorGetSpecificActivityResponse>(rawResponse);
                                }
                            }
                        }

                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\UpdateGooglePlusActivity";
                            ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "activity_id" + activityID + "object_type" + objectType + "activity_type" + activityType + "post_content" + postContent + "post_URL" + postURL + rawResponse;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            #endregion
                        }

                    }

                    else
                    {
                        objGooglePlusResponse.error.message = "Please specify a valid objectType (video or article) for the URL entered";
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                objGooglePlusPostResponse.error = objGooglePlusResponse.error;

                objGooglePlusPostResponse.postURL = rawresult.url;

                if (!string.IsNullOrEmpty(rawresult.published))
                {
                    objGooglePlusPostResponse.IsSuccess = true;
                }

                else { objGooglePlusPostResponse.IsSuccess = false; }

                objGooglePlusPostResponse.finalActivityresponse = rawresult;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\UpdateGooglePlusActivity";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return objGooglePlusPostResponse;
        }

        #endregion

        #region COMMENTS

        public static GooglePlusPostResponse AddGooglePlusComment(string access_token, string pageID, string activityID, string commentContent, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddCommentRequest postParams = new GooglePlusAddCommentRequest();
            GooglePlusAddorUpdateOrGetSpecificCommentResponse rawresult = new GooglePlusAddorUpdateOrGetSpecificCommentResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(activityID) && !string.IsNullOrEmpty(commentContent))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCommentCreate))
                    {
                        GooglePlusAPIURLCommentCreate = "https://www.googleapis.com/plusPages/v2/activities/{0}/comments?access_token={1}&onBehalfOf={2}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusAPIURLCommentCreate, activityID, access_token, pageID, sdUserId);

                    postParams.obj.originalContent = commentContent;

                    string json = JsonConvert.SerializeObject(postParams);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "POSTviaJSON";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                rawresult = jss.Deserialize<GooglePlusAddorUpdateOrGetSpecificCommentResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusComment";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "activity_id" + activityID + "comment_content" + commentContent + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                rawresult.error = objGooglePlusResponse.error;
                objGooglePlusPostResponse.postURL = rawresult.selfLink;

                if (!string.IsNullOrEmpty(rawresult.published))
                {
                    objGooglePlusPostResponse.IsSuccess = true;
                }
                else { objGooglePlusPostResponse.IsSuccess = false; }

                objGooglePlusPostResponse.finalCommentresponse = rawresult;

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusComment";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusPostResponse;
        }

        public static GooglePlusGetCommentsListResponse GetGooglePlusCommentsListForAnActivity(string activityID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetCommentsListResponse result = new GooglePlusGetCommentsListResponse();

            try
            {
                if (!string.IsNullOrEmpty(activityID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCommentGetForAnActivity))
                    {
                        GooglePlusAPIURLCommentGetForAnActivity = "https://www.googleapis.com/plus/v1/activities/{0}/comments?key={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusAPIURLCommentGetForAnActivity, activityID, GooglePlusAPIKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetCommentsListResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCommentsListForAnActivity";
                        ObjPublishExceptionLog.ExtraInfo = "activity_id" + activityID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                result.error = objGooglePlusResponse.error;
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCommentsListForAnActivity";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return result;
        }

        public static GooglePlusAddorUpdateOrGetSpecificCommentResponse GetGooglePlusCommentByCommentID(string commentID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddorUpdateOrGetSpecificCommentResponse result = new GooglePlusAddorUpdateOrGetSpecificCommentResponse();

            try
            {
                if (!string.IsNullOrEmpty(commentID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCommentGetSpecific))
                    {
                        GooglePlusAPIURLCommentGetSpecific = "https://www.googleapis.com/plus/v1/comments/{0}?key={1}&quotaUser={2}";
                    }


                    string url = string.Format(GooglePlusAPIURLCommentGetSpecific, commentID, GooglePlusAPIKey, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusAddorUpdateOrGetSpecificCommentResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCommentByCommentID";
                        ObjPublishExceptionLog.ExtraInfo = "comment_id" + commentID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCommentByCommentID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusResponse DeleteGooglePlusComment(string access_token, string pageID, string commentID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(commentID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCommentDelete))
                    {
                        GooglePlusAPIURLCommentDelete = "https://www.googleapis.com/plusPages/v2/comments/{0}?access_token={1}&amp;onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLCommentDelete, commentID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        var objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "DELETE";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                        }

                        else
                        {
                            objGooglePlusResponse.error.message = "You have succesfully deleted the comment";
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusComment";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "comment_id" + commentID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusComment";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusResponse;
        }

        public static GooglePlusPostResponse UpdateGooglePlusComment(string access_token, string pageID, string commentID, string commentContent, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddCommentRequest postParams = new GooglePlusAddCommentRequest();
            GooglePlusAddorUpdateOrGetSpecificCommentResponse rawresult = new GooglePlusAddorUpdateOrGetSpecificCommentResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(commentID) && !string.IsNullOrEmpty(commentContent))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCommentUpdate))
                    {
                        GooglePlusAPIURLCommentUpdate = "https://www.googleapis.com/plusPages/v2/comments/{0}?accesstoken={1}&amp;onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLCommentUpdate, commentID, access_token, pageID, sdUserId);


                    postParams.obj.originalContent = commentContent;

                    string json = JsonConvert.SerializeObject(postParams);

                    string rawResponse = string.Empty;

                    try
                    {
                        var objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "PUT";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                rawresult = jss.Deserialize<GooglePlusAddorUpdateOrGetSpecificCommentResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\UpdateGooglePlusComment";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "comment_id" + commentID + "comment_content" + commentContent + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

                objGooglePlusPostResponse.error = objGooglePlusResponse.error;
                objGooglePlusPostResponse.postURL = rawresult.selfLink;

                if (!string.IsNullOrEmpty(rawresult.published))
                {
                    objGooglePlusPostResponse.IsSuccess = true;
                }
                else { objGooglePlusPostResponse.IsSuccess = false; }

                objGooglePlusPostResponse.finalCommentresponse = rawresult;

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\UpdateGooglePlusComment";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusPostResponse;
        }

        #endregion

        #region CIRCLES

        public static GooglePlusCreateOrUpdateOrGetSpecificCircleResponse CreateGooglePlusCircle(string access_token, string pageID, string circleName, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusCreateorUpdateCircleRequest postParams = new GooglePlusCreateorUpdateCircleRequest();
            GooglePlusCreateOrUpdateOrGetSpecificCircleResponse result = new GooglePlusCreateOrUpdateOrGetSpecificCircleResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleName))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesCreate))
                    {
                        GooglePlusAPIURLCirclesCreate = "https://www.googleapis.com/plusPages/v2/people/{0}/circles?access_token={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesCreate, pageID, access_token, sdUserId);

                    postParams.displayName = circleName;

                    string json = JsonConvert.SerializeObject(postParams);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "POSTviaJSON";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusCreateOrUpdateOrGetSpecificCircleResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\CreateGooglePlusCircle";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "circle_name" + circleName + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\CreateGooglePlusCircle";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusGetCirclesListResponse GetGooglePlusCirclesListForAPage(string access_token, string pageID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetCirclesListResponse result = new GooglePlusGetCirclesListResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesGetListForAPage))
                    {
                        GooglePlusAPIURLCirclesGetListForAPage = "https://www.googleapis.com/plusPages/v2/people/{0}/circles?access_token={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesGetListForAPage, pageID, access_token, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetCirclesListResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCirclesListForAPage";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCirclesListForAPage";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusCreateOrUpdateOrGetSpecificCircleResponse GetGooglePlusCircleByCircleID(string access_token, string pageID, string circleID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusCreateOrUpdateOrGetSpecificCircleResponse result = new GooglePlusCreateOrUpdateOrGetSpecificCircleResponse();

            try
            {

                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesGetSpecific))
                    {
                        GooglePlusAPIURLCirclesGetSpecific = "https://www.googleapis.com/plusPages/v2/circles/{0}?access_token={1}&amp;onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesGetSpecific, circleID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusCreateOrUpdateOrGetSpecificCircleResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCircleByCircleID";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "circle_id" + circleID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusCircleByCircleID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusResponse DeleteGooglePlusCircle(string access_token, string pageID, string circleID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesDelete))
                    {
                        GooglePlusAPIURLCirclesDelete = "https://www.googleapis.com/plusPages/v2/circles/{0}?access_token={1}&amp;onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesDelete, circleID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        var objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "DELETE";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                        }

                        else
                        {
                            objGooglePlusResponse.error.message = "You have succesfully deleted the circle";
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusCircle";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "circle_id" + circleID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusCircle";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusResponse;
        }

        public static GooglePlusCreateOrUpdateOrGetSpecificCircleResponse UpdateGooglePlusCircle(string access_token, string pageID, string circleID, string circleName, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusCreateorUpdateCircleRequest postParams = new GooglePlusCreateorUpdateCircleRequest();
            GooglePlusCreateOrUpdateOrGetSpecificCircleResponse result = new GooglePlusCreateOrUpdateOrGetSpecificCircleResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleID) && !string.IsNullOrEmpty(circleName))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesUpdate))
                    {
                        GooglePlusAPIURLCirclesUpdate = "https://www.googleapis.com/plusPages/v2/circles/{0}?access_token={1}&onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesUpdate, circleID, access_token, pageID, sdUserId);

                    postParams.displayName = circleName;

                    string json = JsonConvert.SerializeObject(postParams);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "PUT";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusCreateOrUpdateOrGetSpecificCircleResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\UpdateGooglePlusCircle";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "circle_id" + circleID + "circle_name" + circleName + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\UpdateGooglePlusCircle";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusGetMembersListResponse GetGooglePlusMembersListForACircle(string access_token, string pageID, string circleID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetMembersListResponse result = new GooglePlusGetMembersListResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesGetMembersList))
                    {
                        GooglePlusAPIURLCirclesGetMembersList = "https://www.googleapis.com/plusPages/v2/circles/{0}/people?access_token={1}&onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesGetMembersList, circleID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetMembersListResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusMembersListForACircle";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "circle_id" + circleID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusMembersListForACircle";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusCreateOrUpdateOrGetSpecificCircleResponse AddGooglePlusPeopleToACircle(string access_token, string pageID, string circleID, string userID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusCreateOrUpdateOrGetSpecificCircleResponse result = new GooglePlusCreateOrUpdateOrGetSpecificCircleResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleID) && !string.IsNullOrEmpty(userID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesAddTo))
                    {
                        GooglePlusAPIURLCirclesAddTo = "https://www.googleapis.com/plusPages/v2/circles/{0}/people?access_token={1}&onBehalfOf={2}&userId={3)&quotaUser={4}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesAddTo, circleID, access_token, pageID, userID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "PUT";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusCreateOrUpdateOrGetSpecificCircleResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusPeopleToACircle";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "circle_id" + circleID + "user_id" + userID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusPeopleToACircle";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusResponse RemoveGooglePlusPersonFromACircle(string access_token, string pageID, string circleID, string userID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(circleID) && !string.IsNullOrEmpty(userID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLCirclesRemoveFrom))
                    {
                        GooglePlusAPIURLCirclesRemoveFrom = "https://www.googleapis.com/plusPages/v2/circles/{0}/people?access_token={1}&onBehalfOf={2}&userId={3}&quotaUser={4}";
                    }

                    string url = string.Format(GooglePlusAPIURLCirclesRemoveFrom, circleID, access_token, pageID, userID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        var objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "DELETE";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                        }

                        else
                        {
                            objGooglePlusResponse.error.message = "You have succesfully removed the user from the chosen circle";
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\RemoveGooglePlusPersonFromACircle";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "circle_id" + circleID + "user_id" + userID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\RemoveGooglePlusPersonFromACircle";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusResponse;
        }

        #endregion

        #region MEDIA

        public static GooglePlusUploadPhotoResponse UploadGooglePlusPhotoToTempServer(string access_token, string pageID, string pathToImage, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusUploadPhotoResponse result = new GooglePlusUploadPhotoResponse();
            string rawResponse = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(pathToImage))
                {
                    // Create Boundary
                    string boundary = "--" + DateTime.Now.Ticks.ToString("x");

                    // Create HttpWebRequest
                    HttpWebRequest httpRequest;

                    if (string.IsNullOrEmpty(GooglePlusAPIURLMediaCreate))
                    {
                        GooglePlusAPIURLMediaCreate = "https://www.googleapis.com/upload/plusPages/v2/people/{0}/media/cloud?access_token={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusAPIURLMediaCreate, pageID, access_token, sdUserId);

                    httpRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                    httpRequest.ServicePoint.Expect100Continue = false;
                    httpRequest.UserAgent = "Mozilla/4.0 (compatible; Windows NT)";
                    httpRequest.ContentType = "multipart/form-data; start=\"<image>\"; type=\"image/jpeg\"; boundary=" + boundary;
                    httpRequest.KeepAlive = false;
                    httpRequest.Method = "POST";

                    // New String Builder
                    StringBuilder sb = new StringBuilder();

                    // Header
                    string headerTemplate = "--{0}\r\nContent-Disposition: form-data; name=\"image\"; filename=\"image\"\r\nContent-Type: image/jpeg\r\n\r\n";

                    sb.AppendFormat(headerTemplate, boundary, "source", pathToImage, @"application/octet-stream");

                    // File  
                    string formString = sb.ToString();
                    byte[] formBytes = Encoding.UTF8.GetBytes(formString);
                    byte[] trailingBytes = Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
                    byte[] image;
                    image = DownloadImage(pathToImage);

                    // Memory Stream
                    MemoryStream imageMemoryStream = new MemoryStream();
                    imageMemoryStream.Write(image, 0, image.Length);

                    // Set Content Length
                    long imageLength = imageMemoryStream.Length;
                    long contentLength = formBytes.Length + imageLength + trailingBytes.Length;
                    httpRequest.ContentLength = contentLength;

                    // Get Request Stream
                    httpRequest.AllowWriteStreamBuffering = false;
                    Stream strm_out = httpRequest.GetRequestStream();

                    // Write to Stream
                    strm_out.Write(formBytes, 0, formBytes.Length);
                    byte[] buffer = new Byte[checked((uint)Math.Min(4096, (int)imageLength))];
                    int bytesRead = 0;
                    int bytesTotal = 0;
                    imageMemoryStream.Seek(0, SeekOrigin.Begin);
                    while ((bytesRead = imageMemoryStream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        strm_out.Write(buffer, 0, bytesRead); bytesTotal += bytesRead;
                    }
                    strm_out.Write(trailingBytes, 0, trailingBytes.Length);

                    // Close Stream
                    strm_out.Close();

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_UploadPhoto");

                    if (!string.IsNullOrEmpty(rawResponse))
                    {
                        var jss = new JavaScriptSerializer();
                        if (rawResponse.Contains("errorType"))
                        {


                        }

                        result = jss.Deserialize<GooglePlusUploadPhotoResponse>(rawResponse);
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }
            catch (WebException webex)
            {
                #region WEB EXCEPTION ERROR HANDLING
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
                Debug.WriteLine(webex);

                #endregion
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\UploadGooglePlusPhotoToTempServer";
                ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "path_to_Image" + pathToImage + rawResponse;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusPostResponse PostGooglePlusPhoto(string access_token, string pageID, string accessType, string activityType, string postContent, string postURL, string pathToImage, string circleID, string userID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            GooglePlusPostResponse objGooglePlusPostResponse = new GooglePlusPostResponse();
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            var rawresult = new GooglePlusUploadPhotoResponse();

            try
            {

                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(pathToImage) && !string.IsNullOrEmpty(postContent))
                {
                    rawresult = UploadGooglePlusPhotoToTempServer(access_token, pageID, pathToImage, sdUserId);
                    objGooglePlusPostResponse = AddGooglePlusActivity(access_token, pageID, accessType, "photo", activityType, postContent, rawresult.id, postURL, circleID, userID, sdUserId);
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\PostGooglePlusPhoto";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return objGooglePlusPostResponse;

        }

        public static GooglePlusGetSpecificMediaResponse GetGooglePlusMediabyMediaID(string access_token, string mediaID, string pageID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetSpecificMediaResponse result = new GooglePlusGetSpecificMediaResponse();

            try
            {
                if (!string.IsNullOrEmpty(mediaID) && !string.IsNullOrEmpty(pageID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLMediaGetSpecific))
                    {
                        GooglePlusAPIURLMediaGetSpecific = "https://www.googleapis.com/plusPages/v2/media/{0}?key={1}&onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLMediaGetSpecific, mediaID, GooglePlusAPIKey, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetSpecificMediaResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusMediabyMediaID";
                        ObjPublishExceptionLog.ExtraInfo = "access_token : " + access_token + "," + "page_id" + pageID + "media_Id" + mediaID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusMediabyMediaID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        #endregion

        #region PLACES/EVENTS

        public static GooglePlusPlaceSearchResponse PlacesSearchGooglePlus(string searchlocation, string searchRadius, string searchKeyWord, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            // this method finds a place when passed in paramters - name, location (coordinates), radius and sensor type

            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusParams googlePlusSearch = new GooglePlusParams();
            GooglePlusPlaceSearchResponse result = new GooglePlusPlaceSearchResponse();

            try
            {
                if (!string.IsNullOrEmpty(searchKeyWord) && !string.IsNullOrEmpty(searchlocation) && !string.IsNullOrEmpty(searchRadius))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLPlaceSearch))
                    {
                        GooglePlusURLPlaceSearch = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?key={0}&location={1}&radius={2}&sensor=false&keyword={3}&quotaUser={4}";
                    }

                    googlePlusSearch.coordinates = searchlocation;
                    googlePlusSearch.radius = searchRadius;
                    googlePlusSearch.keyword = searchKeyWord;

                    string url = string.Format(GooglePlusURLPlaceSearch, GooglePlusAPIKey, googlePlusSearch.coordinates, googlePlusSearch.radius, googlePlusSearch.keyword, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }
                            else
                            {
                                result = jss.Deserialize<GooglePlusPlaceSearchResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\PlacesSearchGooglePlus";
                        ObjPublishExceptionLog.ExtraInfo = "search_location" + searchlocation + "search_keyword" + searchKeyWord + "search_radius" + searchRadius + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\PlacesSearchGooglePlus";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusPlaceDetailResponse PlacesDetailGooglePlus(string reference, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            // this method finds the details of a place when passed in parameter - reference key of the place)

            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusParams googlePlusSearchParams = new GooglePlusParams();
            GooglePlusPlaceDetailResponse result = new GooglePlusPlaceDetailResponse();

            try
            {
                if (!string.IsNullOrEmpty(reference))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLPlaceDetail))
                    {
                        GooglePlusURLPlaceDetail = "https://maps.googleapis.com/maps/api/place/details/json?key={0}&sensor=false&reference={2}&quotaUser={3}";
                    }

                    googlePlusSearchParams.reference = reference;

                    string url = string.Format(GooglePlusURLPlaceDetail, GooglePlusAPIKey, googlePlusSearchParams.reference, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusPlaceDetailResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\PlacesDetailGooglePlus";
                        ObjPublishExceptionLog.ExtraInfo = "reference" + reference + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\PlacesDetailGooglePlus";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusAddPlaceResponse AddGooglePlusPlace(double latitude, double longtitude, int accuracy, string searchName, string types, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusAddPlaceRequest postParams = new GooglePlusAddPlaceRequest();
            GooglePlusAddPlaceResponse result = new GooglePlusAddPlaceResponse();

            try
            {
                if (latitude != null && longtitude != null && accuracy != null && !string.IsNullOrEmpty(searchName) && !string.IsNullOrEmpty(types))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLPlaceCreate))
                    {
                        GooglePlusURLPlaceCreate = "https://maps.googleapis.com/maps/api/place/add/json?sensor=false&key={1}&quotaUser={2}";
                    }

                    string url = string.Format(GooglePlusURLPlaceCreate, GooglePlusAPIKey, sdUserId);

                    postParams.location.lat = latitude;
                    postParams.location.lng = longtitude;
                    postParams.accuracy = accuracy;
                    postParams.name = searchName;
                    postParams.types.Add(types);

                    string json = JsonConvert.SerializeObject(postParams);
                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "POSTviaJSON";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusAddPlaceResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusPlace";
                        ObjPublishExceptionLog.ExtraInfo = "latitude" + latitude + "longtitude" + longtitude + "accuracy" + accuracy + "searchName" + searchName + "types" + types + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusPlace";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusDeletePlaceResponse DeleteGooglePlusPlace(string reference, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusDeletePlaceRequest postParams = new GooglePlusDeletePlaceRequest();
            GooglePlusDeletePlaceResponse result = new GooglePlusDeletePlaceResponse();

            try
            {
                if (!string.IsNullOrEmpty(reference))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLPlaceDelete))
                    {
                        GooglePlusURLPlaceDelete = "https://maps.googleapis.com/maps/api/place/delete/json?sensor=false&key={0}&quotaUser={1}";
                    }

                    string url = string.Format(GooglePlusURLPlaceDelete, GooglePlusAPIKey, sdUserId);

                    postParams.reference = reference;

                    string json = JsonConvert.SerializeObject(postParams);
                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "POSTviaJSON";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusDeletePlaceResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusPlace";
                        ObjPublishExceptionLog.ExtraInfo = "reference" + reference + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusPlace";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return result;
        }

        public static GooglePlusAddEventResponse AddGooglePlusEvent(int duration, string reference, string summary, string postURL, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusEventAddRequest postParams = new GooglePlusEventAddRequest();
            GooglePlusAddEventResponse result = new GooglePlusAddEventResponse();

            try
            {
                if (duration != 0 && !string.IsNullOrEmpty(reference) && !string.IsNullOrEmpty(summary))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLEventCreate))
                    {
                        GooglePlusURLEventCreate = "https://maps.googleapis.com/maps/api/place/event/add/json?sensor=false&key={0}&quotaUser={1}";
                    }

                    string url = string.Format(GooglePlusURLEventCreate, GooglePlusAPIKey, sdUserId);

                    postParams.duration = duration;
                    postParams.reference = reference;
                    postParams.summary = summary;
                    postParams.url = postURL;

                    string json = JsonConvert.SerializeObject(postParams);
                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "POSTviaJSON";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusAddEventResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusEvent";
                        ObjPublishExceptionLog.ExtraInfo = "duration" + duration + "reference" + reference + "summary" + summary + "postURL" + postURL + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\AddGooglePlusEvent";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusEventDeleteResponse DeleteGooglePlusEvent(string reference, string event_id, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusEventDeleteRequest postParams = new GooglePlusEventDeleteRequest();
            GooglePlusEventDeleteResponse result = new GooglePlusEventDeleteResponse();

            try
            {
                if (!string.IsNullOrEmpty(reference))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLEventDelete))
                    {
                        GooglePlusURLEventDelete = "https://maps.googleapis.com/maps/api/place/event/delete/json?sensor=false&key={0}&quotaUser={1}";
                    }

                    string url = string.Format(GooglePlusURLEventDelete, GooglePlusAPIKey, sdUserId);

                    postParams.reference = reference;
                    postParams.event_id = event_id;

                    string json = JsonConvert.SerializeObject(postParams);
                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();
                        objGooglePlusRequest.Parameters = json;

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "POSTviaJSON";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusEventDeleteResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusEvent";
                        ObjPublishExceptionLog.ExtraInfo = "reference" + reference + "eventid" + event_id + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\DeleteGooglePlusEvent";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return result;
        }

        public static GooglePlusEventDetailResponse EventDetailGooglePlus(string reference, string event_id, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            // this method finds the details of a place when passed in paramter - reference key of the place)

            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusParams googlePlusSearchParams = new GooglePlusParams();
            GooglePlusEventDetailResponse result = new GooglePlusEventDetailResponse();

            try
            {
                if (!string.IsNullOrEmpty(reference))
                {
                    if (string.IsNullOrEmpty(GooglePlusURLEventDetails))
                    {
                        GooglePlusURLEventDetails = "https://maps.googleapis.com/maps/api/place/details/json?key={0}&sensor=false&reference={1}&event_id={2}&quotaUser={3}";
                    }

                    googlePlusSearchParams.reference = reference;
                    googlePlusSearchParams.event_id = event_id;

                    string url = string.Format(GooglePlusURLEventDetails, GooglePlusAPIKey, googlePlusSearchParams.reference, googlePlusSearchParams.event_id, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusEventDetailResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\EventDetailGooglePlus";
                        ObjPublishExceptionLog.ExtraInfo = "reference" + reference + "eventid" + event_id + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\EventDetailGooglePlus";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        #endregion

        #region STATISTICS

        public static GooglePlusPlusonsersResponse GetGooglePlusStatisticsPlusoners(string access_token, string pageID, string activityID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusPlusonsersResponse result = new GooglePlusPlusonsersResponse();

            try
            {

                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(activityID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLStatsGetPlusoners))
                    {
                        GooglePlusAPIURLStatsGetPlusoners = "https://www.googleapis.com/plusPages/v2/activities/{0}/people/plusoners?access_token={1}&onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLStatsGetPlusoners, activityID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {



                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusPlusonsersResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusStatisticsPlusoners";
                        ObjPublishExceptionLog.ExtraInfo = "access_token" + access_token + "page_id" + pageID + "activity_id" + activityID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusStatisticsPlusoners";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        public static GooglePlusResharersResponse GetGooglePlusStatisticsReSharers(string access_token, string pageID, string activityID, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusResharersResponse result = new GooglePlusResharersResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(activityID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLStatsResharers))
                    {
                        GooglePlusAPIURLStatsResharers = "https://www.googleapis.com/plusPages/v2/activities/{0}/people/resharers?access_token={1}&amp;onBehalfOf={2}&quotaUser={3}";
                    }

                    string url = string.Format(GooglePlusAPIURLStatsResharers, activityID, access_token, pageID, sdUserId);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusResharersResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusStatisticsReSharers";
                        ObjPublishExceptionLog.ExtraInfo = "access_token" + access_token + "page_id" + pageID + "activity_id" + activityID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusStatisticsReSharers";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        #endregion

        #region TOKEN VERIFICATION

        public static void ValidateALLGooglePlusToken(int DealerLocationID)
        {
            PublishManager objPublishManager = new PublishManager();            
            SocialDealerManager objSocialDealerManager = new SocialDealerManager();
            try
            {
                int accountGPCounter = 0;
                int activeGPCounter = 0;
                int inActiveGPCounter = 0;
                var objGooglePlusAccessTokenRequestResponse = new GooglePlusAccessTokenRequestResponse();
                var gpTokenResults = objSocialDealerManager.GetSocialNetWorkByDealerLocationID(DealerLocationID, 29);

                foreach (var gpTokenResult in gpTokenResults)
                {
                    accountGPCounter++;
                    try
                    {
                        objGooglePlusAccessTokenRequestResponse = GooglePlus.GetGooglePlusAccessToken(gpTokenResult.AppKey, gpTokenResult.AppSecret, gpTokenResult.Token, "refresh_token");
                        if (String.IsNullOrEmpty(objGooglePlusAccessTokenRequestResponse.access_token))
                        {
                            inActiveGPCounter++;
                            objSocialDealerManager.UpdateTokenStatus(true, string.Empty, gpTokenResult.ID);
                            //context.spValidateTokenStatus(false, gpTokenResult.ID);
                        }
                        else
                        {
                            activeGPCounter++;
                            objSocialDealerManager.UpdateTokenStatus(false, string.Empty, gpTokenResult.ID);
                            //context.spValidateTokenStatus(true, gpTokenResult.ID);
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\ValidateALLGooglePlusToken";
                        ObjPublishExceptionLog.ExtraInfo = "client_id" + gpTokenResult.AppKey + " client_secret" + gpTokenResult.AppSecret + "  access_token" + gpTokenResult.Token;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                //PublishServiceLog ObjPublishServiceLog = new PublishServiceLog();
                //ObjPublishServiceLog.Message = "Validating GooglePlus Tokens: " + activeGPCounter + " Valid & " + inActiveGPCounter + " Invalid";
                //ObjPublishServiceLog.Exception = "Total GP Accounts Validated: " + accountGPCounter;
                //ObjPublishServiceLog.MethodName = "GooglePlus\\ValidateAllGooglePlusToken";
                //objPublishManager.AddPublishServiceLog(ObjPublishServiceLog); 
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\ValidateALLGooglePlusToken";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }
		

        #endregion

        #endregion

        #region Private Methods

        private static string WebRequest(HttpWebRequest httpRequest, int DealerLocationSocialNetworkPostID, int DealerLocationSocialNetworkPostQueueID, string MethodName)
        {

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "GooglePlus\\" + MethodName + "\\WebRequest\\GetResponseStream";
                            ObjPublishExceptionLog.ExtraInfo = string.Empty;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";

                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "GooglePlus\\" + MethodName + "\\WebRequest\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                rawResponse = @"{""error"": {    ""message"": """ + "" + ex.Message + "" + @""", ""type"": ""Exception"",    ""code"": 000  }}";
                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Get(GooglePlusRequest _googlePlusRequest)
        {

            var objPublishManager = new PublishManager();
            var rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "GET"
                var encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_googlePlusRequest.RequestURL))
                {
                    var httpRequest = (HttpWebRequest)HttpWebRequest.Create(_googlePlusRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_googlePlusRequest.Method))
                        httpRequest.Method = _googlePlusRequest.Method;

                    httpRequest.Timeout = _googlePlusRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Get");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "GooglePlus\\ExecuteCommand_Get";
                ObjPublishExceptionLog.ExtraInfo = _googlePlusRequest.RequestURL + "," + _googlePlusRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Post(GooglePlusRequest _googlePlusRequest)
        {
            // This method handles posting in form-urlencoded format, JSON format & also the the "PUT" (Update) command

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST/DELETE/PUT"
                UTF8Encoding encoding = new UTF8Encoding();

                if (!String.IsNullOrEmpty(_googlePlusRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_googlePlusRequest.RequestURL);
                    httpRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6";

                    if (!String.IsNullOrEmpty(_googlePlusRequest.Method))
                        httpRequest.Method = _googlePlusRequest.Method;

                    byte[] postBytes;
                    if (httpRequest.Method == "POST")
                    {
                        httpRequest.ContentType = "application/x-www-form-urlencoded";
                        httpRequest.Host = "accounts.google.com";
                        httpRequest.Accept = "*/*";
                    }

                    else if (httpRequest.Method == "POSTviaJSON")
                    {
                        httpRequest.Method = "POST";
                        httpRequest.ContentType = "application/json";
                    }

                    else if (httpRequest.Method == "PUT")
                    {
                        httpRequest.ContentType = "application/json";
                    }

                    postBytes = encoding.GetBytes(_googlePlusRequest.Parameters);
                    using (Stream reqStream = httpRequest.GetRequestStream())
                    {
                        reqStream.Write(postBytes, 0, postBytes.Length);
                        reqStream.Flush();
                        reqStream.Close();
                    }

                    httpRequest.Timeout = _googlePlusRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Post");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "GooglePlus\\ExecuteCommand_Post";
                ObjPublishExceptionLog.ExtraInfo = _googlePlusRequest.RequestURL + "," + _googlePlusRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static byte[] DownloadImage(string ImageUrl)
        {
            PublishManager objPublishManager = new PublishManager();
            try
            {
                var httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(ImageUrl);
                httpWebRequest.AllowWriteStreamBuffering = true;
                httpWebRequest.Timeout = 20000;
                var webResponse = httpWebRequest.GetResponse();
                var webStream = webResponse.GetResponseStream();

                using (var ms = new MemoryStream())
                {
                    webStream.CopyTo(ms);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "GooglePlus\\DownloadImage";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                byte[] bytes = null;
                return bytes;

                #endregion
            }
        }

        private static GooglePlusGetActivityListByPageResponse GetGooglePlusActivitiesPartialList(string pageID, string pageToken, string sdUserId = "00000000-0000-0000-0000-000000000000")
        {
            PublishManager objPublishManager = new PublishManager();
            GooglePlusResponse objGooglePlusResponse = new GooglePlusResponse();
            GooglePlusGetActivityListByPageResponse result = new GooglePlusGetActivityListByPageResponse();

            try
            {
                if (!string.IsNullOrEmpty(pageID))
                {
                    if (string.IsNullOrEmpty(GooglePlusAPIURLActivityGetListForAPage))
                    {
                        GooglePlusAPIURLActivityGetListForAPage = "https://www.googleapis.com/plusPages/v2/people/{0}/activities/public?key={1}&quotaUser={2}&maxResults={3}&pageToken={4}";
                    }

                    string url = string.Format(GooglePlusAPIURLActivityGetListForAPage, pageID, GooglePlusAPIKey, sdUserId, GooglePlusAPIMaxResults, pageToken);

                    string rawResponse = string.Empty;

                    try
                    {
                        GooglePlusRequest objGooglePlusRequest = new GooglePlusRequest();

                        objGooglePlusRequest.RequestURL = url;
                        objGooglePlusRequest.Method = "GET";
                        objGooglePlusRequest.TimeOut = Convert.ToInt32(GooglePlusAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objGooglePlusRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objGooglePlusResponse = jss.Deserialize<GooglePlusResponse>(rawResponse);
                            }

                            else
                            {
                                result = jss.Deserialize<GooglePlusGetActivityListByPageResponse>(rawResponse);

                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusActivitiesPartialList";
                        ObjPublishExceptionLog.ExtraInfo = "page_id : " + pageID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objGooglePlusResponse.error.message = "You have entered invalid parameters";
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetGooglePlusActivitiesPartialList";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            result.error = objGooglePlusResponse.error;
            return result;
        }

        #endregion

        #endregion
    }
}

