﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using BusinessEntities;
using System.Web.Script.Serialization;
using DataLayer;
using Newtonsoft.Json;
using System.Configuration;

namespace BusinessLogic
{
    public static class Blogger
    {
        #region VARIABLES

        // General 
        private static string BloggerAPITimeOut = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPI.TimeOut"]);
        private static string BloggerAPIKey = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPI.Key"]);

        // Authentication URL's
        private static string BloggerAPIURLAuthenticationHeader = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Authentication.Header"]);
        private static string BloggerAPIURLGetAuthCode = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.GetAuthCode"]);
        private static string BloggerAPIURLGetTokenStatus = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.GetTokenStatus"]);

        // Blogs
        private static string BloggerAPIURLGetBlogByAccessToken = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.GetBlogByAccessToken"]);
        private static string BloggerAPIURLGetBlogByBlogID = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.GetBlogByBlogID"]);
        private static string BloggerAPIURLGetBlogByBlogURL = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.GetBlogByBlogURL"]);

        // Posts
        private static string BloggerAPIURLPostCreate = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Post.Create"]);
        private static string BloggerAPIURLPostGetListByBlog = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Post.GetListByBlog"]);
        private static string BloggerAPIURLPostSearch = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Post.Search"]);
        private static string BloggerAPIURLPostGetSpecific = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Post.GetSpecific"]);
        private static string BloggerAPIURLPostUpdate = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Post.Update"]);
        private static string BloggerAPIURLPostDelete = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Post.Delete"]);

        // Comments
        private static string BloggerAPIURLCommentGetListByPost = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Comment.GetListByPost"]);
        private static string BloggerAPIURLCommentGetSpecific = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Comment.GetSpecific"]);

        // Pages
        private static string BloggerAPIURLPageGetListByBlog = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Pages.GetListByBlog"]);
        private static string BloggerAPIURLGetSpecific = Convert.ToString(ConfigurationManager.AppSettings["BloggerAPIURL.Pages.GetSpecific"]);
       
        #endregion

        #region METHODS

        #region Public

        #region AUTHENTICATION

        public static string GetBloggerAuthCode(string client_id, string redirect_URL)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerErrorResponse objBloggerErrorResponse = new BloggerErrorResponse();
            BloggerAuthParams objBloggerAuthParams = new BloggerAuthParams();
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(redirect_URL))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLGetAuthCode))
                    {
                        BloggerAPIURLGetAuthCode = "https://accounts.google.com/o/oauth2/auth?client_id={0}&redirect_uri={1}&scope=https://www.googleapis.com/auth/blogger+https://www.blogger.com/feeds+https://www.googleapis.com/auth/userinfo.profile%20https://www.googleapis.com/auth/userinfo.email&response_type=code&access_type=offline&approval_prompt=force";
                    }

                    objBloggerAuthParams.client_id = client_id;
                    objBloggerAuthParams.redirect_URL = redirect_URL;

                    string url = string.Format(BloggerAPIURLGetAuthCode, client_id, redirect_URL);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.StartsWith(@"{""error"":"))
                            {
                                objBloggerErrorResponse = jss.Deserialize<BloggerErrorResponse>(rawResponse);
                            }

                            else
                            {

                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerAuthCode";
                        ObjPublishExceptionLog.ExtraInfo = "client_id: " + client_id + "redirect_url: " + redirect_URL +  rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    
                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerAuthCode";
                ObjPublishExceptionLog.ExtraInfo = "client_id: " + client_id + "redirect_url: " + redirect_URL;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return result;
        }

        public static string GetBloggerAuthCodeURL(string client_id, string redirect_URL)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerErrorResponse objBloggerErrorResponse = new BloggerErrorResponse();
            BloggerAuthParams objBloggerAuthParams = new BloggerAuthParams();
            string result = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(redirect_URL))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLGetAuthCode))
                    {
                        BloggerAPIURLGetAuthCode = "https://accounts.google.com/o/oauth2/auth?client_id={0}&redirect_uri={1}&scope=https://www.googleapis.com/auth/blogger+https://www.blogger.com/feeds+https://www.googleapis.com/auth/userinfo.profile%20https://www.googleapis.com/auth/userinfo.email&response_type=code&access_type=offline&approval_prompt=force";
                    }

                    objBloggerAuthParams.client_id = client_id;
                    objBloggerAuthParams.redirect_URL = redirect_URL;

                    result = string.Format(BloggerAPIURLGetAuthCode, client_id, redirect_URL);
                }

                else
                {

                }
            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerAuthCode";
                ObjPublishExceptionLog.ExtraInfo = "client_id: " + client_id + "redirect_url: " + redirect_URL;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }


            return result;
        }

        public static BloggerRefreshTokenRequestResponse GetBloggerRefreshToken(string authCode, string client_id, string client_secret, string redirect_URL, string grant_type)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerErrorResponse objBloggerErrorResponse = new BloggerErrorResponse();
            BloggerAuthParams objBloggerAuthParams = new BloggerAuthParams();
            BloggerRefreshTokenRequestResponse objBloggerRefreshTokenRequestResponse = new BloggerRefreshTokenRequestResponse();

            try
            {
                if (!string.IsNullOrEmpty(authCode) && !string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(client_secret) && !string.IsNullOrEmpty(redirect_URL) && !string.IsNullOrEmpty(grant_type))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLAuthenticationHeader))
                    {
                        BloggerAPIURLAuthenticationHeader = "https://accounts.google.com/o/oauth2/token";
                    }

                    var postParamBuilder = new StringBuilder();

                    objBloggerAuthParams.client_id = client_id;
                    objBloggerAuthParams.client_secret = client_secret;
                    objBloggerAuthParams.redirect_URL = redirect_URL;
                    objBloggerAuthParams.grant_type = grant_type;

                    postParamBuilder.Append("code=" + System.Web.HttpUtility.UrlEncode(authCode));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("client_id") + "=" + System.Web.HttpUtility.UrlEncode(objBloggerAuthParams.client_id));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("client_secret") + "=" + System.Web.HttpUtility.UrlEncode(objBloggerAuthParams.client_secret));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("redirect_uri") + "=" + System.Web.HttpUtility.UrlEncode(objBloggerAuthParams.redirect_URL));
                    postParamBuilder.Append("&" + System.Web.HttpUtility.UrlEncode("grant_type") + "=" + System.Web.HttpUtility.UrlEncode(objBloggerAuthParams.grant_type));

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();
                        objBloggerRequest.Parameters = postParamBuilder.ToString();

                        objBloggerRequest.RequestURL = BloggerAPIURLAuthenticationHeader;
                        objBloggerRequest.Method = "POST";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {

                                objBloggerErrorResponse = jss.Deserialize<BloggerErrorResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerRefreshTokenRequestResponse = jss.Deserialize<BloggerRefreshTokenRequestResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerRefreshToken";
                        ObjPublishExceptionLog.ExtraInfo =  "authCode: " + authCode +  "client_id: " + client_id + "client_secret: " +  client_secret + "redirect_URL: "+ redirect_URL + "grant_type : " + grant_type + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objBloggerErrorResponse.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerRefreshToken";
                ObjPublishExceptionLog.ExtraInfo = "authCode: " + authCode + "client_id: " + client_id + "client_secret: " + client_secret + "redirect_URL: " + redirect_URL + "grant_type : " + grant_type;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerRefreshTokenRequestResponse.errorResponse = objBloggerErrorResponse;
            return objBloggerRefreshTokenRequestResponse;
        }

        public static BloggerAccessTokenRequestResponse GetBloggerAccessToken(string client_id, string client_secret, string refresh_token, string grant_type)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerErrorResponse objBloggerErrorResponse = new BloggerErrorResponse();
            BloggerAuthParams objBloggerAuthParams = new BloggerAuthParams();
            BloggerAccessTokenRequestResponse objBloggerAccessTokenRequestResponse = new BloggerAccessTokenRequestResponse();

            try
            {
                if (!string.IsNullOrEmpty(client_id) && !string.IsNullOrEmpty(client_secret) && !string.IsNullOrEmpty(refresh_token) && !string.IsNullOrEmpty(grant_type))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLAuthenticationHeader))
                    {
                        BloggerAPIURLAuthenticationHeader = "https://accounts.google.com/o/oauth2/token";
                    }

                    var postParamBuilder = new StringBuilder();

                    objBloggerAuthParams.client_id = client_id;
                    objBloggerAuthParams.client_secret = client_secret;
                    objBloggerAuthParams.refresh_token = refresh_token;
                    objBloggerAuthParams.grant_type = grant_type;

                    postParamBuilder.Append("client_id=" + objBloggerAuthParams.client_id);
                    postParamBuilder.Append("&client_secret=" + objBloggerAuthParams.client_secret);
                    postParamBuilder.Append("&refresh_token=" + objBloggerAuthParams.refresh_token);
                    postParamBuilder.Append("&grant_type=" + objBloggerAuthParams.grant_type);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();
                        objBloggerRequest.Parameters = postParamBuilder.ToString();

                        objBloggerRequest.RequestURL = BloggerAPIURLAuthenticationHeader;
                        objBloggerRequest.Method = "POST";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objBloggerErrorResponse = jss.Deserialize<BloggerErrorResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerAccessTokenRequestResponse = jss.Deserialize<BloggerAccessTokenRequestResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerAccessToken";
                        ObjPublishExceptionLog.ExtraInfo = "client_id: " + client_id + "client_secret: " + client_secret + "refresh_token: " + refresh_token + "grant_type : " + grant_type + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objBloggerErrorResponse.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerAccessToken";
                ObjPublishExceptionLog.ExtraInfo = "client_id: " + client_id + "client_secret: " + client_secret + "refresh_token: " + refresh_token + "grant_type : " + grant_type;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerAccessTokenRequestResponse.errorResponse = objBloggerErrorResponse;
            return objBloggerAccessTokenRequestResponse;
        }

        public static BloggerTokenStatusResponse GetStatusOfBloggerAccessToken(string accessToken)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerErrorResponse objBloggerErrorResponse = new BloggerErrorResponse();
            BloggerTokenStatusResponse obBloggerTokenStatusResponse = new BloggerTokenStatusResponse();

            try
            {
                if (!string.IsNullOrEmpty(accessToken))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLGetTokenStatus))
                    {
                        BloggerAPIURLGetTokenStatus = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token={0}";
                    }

                    string url = string.Format(BloggerAPIURLGetTokenStatus, accessToken);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objBloggerErrorResponse = jss.Deserialize<BloggerErrorResponse>(rawResponse);
                            }

                            else
                            {
                                obBloggerTokenStatusResponse = jss.Deserialize<BloggerTokenStatusResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetStatusOfBloggerAccessToken";
                        ObjPublishExceptionLog.ExtraInfo = "access_token: " + accessToken + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerErrorResponse.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\GooglePlus\\GetStatusOfBloggerAccessToken";
                ObjPublishExceptionLog.ExtraInfo = "access_token: " + accessToken;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            obBloggerTokenStatusResponse.errorResponse = objBloggerErrorResponse;
            return obBloggerTokenStatusResponse;
        }

        #endregion

        #region BLOGS

        public static BloggerGetBlogInfoByAccessToken GetBlogInfobyAccessToken(string access_token)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetBlogInfoByAccessToken objBloggerGetBlogInfoByAccessToken = new BloggerGetBlogInfoByAccessToken();

            try
            {
                if (!string.IsNullOrEmpty(access_token))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLGetBlogByAccessToken))
                    {
                        BloggerAPIURLGetBlogByAccessToken = "https://www.googleapis.com/blogger/v2/users/self/blogs?access_token={0}";
                    }

                    string url = string.Format(BloggerAPIURLGetBlogByAccessToken, access_token);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {

                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetBlogInfoByAccessToken = jss.Deserialize<BloggerGetBlogInfoByAccessToken>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBlogInfobyAccessToken";
                        ObjPublishExceptionLog.ExtraInfo = "access_token" + access_token + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBlogInfobyAccessToken";
                ObjPublishExceptionLog.ExtraInfo = "access_token" + access_token;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetBlogInfoByAccessToken.error = objBloggerResponse;
            return objBloggerGetBlogInfoByAccessToken;
        }

        public static BloggerGetBlogInfoByURLorIDResponse GetBlogInfobyBlogID(string blogID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetBlogInfoByURLorIDResponse objBloggerGetBlogInfoByURLorIDResponse = new BloggerGetBlogInfoByURLorIDResponse();

            try
            {
                if (!string.IsNullOrEmpty(blogID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLGetBlogByBlogID))
                    {
                        BloggerAPIURLGetBlogByBlogID = "https://www.googleapis.com/blogger/v3/blogs/{0}?key={1}";
                    }

                    string url = string.Format(BloggerAPIURLGetBlogByBlogID, blogID, BloggerAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {



                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetBlogInfoByURLorIDResponse = jss.Deserialize<BloggerGetBlogInfoByURLorIDResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBlogInfobyBlogID";
                        ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerGetBlogInfobyBlogID";
                ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetBlogInfoByURLorIDResponse.error = objBloggerResponse;
            return objBloggerGetBlogInfoByURLorIDResponse;
        }

        public static BloggerGetBlogInfoByURLorIDResponse GetBlogInfobyBlogURL(string blogURL)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetBlogInfoByURLorIDResponse objBloggerGetBlogInfoByURLorIDResponse = new BloggerGetBlogInfoByURLorIDResponse();

            try
            {

                if (!string.IsNullOrEmpty(blogURL))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLGetBlogByBlogURL))
                    {
                        BloggerAPIURLGetBlogByBlogURL = "https://www.googleapis.com/blogger/v3/blogs/byurl?url={0}?key={1}";
                    }

                    string url = string.Format(BloggerAPIURLGetBlogByBlogURL, blogURL, BloggerAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetBlogInfoByURLorIDResponse = jss.Deserialize<BloggerGetBlogInfoByURLorIDResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBlogInfobyBlogURL";
                        ObjPublishExceptionLog.ExtraInfo = "blog_URL: " + blogURL + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerGetBlogInfobyBlogURL";
                ObjPublishExceptionLog.ExtraInfo = "blog_URL: " + blogURL;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetBlogInfoByURLorIDResponse.error = objBloggerResponse;
            return objBloggerGetBlogInfoByURLorIDResponse;
        }

        #endregion

        #region POSTS

        public static BloggerPostResponse AddBloggerPost(string access_token, string blogID, string postTitle, string postContent)
        {
            BloggerPostResponse objBloggerPostResponse = new BloggerPostResponse();
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerAddPostRequest postParams = new BloggerAddPostRequest();      
            try
            {
                if (!string.IsNullOrEmpty(blogID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(postTitle) && !string.IsNullOrEmpty(postContent))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLPostCreate))
                    {
                        BloggerAPIURLPostCreate = "https://www.googleapis.com/blogger/v3/blogs/{0}/posts?access_token={1}";
                    }

                    string url = string.Format(BloggerAPIURLPostCreate, blogID, access_token);

                    postParams.title = postTitle;
                    postParams.content = postContent;

                    string json = JsonConvert.SerializeObject(postParams);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();
                        objBloggerRequest.Parameters = json;

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "POSTviaJSON";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Post(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerPostResponse = jss.Deserialize<BloggerPostResponse>(rawResponse);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = objBloggerResponse.error.ToString();
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\AddBloggerPost";
                        ObjPublishExceptionLog.ExtraInfo = "access_token" + access_token + "blog_id: " + blogID + "post_Title: " + postTitle + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = objBloggerResponse.error.ToString();
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\AddBloggerPost";
                ObjPublishExceptionLog.ExtraInfo = "access_token" + access_token + "blog_id: " + blogID + "post_Title: " + postTitle;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerPostResponse.error = objBloggerResponse;

            if (!string.IsNullOrEmpty(objBloggerPostResponse.published))
            {
                objBloggerPostResponse.IsSuccess = true;
            }

            else { objBloggerPostResponse.IsSuccess = false; }

            return objBloggerPostResponse;
        }

        public static BloggerGetListResponse ListBloggerPostsByBlog(string blogID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetListResponse objBloggerGetListResponse = new BloggerGetListResponse();

            try
            {
               if (!string.IsNullOrEmpty(blogID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLPostGetListByBlog))
                    {
                        BloggerAPIURLPostGetListByBlog = "https://www.googleapis.com/blogger/v3/blogs/{0}/posts?key={1}";
                    }

                    string url = string.Format(BloggerAPIURLPostGetListByBlog, blogID, BloggerAPIKey);
                 
                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();
                    
                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                               
                                

                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetListResponse = jss.Deserialize<BloggerGetListResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\ListBloggerPostsByBlog";
                        ObjPublishExceptionLog.ExtraInfo = "blogid: " +  blogID +  rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerListPostsByBlog";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetListResponse.error = objBloggerResponse;
            return objBloggerGetListResponse;
        }

        public static BloggerGetListResponse SearchForBloggerPost(string blogID, string searchKeyword)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetListResponse objBloggerGetListResponse = new BloggerGetListResponse();

            try
            {
                
                if (!string.IsNullOrEmpty(blogID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLPostSearch))
                    {
                        BloggerAPIURLPostSearch = "https://www.googleapis.com/blogger/v3/blogs/{0}/posts/search?q={1}&key={2}";
                    }

                    string url = string.Format(BloggerAPIURLPostSearch, blogID, searchKeyword, BloggerAPIKey);
                 
                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();
                    
                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                               
                                

                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetListResponse = jss.Deserialize<BloggerGetListResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\SearchForBloggerPost";
                        ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + "search_keyword: " + searchKeyword + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerListPostsByBlog";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetListResponse.error = objBloggerResponse;
            return objBloggerGetListResponse;
        }

        public static BloggerPostResponse GetBloggerPostbyPostID(string blogID, string postID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerPostResponse objBloggerPostResponse = new BloggerPostResponse();

            try
            {

                if (!string.IsNullOrEmpty(blogID) && !string.IsNullOrEmpty(postID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLPostGetSpecific))
                    {
                        BloggerAPIURLPostGetSpecific = "https://www.googleapis.com/blogger/v3/blogs/{0}/posts/{1}?key={2}";
                    }

                    string url = string.Format(BloggerAPIURLPostGetSpecific, blogID,postID, BloggerAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut);

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                               
                                

                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerPostResponse = jss.Deserialize<BloggerPostResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerPostbyPostID";
                        ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + "post_id: " + postID +  rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerGetPostbyPostID";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerPostResponse.error = objBloggerResponse;
            return objBloggerPostResponse;
        }

        public static BloggerPostResponse UpdateBloggerPost(string access_token, string blogID, string postID, string postTitle, string postContent)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerUpdatePostRequest postParams = new BloggerUpdatePostRequest();
            BloggerPostResponse objBloggerPostResponse = new BloggerPostResponse();

            try
            {

                if (!string.IsNullOrEmpty(blogID) && !string.IsNullOrEmpty(postID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(postTitle) && !string.IsNullOrEmpty(postContent))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLPostUpdate))
                    {
                        BloggerAPIURLPostUpdate = "https://www.googleapis.com/blogger/v3/blogs/{0}/posts/{1}?access_token={2}";
                    }

                    string url = string.Format(BloggerAPIURLPostUpdate, blogID, postID, access_token);

                    postParams.title = postTitle;
                    postParams.content = postContent;
                    postParams.id = postID;

                    string json = JsonConvert.SerializeObject(postParams);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();
                        objBloggerRequest.Parameters = json;

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "PUT";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut); 

                        rawResponse = ExecuteCommand_Post(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerPostResponse = jss.Deserialize<BloggerPostResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\UpdateBloggerPost";
                        ObjPublishExceptionLog.ExtraInfo = "access_token: " + access_token + "blog_id: " + blogID + "post_id: " + postID + "post_title: " + postTitle + "post_Content: " + postContent + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }
                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerUpdatePost";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerPostResponse.error = objBloggerResponse;
            return objBloggerPostResponse;
        }

        public static BloggerResponse DeleteBloggerPost(string access_token,string blogID, string postID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
           
            try
            {
                if (!string.IsNullOrEmpty(blogID) && !string.IsNullOrEmpty(access_token) && !string.IsNullOrEmpty(postID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLPostDelete))
                    {
                        BloggerAPIURLPostDelete = "https://www.googleapis.com/blogger/v3/blogs/{0}/posts/{1}?access_token={2}";
                    }

                    string url = string.Format(BloggerAPIURLPostDelete, blogID, postID, access_token);
                    
                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();
           
                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "DELETE";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut); 

                        rawResponse = ExecuteCommand_Post(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }
                        }

                        else
                        {
                            objBloggerResponse.error.message = "You have succesfully deleted the post";
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\DeleteBloggerPost";
                        ObjPublishExceptionLog.ExtraInfo = "access_token: " + access_token + "blog_id: " + blogID + "post_id: " + postID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerDeletePost";
                ObjPublishExceptionLog.ExtraInfo = "access_token: " + access_token + "blog_id: " + blogID + "post_id: " + postID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

           return objBloggerResponse;
        }

        #endregion

        #region COMMENTS

        public static BloggerGetCommentListResponse GetBloggerCommentsListByPost(string blogID, string postID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetCommentListResponse objBloggerGetCommentListResponse = new BloggerGetCommentListResponse();

            try
            {
                if (!string.IsNullOrEmpty(blogID) && !string.IsNullOrEmpty(postID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLCommentGetListByPost))
                    {
                        BloggerAPIURLCommentGetListByPost = "https://www.googleapis.com/blogger/v3/blogs/{0}/posts/{1}/comments?key={2}";
                    }

                    string url = string.Format(BloggerAPIURLCommentGetListByPost, blogID, postID, BloggerAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut); 

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetCommentListResponse = jss.Deserialize<BloggerGetCommentListResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerCommentsListByPost";
                        ObjPublishExceptionLog.ExtraInfo =  "blog_id: " + blogID + "post_id " + postID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\BloggerGetCommentsListByPost";
                ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + "post_id " + postID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetCommentListResponse.error = objBloggerResponse;
            return objBloggerGetCommentListResponse;
        }

        public static BloggerGetSpecificCommentResponse GetBloggerCommentByCommentID(string blogID, string postID, string commentID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetSpecificCommentResponse objBloggerGetSpecificCommentResponse = new BloggerGetSpecificCommentResponse();

            try
            {
                if (!string.IsNullOrEmpty(blogID) && !string.IsNullOrEmpty(postID) && !string.IsNullOrEmpty(commentID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLCommentGetSpecific))
                    {
                        BloggerAPIURLCommentGetSpecific = "https://www.googleapis.com/blogger/v3/blogs{0}/posts/{1}/comments/{2}?key={3}";
                    }

                    string url = string.Format(BloggerAPIURLCommentGetSpecific, blogID, postID, commentID, BloggerAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut); 

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                               
                                

                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetSpecificCommentResponse = jss.Deserialize<BloggerGetSpecificCommentResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerCommentByCommentID";
                        ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + "post_id " + postID + "comment_id: " + commentID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerCommentByCommentID";
                ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + "post_id " + postID + "comment_id: " + commentID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetSpecificCommentResponse.error = objBloggerResponse;
            return objBloggerGetSpecificCommentResponse;
        }

        #endregion

        #region PAGES

        public static BloggerGetPageListResponse GetBloggerPageListByBlog(string blogID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetPageListResponse objBloggerGetPageListResponse = new BloggerGetPageListResponse();

            try
            {
                if (!string.IsNullOrEmpty(blogID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLPageGetListByBlog))
                    {
                        BloggerAPIURLPageGetListByBlog = "https://www.googleapis.com/blogger/v3/blogs/{0}/pages?key={1}";
                    }

                    string url = string.Format(BloggerAPIURLPageGetListByBlog, blogID, BloggerAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut); 

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                               
                                

                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetPageListResponse = jss.Deserialize<BloggerGetPageListResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerPageListByBlog";
                        ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerPageListByBlog";
                ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetPageListResponse.error = objBloggerResponse;
            return objBloggerGetPageListResponse;
        }

        public static BloggerGetSpecificPageResponse GetBloggerPageByPageID(string blogID, string pageID)
        {
            PublishManager objPublishManager = new PublishManager();
            BloggerResponse objBloggerResponse = new BloggerResponse();
            BloggerGetSpecificPageResponse objBloggerGetSpecificPageResponse = new BloggerGetSpecificPageResponse();

            try
            {

                if (!string.IsNullOrEmpty(blogID) && !string.IsNullOrEmpty(pageID))
                {
                    if (string.IsNullOrEmpty(BloggerAPIURLGetSpecific))
                    {
                        BloggerAPIURLGetSpecific = "https://www.googleapis.com/blogger/v3/blogs{0}/pages/{1}?key={2}";
                    }

                    string url = string.Format(BloggerAPIURLGetSpecific, blogID, pageID, BloggerAPIKey);

                    string rawResponse = string.Empty;

                    try
                    {
                        BloggerRequest objBloggerRequest = new BloggerRequest();

                        objBloggerRequest.RequestURL = url;
                        objBloggerRequest.Method = "GET";
                        objBloggerRequest.TimeOut = Convert.ToInt32(BloggerAPITimeOut); 

                        rawResponse = ExecuteCommand_Get(objBloggerRequest);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            var jss = new JavaScriptSerializer();

                            if (rawResponse.Contains("error"))
                            {
                               
                                

                                objBloggerResponse = jss.Deserialize<BloggerResponse>(rawResponse);
                            }

                            else
                            {
                                objBloggerGetSpecificPageResponse = jss.Deserialize<BloggerGetSpecificPageResponse>(rawResponse);
                            }
                        }
                    }


                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerPageByPageID";
                        ObjPublishExceptionLog.ExtraInfo =  "blog_id: " + blogID + "page_id: " + pageID + rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        #endregion
                    }

                }

                else
                {
                    objBloggerResponse.error.message = "You have entered invalid parameters";
                }

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Blogger\\GetBloggerPageByPageID";
                ObjPublishExceptionLog.ExtraInfo = "blog_id: " + blogID + "page_id: " + pageID;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            objBloggerGetSpecificPageResponse.error = objBloggerResponse;
            return objBloggerGetSpecificPageResponse;
        }

        #endregion

        #endregion

        #region Private

        private static string WebRequest(HttpWebRequest httpRequest, int DealerLocationSocialNetworkPostID, int DealerLocationSocialNetworkPostQueueID, string MethodName)
        {

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "Blogger\\" + MethodName + "\\WebRequest\\GetResponseStream";
                            ObjPublishExceptionLog.ExtraInfo = "httpRequest: " + httpRequest + "DealerLocationSocialNetworkPostID" + DealerLocationSocialNetworkPostID + "DealerLocationSOcialNetworkPostQueueID" + DealerLocationSocialNetworkPostQueueID + "MethodName" + MethodName + rawResponse;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                            rawResponse = "error: { message:" +  ex.Message +"}}";

                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Blogger\\" + MethodName + "\\WebRequest\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = "httpRequest: " + httpRequest + "DealerLocationSocialNetworkPostID" + DealerLocationSocialNetworkPostID + "DealerLocationSOcialNetworkPostQueueID" + DealerLocationSocialNetworkPostQueueID + "MethodName" + MethodName;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                rawResponse = "error: { message:" + ex.Message + "}}";

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Get(BloggerRequest _bloggerRequest)
        {

            var objPublishManager = new PublishManager();
            var rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "GET"
                var encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_bloggerRequest.RequestURL))
                {
                    var httpRequest = (HttpWebRequest)HttpWebRequest.Create(_bloggerRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_bloggerRequest.Method))
                        httpRequest.Method = _bloggerRequest.Method;

                    httpRequest.Timeout = _bloggerRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Get");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Blogger\\ExecuteCommand_Get";
                ObjPublishExceptionLog.ExtraInfo = "blogger_requestURL: " + _bloggerRequest.RequestURL + "," + "blogger_requestParameters: " + _bloggerRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Post(BloggerRequest _bloggerRequest)
        {
            // This method handles posting in form-urlencoded format, JSON format & also the the "PUT" (Update) command

            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST/DELETE/PUT"
                UTF8Encoding encoding = new UTF8Encoding();

                if (!String.IsNullOrEmpty(_bloggerRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_bloggerRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_bloggerRequest.Method))
                        httpRequest.Method = _bloggerRequest.Method;

                    byte[] postBytes;
                    if (httpRequest.Method == "POST")
                    {
                        httpRequest.ContentType = "application/x-www-form-urlencoded";
                    }

                    else if (httpRequest.Method == "POSTviaJSON")
                    {
                        httpRequest.Method = "POST";
                        httpRequest.ContentType = "application/json";
                    }

                    else if (httpRequest.Method == "PUT")
                    {
                        httpRequest.ContentType = "application/json";
                    }

                    postBytes = encoding.GetBytes(_bloggerRequest.Parameters);
                    using (Stream reqStream = httpRequest.GetRequestStream())
                    {
                        reqStream.Write(postBytes, 0, postBytes.Length);
                        reqStream.Flush();
                        reqStream.Close();
                    }

                    httpRequest.Timeout = _bloggerRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Post");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Blogger\\ExecuteCommand_Post";
                ObjPublishExceptionLog.ExtraInfo = "blogger_requestURL: " + _bloggerRequest.RequestURL + "," + "blogger_requestParameters: " + _bloggerRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;      
        }


        #endregion

        #endregion
    }
}
