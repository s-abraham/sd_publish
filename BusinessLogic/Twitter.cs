﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;
using System.Net;
using DataLayer;
using System.IO;
using BusinessEntities;
using System.Web.Script.Serialization;
using Twitterizer;

namespace BusinessLogic
{
    public class Twitter
    {
        #region variable

        private static string TwitterAPIURLStatusUpdate = Convert.ToString(ConfigurationManager.AppSettings["TwitterAPIURL.Status.Update"]);
        private static string TwitterAPITimeOut = Convert.ToString(ConfigurationManager.AppSettings["TwitterAPITimeOut"]);
        private static string TwitterMaxLength = Convert.ToString(ConfigurationManager.AppSettings["TwitterMaxLength"]);

        //private static string oauth_consumer_key = Convert.ToString(ConfigurationManager.AppSettings["oauth_consumer_key"]);
        //private static string oauth_consumer_secret = Convert.ToString(ConfigurationManager.AppSettings["oauth_consumer_secret"]);
        private static string oauth_version = Convert.ToString(ConfigurationManager.AppSettings["oauth_version"]);
        private static string oauth_signature_method = Convert.ToString(ConfigurationManager.AppSettings["oauth_signature_method"]);
        private static string TwitterAPIURLBaseFormat = Convert.ToString(ConfigurationManager.AppSettings["TwitterAPIURL.BaseFormat"]);
        private static string TwitterAPIURLHeaderFormat = Convert.ToString(ConfigurationManager.AppSettings["TwitterAPIURL.HeaderFormat"]);
        private static string TwitterAPIURLGetTweeterInfo = Convert.ToString(ConfigurationManager.AppSettings["TwitterAPIURL.GetTweeterInfo"]);

        #endregion

        #region Functions

        #region Public

        public static TwitterResponse TwitterStatusUpdate2(string status, string link, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret, long DealerLocationSocialNetworkPostID, long DealerLocationSocialNetworkPostQueueID)
        {
            TwitterResponse objTwitterResponse = new TwitterResponse();
            PublishManager objPublishManager = new PublishManager();
            OAuthTokens tokens = new OAuthTokens();
            var modifiedStatus = string.Empty;
            int maxLength = 0;

            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    
                    if (!string.IsNullOrEmpty(status) || link.Length > 0)
                    {
                        if (string.IsNullOrEmpty(status))
                        {
                            status = link;
                        }
                  
                        int linklength = 0;

                        if(!string.IsNullOrEmpty(link))
                        {
                            linklength  = link.Length;
                        }

                        if (linklength > 0)
                        {
                            maxLength = Convert.ToInt32(TwitterMaxLength) - link.Length - 1;
                            if (status.Length > maxLength)
                            {
                                status = status.Substring(0, maxLength);
                            }
                            status = status + " " + link;
                        }

                        else if (status.Length > Convert.ToInt32(TwitterMaxLength))
                        {
                            status = status.Substring(0, Convert.ToInt32(TwitterMaxLength));
                        }

                        tokens.ConsumerKey = oauth_consumer_key;
                        tokens.ConsumerSecret = oauth_consumer_secret;
                        tokens.AccessToken = oauth_token;
                        tokens.AccessTokenSecret = oauth_token_secret;

                       
                        var tweetResponse = TwitterStatus.Update(tokens, status);

                        
                        if (!String.IsNullOrEmpty(tweetResponse.ErrorMessage))
                        {
                            objTwitterResponse.error = tweetResponse.ErrorMessage;
                            objTwitterResponse.code = "0000";
                        }
                        else if (!String.IsNullOrEmpty(tweetResponse.Content))
                        {
                            if (tweetResponse.Content.StartsWith(@"{""errors"))
                            {
                                objTwitterResponse.error = tweetResponse.Result + ", " + tweetResponse.Content;
                                objTwitterResponse.code = "0000";
                            }
                            else
                            {
                                objTwitterResponse.ID = Convert.ToString(tweetResponse.ResponseObject.Id);
                                objTwitterResponse.error = string.Empty;
                                objTwitterResponse.code = string.Empty;
                            }
                        }
                        else
                        {
                            //objTwitterResponse.ID = Convert.ToString(tweetResponse.ResponseObject.Id);//.StringId;
                            objTwitterResponse.message = tweetResponse.ErrorMessage;

                            objTwitterResponse.error = string.Empty;
                            objTwitterResponse.code = string.Empty;
                        }
                        //if (!String.IsNullOrEmpty(tweetResponse.ErrorMessage))
                        //{
                        //    objTwitterResponse.error = tweetResponse.ErrorMessage;
                        //    objTwitterResponse.code = "0000";
                        //}
                        //else
                        //{
                        //    objTwitterResponse.ID = Convert.ToString(tweetResponse.ResponseObject.Id);//.StringId;
                        //    objTwitterResponse.message = tweetResponse.ErrorMessage;

                        //    objTwitterResponse.error = string.Empty;
                        //    objTwitterResponse.code = string.Empty;
                        //}
                    }
                    else
                    {
                        objTwitterResponse.error = "Missing Required Parameter: Status";
                        objTwitterResponse.code = "0000";
                    }
                }
                else
                {
                    objTwitterResponse.error = "Missing Required Parameter: oauth_token or oauth_token_secret";
                    objTwitterResponse.code = "0000";
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = modifiedStatus + " " + ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Twitter\\TwitterStatusUpdate";
                //ObjPublishExceptionLog.MethodName = "FaceBook\\TwitterStatusUpdate";
                //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog); 
                #endregion
            }

            return objTwitterResponse;
        }

        public static TwitterResponse TwitterStatusUpdate(string status, string link, string oauth_token, string oauth_token_secret,string oauth_consumer_key,string oauth_consumer_secret, long DealerLocationSocialNetworkPostID, long DealerLocationSocialNetworkPostQueueID)
        {
            PublishManager objPublishManager = new PublishManager();
            TwitterRequest objTwitterRequest = new TwitterRequest();
            TwitterResponse objTwitterResponse = new TwitterResponse();
            try
            {
                if (!string.IsNullOrEmpty(status) && !string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    if (string.IsNullOrEmpty(TwitterAPIURLStatusUpdate))
                    {
                        TwitterAPIURLStatusUpdate = "http://api.twitter.com/1.1/statuses/update.json";
                    }
                    if (string.IsNullOrEmpty(TwitterAPITimeOut))
                    {
                        TwitterAPITimeOut = "20000";
                    }

                    string rawResponse = string.Empty;
                    try
                    {
                        status = status.Substring(0, Convert.ToInt32(TwitterMaxLength) > status.Length + link.Length ? status.Length : Convert.ToInt32(TwitterMaxLength) - link.Length - 3);

                        objTwitterRequest.oauth_consumer_key = oauth_consumer_key;
                        objTwitterRequest.oauth_consumer_secret = oauth_consumer_secret;
                        objTwitterRequest.RequestURL = TwitterAPIURLStatusUpdate;
                        objTwitterRequest.oauth_token = oauth_token;
                        objTwitterRequest.oauth_token_secret = oauth_token_secret;
                        objTwitterRequest.Method = "POST";
                        objTwitterRequest.Status = status;
                        objTwitterRequest.link = link;
                        objTwitterRequest.TimeOut = Convert.ToInt32(TwitterAPITimeOut);
                        objTwitterRequest.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                        objTwitterRequest.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);

                        objTwitterRequest.Status = objTwitterRequest.Status.Replace("!", ".").Replace("?", ".") + " " + objTwitterRequest.link;

                        var jss = new JavaScriptSerializer();

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);
                        ObjPublishExceptionLog.ExceptionMessage = objTwitterRequest.Status;
                        ObjPublishExceptionLog.InnerException = string.Empty;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = string.Empty;
                        ObjPublishExceptionLog.MethodName = "Twitter\\TwitterStatusUpdate";
                        ObjPublishExceptionLog.ExtraInfo = string.Empty;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        

                        if (objTwitterRequest.Status.Length <= 140)
                        {
                            rawResponse = ExecuteCommand_StatusUpdate(objTwitterRequest);

                            if (!string.IsNullOrEmpty(rawResponse))
                            {                                
                                objTwitterResponse = jss.Deserialize<TwitterResponse>(rawResponse);
                            }

                        }
                        else
                        {
                            rawResponse = @"{""error"":""Status is over 140 characters."",""request"":""\/1\/statuses\/update.json""}";
                            objTwitterResponse = jss.Deserialize<TwitterResponse>(rawResponse);
                        }

                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "FaceBook\\TwitterStatusUpdate";
                        ObjPublishExceptionLog.ExtraInfo = rawResponse;
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                        if (!string.IsNullOrEmpty(rawResponse))
                        {
                            objTwitterResponse.error = ex.Message;
                            objTwitterResponse.code = string.Empty;
                            objTwitterResponse.type = "Exception\\TwitterStatusUpdate";
                            objTwitterResponse.ID = string.Empty;
                        }

                        #endregion
                    }
                }
                else
                {
                    objTwitterResponse.error = "Missing Parameters.";
                    objTwitterResponse.code = string.Empty;
                    objTwitterResponse.type = "Exception\\TwitterStatusUpdate";
                    objTwitterResponse.ID = string.Empty;

                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(DealerLocationSocialNetworkPostID);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(DealerLocationSocialNetworkPostQueueID);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Twitter\\TwitterStatusUpdate";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                objTwitterResponse.ID = string.Empty;
                objTwitterResponse.error = ex.Message;
                objTwitterResponse.type = "Exception\\TwitterStatusUpdate";
                objTwitterResponse.code = string.Empty;

                #endregion
            }
            return objTwitterResponse;
        }

        //public static TwitterPageInsightsResponse GetTwitterPageInsights(string uniqueID, string token, string screenName)
        //{
        //    PublishManager objPublishManager = new PublishManager();
        //    TwitterPageInsightsResponse objTwitterPageInsightsResponse = new TwitterPageInsightsResponse();

        //    try
        //    {
        //        if (!string.IsNullOrEmpty(uniqueID) && !string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(screenName))
        //        {
        //            if (string.IsNullOrEmpty(TwitterAPIURLGetTweeterInfo))
        //            {
        //                TwitterAPIURLGetTweeterInfo = "http://api.twitter.com/1/users/show.json?user_id={0}&screen_name={1}&oauth_token={2}";
        //            }

        //            string url = string.Format(TwitterAPIURLGetTweeterInfo, uniqueID, screenName, token);

        //            string rawResponse = string.Empty;

        //            try
        //            {
        //                TwitterRequest objTwitterRequest = new TwitterRequest();

        //                objTwitterRequest.RequestURL = url;
        //                objTwitterRequest.Method = "GET";
        //                objTwitterRequest.TimeOut = Convert.ToInt32(TwitterAPITimeOut);

        //                rawResponse = ExecuteCommand_Get(objTwitterRequest);

        //                if (!string.IsNullOrEmpty(rawResponse))
        //                {
        //                    var jss = new JavaScriptSerializer();

        //                    objTwitterPageInsightsResponse = jss.Deserialize<TwitterPageInsightsResponse>(rawResponse);
        //                }
        //            }

        //            catch (Exception ex)
        //            {
        //                #region ERROR HANDLING

        //                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //                ObjPublishExceptionLog.PostResponse = string.Empty;
        //                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //                ObjPublishExceptionLog.MethodName = "BusinessLogic\\Twitter\\GetTwitterPageInsights";
        //                ObjPublishExceptionLog.ExtraInfo = string.Empty;
        //                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
        //                //objTwitterPageInsightsResponse.errors[0].message = ex.Message;
        //                #endregion
        //            }
        //        }

        //        else
        //        {
        //            //objTwitterPageInsightsResponse.errors[0].message = "You have entered invalid parameters";
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
        //        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        ObjPublishExceptionLog.PostResponse = string.Empty;
        //        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
        //        ObjPublishExceptionLog.MethodName = "BusinessLogic\\Twitter\\GetTwitterPageInsights";
        //        ObjPublishExceptionLog.ExtraInfo = string.Empty;
        //        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

        //        #endregion
        //    }

        //    return objTwitterPageInsightsResponse;
        //}

        public static TwitterPageInsightsResponse GetTwitterPageInsights(string uniqueID, string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            TwitterPageInsightsResponse objTwitterPageInsightsResponse = new TwitterPageInsightsResponse();

            TwitterResponse objTwitterResponse = new TwitterResponse();
            PublishManager objPublishManager = new PublishManager();
            OAuthTokens tokens = new OAuthTokens();

            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    tokens.ConsumerKey = oauth_consumer_key;
                    tokens.ConsumerSecret = oauth_consumer_secret;
                    tokens.AccessToken = oauth_token;
                    tokens.AccessTokenSecret = oauth_token_secret;

                    var tweetResponse = TwitterUser.Show(tokens, Convert.ToDecimal(uniqueID));
                    var jss = new JavaScriptSerializer();

                    if (!String.IsNullOrEmpty(tweetResponse.ErrorMessage))
                    {
                        objTwitterResponse.error = tweetResponse.ErrorMessage;
                        objTwitterResponse.code = "0000";

                        try
                        {
                            objTwitterPageInsightsResponse = jss.Deserialize<TwitterPageInsightsResponse>(tweetResponse.Content);
                        }
                        catch (Exception ex)
                        {
                            objTwitterResponse.error = objTwitterResponse.error + " ,Exception : " + ex.Message;
                        }
                        
                    }
                    else
                    {                        
                        objTwitterPageInsightsResponse = jss.Deserialize<TwitterPageInsightsResponse>(tweetResponse.Content);
                    }
                }
                else
                {
                    objTwitterResponse.error = "Missing Required Parameter: oauth_token or oauth_token_secret";
                    objTwitterResponse.code = "0000";
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = Convert.ToInt32(0);
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(0);
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "FaceBook\\TwitterStatusUpdate";
                //ObjPublishExceptionLog.ExtraInfo = rawResponse;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
                #endregion
            }

            objTwitterPageInsightsResponse.errors = objTwitterResponse;

            return objTwitterPageInsightsResponse;
        }

        public static bool VerifyTwitterToken(string oauth_token, string oauth_token_secret, string oauth_consumer_key, string oauth_consumer_secret)
        {
            bool result = false;
            PublishManager objPublishManager = new PublishManager();
            OAuthTokens tokens = new OAuthTokens();

            try
            {
                if (!string.IsNullOrEmpty(oauth_token) && !string.IsNullOrEmpty(oauth_token_secret))
                {
                    tokens.ConsumerKey = oauth_consumer_key;
                    tokens.ConsumerSecret = oauth_consumer_secret;
                    tokens.AccessToken = oauth_token;
                    tokens.AccessTokenSecret = oauth_token_secret;

                    var tokenStatusResponse = TwitterAccount.VerifyCredentials(tokens);

                    if (tokenStatusResponse.Result.ToString().ToLower() == "success")
                    {
                        result = true;
                    }
                }

            }
            catch (Exception ex)
            {
                #region ErrorHandler
                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Twitter\\VerifyTwitterToken";                
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
                #endregion
            }

            return result;
        }

        public static void ValidateALLTwitterTokens(int DealerLocationID)
        {
            PublishManager objPublishManager = new PublishManager();
            SocialDealerManager objSocialDealerManager = new SocialDealerManager();
            try
            {
                var twTokenResults = objSocialDealerManager.GetSocialNetWorkByDealerLocationID(DealerLocationID, 3);
                bool isValid = false;
                int accountTWCounter = 0;
                int activeTWCounter = 0;
                int inActiveTWCounter = 0;

                foreach (var twTokenResult in twTokenResults)
                {
                    accountTWCounter++;
                    try
                    {
                        isValid = VerifyTwitterToken(twTokenResult.Token, twTokenResult.TokenSecret, twTokenResult.AppKey, twTokenResult.AppSecret);
                        if (isValid == true)
                        {
                            activeTWCounter++;
                            objSocialDealerManager.UpdateTokenStatus(true, string.Empty, twTokenResult.ID);
                            //context.spValidateTokenStatus(true, twTokenResult.ID);
                        }
                        else
                        {
                            inActiveTWCounter++;
                            objSocialDealerManager.UpdateTokenStatus(false, string.Empty, twTokenResult.ID);
                            //context.spValidateTokenStatus(false, twTokenResult.ID);
                        }
                    }

                    catch (Exception ex)
                    {
                        #region ErrorHandler
                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                        ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjPublishExceptionLog.PostResponse = string.Empty;
                        ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                        ObjPublishExceptionLog.MethodName = "Twitter\\ValidateALLTwitterTokens";
                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
                        #endregion
                    }
                }

                //PublishServiceLog ObjPublishServiceLog = new PublishServiceLog();
                //ObjPublishServiceLog.Message = "Validating Twitter Tokens: " + activeTWCounter + " Valid & " + inActiveTWCounter + " Invalid";
                //ObjPublishServiceLog.Exception = "Total FB Accounts Validated: " + accountTWCounter;
                //ObjPublishServiceLog.MethodName = "Twitter\\ValidateAllTwitterTokens";
                //objPublishManager.AddPublishServiceLog(ObjPublishServiceLog); 
            }

            catch (Exception ex)
            {
                #region ErrorHandler
                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Twitter\\ValidateALLTwitterTokens";
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
                #endregion
            }
        }
                
        #endregion

        #region Private

        private static string ExecuteCommand_StatusUpdate(TwitterRequest _TwitterRequest)
        {
            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "POST/DELETE"

                if (string.IsNullOrEmpty(oauth_version))
                {
                    oauth_version = "1.0";
                }
                if (string.IsNullOrEmpty(oauth_signature_method))
                {
                    oauth_signature_method = "HMAC-SHA1";
                }
                if (string.IsNullOrEmpty(TwitterAPIURLBaseFormat))
                {
                    // create oauth signature
                    TwitterAPIURLBaseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&status={6}";
                }
                if (string.IsNullOrEmpty(TwitterAPIURLHeaderFormat))
                {
                    // create the request header
                    TwitterAPIURLHeaderFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                                       "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                                       "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                                       "oauth_version=\"{6}\"";
                }

                // unique request details
                var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
                var timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
                                
                string baseString = string.Format(TwitterAPIURLBaseFormat, _TwitterRequest.oauth_consumer_key, oauth_nonce, oauth_signature_method, oauth_timestamp, _TwitterRequest.oauth_token, oauth_version, Uri.EscapeDataString(_TwitterRequest.Status));

                baseString = string.Concat("POST&", Uri.EscapeDataString(TwitterAPIURLStatusUpdate), "&", Uri.EscapeDataString(baseString));
                
                var compositeKey = string.Concat(Uri.EscapeDataString(_TwitterRequest.oauth_consumer_secret), "&", Uri.EscapeDataString(_TwitterRequest.oauth_token_secret));



                string oauth_signature;
                using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
                {
                    oauth_signature = Convert.ToBase64String(
                        hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
                }

                var authHeader = string.Format(TwitterAPIURLHeaderFormat,
                                                Uri.EscapeDataString(oauth_nonce),
                                                Uri.EscapeDataString(oauth_signature_method),
                                                Uri.EscapeDataString(oauth_timestamp),
                                                Uri.EscapeDataString(_TwitterRequest.oauth_consumer_key),
                                                Uri.EscapeDataString(_TwitterRequest.oauth_token),
                                                Uri.EscapeDataString(oauth_signature),
                                                Uri.EscapeDataString(oauth_version));

                // make the request
                var postBody = "status=" + Uri.EscapeDataString(_TwitterRequest.Status);                
                ServicePointManager.Expect100Continue = false;

                if (!String.IsNullOrEmpty(_TwitterRequest.RequestURL))
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(_TwitterRequest.RequestURL);
                    httpRequest.Headers.Add("Authorization", authHeader);
                    httpRequest.Method = _TwitterRequest.Method;
                    httpRequest.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";// "application/x-www-form-urlencoded";
                    httpRequest.Timeout = _TwitterRequest.TimeOut;

                    using (Stream stream = httpRequest.GetRequestStream())
                    {
                        byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                        stream.Write(content, 0, content.Length);
                    }
                    rawResponse = WebRequest(httpRequest, _TwitterRequest.DealerLocationSocialNetworkPostID, _TwitterRequest.DealerLocationSocialNetworkPostQueueID, "ExecuteCommand_StatusUpdate");
                }

                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = _TwitterRequest.DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = _TwitterRequest.DealerLocationSocialNetworkPostQueueID;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Twitter\\ExecuteCommand_StatusUpdate";
                ObjPublishExceptionLog.ExtraInfo = _TwitterRequest.RequestURL;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        private static string WebRequest(HttpWebRequest httpRequest, int DealerLocationSocialNetworkPostID, int DealerLocationSocialNetworkPostQueueID, string MethodName)
        {
            PublishManager objPublishManager = new PublishManager();
            string rawResponse = string.Empty;
            try
            {
                //Get Response

                using (HttpWebResponse svcResponse = (HttpWebResponse)httpRequest.GetResponse())
                {
                    using (Stream responseStream = svcResponse.GetResponseStream())
                    {
                        try
                        {
                            using (StreamReader readr = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                rawResponse = readr.ReadToEnd();
                                rawResponse = rawResponse.Trim();
                            }
                        }
                        catch (Exception ex)
                        {
                            #region ERROR HANDLING

                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                            ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                            ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                            ObjPublishExceptionLog.PostResponse = string.Empty;
                            ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                            ObjPublishExceptionLog.MethodName = "Twitter\\" + MethodName + "\\WebResust\\GetResponseStream";
                            ObjPublishExceptionLog.ExtraInfo = string.Empty;
                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);
                                                        
                            rawResponse = @"{""error"":" + "" + ex.Message + "" + @", ""request"":""\/1\/statuses\/update.json""}";

                            #endregion
                        }
                    }

                }
            }
            catch (WebException webex)
            {
                using (var reader = new StreamReader(webex.Response.GetResponseStream()))
                {
                    var responseText = reader.ReadToEnd();
                    rawResponse = responseText;
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = DealerLocationSocialNetworkPostID;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = DealerLocationSocialNetworkPostQueueID;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Twitter\\" + MethodName + "\\WebRequest\\GetResponse";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                rawResponse = @"{""error"":" + "" + ex.Message + "" + @", ""request"":""\/1\/statuses\/update.json""}";

                #endregion
            }

            return rawResponse;
        }

        private static string ExecuteCommand_Get(TwitterRequest _twitterRequest)
        {

            var objPublishManager = new PublishManager();
            var rawResponse = string.Empty;
            try
            {
                #region HTTP REQUEST "GET"
                var encoding = new UTF8Encoding();
                if (!String.IsNullOrEmpty(_twitterRequest.RequestURL))
                {
                    var httpRequest = (HttpWebRequest)HttpWebRequest.Create(_twitterRequest.RequestURL);

                    if (!String.IsNullOrEmpty(_twitterRequest.Method))
                        httpRequest.Method = _twitterRequest.Method;

                    httpRequest.Timeout = _twitterRequest.TimeOut;
                    httpRequest.Credentials = CredentialCache.DefaultCredentials;

                    rawResponse = WebRequest(httpRequest, 0, 0, "ExecuteCommand_Get");
                }
                #endregion

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "Twitter\\ExecuteCommand_Get";
                ObjPublishExceptionLog.ExtraInfo = _twitterRequest.RequestURL + "," + _twitterRequest.Parameters;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }

            return rawResponse;
        }

        #endregion

        #endregion
    }
}
