﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataLayer;
using System.Data.Objects;
using DataLayerInsights;
using spGetDealerLocationSocialNetworkList_Result = DataLayer.spGetDealerLocationSocialNetworkList_Result;

namespace BusinessLogic
{
    public class InsightStorageRTManager
    {
        private InsightStorageRTEntities dbContext = new InsightStorageRTEntities();

        public InsightStorageRTManager()
        {
        }

        public void AddExceptionLog(ExceptionLog entity)
        {
            try
            {
                entity.CreatedDate = DateTime.Now;

                ExceptionLog _ExceptionLog = new ExceptionLog();
                _ExceptionLog.CreatedDate = entity.CreatedDate;
                _ExceptionLog.DealerLocationSocialNetworkID = entity.DealerLocationSocialNetworkID;
                _ExceptionLog.DealerLocationSocialNetworkPostID = entity.DealerLocationSocialNetworkPostID;
                _ExceptionLog.DealerLocationSocialNetworkPostQueueID = entity.DealerLocationSocialNetworkPostQueueID;
                _ExceptionLog.ExceptionMessage = entity.ExceptionMessage;
                _ExceptionLog.InnerException = entity.InnerException;
                _ExceptionLog.PostResponse = entity.PostResponse;
                _ExceptionLog.StackTrace = entity.StackTrace;
                _ExceptionLog.MethodName = entity.MethodName;
                _ExceptionLog.ExtraInfo = entity.ExtraInfo;

                dbContext.AddToExceptionLogs(_ExceptionLog);
                dbContext.SaveChanges();

                // _dc.clientalert_errors.InsertOnSubmit(entity);
            }
            catch (Exception ex)
            {

            }
        }

        public List<spGetDealerLocationSocialNetworkList_Result> GetDealerLocationSocialNetworkList(int socialNetworkId, string DealerLocationIDs)
        {
            List<spGetDealerLocationSocialNetworkList_Result> listspGetDealerLocationSocialNetworkList_Result = new List<spGetDealerLocationSocialNetworkList_Result>();
            try
            {
                ObjectResult<spGetDealerLocationSocialNetworkList_Result> objspGetDealerLocationSocialNetworkList_Result = dbContext.spGetDealerLocationSocialNetworkList(socialNetworkId, DealerLocationIDs);

                listspGetDealerLocationSocialNetworkList_Result = objspGetDealerLocationSocialNetworkList_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\GetDealerLocationSocialNetworkList";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }

            return listspGetDealerLocationSocialNetworkList_Result;
        }

        public void SaveFBPageDataXML(string pageDataXML, int dealerLocationSocialNetworkId, string uniqueID)
        {
            try
            {
                dbContext.spSaveFBPageDataXML(pageDataXML, dealerLocationSocialNetworkId, uniqueID);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveFBPageDataXML";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }

        public void SaveFBPageInsightXML(string pageDataXML, int dealerLocationSocialNetworkId, string uniqueID)
        {
            try
            {
                dbContext.spSaveFBPageInsightXML(pageDataXML, dealerLocationSocialNetworkId, uniqueID);
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveFBPageInsightXML";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }

        public bool SaveFBInsightData(InsightModel insightModel)
        {
            var result = false;
            try
            {
                var insertId = dbContext.spFacebookDetailInsert(
                    insightModel.DateofQuery,
                    insightModel.LocationId,
                    insightModel.DateUpdated,
                    insightModel.UniqueId.Trim(),
                    Convert.ToInt32(insightModel.page_fan_adds_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_fan_adds ?? "0"),
                    Convert.ToInt32(insightModel.page_fan_removes_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_fan_removes ?? "0"),
                    Convert.ToInt32(insightModel.page_subscriber_adds_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_subscriber_adds ?? "0"),
                    Convert.ToInt32(insightModel.page_subscriber_removes_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_subscriber_removes ?? "0"),
                    Convert.ToInt32(insightModel.page_views_login_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_views_login ?? "0"),
                    Convert.ToInt32(insightModel.page_views_logout ?? "0"),
                    Convert.ToInt32(insightModel.page_views ?? "0"),
                    insightModel.page_tab_views_login_top_unique,
                    insightModel.page_tab_views_login_top,
                    insightModel.page_tab_views_logout_top,
                    insightModel.page_views_internal_referrals,
                    insightModel.page_views_external_referrals,
                    Convert.ToInt32(insightModel.page_story_adds_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_story_adds ?? "0"),
                    insightModel.page_story_adds_by_story_type_unique,
                    insightModel.page_story_adds_by_story_type,
                    insightModel.page_impressions_by_age_gender_unique,
                    insightModel.page_impressions_by_country_unique,
                    insightModel.page_impressions_by_locale_unique,
                    insightModel.page_impressions_by_city_unique,
                    insightModel.page_story_adds_by_age_gender_unique,
                    insightModel.page_story_adds_by_country_unique,
                    insightModel.page_story_adds_by_city_unique,
                    insightModel.page_story_adds_by_locale_unique,
                    Convert.ToInt32(insightModel.page_impressions_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_impressions ?? "0"),
                    Convert.ToInt32(insightModel.page_impressions_paid_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_impressions_paid ?? "0"),
                    Convert.ToInt32(insightModel.page_impressions_organic_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_impressions_organic ?? "0"),
                    Convert.ToInt32(insightModel.page_impressions_viral_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_impressions_viral ?? "0"),
                    insightModel.page_impressions_by_story_type_unique,
                    insightModel.page_impressions_by_story_type,
                    Convert.ToInt32(insightModel.page_places_checkin_total ?? "0"),
                    Convert.ToInt32(insightModel.page_places_checkin_total_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_places_checkin_mobile ?? "0"),
                    Convert.ToInt32(insightModel.page_places_checkin_mobile_unique ?? "0"),
                    insightModel.page_places_checkins_by_age_gender,
                    insightModel.page_places_checkins_by_country,
                    insightModel.page_places_checkins_by_city,
                    insightModel.page_places_checkins_by_locale,
                    Convert.ToInt32(insightModel.page_posts_impressions_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_posts_impressions ?? "0"),
                    Convert.ToInt32(insightModel.page_posts_impressions_paid_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_posts_impressions_paid ?? "0"),
                    Convert.ToInt32(insightModel.page_posts_impressions_organic_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_posts_impressions_organic ?? "0"),
                    Convert.ToInt32(insightModel.page_posts_impressions_viral_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_posts_impressions_viral ?? "0"),
                    Convert.ToInt32(insightModel.page_consumptions_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_consumptions ?? "0"),
                    insightModel.page_consumptions_by_consumption_type_unique,
                    insightModel.page_consumptions_by_consumption_type,
                    insightModel.page_fans_by_like_source_unique,
                    insightModel.page_fans_by_like_source,
                    insightModel.page_subscribers_by_subscribe_source_unique,
                    insightModel.page_subscribers_by_subscribe_source,
                    Convert.ToInt32(insightModel.page_negative_feedback_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_negative_feedback ?? "0"),
                    insightModel.page_negative_feedback_by_type_unique,
                    insightModel.page_negative_feedback_by_type,
                    Convert.ToInt32(insightModel.page_fans ?? "0"),
                    insightModel.page_fans_locale,
                    insightModel.page_fans_city,
                    insightModel.page_fans_country,
                    insightModel.page_fans_gender,
                    insightModel.page_fans_age,
                    insightModel.page_fans_gender_age,
                    Convert.ToInt32(insightModel.page_friends_of_fans ?? "0"),
                    Convert.ToInt32(insightModel.page_subscribers ?? "0"),
                    insightModel.page_subscribers_locale,
                    insightModel.page_subscribers_city,
                    insightModel.page_subscribers_country,
                    insightModel.page_subscribers_gender_age,
                    Convert.ToInt32(insightModel.page_storytellers ?? "0"),
                    insightModel.page_storytellers_by_story_type,
                    insightModel.page_storytellers_by_age_gender,
                    insightModel.page_storytellers_by_country,
                    insightModel.page_storytellers_by_city,
                    insightModel.page_storytellers_by_locale,
                    Convert.ToInt32(insightModel.page_engaged_users ?? "0"),
                    insightModel.page_impressions_frequency_distribution,
                    insightModel.page_impressions_viral_frequency_distribution,
                    insightModel.page_posts_impressions_frequency_distribution,
                    Convert.ToInt32(insightModel.page_views_unique ?? "0"),
                    Convert.ToInt32(insightModel.page_stories ?? "0"),
                    insightModel.page_stories_by_story_type,
                    Convert.ToInt32(insightModel.page_admin_num_posts ?? "0"),
                    insightModel.page_admin_num_posts_by_type
                ).SingleOrDefault() ?? 0;

                if (insertId > 0)
                {
                    result = true;
                }
                
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveFBPageDataXML";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
            return result;
        }

        public List<spGetFBWorkListforStatistics_Result> GetFBWorkListforStatistics(int dealerLocationID, Guid userID, int FromHours, int ToHours)
        {
            List<spGetFBWorkListforStatistics_Result> listspGetFBWorkListforStatistics_Result = new List<spGetFBWorkListforStatistics_Result>();
            try
            {
                ObjectResult<spGetFBWorkListforStatistics_Result> objspGetFBWorkListforStatistics_Result = dbContext.spGetFBWorkListforStatistics(dealerLocationID, userID,FromHours, ToHours);

                listspGetFBWorkListforStatistics_Result = objspGetFBWorkListforStatistics_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\GetFBWorkListforStatistics";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }

            return listspGetFBWorkListforStatistics_Result;
        }

        public void SaveFBPostDataXML(string postDataXML, string PostInsightXML, int postQueueId, string resultId)
        {   
            try
            {
                dbContext.spSaveFBPostDataXML(postDataXML,PostInsightXML, postQueueId, resultId);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveFBPostDataXML";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
            
        }

        public void SaveFBEventDataXML(string EventDataXML, int postQueueId, string resultId)
        {   
            try
            {
                dbContext.spSaveFBEventDataXML(EventDataXML, postQueueId, resultId);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveFBEventDataXML";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
            
        }

        public void SaveFBQuestionDataXML(string FBQuestionDataXML, int postQueueId, string resultId)
        {   
            try
            {
                dbContext.spSaveFBQuestionDataXML(FBQuestionDataXML, postQueueId, resultId);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveFBQuestionDataXML";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
            
        }


        public void SaveGooglePlusPage(int googlePlusPageId, int dealerLocationSocialNetworkId, string uniqueID, string googlePlusName, int plusOneCount, int followerCount, int postCount, string post_Error, string plusOne_Follower_Error)
        {
            try
            {
                dbContext.spSaveGooglePlusPage(googlePlusPageId, dealerLocationSocialNetworkId, uniqueID, googlePlusName, plusOneCount, followerCount, postCount, post_Error, plusOne_Follower_Error);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveGooglePlusPage";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }

        }

        public void SaveTwitterPageInsight(int TwitterPageInsightId, int dealerLocationSocialNetworkId, string uniqueID, string twitterScreenName, int tweetsCount, int followingCount, int followersCount, int listedCount, int favoritesCount, int friendsCount, int retweetsCount, string error)
        {
            try
            {
                dbContext.spSaveTwitterPageInsight(TwitterPageInsightId, dealerLocationSocialNetworkId, uniqueID, twitterScreenName, tweetsCount, followingCount, followersCount, listedCount, favoritesCount, friendsCount, retweetsCount, error);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveTwitterPageInsight";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }

        public void SaveYouTubePageInsight(int youTubePageId, int dealerLocationSocialNetworkId, string uniqueID, int subscriberCount, int channelViewsCount, int totalUploadViews, int videosCount, int subscriptionsCount, int favoritesCount, int contactsCount, string error)
        {
            try
            {
                dbContext.spSaveYouTubePageInsight(youTubePageId, dealerLocationSocialNetworkId, uniqueID, subscriberCount, channelViewsCount, totalUploadViews, videosCount, subscriptionsCount, favoritesCount, contactsCount, error);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\SaveYouTubePageInsight";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }

        public void InsertMessageSyndicationByPost(string postQueueID, string InsertToStreamTable)
        {
            try
            {
                dbContext.CommandTimeout = 500;
                dbContext.spInsertMessageSyndicationByPost(postQueueID, InsertToStreamTable.ToLower());
                dbContext.CommandTimeout = null;
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\InsertMessageSyndicationByPost";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }

        public void InsertMessageSyndicationByPostQueue(string postQueueID, string InsertToStreamTable)
        {
            try
            {
                dbContext.CommandTimeout = 500;
                dbContext.spInsertMessageSyndicationByPostQueue(postQueueID, InsertToStreamTable.ToLower());
                dbContext.CommandTimeout = null;

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\InsertMessageSyndicationByPostQueue";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }

        

        public void InsertServiceLog(string serviceName, string message)
        {
            try
            {
                dbContext.spInsertServiceLog(serviceName, message);

            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "InsightStorageRTManager\\InsertServiceLog";
                ObjExceptionLog.ExtraInfo = string.Empty;

                AddExceptionLog(ObjExceptionLog);

                #endregion
            }
        }
        
    }
}
