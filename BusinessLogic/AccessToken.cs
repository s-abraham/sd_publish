﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic
{
    public class AccessToken
    {
        private Dictionary<int, string> AccessTokens = new Dictionary<int, string>();

        public AccessToken()
        {
            AccessTokens.Add(1, "AAAAANqD36fQBAC9dnizzkFnOQyHtAtAMr5VMWW7cPcfldz9PR8KFwXvdAsuaoadLWJZAwu1OzMUuHiVEml8tzOVxHmAUchLto59Ge3DRvo6Guc7li"); // ABCD of Automobile (Arpan)
            AccessTokens.Add(2, "AAAAANqD36fQBANQBvHvMtAV4wcpXnPvHk7rIuMIR7MaLgUhUMPpZBju6BamPrZBiGWkcnNMvJqRWqmrcZCMqHzZAHkvsKHRlgGAXCAdm6WR5DwbDL1E6"); //Elmhurst Hospital (Lijin)
            AccessTokens.Add(3, "AAAAANqD36fQBALKrCTdhNvHw5mOai3ZBfCPewLCKjQZACoX9pM5wZCUZBnZBVs6tWGzxwQZCMJZCKA3gfLU0Mc10SenLdOUz5GN29G9fZBh8URAdlQGjEuWU"); // Anjali's Place (Sanju)
            AccessTokens.Add(4, "AAAAANqD36fQBAMox6k5t4cfm7QTW7vADObwtnbvZB1JM5c3ysECHLcGZA6u3RCK1AEpe3pH58US6oem4UjthuKkOVP0U2ZC109en225ZBaapf6Je9N1z"); // Aridanisk (Sanju)
            AccessTokens.Add(5, "AAAAANqD36fQBALeE8fTxRQ3kSfoZBslyqSwGgiUxvZAGoZAXZBLKgQVOy5Pp17oijgUR07k8lrw5hVZBnodY9GRsYfsCKcLoNVphbxg9aA1J5q3y88zM0"); //AM Automobile  (Arpan)
            AccessTokens.Add(6, "AAAAANqD36fQBAH8I6Lsf6w1TulC3ZAMZB4kxDZCb0nqHt5PVVSJhTwyG7gRvrZBwZBduiKVZAhkcv54fXJxWytZCinFawvNoTSxRhw755Tk6tWGNZBPo7ld6"); //Dave Motors  (Lijin)            
            AccessTokens.Add(7, "AAAAANqD36fQBAN8QXfUCJsuiJKwQX3ZA9SeNJaVQVM8enAf1iXzslL5xMCSsTjoBTU4IuZADMqKX5DNX4ZBuYq3EVNUFkUoKB0CKEyXjjNGv6SVIZCO0"); //Fancy Motors  (Lijin)
            AccessTokens.Add(8, "AAAAANqD36fQBAMTHZBs9Wv6u04UVkYCPoFZBKqeuZC6sB96w1NZCrIROLvB1eZAT9MqTaxRHUaKJHAIpsQHs2t7X79feZCkSqTbXbN6RY4KXhLlJZC07JBE"); //Bluernie  ()

            //Password May Changed.
            //AccessTokens.Add(7, "AAAAANqD36fQBAENTAq9BZBZA8MKx7BovKXceGzP0LZB4LZANmMZAXhd7UYkoYTol6mPsvq4nMRZBDtJWuvK7fsSEmhnZBAtUdBGGBd4urn3Of4JOXY2R9oq"); //Poppy Seeds House  (Phil)                       
            
        }

        public string GetRandomAccessToken()
        {
            var result = string.Empty;
            var random = new Random();
            var randomNumber = random.Next(1, 8);

            try
            {
                result = AccessTokens[randomNumber];
            }
            catch (Exception ex)
            {
                // Add Exception               
            }
            return result;
        }
    }
}
