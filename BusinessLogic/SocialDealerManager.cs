﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data.Objects;

namespace BusinessLogic
{
    public class SocialDealerManager
    {
        private SocialDealerEntities dbContext = new SocialDealerEntities();


        public spGetSocialRepCardStatsbyLocationAndDate_Result1 GetSocialRepCardStatsbyLocationAndDate(int dealerLocationID, int month, int year)
        {
            spGetSocialRepCardStatsbyLocationAndDate_Result1 listspGetSocialRepCardStatsbyLocationAndDate_Result1 = new spGetSocialRepCardStatsbyLocationAndDate_Result1();
            try
            {
                ObjectResult<spGetSocialRepCardStatsbyLocationAndDate_Result1> objspGetSocialRepCardStatsbyLocationAndDate_Result1 = dbContext.spGetSocialRepCardStatsbyLocationAndDate(dealerLocationID, month, year);

                listspGetSocialRepCardStatsbyLocationAndDate_Result1 = objspGetSocialRepCardStatsbyLocationAndDate_Result1.FirstOrDefault();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                #endregion
            }

            return listspGetSocialRepCardStatsbyLocationAndDate_Result1;
        }

        public List<spGetReviewsByLocation_Result> GetReviewsByLocation(int dealerLocationID)
        {
            List<spGetReviewsByLocation_Result> listspGetReviewsByLocation_Result = new List<spGetReviewsByLocation_Result>();
            try
            {
                ObjectResult<spGetReviewsByLocation_Result> objspGetReviewsByLocation_Result = dbContext.spGetReviewsByLocation(dealerLocationID);

                listspGetReviewsByLocation_Result = objspGetReviewsByLocation_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                #endregion
            }

            return listspGetReviewsByLocation_Result;
        }

        public spGetRepReportCardByLocationIDAndDate_Result GetRepReportCardByLocationIDAndDate(int dealerLocationID,int month, int year)
        {
            spGetRepReportCardByLocationIDAndDate_Result listspGetRepReportCardByLocationIDAndDate_Result = new spGetRepReportCardByLocationIDAndDate_Result();
            try
            {
                ObjectResult<spGetRepReportCardByLocationIDAndDate_Result> objspGetRepReportCardByLocationIDAndDate_Result = dbContext.spGetRepReportCardByLocationIDAndDate(dealerLocationID, month, year);

                listspGetRepReportCardByLocationIDAndDate_Result = objspGetRepReportCardByLocationIDAndDate_Result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                #endregion
            }

            return listspGetRepReportCardByLocationIDAndDate_Result;
        }


        public List<spGetSocialNetWorkByDealerLocationID_Result> GetSocialNetWorkByDealerLocationID(int dealerLocationID, int socialNetworkID)
        {
            List<spGetSocialNetWorkByDealerLocationID_Result> listspGetSocialNetWorkByDealerLocationID_Result = new List<spGetSocialNetWorkByDealerLocationID_Result>();
            try
            {
                ObjectResult<spGetSocialNetWorkByDealerLocationID_Result> objspGetSocialNetWorkByDealerLocationID_Result = dbContext.spGetSocialNetWorkByDealerLocationID(dealerLocationID, socialNetworkID);

                listspGetSocialNetWorkByDealerLocationID_Result = objspGetSocialNetWorkByDealerLocationID_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                #endregion
            }

            return listspGetSocialNetWorkByDealerLocationID_Result;
        }

        public void UpdateTokenStatus(bool TokenStatus, string AppkeyFromFacebook, int SocialNetWorkID)
        {
            
            try
            {
                dbContext.spUpdateTokenStatus(TokenStatus, AppkeyFromFacebook, SocialNetWorkID);
                
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                #endregion
            }            
        }

        public List<spgetposttodelete_Result> getposttodelete()
        {
            List<spgetposttodelete_Result> listspgetposttodelete_Result = new List<spgetposttodelete_Result>();
            try
            {
                ObjectResult<spgetposttodelete_Result> objspgetposttodelete_Result = dbContext.spgetposttodelete();

                listspgetposttodelete_Result = objspgetposttodelete_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                #endregion
            }

            return listspgetposttodelete_Result;
        }
        
        
    }
}
