﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BusinessEntities;

namespace DataLayerInsights
{
    public static class InsightStorageDL
    {
        public static List<WorkList> GetActiveWorkList(int socialNetwork = 2)
        {
            var results = new List<WorkList>();
            var worklist = new WorkList();

            try
            {
                using (var context = new InsightStorageEntities())
                {
                    var rawResults = context.spGetDealerLocationSocialNetworkList(Convert.ToInt32(socialNetwork));

                    foreach (var item in rawResults)
                    {
                        worklist = new WorkList();
                        worklist.LocationId = item.LocationID;
                        worklist.DealerLocationSocialNetworkId = item.ID;
                        worklist.PageFriendlyName = item.PageFriendlyName;
                        worklist.SocialNetworkId = item.SocialNetworkID;
                        worklist.Token = item.Token;
                        worklist.UniqueId = item.UniqueID;
                        worklist.TwitterScreenName = item.TwitterScreenName;
                        results.Add(worklist);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                throw;
            }

            return results;
        }

        //public static object SaveFacebookBasic()
        //{

        //}

        //public static object SaveFacebookInsightDetail()
        //{

        //}
    }
}
