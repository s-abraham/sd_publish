﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using System.Windows.Forms;
using SOCIALDEALER.PublishMSMQService.WinServiceTest;

namespace SOCIALDEALER.PublishMSMQService
{
    static class Program
    {
        private static string EventLogSourceName = Convert.ToString(ConfigurationManager.AppSettings["EventLogSourceName"]);
        private static string EventLogMessage = string.Empty;
        private static EventLog Logger;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (!EventLog.SourceExists(EventLogSourceName))
                {
                    EventLog.CreateEventSource(EventLogSourceName, EventLogSourceName);
                }
                Logger = new EventLog();
                Logger.Source = EventLogSourceName;

                if (args[0].ToLower().Equals("/debug"))
                {
                    Application.Run(new ServiceRunner(new MSMQService()));
                }
            }
            else
            {
                ServiceBase[] ServicesToRun = new ServiceBase[] { new MSMQService() };
                ServiceBase.Run(ServicesToRun);
            }

            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new MSMQService() 
            //};
            //ServiceBase.Run(ServicesToRun);
        }
    }
}
