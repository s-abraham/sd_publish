﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace SOCIALDEALER.PublishMSMQService
{
    [RunInstaller(true)]
    public partial class PublishMSMQServiceInstaller : System.Configuration.Install.Installer
    {
        public PublishMSMQServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
