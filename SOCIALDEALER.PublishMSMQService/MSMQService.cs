﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using SOCIALDEALER.PublishMSMQService.WinServiceTest;
using System.Configuration;
using System.Messaging;
using BusinessEntities;
using BusinessLogic;
using DataLayer;

namespace SOCIALDEALER.PublishMSMQService
{
    partial class MSMQService : ServiceBase, IDebuggableService
    {
        private string stMSMQPrivateQueue = string.Empty;
        private EventLog _logger;
        private string EventLogMessage = string.Empty;
        private string EventLogSourceName = Convert.ToString(ConfigurationManager.AppSettings["EventLogSourceName"]);
        MessageQueue MyMessageQ = null;

        public MSMQService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            _logger = new EventLog { Source = EventLogSourceName };

            stMSMQPrivateQueue = ConfigurationManager.AppSettings["QueueName"];
            MSMQTimer.Enabled = true;
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        private void MSMQTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            MSMQTimer.Enabled = false;
            string strPost = string.Empty;

            //svrNotificationMgr.NotificationMgrClient ObjsvrNotificationMgr = new svrNotificationMgr.NotificationMgrClient();
            try
            {
                using (MyMessageQ = new MessageQueue(stMSMQPrivateQueue))
                {
                    MyMessageQ.Formatter = new XmlMessageFormatter(new string[] { "System.String" });
                    Message MyMessage = MyMessageQ.Receive();
                    if (MyMessage != null)
                    {
                        MSMQTimer.Interval = 2000;
                        if (MyMessage.Label == "SocialNetworkPost")
                        {
                            strPost = MyMessage.Body.ToString();
                            try
                            {
                                if (!string.IsNullOrEmpty(strPost))
                                {
                                    EventLogMessage = String.Format("[PublishMSMQService.MSMQService] MSMQ Reveived, Post  : {0}", strPost);
                                    _logger.WriteEntry(EventLogMessage, EventLogEntryType.Warning);

                                    PublishMSMQ objPublishMSMQ;

                                    objPublishMSMQ = Utility.FromXml<PublishMSMQ>(strPost);

                                    if (objPublishMSMQ != null)
                                    {
                                        if (!string.IsNullOrEmpty(objPublishMSMQ.DealerLocationSocialNetworkPostID) || !string.IsNullOrEmpty(objPublishMSMQ.DealerLocationSocialNetworkPostQueueID))
                                        {
                                            PublishToSocialNetwork.ProcessPosts(objPublishMSMQ);
                                        }
                                        else
                                        {
                                            #region ERROR HANDLING

                                            PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                            PublishManager objPublishManager = new PublishManager();

                                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                                            ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                                            ObjPublishExceptionLog.ExceptionMessage = "DealerLocationSocialNetworkPostID is 0";
                                            ObjPublishExceptionLog.InnerException = string.Empty;
                                            ObjPublishExceptionLog.PostResponse = string.Empty;
                                            ObjPublishExceptionLog.StackTrace = string.Empty;
                                            ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\MSMQTimer_Elapsed";
                                            ObjPublishExceptionLog.ExtraInfo = strPost;

                                            objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region ERROR HANDLING

                                        PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                        PublishManager objPublishManager = new PublishManager();

                                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                                        ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                                        ObjPublishExceptionLog.ExceptionMessage = "Unable to Convert XML to Object of PublishMSMQ";
                                        ObjPublishExceptionLog.InnerException = string.Empty;
                                        ObjPublishExceptionLog.PostResponse = string.Empty;
                                        ObjPublishExceptionLog.StackTrace = string.Empty;
                                        ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\MSMQTimer_Elapsed";
                                        ObjPublishExceptionLog.ExtraInfo = strPost;
                                        
                                        objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                        #endregion
                                    }
                                  
                                    //ObjsvrNotificationMgr.CreateNewNotification(Convert.ToInt32(strReviewNotification));


                                }
                            }
                            catch (Exception ex)
                            {
                                #region ERROR HANDLING

                                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                                PublishManager objPublishManager = new PublishManager();

                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                                ObjPublishExceptionLog.PostResponse = string.Empty;
                                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\MSMQTimer_Elapsed";
                                ObjPublishExceptionLog.ExtraInfo = strPost;

                                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                                #endregion
                            }
                            
                        }
                        MyMessage.Dispose();
                        MyMessage = null;
                    }
                    else
                    {
                        MSMQTimer.Interval = 30000;
                    }
                }
                MyMessageQ = null;
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PublishToSocialNetwork\\MSMQTimer_Elapsed";
                ObjPublishExceptionLog.ExtraInfo = strPost;

                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion

                
            }
            MSMQTimer.Enabled = true;
        }
        #region IDebuggableService Members

        public void Start(string[] args)
        {
            OnStart(args);
        }

        public void StopService()
        {
            OnStop();
        }

        public void Pause()
        {
            OnPause();
        }

        public void Continue()
        {
            OnContinue();
        }

        #endregion        
    }
}
