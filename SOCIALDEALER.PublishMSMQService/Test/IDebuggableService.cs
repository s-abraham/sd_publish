﻿using System.ServiceProcess;

namespace SOCIALDEALER.PublishMSMQService.WinServiceTest
{
    public interface IDebuggableService
    {
        void Start(string[] args);
        void StopService();
        void Pause();
        void Continue();
    }
}
