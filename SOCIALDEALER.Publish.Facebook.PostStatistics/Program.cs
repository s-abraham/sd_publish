﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using BusinessLogic;
using BusinessEntities;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;

namespace SOCIALDEALER.Publish.Facebook.PostStatistics
{
    class Program
    {
        //private static string FacebookAccessTokenFeedID = Convert.ToString(ConfigurationManager.AppSettings["Facebook.AccessToken.FeedID"]);
        private static string FacebookAccessTokenFeedID = string.Empty;
        private static int FromHours = Convert.ToInt32(ConfigurationManager.AppSettings["FromHours"]);
        private static int ToHours = Convert.ToInt32(ConfigurationManager.AppSettings["ToHours"]);
        private static string UserID = Convert.ToString(ConfigurationManager.AppSettings["UserID"]);
        private static string ServiceName = Convert.ToString(ConfigurationManager.AppSettings["ServiceName"]);
        private static string InsertToStreamTable = Convert.ToString(ConfigurationManager.AppSettings["InsertToStreamTable"]);
        
        static void Main(string[] args)
        {
            //delete objdelete = new delete();

            //objdelete.test();

            //GetFacebookPostStatistics();
            GetCarFaxFacebookCombinedPostStatistics();
            

        }
                
        //private static void GetFacebookPostStatistics()
        //{
        //    InsightStorageRTManager objInsightStorageRTManager = new InsightStorageRTManager();
        //    Guid userID = new Guid("00000000-0000-0000-0000-000000000000");
        //    try
        //    {
        //        List<spGetFBWorkListforStatistics_Result> listspGetFBWorkListforStatistics_Result = objInsightStorageRTManager.GetFBWorkListforStatistics(-1, userID, FromHours, ToHours);
        //        int iCounter = 1;
        //        foreach (var spGetFBWorkListforStatistics_Result in listspGetFBWorkListforStatistics_Result)
        //        {
        //            try
        //            {
        //                string strxml = string.Empty;
        //                Console.WriteLine(iCounter + " : Process Statistics for : " + spGetFBWorkListforStatistics_Result.ResultID);
        //                if (spGetFBWorkListforStatistics_Result.PostTypeId == Convert.ToInt32(PostType.FacebookStatus) || spGetFBWorkListforStatistics_Result.PostTypeId == Convert.ToInt32(PostType.FacebookLink))
        //                {
        //                    FacebookPostStatistics objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(spGetFBWorkListforStatistics_Result.ResultID, spGetFBWorkListforStatistics_Result.Token,Convert.ToInt32(spGetFBWorkListforStatistics_Result.PostTypeId));
        //                    strxml = Utility.SerializeObject<FacebookPostStatistics>(objFacebookPostStatistics);
                            
        //                    if (!string.IsNullOrEmpty(strxml))
        //                    {
        //                        objInsightStorageRTManager.SaveFBPostDataXML(Utility.Trim(strxml),string.Empty, Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID), spGetFBWorkListforStatistics_Result.ResultID);
        //                    }
        //                    else
        //                    {
        //                        #region ERROR HANDLING

        //                        ExceptionLog ObjExceptionLog = new ExceptionLog();
        //                        ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID);
        //                        ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML for " + spGetFBWorkListforStatistics_Result.PostTypeId;
        //                        ObjExceptionLog.InnerException = string.Empty;
        //                        ObjExceptionLog.PostResponse = string.Empty;
        //                        ObjExceptionLog.StackTrace = string.Empty;
        //                        ObjExceptionLog.MethodName = "Program\\GetSetPostStatistics\\SerializeObject";
        //                        ObjExceptionLog.ExtraInfo = "ResultID : " + spGetFBWorkListforStatistics_Result.ResultID + " , UniqueID : " + spGetFBWorkListforStatistics_Result.UniqueID + ", Access Token : " + spGetFBWorkListforStatistics_Result.Token;

        //                        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //                        #endregion
        //                    }
        //                }
        //                else if (spGetFBWorkListforStatistics_Result.PostTypeId == Convert.ToInt32(PostType.FacebookPhoto) || spGetFBWorkListforStatistics_Result.PostTypeId == Convert.ToInt32(PostType.FacebookMultiplePhoto))
        //                {
        //                    FacebookPostStatistics objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(spGetFBWorkListforStatistics_Result.ResultID, spGetFBWorkListforStatistics_Result.Token, Convert.ToInt32(spGetFBWorkListforStatistics_Result.PostTypeId));
        //                    strxml = Utility.SerializeObject<FacebookPostStatistics>(objFacebookPostStatistics);
                            
        //                    if (!string.IsNullOrEmpty(strxml))
        //                    {
        //                        objInsightStorageRTManager.SaveFBPostDataXML(Utility.Trim(strxml),string.Empty, Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID), spGetFBWorkListforStatistics_Result.ResultID);
        //                    }
        //                    else
        //                    {
        //                        #region ERROR HANDLING

        //                        ExceptionLog ObjExceptionLog = new ExceptionLog();
        //                        ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID);
        //                        ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML for " + spGetFBWorkListforStatistics_Result.PostTypeId;
        //                        ObjExceptionLog.InnerException = string.Empty;
        //                        ObjExceptionLog.PostResponse = string.Empty;
        //                        ObjExceptionLog.StackTrace = string.Empty;
        //                        ObjExceptionLog.MethodName = "Program\\GetSetPostStatistics\\SerializeObject";
        //                        ObjExceptionLog.ExtraInfo = "ResultID : " + spGetFBWorkListforStatistics_Result.ResultID + " , UniqueID : " + spGetFBWorkListforStatistics_Result.UniqueID + ", Access Token : " + spGetFBWorkListforStatistics_Result.Token;

        //                        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //                        #endregion
        //                    }
        //                }
        //                else if (spGetFBWorkListforStatistics_Result.PostTypeId == Convert.ToInt32(PostType.FacebookEvent))
        //                {
        //                    FacebookEventStatistics objFacebookEventStatistics = new FacebookEventStatistics();
        //                    objFacebookEventStatistics = FaceBook.GetFacebookEventStatistics(spGetFBWorkListforStatistics_Result.ResultID, spGetFBWorkListforStatistics_Result.Token);
        //                    strxml = Utility.SerializeObject<FacebookEventStatistics>(objFacebookEventStatistics);

        //                    if (!string.IsNullOrEmpty(strxml))
        //                    {
        //                        objInsightStorageRTManager.SaveFBEventDataXML(Utility.Trim(strxml), Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID), spGetFBWorkListforStatistics_Result.ResultID);
        //                    }
        //                    else
        //                    {
        //                        #region ERROR HANDLING

        //                        ExceptionLog ObjExceptionLog = new ExceptionLog();
        //                        ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID);
        //                        ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML for Facebook Event";
        //                        ObjExceptionLog.InnerException = string.Empty;
        //                        ObjExceptionLog.PostResponse = string.Empty;
        //                        ObjExceptionLog.StackTrace = string.Empty;
        //                        ObjExceptionLog.MethodName = "Program\\GetSetPostStatistics\\SerializeObject";
        //                        ObjExceptionLog.ExtraInfo = "ResultID : " + spGetFBWorkListforStatistics_Result.ResultID + " , UniqueID : " + spGetFBWorkListforStatistics_Result.UniqueID + ", Access Token : " + spGetFBWorkListforStatistics_Result.Token;

        //                        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //                        #endregion
        //                    }
        //                }
        //                else if (spGetFBWorkListforStatistics_Result.PostTypeId == Convert.ToInt32(PostType.FacebookQuestion))
        //                {
        //                    FacebookQuestionStatistics objFacebookQuestionStatistics = new FacebookQuestionStatistics();
        //                    objFacebookQuestionStatistics = FaceBook.GetFacebookQuestionStatistics(spGetFBWorkListforStatistics_Result.ResultID, spGetFBWorkListforStatistics_Result.Token);
        //                    strxml = Utility.SerializeObject<FacebookQuestionStatistics>(objFacebookQuestionStatistics);

        //                    if (!string.IsNullOrEmpty(strxml))
        //                    {
        //                        objInsightStorageRTManager.SaveFBQuestionDataXML(Utility.Trim(strxml), Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID), spGetFBWorkListforStatistics_Result.ResultID);
        //                    }
        //                    else
        //                    {
        //                        #region ERROR HANDLING

        //                        ExceptionLog ObjExceptionLog = new ExceptionLog();
        //                        ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID);
        //                        ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML for Facebook Question";
        //                        ObjExceptionLog.InnerException = string.Empty;
        //                        ObjExceptionLog.PostResponse = string.Empty;
        //                        ObjExceptionLog.StackTrace = string.Empty;
        //                        ObjExceptionLog.MethodName = "Program\\GetSetPostStatistics\\SerializeObject";
        //                        ObjExceptionLog.ExtraInfo = "ResultID : " + spGetFBWorkListforStatistics_Result.ResultID + " , UniqueID : " + spGetFBWorkListforStatistics_Result.UniqueID + ", Access Token : " + spGetFBWorkListforStatistics_Result.Token;

        //                        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //                        #endregion
        //                    }
        //                }

        //            }
        //            catch (Exception ex)
        //            {
        //                #region ERROR HANDLING

        //                ExceptionLog ObjExceptionLog = new ExceptionLog();
        //                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID);
        //                ObjExceptionLog.ExceptionMessage = ex.Message;
        //                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //                ObjExceptionLog.PostResponse = string.Empty;
        //                ObjExceptionLog.StackTrace = ex.StackTrace;
        //                ObjExceptionLog.MethodName = "Program\\GetFacebookPostStatistics\\forEach";
        //                ObjExceptionLog.ExtraInfo = "UniqueID : " + spGetFBWorkListforStatistics_Result.UniqueID + ", Access Token : " + spGetFBWorkListforStatistics_Result.Token;

        //                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //                #endregion
        //            }
        //            iCounter++;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        ExceptionLog ObjExceptionLog = new ExceptionLog();
        //        ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //        ObjExceptionLog.ExceptionMessage = ex.Message;
        //        ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        ObjExceptionLog.PostResponse = string.Empty;
        //        ObjExceptionLog.StackTrace = ex.StackTrace;
        //        ObjExceptionLog.MethodName = "Program\\GetFacebookPostStatistics";
        //        ObjExceptionLog.ExtraInfo = string.Empty;

        //        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //        #endregion
        //    }
        //}

        #region CarfaxCombinedPostStats

        public static string GetFeedIDByResultID(string UniqueID, string Token, string FromDate, string ToDate, string limit, string ResultID, string Comment)
        {
            InsightStorageRTManager objInsightStorageRTManager = new InsightStorageRTManager();
            FacebookFeed objFacebookFeed = new FacebookFeed();
            string FeedID = string.Empty;

            try
            {
                DataFeed data = new DataFeed();

                for (int iCounter = 1; iCounter <= 1; iCounter++)
                {
                    objFacebookFeed = FaceBook.GetFacebookFeedID(UniqueID, Token, FromDate, ToDate, limit);

                    if (string.IsNullOrEmpty(objFacebookFeed.facebookResponse.message))
                    {
                        data = objFacebookFeed.feed.data.Where(o => o.object_id == ResultID || o.id == ResultID || o.message == Comment).SingleOrDefault();

                        if (data == null)
                        {
                            data = new DataFeed();
                        }
                    }
                    else
                    {
                        data = new DataFeed();
                    }

                    
                    if (!string.IsNullOrEmpty(data.id))
                    {
                        FeedID = data.id;
                        break;
                    }
                    else
                    {
                        AccessToken objAccessToken = new AccessToken();
                        FacebookAccessTokenFeedID =  objAccessToken.GetRandomAccessToken();

                        objFacebookFeed = FaceBook.GetFacebookFeedID(UniqueID, FacebookAccessTokenFeedID, FromDate, ToDate, limit);
                        data = objFacebookFeed.feed.data.Where(o => o.object_id == ResultID || o.id == ResultID).SingleOrDefault();

                        if (data == null)
                        {
                            data = new DataFeed();
                        }
                    }

                    if (!string.IsNullOrEmpty(data.id))
                    {
                        FeedID = data.id;
                        break;
                    }

                    limit = Convert.ToString(Convert.ToInt32(limit) + 500);
                }
                if (string.IsNullOrEmpty(FeedID))
                {

                    if (ResultID.Contains('_'))
                    {
                        FeedID = ResultID;
                    }
                }                
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING                
                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "Program\\GetFeedIDByResultID";
                ObjExceptionLog.ExtraInfo = "ResultID: " + ResultID + ", Access Token : " + Token;

                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                #endregion
            }

            return FeedID;
        }

        public static List<FacebookCombinedPostStatistics> GetCarFaxFacebookCombinedPostStatistics()
        {
            InsightStorageRTManager objInsightStorageRTManager = new InsightStorageRTManager();
            Guid userID = new Guid(UserID);

            List<FacebookCombinedPostStatistics> listFacebookCombinedPostStatistics = new List<FacebookCombinedPostStatistics>();            
            FacebookCombinedPostStatistics objFacebookCombinedPostStatistics;            
            var counter = 0;
            string message = string.Empty;
            try
            {
                List<spGetFBWorkListforStatistics_Result> listspGetFBWorkListforStatistics_Result = objInsightStorageRTManager.GetFBWorkListforStatistics(-1, userID, FromHours, ToHours);
                List<string> listPostQueueIDs = new List<string>();
                AccessToken objAccessToken = new AccessToken();

                message = "ResultId to Process : " + listspGetFBWorkListforStatistics_Result.Count;
                objInsightStorageRTManager.InsertServiceLog(ServiceName, message);

                Console.WriteLine("Found {0} records to process", listspGetFBWorkListforStatistics_Result.Count);
                //string DeletedPostList = string.Empty;


                foreach (var spGetFBWorkListforStatistics_Result in listspGetFBWorkListforStatistics_Result)                
                {
                    try
                    {
                        counter++;
                        listPostQueueIDs.Add(Convert.ToString(spGetFBWorkListforStatistics_Result.PostId));

                        objFacebookCombinedPostStatistics = new FacebookCombinedPostStatistics();

                        string strxml = string.Empty;
                        string FeedID = Convert.ToString(spGetFBWorkListforStatistics_Result.FeedID);
                                                
                        //Debug.WriteLine("Starting to process # {0}", counter);

                        // if FeedID = string.Empty (NEw Post Need to get FeedID.)
                        // if FeedID = "0" (Not Able to get FeedID, Post Might Deleted)
                        // if FeedID !=  "0" OR FeedID !=  string.Empty  (Do not Need to get FeedID, Get Only Insights)
                        
                        if (string.IsNullOrEmpty(FeedID) && FeedID != "0")
                        {                            
                            FacebookAccessTokenFeedID = objAccessToken.GetRandomAccessToken();

                            FeedID = GetFeedIDByResultID(Convert.ToString(spGetFBWorkListforStatistics_Result.UniqueID), FacebookAccessTokenFeedID, Convert.ToString(Convert.ToDateTime(spGetFBWorkListforStatistics_Result.ProcessedDate).ToShortDateString()), Convert.ToString(Convert.ToDateTime(spGetFBWorkListforStatistics_Result.ProcessedDate).ToShortDateString()), "500", Convert.ToString(spGetFBWorkListforStatistics_Result.ResultID), spGetFBWorkListforStatistics_Result.Comment);
                            if (string.IsNullOrEmpty(FeedID))  
                            {
                                FeedID = "0";
                            }
                        }

                        if (Convert.ToString(spGetFBWorkListforStatistics_Result.FeedID) == "")
                        {
                            PublishManager ObjPublishManager = new PublishManager();
                            ObjPublishManager.UpdateDealerLocationSocialNetworkPostQueue_FeedID(Convert.ToInt64(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID), FeedID);
                        }
                        //Debug.WriteLine("{0} ResultID : {1} , FeedID : {2}", counter, Convert.ToString(spGetFBWorkListforStatistics_Result.ResultID), FeedID);
                        Console.WriteLine("{0} ResultID : {1} , FeedID : {2}", counter, Convert.ToString(spGetFBWorkListforStatistics_Result.ResultID), FeedID);                        
                        
                        FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();
                        FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();
                        FacebookInsights objFacebookInsights = new FacebookInsights();
                        
                        int PostTypeID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.PostTypeId);
                                                
                        objFacebookCombinedPostStatistics.ResultID = Convert.ToString(spGetFBWorkListforStatistics_Result.ResultID); 
                        objFacebookCombinedPostStatistics.FeedID = FeedID;
                        objFacebookCombinedPostStatistics.PostTypeID = PostTypeID;
                        objFacebookCombinedPostStatistics.PostDate = Convert.ToString(spGetFBWorkListforStatistics_Result.PostDate);
                        //objFacebookCombinedPostStatistics.Comment = Convert.ToString(spGetFBWorkListforStatistics_Result.Comment);

                        if (PostTypeID == Convert.ToInt32(PostType.FacebookStatus) || PostTypeID == Convert.ToInt32(PostType.FacebookLink) || PostTypeID == Convert.ToInt32(PostType.FacebookPhoto) || PostTypeID == Convert.ToInt32(PostType.FacebookMultiplePhoto))
                        {
                            //Get Facebook Post Statistics
                            
                            FacebookAccessTokenFeedID = objAccessToken.GetRandomAccessToken();

                            objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(Convert.ToString(spGetFBWorkListforStatistics_Result.ResultID), FacebookAccessTokenFeedID, PostTypeID);

                            //Retry with Castle Chevrolet Access Token
                            if (objFacebookPostStatistics.facebookResponse.message.ToLower() == Convert.ToString("Error validating access token: The session has been invalidated because the user has changed the password.").ToLower() ||
                                objFacebookPostStatistics.facebookResponse.message.ToLower() == Convert.ToString("The user must be an administrator of the page in order to impersonate it.").ToLower())                                
                            {
                                
                                FacebookAccessTokenFeedID = objAccessToken.GetRandomAccessToken();
                                objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(Convert.ToString(spGetFBWorkListforStatistics_Result.ResultID), FacebookAccessTokenFeedID, PostTypeID);
                            }

                            objFacebookPostStatistics.postStatistics.type = Convert.ToString(spGetFBWorkListforStatistics_Result.PostTypeId);

                            if (FeedID != "0")
                            {
                                FacebookAccessTokenFeedID = objAccessToken.GetRandomAccessToken();
                                objFacebookPostStatisticsFeedID = FaceBook.GetFacebookPostStatisticsByFeedId(FeedID, FacebookAccessTokenFeedID);

                                if (string.IsNullOrEmpty(objFacebookPostStatisticsFeedID.facebookResponse.message))
                                {
                                    objFacebookPostStatistics.postStatistics.likescount = objFacebookPostStatisticsFeedID.postStatisticsbyfeedid.likes.count;
                                    objFacebookPostStatistics.postStatistics.commentscount = objFacebookPostStatisticsFeedID.postStatisticsbyfeedid.comments.count;
                                    objFacebookPostStatistics.postStatistics.sharescount = objFacebookPostStatisticsFeedID.postStatisticsbyfeedid.shares.count;
                                    objFacebookPostStatistics.postStatistics.iscountcalulated = true;
                                }                                
                            }
                            else
                            {
                                objFacebookPostStatistics.postStatistics.likescount = 0;
                                objFacebookPostStatistics.postStatistics.commentscount = 0;
                                objFacebookPostStatistics.postStatistics.sharescount = 0;
                                objFacebookPostStatistics.postStatistics.iscountcalulated = false;
                            }

                            strxml = Utility.SerializeObject<FacebookPostStatistics>(objFacebookPostStatistics);

                            string PostInsightXML = string.Empty;

                            if (FeedID != "0")
                            {
                                //Get Facebook Post Insights
                                
                                objFacebookInsights = FaceBook.GetFacebookInsights(0, string.Empty, Convert.ToString(spGetFBWorkListforStatistics_Result.Token), DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), "lifetime", FeedID);

                                ////Retry with Castle Chevrolet Access Token
                                //if (objFacebookInsights.facebookResponse.message.ToLower() == Convert.ToString("Error validating access token: The session has been invalidated because the user has changed the password.").ToLower() ||
                                //    objFacebookInsights.facebookResponse.message.ToLower() == Convert.ToString("The user must be an administrator of the page in order to impersonate it.").ToLower())
                                //{
                                    
                                //    FacebookAccessTokenFeedID = objAccessToken.GetRandomAccessToken();
                                //    objFacebookInsights = FaceBook.GetFacebookInsights(0, string.Empty, FacebookAccessTokenFeedID, DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), "lifetime", FeedID);
                                //}

                                try
                                {
                                    int sum = 0;
                                    foreach (var item in objFacebookInsights.insights.data)
                                    {
                                        sum = 0;
                                        var itemval = item.values.FirstOrDefault();

                                        if (itemval != null)
                                        {
                                            if (itemval._value == "[]" || string.IsNullOrEmpty(itemval._value))
                                            {
                                                sum = 0;
                                            }
                                            else if (Utility.IsAllDigits(itemval._value))
                                            {
                                                sum += Convert.ToInt32(itemval._value);
                                            }
                                            else
                                            {
                                                var jss = new JavaScriptSerializer();
                                                dynamic data = jss.Deserialize<dynamic>(itemval._value);

                                                foreach (KeyValuePair<string, object> subitem in data)
                                                {
                                                    sum += Convert.ToInt32(subitem.Value);
                                                }
                                            }
                                            item.sum = sum;
                                        }
                                    }

                                }
                                catch (Exception ex)
                                {
                                    #region ERROR HANDLING

                                    #endregion
                                }

                            }
                            else
                            {
                                objFacebookInsights.facebookResponse.code = "0000";
                                objFacebookInsights.facebookResponse.message = "Feed Id :" + FeedID + " , " + "No Insights Record Found.";

                            }

                            PostInsightXML = Utility.SerializeObject<FacebookInsights>(objFacebookInsights);
                            PostInsightXML = Utility.Trim(PostInsightXML).Replace(@"<value xsi:type=""xsd:string"">", "<value>");

                            if (!string.IsNullOrEmpty(strxml))
                            {
                                objInsightStorageRTManager.SaveFBPostDataXML(Utility.Trim(strxml), PostInsightXML, Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID), spGetFBWorkListforStatistics_Result.ResultID);
                            }
                            else
                            {
                                #region ERROR HANDLING

                                ExceptionLog ObjExceptionLog = new ExceptionLog();
                                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID);
                                ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML for " + spGetFBWorkListforStatistics_Result.PostTypeId;
                                ObjExceptionLog.InnerException = string.Empty;
                                ObjExceptionLog.PostResponse = string.Empty;
                                ObjExceptionLog.StackTrace = string.Empty;
                                ObjExceptionLog.MethodName = "Program\\GetSetPostStatistics\\SerializeObject";
                                ObjExceptionLog.ExtraInfo = "ResultID : " + spGetFBWorkListforStatistics_Result.ResultID + " , UniqueID : " + spGetFBWorkListforStatistics_Result.UniqueID + ", Access Token : " + spGetFBWorkListforStatistics_Result.Token;

                                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                                #endregion
                            }

                            

                            objFacebookCombinedPostStatistics.facebookPostStatistics = objFacebookPostStatistics;
                            objFacebookCombinedPostStatistics.facebookInsights = objFacebookInsights;
                        }
                        //else if (PostTypeID == Convert.ToInt32(PostType.FacebookPhoto) || PostTypeID == Convert.ToInt32(PostType.FacebookMultiplePhoto))
                        //{
                        //    objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(Convert.ToString(spGetFBWorkListforStatistics_Result.ResultID), Convert.ToString(spGetFBWorkListforStatistics_Result.Token), PostTypeID);

                        //    if (FeedID != "0")
                        //    {
                        //        objFacebookInsights = FaceBook.GetFacebookInsights(0, string.Empty, Convert.ToString(spGetFBWorkListforStatistics_Result.Token), DateTime.Now.ToShortDateString(), DateTime.Now.ToShortDateString(), "lifetime", FeedID);
                        //    }

                        //    objFacebookCombinedPostStatistics.facebookPostStatistics = objFacebookPostStatistics;
                        //    objFacebookCombinedPostStatistics.facebookInsights = objFacebookInsights;

                        //    //statItem = new KeyValuePair<string, IStatistics>(ResultID, objFacebookCombinedPostStatistics);
                        //}
                        else if (PostTypeID == Convert.ToInt32(PostType.FacebookQuestion))
                        {

                        }
                        else if (PostTypeID == Convert.ToInt32(PostType.FacebookEvent))
                        {

                        }

                        listFacebookCombinedPostStatistics.Add(objFacebookCombinedPostStatistics);
                       
                        //Debug.WriteLine("Successfully processed post #{0} with URL  https://graph.facebook.com/{1}&access_token={2}", counter, spGetFBWorkListforStatistics_Result.ResultID, spGetFBWorkListforStatistics_Result.Token);

                        

                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING
                        Debug.WriteLine("         Error processing post {0}", spGetFBWorkListforStatistics_Result.ResultID);
                        ExceptionLog ObjExceptionLog = new ExceptionLog();
                        ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result.DealerLocationSocialNetworkPostQueueID);
                        ObjExceptionLog.ExceptionMessage = ex.Message;
                        ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        ObjExceptionLog.PostResponse = string.Empty;
                        ObjExceptionLog.StackTrace = ex.StackTrace;
                        ObjExceptionLog.MethodName = "Program\\GetFacebookPostStatistics\\forEach";
                        ObjExceptionLog.ExtraInfo = "UniqueID : " + spGetFBWorkListforStatistics_Result.UniqueID.ToString() + ", Access Token : " + spGetFBWorkListforStatistics_Result.Token.ToString();

                        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                        #endregion
                    }
                }

                if (listPostQueueIDs.Count > 0)
                {
                    
                    string PostQueueIDXML = Utility.SerializeObject<List<string>>(listPostQueueIDs);
                    PostQueueIDXML = Utility.Trim(PostQueueIDXML);

                    message = "Insert/Update MessageSyndicationByPost Table : " + listPostQueueIDs.Count;
                    
                    objInsightStorageRTManager.InsertServiceLog(ServiceName, message);
                    objInsightStorageRTManager.InsertMessageSyndicationByPost(PostQueueIDXML, InsertToStreamTable);

                    message = "Insert/Update MessageSyndicationByPostQueue Table : " + listPostQueueIDs.Count;

                    objInsightStorageRTManager.InsertServiceLog(ServiceName, message);
                    objInsightStorageRTManager.InsertMessageSyndicationByPostQueue(PostQueueIDXML, InsertToStreamTable);
                    
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                ExceptionLog ObjExceptionLog = new ExceptionLog();
                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjExceptionLog.ExceptionMessage = ex.Message;
                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjExceptionLog.PostResponse = string.Empty;
                ObjExceptionLog.StackTrace = ex.StackTrace;
                ObjExceptionLog.MethodName = "Program\\GetFacebookPostStatistics";
                ObjExceptionLog.ExtraInfo = string.Empty;

                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

                #endregion
            }

            return listFacebookCombinedPostStatistics;
        }

        #endregion

        #region ProcessCarfaxStats

        private static FacebookSocialStatisticsCollection ProcessCarfaxStats()
        {
            var facebookSocialStatisticsCollection = new FacebookSocialStatisticsCollection();
            List<FacebookCombinedPostStatistics> listFacebookCombinedPostStatistics = GetCarFaxFacebookCombinedPostStatistics();
            
            try
            {
                Debug.WriteLine("\r\n  \r\n Detailed Result \r\n -------------- \r\n ");
                Debug.WriteLine("Post Date" + "|" + "Post Message" + "|" + "Total Organic Impressions" + "|" + "Total Reach (Unique)" + "|" + "Engaged Users (Clicks)" + "|" + "Viral Impressions (Shares)" + "|" + "Likes" + "|" + "               Result_id");

                foreach (var item in listFacebookCombinedPostStatistics)
                {
                    var facebookSocialStatistic = new FacebookSocialStatistic();

                    if(item.PostTypeID == Convert.ToInt32(PostType.FacebookStatus) || item.PostTypeID == Convert.ToInt32(PostType.FacebookLink))
                    {
                        facebookSocialStatistic.Likes = item.facebookPostStatistics.postStatistics.likes != null && item.facebookPostStatistics.postStatistics.likes.data != null ? item.facebookPostStatistics.postStatistics.likes.data.Count : 0;
                        facebookSocialStatistic.PostDate = item.facebookPostStatistics.postStatistics.created_time.ToShortDateString() != "1/1/0001" ? item.facebookPostStatistics.postStatistics.created_time.ToShortDateString() : item.PostDate;
                        facebookSocialStatistic.PostMessage = item.facebookPostStatistics.postStatistics.message ?? item.Comment;

                        // Checking if Post Insight Statistics has data for a particular Result_ID and if so, adds the insight stats to collection. If not, the insight stats are set to 0 by default
                        if (item.facebookInsights.insights.data != null)
                        {
                            if (item.facebookInsights.insights.data.Count != 0)
                            {
                                DataInsights engagedUsers = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_engaged_users"));
                                DataInsights totalReach = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_impressions_unique"));
                                DataInsights totalOrganicImpressions = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_impressions_organic"));
                                DataInsights viralImpressions = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_impressions_viral"));

                                facebookSocialStatistic.EngagedUsers = engagedUsers != null ? Convert.ToInt32(engagedUsers.values.First().value) : 0;
                                facebookSocialStatistic.TotalReach = totalReach != null ? Convert.ToInt32(totalReach.values.First().value) : 0;
                                facebookSocialStatistic.TotalOrganicImpressions = totalOrganicImpressions != null ? Convert.ToInt32(totalOrganicImpressions.values.First().value) : 0;
                                facebookSocialStatistic.ViralImpressions = viralImpressions != null ? Convert.ToInt32(viralImpressions.values.First().value) : 0;
                            }
                        }

                    }
                    else if (item.PostTypeID == Convert.ToInt32(PostType.FacebookPhoto) || item.PostTypeID == Convert.ToInt32(PostType.FacebookMultiplePhoto))
                    {
                        facebookSocialStatistic.Likes = item.facebookPostStatistics.postStatistics.likes != null && item.facebookPostStatistics.postStatistics.likes.data != null ? item.facebookPostStatistics.postStatistics.likes.data.Count : 0;
                        facebookSocialStatistic.PostDate = item.facebookPostStatistics.postStatistics.created_time.ToShortDateString() != "1/1/0001" ? item.facebookPostStatistics.postStatistics.created_time.ToShortDateString() : item.PostDate;
                        facebookSocialStatistic.PostMessage = item.facebookPostStatistics.postStatistics.message ?? item.Comment;

                        // Checking if Post Insight Statistics has data for a particular Result_ID and if so, adds the insight stats to collection. If not, the insight stats are set to 0 by default
                        if (item.facebookInsights.insights.data != null)
                        {
                            if (item.facebookInsights.insights.data.Count != 0)
                            {
                                DataInsights engagedUsers = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_engaged_users"));
                                DataInsights totalReach = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_impressions_unique"));
                                DataInsights totalOrganicImpressions = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_impressions_organic"));
                                DataInsights viralImpressions = item.facebookInsights.insights.data.FirstOrDefault(d => d.name.Equals("post_impressions_viral"));

                                facebookSocialStatistic.EngagedUsers = engagedUsers != null ? Convert.ToInt32(engagedUsers.values.First().value) : 0;
                                facebookSocialStatistic.TotalReach = totalReach != null ? Convert.ToInt32(totalReach.values.First().value) : 0;
                                facebookSocialStatistic.TotalOrganicImpressions = totalOrganicImpressions != null ? Convert.ToInt32(totalOrganicImpressions.values.First().value) : 0;
                                facebookSocialStatistic.ViralImpressions = viralImpressions != null ? Convert.ToInt32(viralImpressions.values.First().value) : 0;
                            }
                        }
                    }
                    else if (item.PostTypeID == Convert.ToInt32(PostType.FacebookQuestion))
                    {

                    }
                    else if (item.PostTypeID == Convert.ToInt32(PostType.FacebookEvent))
                    {

                    }

                    Debug.WriteLine(facebookSocialStatistic.PostDate + "|" + facebookSocialStatistic.PostMessage + "|" + facebookSocialStatistic.TotalOrganicImpressions + "|" + facebookSocialStatistic.TotalReach + "|" + facebookSocialStatistic.EngagedUsers + "|" + facebookSocialStatistic.ViralImpressions + "|" + facebookSocialStatistic.Likes.ToString() + "|" + "         ResultId: " + item.ResultID);

                    facebookSocialStatisticsCollection.TotalOrganicImpressions += facebookSocialStatistic.TotalOrganicImpressions;
                    facebookSocialStatisticsCollection.TotalReach += facebookSocialStatistic.TotalReach;
                    facebookSocialStatisticsCollection.EngagedUsers += facebookSocialStatistic.EngagedUsers;
                    facebookSocialStatisticsCollection.ViralImpressions += facebookSocialStatistic.ViralImpressions;
                    facebookSocialStatisticsCollection.Likes += facebookSocialStatistic.Likes;

                    facebookSocialStatisticsCollection.StatisticsCollection.Add(facebookSocialStatistic);
                }

                Debug.WriteLine("\r\n \r\n Grouped Result \r\n -------------- \r\n");
                Debug.WriteLine("Post Date" + "|" + "Post Message" + "|" + "Total Organic Impressions" + "|" + "Total Reach (Unique)" + "|" + "Engaged Users (Clicks)" + "|" + "Viral Impressions (Shares)" + "|" + "Likes" + "|" + "Number Of Posts");

                // Grouping the Results By Post Message & Post Date using Linq assorted commands
                var groupedStat = from stat in facebookSocialStatisticsCollection.StatisticsCollection
                                  group stat by new { stat.PostMessage, stat.PostDate } into newGroup
                                  select new
                                  {
                                      PostDate = newGroup.Key.PostDate,
                                      PostMessage = newGroup.Key.PostMessage,
                                      TotalOrganicImpressions = newGroup.Sum(s => s.TotalOrganicImpressions),
                                      TotalReach = newGroup.Sum(s => s.TotalReach),
                                      EngagedUsers = newGroup.Sum(s => s.EngagedUsers),
                                      ViralImpressions = newGroup.Sum(s => s.ViralImpressions),
                                      Likes = newGroup.Sum(s => s.Likes),
                                      NumberOfPosts = newGroup.Count()
                                  };

                // Iterating through each grouped stat to output the grouped results

                foreach (var facebookSocialStatistic in groupedStat)
                {
                    Debug.WriteLine(facebookSocialStatistic.PostDate + "|" + facebookSocialStatistic.PostMessage + "|" + facebookSocialStatistic.TotalOrganicImpressions +
                      "|" + facebookSocialStatistic.TotalReach + "|" + facebookSocialStatistic.EngagedUsers + "|" + facebookSocialStatistic.ViralImpressions + "|" +
                      facebookSocialStatistic.Likes.ToString() + "|" + facebookSocialStatistic.NumberOfPosts);
                }

            }

            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            Debug.WriteLine("\r\n  \r\n Summary Result \r\n --------------\r\n");
            Debug.WriteLine("Total Organic Impressions" + "|" + "Total Reach (Unique)" + "|" + "Engaged Users (Clicks)" + "|" + "Viral Impressions (Shares)" + "|" + "Likes");
            Debug.WriteLine(facebookSocialStatisticsCollection.TotalOrganicImpressions + "|" + facebookSocialStatisticsCollection.TotalReach + "|" + facebookSocialStatisticsCollection.EngagedUsers + "|" + facebookSocialStatisticsCollection.ViralImpressions + "|" + facebookSocialStatisticsCollection.Likes.ToString());

            return facebookSocialStatisticsCollection;
        }

        #endregion

        #region Test
        //private static void Test()
        //{
        //    string co = System.Web.HttpUtility.UrlEncode("454545454 fb & twitter  ?? ? ? ? ? ' ' '  '");
        //    FaceBook.CreateFacebookWallPost("113214418756138", "AAAEOk2OTej8BAHCrIzcPDFDhXOvF7cYvyrb2CMtFBghs7ZC9tgjpSAkmDxG18DmX5XU9EID0usNBe6yRIIJnGXSODkyZC8vZB0kiKPaih7JHgLenaxN", co, co, "", "", "", "", 0, "", "{'countries':'" + "" + "'}");
        //}

        //private static void GetFacebookPostStatistics1()
        //{
        //    InsightStorageRTManager objInsightStorageRTManager = new InsightStorageRTManager();
        //    try
        //    {
        //        string myXMLfile = @"C:\Dev\SOCIALDEALER\SourceCodeMain\SOCIALDEALER.Publish\SOCIALDEALER.Publish.Facebook.PostStatistics\XMLFile1.xml";
        //        DataSet ds = new DataSet();
        //        try
        //        {
        //            ds.ReadXml(myXMLfile);
        //        }
        //        catch (Exception ex)
        //        {
        //        }

        //        int iCounter = 1;
        //        foreach (DataRow spGetFBWorkListforStatistics_Result in ds.Tables[0].Rows)
        //        {
        //            try
        //            {
        //                string strxml = string.Empty;

        //                if (Convert.ToString(spGetFBWorkListforStatistics_Result["ResultID"]).Contains('_')) // 105661099481954_431391626908898 (Status Or Link)
        //                {
        //                    FacebookPostStatistics objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(Convert.ToString(spGetFBWorkListforStatistics_Result["ResultID"]), Convert.ToString(spGetFBWorkListforStatistics_Result["Token"]));
        //                    strxml = Utility.SerializeObject<FacebookPostStatistics>(objFacebookPostStatistics);
        //                }
        //                else // 10151272931772889 (Photo)
        //                {
        //                    FacebookPhotoStatistics objFacebookPhotoStatistics = FaceBook.GetFacebookPhotoStatistics(Convert.ToString(spGetFBWorkListforStatistics_Result["ResultID"]), Convert.ToString(spGetFBWorkListforStatistics_Result["Token"]));
        //                    objFacebookPhotoStatistics.photoStatistics.type = "photo";
        //                    strxml = Utility.SerializeObject<FacebookPhotoStatistics>(objFacebookPhotoStatistics);
        //                }

        //                if (!string.IsNullOrEmpty(strxml))
        //                {
        //                    objInsightStorageRTManager.SaveFBPostDataXML(Utility.Trim(strxml), Convert.ToInt32(spGetFBWorkListforStatistics_Result["DealerLocationSocialNetworkPostQueueID"]), Convert.ToString(spGetFBWorkListforStatistics_Result["ResultID"]));
        //                }
        //                else
        //                {
        //                    #region ERROR HANDLING

        //                    ExceptionLog ObjExceptionLog = new ExceptionLog();
        //                    ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //                    ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                    ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result["DealerLocationSocialNetworkPostQueueID"]);
        //                    ObjExceptionLog.ExceptionMessage = "Unable to SerializeObject XML";
        //                    ObjExceptionLog.InnerException = string.Empty;
        //                    ObjExceptionLog.PostResponse = string.Empty;
        //                    ObjExceptionLog.StackTrace = string.Empty;
        //                    ObjExceptionLog.MethodName = "Program\\GetSetPostStatistics1\\SerializeObject";
        //                    ObjExceptionLog.ExtraInfo = "UniqueID : " + spGetFBWorkListforStatistics_Result["UniqueID"].ToString() + ", Access Token : " + spGetFBWorkListforStatistics_Result["Token"].ToString();

        //                    objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //                    #endregion
        //                }
        //                Console.WriteLine(iCounter);
        //                Console.WriteLine("--------");
        //                iCounter++;
        //            }
        //            catch (Exception ex)
        //            {
        //                #region ERROR HANDLING

        //                ExceptionLog ObjExceptionLog = new ExceptionLog();
        //                ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //                ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //                ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = Convert.ToInt32(spGetFBWorkListforStatistics_Result["DealerLocationSocialNetworkPostQueueID"]);
        //                ObjExceptionLog.ExceptionMessage = ex.Message;
        //                ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //                ObjExceptionLog.PostResponse = string.Empty;
        //                ObjExceptionLog.StackTrace = ex.StackTrace;
        //                ObjExceptionLog.MethodName = "Program\\GetFacebookPostStatistics1\\forEach";
        //                ObjExceptionLog.ExtraInfo = "UniqueID : " + spGetFBWorkListforStatistics_Result["UniqueID"].ToString() + ", Access Token : " + spGetFBWorkListforStatistics_Result["Token"].ToString();

        //                objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //                #endregion
        //            }
        //        }

        //        Console.ReadLine();
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ERROR HANDLING

        //        ExceptionLog ObjExceptionLog = new ExceptionLog();
        //        ObjExceptionLog.DealerLocationSocialNetworkID = 0;
        //        ObjExceptionLog.DealerLocationSocialNetworkPostID = 0;
        //        ObjExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
        //        ObjExceptionLog.ExceptionMessage = ex.Message;
        //        ObjExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
        //        ObjExceptionLog.PostResponse = string.Empty;
        //        ObjExceptionLog.StackTrace = ex.StackTrace;
        //        ObjExceptionLog.MethodName = "Program\\GetFacebookPostStatistics1";
        //        ObjExceptionLog.ExtraInfo = string.Empty;

        //        objInsightStorageRTManager.AddExceptionLog(ObjExceptionLog);

        //        #endregion
        //    }
        //}
        #endregion

    }
}
