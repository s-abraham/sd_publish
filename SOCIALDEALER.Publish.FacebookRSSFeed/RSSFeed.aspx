﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RSSFeed.aspx.cs" Inherits="SOCIALDEALER.Publish.FacebookRSSFeed.RSSFeed" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">    
        var pageJSON = <%=_pageJSON%>;
        $(document).ready(function() {      
            createHtm(pageJSON)
        });
        function createHtm(pageJSON)
        {
            var divPostHtm = [];
            var qCount = pageJSON.length;            
            //qCount = 2;

            var PostBreak ="<br /><br />";
            //var divHtm = [] //<hr width=60%>;
            
            for ( var i=0, qlen=qCount; i<qlen; ++i ){
                var MainTableStart = '<table class="fbtable" align="center" cellspacing="0">';            
                var PostTRStart = '<tr>';            
                var PostPictureTd1 = '<td class="fbtd1"><img src="[#PagePicture#]" alt="" height="32px" width="32px" /></td>'
                var PostPictureTd2 = '<td class="fbtd2"><div><span class="fbPostUserpicture"><a href="[#PageLink#]" target="_blank"> ' + 
                                        '<span style="text-decoration: none">[#PageName#]</span></a></span>' +                                     
                                        '</div><div><span class="fbRSSFeedMessage">[#RSSFeedMessage#]</span></div><div><span class="fbPostMessage">[#PageMessage#]</span></div><div class="fbdate">' +  
                                        '<span><img src="Images/fb-like.jpg" alt="Likes"/><span class="fblike">&nbsp;[#PostLikeCount#]</span></span>' + 
                                        '<span>&nbsp;<img src="Images/fb-comment.jpg" alt="Comment"/><span class="fblike">&nbsp;[#PostCommentCount#]</span></span>' + 
                                        '<span>&nbsp;&nbsp;Date:&nbsp;[#PostDate#]</span></div></td>'
                var PostTREnd = '</tr>';
                var MainTableEnd = '</table>';

                var oPost = pageJSON[i];
                var opostStatistics;
                var ocomments;
                var olikes;

                if(oPost.ActionType == 'CommentonUserWallPost'){
                    opostStatistics = oPost.postStatisticsbyfeedid;
                }
                else{
                    opostStatistics = oPost.postStatistics;
                }

                //if(oPost.ActionType == 'CommentonUserWallPost'){
                    
                //}
                //else{
                    //var opostStatistics = oPost.postStatistics;
                    var ocomments = opostStatistics.comments;
                                        
                    var PostCommentCount = 0;
                    if(ocomments != null){
                        var ocommentsdata = ocomments.data;
                        if(ocommentsdata != null){
                            PostCommentCount = ocommentsdata.length;
                        }
                    }                    
                    
                    var olikes = opostStatistics.likes;
                    var PostlikesCount = 0;
                    if(olikes != null){
                        var olikesdata = olikes.data;
                        if(olikesdata != null){
                            PostlikesCount = olikesdata.length;
                        }
                    }                                        
                    var Postdate = new Date(parseInt(opostStatistics.created_time.substr(6)));
                    var Postid = opostStatistics.id;
                    var PostFromId = opostStatistics.from.id;
                    var PostFromName = opostStatistics.from.name;
                    var PostFromPictureurl = opostStatistics.from.pictureurl;
                    var RSSFeedMessage = oPost.RSSFeedMessage;

                    PostPictureTd1 = PostPictureTd1.replace('[#PagePicture#]',PostFromPictureurl)
                    PostPictureTd2 = PostPictureTd2.replace('[#PageName#]',PostFromName)
                                                    .replace('[#RSSFeedMessage#]',RSSFeedMessage)                    
                                                    .replace('[#PageLink#]','https://www.facebook.com/' + Postid)                                                    
                                                    .replace('[#PageMessage#]',opostStatistics.message)
                                                    .replace('[#PostLikeCount#]',PostlikesCount)
                                                    .replace('[#PostCommentCount#]',PostCommentCount)
                                                    .replace('[#PostDate#]',js_mm_dd_yyyy_hh_mm_ss(Postdate));

                    var PostHtml = MainTableStart + PostTRStart + PostPictureTd1 + PostPictureTd2 + PostTREnd;
                    var  PostCommentHtml = '';
                    if(PostCommentCount > 0){
                        var ocommentsdata = ocomments.data;
                        var jCount = ocommentsdata.length;  
                        var PostCommentdivHtm = [];
                        
                        for ( var j=0, jlen=jCount; j<jlen; ++j ){
                            PostCommentHtml = '';
                            
                            var PostCommentTRStart = '<tr>';
                            var PostCommentTD1 = '<td class="fbtd1"></td>';
                            var PostCommentTD2Start = '<td class="fbtd2">';
                            var PostCommentTD2TableStart = '<table class="fbCommentTable" align="center" cellspacing="0">';
                            var PostCommentTD2TableTRstart = '<tr class="fbCommentTR">';
                            var PostCommentTD2TableTD1 = '<td class="fbCommenttd1"><div class="fbCommenttd1div"><img src="[#PostCommentFromPicture#]" alt="" height="32px" width="32px" /></div></td>';
                            var PostCommentTD2TableTD2 = '<td><div class="fbCommenttd2div"><span class="fbPostUserpicture"><a href="[#PostCommentFromPageLink#]" target="_blank"><span style="text-decoration: none">[#PostCommentFromName#]</span></a></span> <span class="fbComment">&nbsp;[#PostCommentMessage#]</span></div><div class="fbdate"><span>Date:&nbsp;[#PostCommentDate#]</span>[#SpanPostCommentCount#]</div></td>';
                            var PostCommentTD2TableTREnd = '</tr>';
                            var PostCommentTD2TableEnd = '</table>';
                            var PostCommentTD2End = '</td>';
                            var PostCommentTREnd = '</tr>';

                            var oPostComment = ocommentsdata[j];

                            var PostCommentId = oPostComment.id;
                            var PostCommentfromId = oPostComment.from.id;
                            var PostCommentfromName = oPostComment.from.name;
                            var PostCommentfromPictureurl = oPostComment.from.pictureurl;
                            var PostCommentmessage = oPostComment.message;
                            var PostCommentdate = new Date(parseInt(oPostComment.created_time.substr(6)));
                            var PostCommentLikeCount = oPostComment.like_count
                            var SpanPostCommentCount = '<span>&nbsp;&nbsp;<img src="Images/fb-like.jpg" alt="Likes"/><span class="fblike">&nbsp;[#PostCommentLikeCount#]</span></span>&nbsp;';

                            if(PostCommentLikeCount > 0){
                                SpanPostCommentCount = SpanPostCommentCount.replace('[#PostCommentLikeCount#]',PostCommentLikeCount);
                            }
                            else{
                                SpanPostCommentCount = '';
                            }

                            PostCommentTD2TableTD1 = PostCommentTD2TableTD1.replace('[#PostCommentFromPicture#]',PostCommentfromPictureurl);

                            PostCommentTD2TableTD2 = PostCommentTD2TableTD2.replace('[#PostCommentFromPageLink#]','https://www.facebook.com/' + PostCommentfromId)
                                                                            .replace('[#PostCommentFromName#]',PostCommentfromName)
                                                                            .replace('[#PostCommentMessage#]',PostCommentmessage)
                                                                            .replace('[#PostCommentDate#]',js_mm_dd_yyyy_hh_mm_ss(PostCommentdate))
                                                                            .replace('[#PostCommentFromName#]',PostCommentfromName)
                                                                            .replace('[#SpanPostCommentCount#]',SpanPostCommentCount);

                            PostCommentHtml = PostCommentTRStart + PostCommentTD1 + PostCommentTD2Start + PostCommentTD2TableStart + PostCommentTD2TableTRstart +
                                        PostCommentTD2TableTD1 + PostCommentTD2TableTD2 + PostCommentTD2TableTREnd + PostCommentTD2TableEnd + 
                                        PostCommentTD2End + PostCommentTREnd;
                            
                            //alert(PostCommentdivHtm);

                            PostCommentdivHtm.push(PostCommentHtml);
                        }
                    }    
                    
                    //alert(PostHtml);
                    PostHtml = PostHtml + PostCommentdivHtm.join("") + MainTableEnd + PostBreak;

                    divPostHtm.push(PostHtml);                    
                //}

                 $("#divPostHtml").html(divPostHtm.join(""));
            }
        }

        function js_mm_dd_yyyy_hh_mm_ss (now) {          
          year = "" + now.getFullYear();
          month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
          day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
          hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
          minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
          second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
          return month + "-" + day + "-" + year + " " + hour + ":" + minute + ":" + second;
       }
    </script>
    <style type="text/css">       
        .fbtable
        {            
            width: 600px;            
        }
        .fbCommentTable
        {           
            width: 560px;                     
        }
        .fbCommentTR
        {   
            vertical-align: top;
            background-color: #EDEFF4;            
        }
        .fbCommenttd1
        {   
            vertical-align: top;
        }
        .fbCommenttd1div
        {   
            width: 32px;            
        }
        .fbCommenttd2div
        {   
            width: 520px;            
        }
        .fbtd1
        {
            vertical-align: top;
            width: 40px;
        }
        .fbtd2
        {            
            vertical-align: top;
            width: 560px;
        }
        .fbdate
        {          
            color: #666666;
            vertical-align: top; 
            font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; 
            font-size: 11px;
        }
        .fbPostUserpicture
        {
            vertical-align: top; 
            color: #3B5998; 
            cursor: pointer; 
            text-decoration: none; 
            font-weight: bold; 
            font-size: 11px; 
            font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;
        }
        .fbPostMessage
        {
            vertical-align: top; 
            font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; 
            font-size: 11px; 
            font-variant: normal; 
            line-height: 1.2;
        }
        .fbRSSFeedMessage
        {
            vertical-align: top; 
            font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; 
            font-size: 11px; 
            font-variant: normal; 
            color: Red;
            line-height: 1.2;
        }
        .fbComment
        {
            vertical-align: top; 
            font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; 
            font-size: 11px; 
            font-variant: normal;                  
        }
        .fblike
        {
            font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; 
            font-size: 11px; 
            font-variant: normal; 
            color: #3B5998;            
        }
        a {
            color: #3B5998;
            cursor: pointer;
            text-decoration: none;
        }
        a:link {text-decoration:none;}    /* unvisited link */
        a:visited {text-decoration:none;} /* visited link */
        a:hover {text-decoration:underline;}   /* mouse over link */
        a:active {text-decoration:underline;}  /* selected link */
    </style>
</head>
<body style="line-height: 1;">
    <form id="form1" runat="server">

    <div id="divPostHtml"></div>
    <br />
    <br />
    <%--<div>
        <table class="fbtable" align="center" cellspacing="0">
           <tr>
                <td class="fbtd1">
                    <img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/186312_1422517335_1194988389_q.jpg" alt=""
                        height="32px" width="32px" />
                </td>
                <td class="fbtd2">
                    <div>
                        <span class="fbPostUserpicture">
                            <a href="https://www.facebook.com/patruax" target="_blank">
                                <span style="text-decoration: none">Patrick A. Truax</span>
                            </a>
                        </span>                        
                    </div>
                    <div>
                        <span class="fbPostMessage">
                                    In other words...DON'T DRIVE WITH YOUR HEAD In other words...In other words...In other words...In other words...In other words...In other words...In other words...!!!
                        </span>
                    </div>
                    <div class="fbdate"> 
                        <span><img src="Images/fb-like.jpg" alt="Likes"/><span class="fblike">&nbsp;350</span></span>
                        <span><img src="Images/fb-comment.jpg" alt="Comment"/><span class="fblike">&nbsp;150</span></span>
                        <span>&nbsp;&nbsp;Date: 2013-06-26 14:06</span>                        
                    </div>                    
                </td>
            </tr>
            <tr>
                <td class="fbtd1">
                </td>
                <td class="fbtd2">
                    <table class="fbCommentTable" align="center" cellspacing="0">
                        <tr class="fbCommentTR">
                            <td class="fbCommenttd1" >
                                <div class="fbCommenttd1div">
                                    <img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/186312_1422517335_1194988389_q.jpg" alt="" height="32px" width="32px" />
                                </div>                                
                            </td>
                            <td>
                                <div class="fbCommenttd2div">
                                    <span class="fbPostUserpicture">
                                        <a href="https://www.facebook.com/patruax" target="_blank"><span style="text-decoration: none">
                                            Patrick A. Truax</span></a></span> <span class="fbComment">&nbsp;&nbsp;In other words...DON'T DRIVE WITH YOUR HEAD In other words...In other words...In other words...In other words...In other words...In other words...In other words..!!!</span>
                                </div>
                                <div class="fbdate">                                    
                                    <span>Date: 2013-06-26 14:06</span>                        
                                </div>                                
                            </td>
                        </tr>                     
                    </table>
                </td>
            </tr>
            <tr>
                <td class="fbtd1">
                </td>
                <td class="fbtd2">
                    <table class="fbCommentTable" align="center" cellspacing="0">
                        <tr class="fbCommentTR">
                            <td class="fbCommenttd1" >
                                <div class="fbCommenttd1div">
                                    <img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-ash3/186312_1422517335_1194988389_q.jpg" alt="" height="32px" width="32px" />
                                </div>                                
                            </td>
                            <td>
                                <div class="fbCommenttd2div">
                                    <span class="fbPostUserpicture">
                                        <a href="https://www.facebook.com/patruax" target="_blank"><span style="text-decoration: none">
                                            Patrick A. Truax</span></a></span> <span class="fbComment">&nbsp;&nbsp;In other words...DON'T DRIVE WITH YOUR HEAD In other words...In other words...In other words...In other words...In other words...In other words...In other words..!!!                                            
                                    </span>
                                </div>
                                <div class="fbdate">                                    
                                    <span><img src="Images/fb-like.jpg" alt="Likes"/><span class="fblike">&nbsp;3</span></span>
                                    <span>&nbsp;Date: 2013-06-26 14:06</span>                        
                                </div>                                
                            </td>
                        </tr>                     
                    </table>
                </td>
            </tr>
        </table>        
    </div>--%>
    </form>
</body>
</html>
