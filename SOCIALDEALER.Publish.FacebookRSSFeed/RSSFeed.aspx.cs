﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using System.Configuration;
using System.Web.Script.Serialization;
using BusinessLogic;
using System.IO;

namespace SOCIALDEALER.Publish.FacebookRSSFeed
{
    public partial class RSSFeed : System.Web.UI.Page
    {
        protected string _pageJSON = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            DirectoryInfo d = new DirectoryInfo(@"c:\FacebookFeedFiles");//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.txt"); //Getting Text files
            string filepath = Files.OrderByDescending(x => x.CreationTime).FirstOrDefault().FullName;

            _pageJSON = File.ReadAllText(filepath);

            #region
            //string Token = "AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe";
            //string FacebookPageName = "Castle Chevrolet";
            //string FacebookPagePicture = "http://profile.ak.fbcdn.net/hprofile-ak-ash2/276832_255547016773_308753143_t.jpg";


            //string FacebookRSSFeedURL = Convert.ToString(ConfigurationManager.AppSettings["FacebookRSSFeedURL"]);

            //FacebookPageRSSFeed objFacebookPageRSSFeed = BusinessLogic.FacebookRSSFeed.GetRSSFeedInformation(FacebookRSSFeedURL);

            //List<FaceBookPostIdType> listFaceBookPostIdType = BusinessLogic.FacebookRSSFeed.ProcessRSSFeed(objFacebookPageRSSFeed);

            //FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();
            //FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();

            //bool GetUserPicture = true;

            //int iCounter = 0;
            //foreach (var item in listFaceBookPostIdType)
            //{
            //    //if (iCounter == 0)
            //    //{
            //    string FeedID = string.Empty;
            //    if (item.ActionType.ToLower() == "CommentonUserWallPost".ToLower())
            //    {
            //        FacebookFeed objFacebookFeed = GetFeedIDsByFacebookPageID("255547016773", Token, DateTime.Now.AddDays(-90).ToShortDateString(), DateTime.Now.AddDays(1).ToShortDateString(), "1000");

            //        DataFeed data = new DataFeed();
            //        if (string.IsNullOrEmpty(objFacebookFeed.facebookResponse.message))
            //        {
            //            data = objFacebookFeed.feed.data.Where(o => o.id.Contains(item.PostID)).SingleOrDefault();

            //            if (data == null)
            //            {
            //                data = new DataFeed();
            //            }
            //        }

            //        if (!string.IsNullOrEmpty(data.id))
            //        {
            //            FeedID = data.id;
            //            objFacebookPostStatisticsFeedID = FaceBook.GetFacebookPostStatisticsByFeedId(FeedID, Token, GetUserPicture);
            //            item.PostID = FeedID;
            //            item.postStatisticsbyfeedid = objFacebookPostStatisticsFeedID.postStatisticsbyfeedid;
            //            item.postStatistics = null;
            //        }
            //        else
            //        {
            //            objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();
            //        }
            //        //objFacebookPostStatisticsFeedID.postStatisticsbyfeedid.comments.data.OrderByDescending(x => x.created_time);

            //    }
            //    else if (!string.IsNullOrEmpty(item.PostID))
            //    {
            //        if (item.PostType.ToLower() == "status".ToLower())
            //        {
            //            objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.PostID, Token, Convert.ToInt32(PostType.FacebookStatus), GetUserPicture);
            //        }
            //        else if (item.PostType.ToLower() == "link".ToLower())
            //        {
            //            objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.PostID, Token, Convert.ToInt32(PostType.FacebookLink), GetUserPicture);
            //        }
            //        else if (item.PostType.ToLower() == "photo".ToLower())
            //        {
            //            objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.PostID, Token, Convert.ToInt32(PostType.FacebookPhoto), GetUserPicture);
            //        }
            //        //objFacebookPostStatistics.postStatistics.comments.data.OrderByDescending(x => x.created_time);

            //        item.postStatistics = objFacebookPostStatistics.postStatistics;
            //        item.postStatisticsbyfeedid = null;
            //    }
            //    //objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(PostID, Token, Convert.ToInt32(PostType.FacebookStatus)); 
            //}
            //JavaScriptSerializer js = new JavaScriptSerializer();
            //_pageJSON = js.Serialize(listFaceBookPostIdType);
            ////}

            //iCounter++;
            #endregion
        }

        private static FacebookFeed GetFeedIDsByFacebookPageID(string UniqueID, string Token, string FromDate, string ToDate, string limit)
        {
            FacebookFeed objFacebookFeed = new FacebookFeed();
            try
            {
                objFacebookFeed = FaceBook.GetFacebookFeedID(UniqueID, Token, FromDate, ToDate, limit);
            }
            catch (Exception ex)
            {

            }


            return objFacebookFeed;

        }
    }
}