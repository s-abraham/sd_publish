﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic;
using BusinessEntities;
using System.Web.Script.Serialization;
using System.Configuration;
using System.IO;

namespace SOCIALDEALER.Publish.Facebook.FeedStatistics
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string Token = "AAAAANqD36fQBAAFAqE1dFdLQG7NSBhtfvQkU5ZBwqabJDbfTGTYdKjw0WwT4h6fODoWoEtbbajypmqO21Akeund5PWzxh0Qvxwi6z6UHJ4ZA53bYxe";
            string UniqueID = "255547016773";
            string FacebookPageName = "Castle Chevrolet";
            string FacebookPagePicture = "http://profile.ak.fbcdn.net/hprofile-ak-ash2/276832_255547016773_308753143_t.jpg";


            string FacebookRSSFeedURL = Convert.ToString(ConfigurationManager.AppSettings["FacebookRSSFeedURL"]);

            FacebookPageRSSFeed objFacebookPageRSSFeed = FacebookRSSFeed.GetRSSFeedInformation(FacebookRSSFeedURL);

            List<FaceBookPostIdType> listFaceBookPostIdType = FacebookRSSFeed.ProcessRSSFeed(objFacebookPageRSSFeed);

            FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();
            FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();

            bool GetUserPicture = true;

            foreach (var item in listFaceBookPostIdType)
            {
                string FeedID = string.Empty;
                
                if (item.ActionType.ToLower() == "CommentonUserWallPost".ToLower())
                {
                    FacebookFeed objFacebookFeed = GetFeedIDsByFacebookPageID(UniqueID, Token, DateTime.Now.AddDays(-90).ToShortDateString(), DateTime.Now.AddDays(1).ToShortDateString(), "1000");

                    DataFeed data = new DataFeed();
                    if (string.IsNullOrEmpty(objFacebookFeed.facebookResponse.message))
                    {
                        data = objFacebookFeed.feed.data.Where(o => o.id.Contains(item.PostID)).SingleOrDefault();

                        if (data == null)
                        {
                            data = new DataFeed();
                        }
                    }

                    if (!string.IsNullOrEmpty(data.id))
                    {
                        FeedID = data.id;
                        objFacebookPostStatisticsFeedID = FaceBook.GetFacebookPostStatisticsByFeedId(FeedID, Token, GetUserPicture);
                        item.PostID = FeedID;
                        item.postStatisticsbyfeedid = objFacebookPostStatisticsFeedID.postStatisticsbyfeedid;
                        item.postStatistics = null;
                    }
                    else
                    {
                        objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();
                    }

                    
                }
                else if (!string.IsNullOrEmpty(item.PostID))
                {
                    if (item.PostType.ToLower() == "status".ToLower())
                    {
                        objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.PostID, Token, Convert.ToInt32(PostType.FacebookStatus), GetUserPicture);
                    }
                    else if (item.PostType.ToLower() == "link".ToLower())
                    {
                        objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.PostID, Token, Convert.ToInt32(PostType.FacebookLink), GetUserPicture);
                    }
                    else if (item.PostType.ToLower() == "photo".ToLower())
                    {
                        objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.PostID, Token, Convert.ToInt32(PostType.FacebookPhoto), GetUserPicture);
                    }

                    item.postStatistics = objFacebookPostStatistics.postStatistics;
                    item.postStatisticsbyfeedid = null;                    
                }
                //objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(PostID, Token, Convert.ToInt32(PostType.FacebookStatus)); 
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            string _pageJSON = js.Serialize(listFaceBookPostIdType);

            string fileLoc = @"c:\FacebookFeedFiles\";
            WriteToFile(fileLoc, _pageJSON);
            


            //Console.ReadLine();
            /*           

            FacebookFeed objFacebookFeed  = GetFeedIDsByFacebookPageID("255547016773", Token,DateTime.Now.AddDays(-10).ToShortDateString(), DateTime.Now.AddDays(1).ToShortDateString(), "1000");
            List<FacebookPageFeed> listFacebookPageFeed = new List<FacebookPageFeed>();
            FacebookPageFeed objFacebookPageFeed;
            if (objFacebookFeed != null)
            {
                if (objFacebookFeed.feed != null)
                {
                    if (objFacebookFeed.feed.data != null)
                    {
                        Console.WriteLine("Total Feeds : " + objFacebookFeed.feed.data.Count());
                        int iCounter = 0;
                        foreach (var item in objFacebookFeed.feed.data)
                        {
                            iCounter++;
                            
                            Console.WriteLine("-----------------------------------------------------------" );
                            Console.WriteLine("Processing : " + iCounter);
                            Console.WriteLine("Feed ID   : " + item.id);
                            Console.WriteLine("Object ID : " + item.object_id);
                            
                            FacebookPostStatisticsFeedID objFacebookPostStatisticsFeedID = new FacebookPostStatisticsFeedID();
                            FacebookPostStatistics objFacebookPostStatistics = new FacebookPostStatistics();
                            
                            if (item.type.ToLower() == "status".ToLower() || item.type.ToLower() == "link".ToLower() || item.type.ToLower() == "photo".ToLower())
                            {                                
                                if (!string.IsNullOrEmpty(item.id))
                                {
                                    objFacebookPostStatisticsFeedID = FaceBook.GetFacebookPostStatisticsByFeedId(item.id, Token);
                                }
                                if (!string.IsNullOrEmpty(item.object_id))
                                {   
                                    if(item.type.ToLower() == "status".ToLower())
                                    {
                                        objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.object_id, Token, Convert.ToInt32(PostType.FacebookStatus)); 
                                    }
                                    else if(item.type.ToLower() == "link".ToLower())
                                    {
                                        objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.object_id, Token, Convert.ToInt32(PostType.FacebookLink));    
                                    }
                                    else if(item.type.ToLower() == "photo".ToLower())
                                    {
                                        objFacebookPostStatistics = FaceBook.GetFacebookPostStatistics(item.object_id, Token, Convert.ToInt32(PostType.FacebookPhoto));    
                                    }                                    
                                }
                            }

                            objFacebookPageFeed = new FacebookPageFeed();

                            objFacebookPageFeed.message = item.message;
                            objFacebookPageFeed.feedid = item.id;
                            objFacebookPageFeed.objectid = item.object_id;
                            if (item.from != null)
                            {
                                objFacebookPageFeed.from = item.from;
                            }
                            else
                            {
                                objFacebookPageFeed.from = new From();
                            }
                            if (item.application != null)
                            {
                                objFacebookPageFeed.application = item.application;
                            }
                            else
                            {
                                objFacebookPageFeed.application = new Application();
                            }

                            if (string.IsNullOrEmpty(objFacebookPostStatisticsFeedID.facebookResponse.message))
                            {
                                objFacebookPageFeed.postStatisticsbyfeedid = objFacebookPostStatisticsFeedID.postStatisticsbyfeedid;
                            }
                            else
                            {
                                objFacebookPageFeed.postStatisticsbyfeedid = null;
                            }

                            if (string.IsNullOrEmpty(objFacebookPostStatistics.facebookResponse.message))
                            {
                                objFacebookPageFeed.postStatistics = objFacebookPostStatistics.postStatistics;
                            }
                            else
                            {
                                objFacebookPageFeed.postStatistics = null;
                            }


                            listFacebookPageFeed.Add(objFacebookPageFeed);

                        }//foreach

                    }//objFacebookFeed.feed.data

                } // objFacebookFeed.feed

            } // objFacebookFeed

            JavaScriptSerializer js = new JavaScriptSerializer();
            string _pageJSON = js.Serialize(listFacebookPageFeed);

            Console.WriteLine("-----------------------------------------------------------");
            Console.ReadLine();
            */
        }

        private static void WriteToFile(string filelocation, string json)
        {
            try
            {
                string datetime = DateTime.Now.ToShortDateString().Replace("/", "_") + "_" + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second;
                filelocation = filelocation + datetime + ".txt";

                System.IO.File.WriteAllText(filelocation, json);
            }
            catch (Exception ex)
            {

            }
        }
        private static FacebookFeed GetFeedIDsByFacebookPageID(string UniqueID, string Token, string FromDate, string ToDate, string limit)
        {            
            FacebookFeed objFacebookFeed = new FacebookFeed();
            try
            {
                objFacebookFeed = FaceBook.GetFacebookFeedID(UniqueID, Token, FromDate, ToDate, limit);
            }
            catch (Exception ex)
            {

            }


            return objFacebookFeed;

        }
    }
}
