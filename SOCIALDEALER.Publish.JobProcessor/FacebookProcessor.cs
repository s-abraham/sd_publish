﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BusinessEntities;
using BusinessLogic;
using DataLayer;
using DataLayerInsights;

using spGetDealerLocationSocialNetworkList_Result = DataLayer.spGetDealerLocationSocialNetworkList_Result;

namespace SOCIALDEALER.Publish.JobProcessor
{
    public static class FacebookProcessor
    {
        //private static string DealerLocationIDs = Convert.ToString(ConfigurationManager.AppSettings["DealerLocationIDs"]);
        private static string DealerLocationIDs = "-1";

        private static void ProcessFacebookPage()
        {
            var objInsightStorageRTManager = new InsightStorageRTManager();
            
            

            try
            {
                var listspGetDealerLocationSocialNetworkList_Result = objInsightStorageRTManager.GetDealerLocationSocialNetworkList(2, DealerLocationIDs);

                foreach (var spGetDealerLocationSocialNetworkList_Result in listspGetDealerLocationSocialNetworkList_Result)
                {
                    try
                    {
                        var objFacebookPage = FaceBook.GetFacebookPageInformation(spGetDealerLocationSocialNetworkList_Result.DealerLocationID
                                                                                            , spGetDealerLocationSocialNetworkList_Result.UniqueID
                                                                                            , spGetDealerLocationSocialNetworkList_Result.Token);
                        
                        var strxml = Utility.SerializeObject<FacebookPage>(objFacebookPage);
                        if (!string.IsNullOrEmpty(strxml))
                        {
                            objInsightStorageRTManager.SaveFBPageDataXML(Utility.Trim(strxml), spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID, spGetDealerLocationSocialNetworkList_Result.UniqueID);
                        }
                        else
                        {
                            #region ERROR HANDLING

                            var objExceptionLog = new ExceptionLog();
                            objExceptionLog.DealerLocationSocialNetworkID = spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID;
                            objExceptionLog.DealerLocationSocialNetworkPostID = 0;
                            objExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                            objExceptionLog.ExceptionMessage = "Unable to SerializeObject XML";
                            objExceptionLog.InnerException = string.Empty;
                            objExceptionLog.PostResponse = string.Empty;
                            objExceptionLog.StackTrace = string.Empty;
                            objExceptionLog.MethodName = "Program\\GetSetPageInformation\\SerializeObject";
                            objExceptionLog.ExtraInfo = "UniqueID : " + spGetDealerLocationSocialNetworkList_Result.UniqueID + ", Access Token : " + spGetDealerLocationSocialNetworkList_Result.Token;

                            objInsightStorageRTManager.AddExceptionLog(objExceptionLog);

                            #endregion
                        }

                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        var objExceptionLog = new ExceptionLog();
                        objExceptionLog.DealerLocationSocialNetworkID = spGetDealerLocationSocialNetworkList_Result.DealerLocationSocialNetworkID;
                        objExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        objExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        objExceptionLog.ExceptionMessage = ex.Message;
                        objExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        objExceptionLog.PostResponse = string.Empty;
                        objExceptionLog.StackTrace = ex.StackTrace;
                        objExceptionLog.MethodName = "Program\\GetSetPageInformation\\forEach";
                        objExceptionLog.ExtraInfo = "UniqueID : " + spGetDealerLocationSocialNetworkList_Result.UniqueID + ", Access Token : " + spGetDealerLocationSocialNetworkList_Result.Token;

                        objInsightStorageRTManager.AddExceptionLog(objExceptionLog);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var objExceptionLog = new ExceptionLog();
                objExceptionLog.DealerLocationSocialNetworkID = 0;
                objExceptionLog.DealerLocationSocialNetworkPostID = 0;
                objExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                objExceptionLog.ExceptionMessage = ex.Message;
                objExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                objExceptionLog.PostResponse = string.Empty;
                objExceptionLog.StackTrace = ex.StackTrace;
                objExceptionLog.MethodName = "Program\\GetSetPageInformation";
                objExceptionLog.ExtraInfo = string.Empty;

                objInsightStorageRTManager.AddExceptionLog(objExceptionLog);

                #endregion
            }
        }

        private static void ProcessFacebookInsights(DateTime queryDate, SocialNetworks socialNetwork = SocialNetworks.FacebookOnly)
        {
            var objInsightStorageRTManager = new InsightStorageRTManager();
            var insightsDictionary = new Dictionary<string, string>();
            var results = new List<Dictionary<string, string>>();

            var sw = Stopwatch.StartNew();
            var startDate = queryDate.ToShortDateString() + " 00:00:00";
            var endDate = queryDate.ToShortDateString() + " 23:59:59";
            var periods = "";
            var counter = 0;

            try
            {
                var worklist = objInsightStorageRTManager.GetDealerLocationSocialNetworkList(2, DealerLocationIDs);
                Debug.WriteLine("GetActiveWorkList Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, worklist.Count);

                foreach (var item in worklist)
                {
                    counter++;
                    Statistics.AddAttempt();
                    try
                    {
                        Debug.WriteLine("Proessing Record: {0}, LocationId: {1}", counter, item.DealerLocationID);
                        var objFacebookInsights = FaceBook.GetFacebookInsights(item.DealerLocationID, item.UniqueID, item.Token, startDate, endDate, periods);
                        Debug.WriteLine("GetFacebookInsights Processing Time: {0}ms", sw.ElapsedMilliseconds);

                        if (String.IsNullOrEmpty(objFacebookInsights.facebookResponse.code))
                        {
                            Statistics.AddSuccess();
                            insightsDictionary = FaceBook.GetFacebookInsightsDictionary(objFacebookInsights);
                            results.Add(insightsDictionary);
                            var insightModel = FaceBook.ConvertDictionary2Object(insightsDictionary);
                            objInsightStorageRTManager.SaveFBInsightData(insightModel);


                            Debug.WriteLine("GetFacebookInsightsDictionary Processing Time: {0}ms Records = {1}", sw.ElapsedMilliseconds, insightsDictionary.Count);
                        }
                        else
                        {
                            Statistics.AddFail(item.DealerLocationID, socialNetwork, objFacebookInsights.facebookResponse.code, objFacebookInsights.facebookResponse.message);
                            Debug.WriteLine("GetFacebookInsightsDictionary Processing Error: {0} for LocationId: {1}", objFacebookInsights.facebookResponse.message, item.DealerLocationID);
                        }

                        

                    }
                    catch (Exception ex)
                    {
                        #region ERROR HANDLING

                        var objExceptionLog = new ExceptionLog();
                        objExceptionLog.DealerLocationSocialNetworkID = item.DealerLocationSocialNetworkID;
                        objExceptionLog.DealerLocationSocialNetworkPostID = 0;
                        objExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                        objExceptionLog.ExceptionMessage = ex.Message;
                        objExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                        objExceptionLog.PostResponse = string.Empty;
                        objExceptionLog.StackTrace = ex.StackTrace;
                        objExceptionLog.MethodName = "ProcessFacebookInsights";
                        objExceptionLog.ExtraInfo = "UniqueID : " + item.UniqueID + ", Access Token : " + item.Token;

                        objInsightStorageRTManager.AddExceptionLog(objExceptionLog);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                var objExceptionLog = new ExceptionLog();
                objExceptionLog.DealerLocationSocialNetworkID = 0;
                objExceptionLog.DealerLocationSocialNetworkPostID = 0;
                objExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                objExceptionLog.ExceptionMessage = ex.Message;
                objExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                objExceptionLog.PostResponse = string.Empty;
                objExceptionLog.StackTrace = ex.StackTrace;
                objExceptionLog.MethodName = "ProcessFacebookInsights";
                objExceptionLog.ExtraInfo = string.Empty;

                objInsightStorageRTManager.AddExceptionLog(objExceptionLog);

                #endregion
            }
        }
    }
}
