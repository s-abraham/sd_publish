﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BusinessEntities;
using BusinessLogic;
using DataLayer;
using System.Configuration;

namespace TokenValidator
{
    public class Program
    {
        private static int SocialNetworkID = Convert.ToInt32(ConfigurationManager.AppSettings["SocialNetworkID"]);
        private static int DealerLocationID = Convert.ToInt32(ConfigurationManager.AppSettings["DealerLocationID"]);
        private static string ServiceName = Convert.ToString(ConfigurationManager.AppSettings["ServiceName"]);
        
        public static void Main()
        {
            //PublishToSocialNetwork.PostToTwitter("test post3", "", "1382617219-o0vdPtL4txQyMavvfNPG488J2blGXmGET4hNAce", "znaukmNf8FJTUdZ2XFv1OeDF5av9K3re72i6okXiY", "3yJ8qfr2ZcWaXwIryI7xg", "2ut16gtlx6DRccjkmd50S7iSwUghdLs8wZb7n8tkfQ", 0, 0, 0, DateTime.Now.AddHours(1), false);
            //Twitter.TwitterStatusUpdate2("test post3","", "1382617219-o0vdPtL4txQyMavvfNPG488J2blGXmGET4hNAce", "znaukmNf8FJTUdZ2XFv1OeDF5av9K3re72i6okXiY", "3yJ8qfr2ZcWaXwIryI7xg", "2ut16gtlx6DRccjkmd50S7iSwUghdLs8wZb7n8tkfQ", 0, 0);
            try
            {              

                PublishManager objPublishManager = new PublishManager();

                #region Logic that defines which Social Network/s will be processed

                PublishServiceLog ObjPublishServiceLog = new PublishServiceLog();
                ObjPublishServiceLog.Message = ServiceName + " Service Started, SocialNetworkID : " + SocialNetworkID + ", DealerLocationID : " + DealerLocationID;
                ObjPublishServiceLog.Exception = string.Empty;
                ObjPublishServiceLog.MethodName = "TokenValidator\\Main";
                objPublishManager.AddPublishServiceLog(ObjPublishServiceLog);                

                if (SocialNetworkID == 2)
                {
                    FaceBook.UpdateAppKeysAndValidateTokensForALLFBAccounts(DealerLocationID);
                }
                else if (SocialNetworkID == 3)
                {                    
                    Twitter.ValidateALLTwitterTokens(DealerLocationID);
                }
                else if (SocialNetworkID == 5)
                {                    
                    Youtube.ValidateALLYoutubeTokens(DealerLocationID);
                }
                else if (SocialNetworkID == 29)
                {
                    GooglePlus.ValidateALLGooglePlusToken(DealerLocationID);
                }
                else
                {
                    FaceBook.UpdateAppKeysAndValidateTokensForALLFBAccounts(DealerLocationID);                
                    Twitter.ValidateALLTwitterTokens(DealerLocationID);                
                    Youtube.ValidateALLYoutubeTokens(DealerLocationID);                
                    GooglePlus.ValidateALLGooglePlusToken(DealerLocationID);
                }

                #endregion

            }

            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.ExtraInfo = string.Empty;
                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

    }
}
