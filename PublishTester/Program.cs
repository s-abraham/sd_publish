﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using SOCIALDEALER.Publish;
using BusinessLogic;
using DataLayer;
using System.Data.Objects;

namespace PublishTester
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string argString = string.Join(",", args);
                string[] argParts = argString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string argPart in argParts)
                {
                    long queueID;
                    if (long.TryParse(argPart.Trim(), out queueID))
                    {
                        publishQueueID(queueID);
                    }
                }
            }
            catch (Exception ex)
            {
                ex = ex;
            }

        }

        private static void publishQueueID(long queueID)
        {
            try
            {
                
                SocialDealerEntities1 dbContext = new SocialDealerEntities1();
                ObjectResult<spGetDealerLocationSocialNetworkPostDetails_Unrestricted_Result> res = dbContext.spGetDealerLocationSocialNetworkPostDetails_Unrestricted("-1", queueID, -1);
                spGetDealerLocationSocialNetworkPostDetails_Unrestricted_Result obj = res.FirstOrDefault();

                if (obj != null)
                {
                    spGetDealerLocationSocialNetworkPostDetails_New_Result param = new spGetDealerLocationSocialNetworkPostDetails_New_Result()
                    {
                        AppKey=obj.AppKey,
                        AppSecret=obj.AppSecret,
                        AppToken=obj.AppToken,
                        CategoryType=obj.CategoryType,
                        City=obj.City,
                        Comment=obj.Comment,
                        Country=obj.Country,
                        DealerLocationID = obj.DealerLocationID,
                        DealerLocationName = obj.DealerLocationName,
                        DealerLocationSocialNetworkID = obj.DealerLocationSocialNetworkID,
                        Description = obj.Description,
                        Encoded = obj.Encoded,
                        EventEndTime = obj.EventEndTime,
                        EventName = obj.EventName,
                        EventStartTime = obj.EventStartTime,
                        FBAlbumDescription = obj.FBAlbumDescription,
                        FBAlbumId = obj.FBAlbumId,
                        FBAlbumName = obj.FBAlbumName,
                        FBAllowNewOptions = obj.FBAllowNewOptions,
                        FBLocation = obj.FBLocation,
                        FBOptions = obj.FBOptions,
                        FBQuestion = obj.FBQuestion,
                        Image = obj.Image,
                        isScheduledPost = obj.isScheduledPost,
                        IsUploadedImage = obj.IsUploadedImage,
                        IsVideoPrivate = obj.IsVideoPrivate,
                        Keywords = obj.Keywords,
                        Link = obj.Link,
                        PageFriendlyName = obj.PageFriendlyName,
                        ParentDealerLocationSocialNetworkPostQueueId = obj.ParentDealerLocationSocialNetworkPostQueueId,
                        PostDate = obj.PostDate,
                        PostExpirationDate = obj.PostExpirationDate,
                        PostID = obj.PostID,
                        PostQueueID = obj.PostQueueID,
                        PostTypeId = obj.PostTypeId,
                        Privacy = obj.Privacy,
                        Region = obj.Region,
                        ResultID = obj.ResultID,
                        ReTryNumber = obj.ReTryNumber,
                        ScheduleDate = obj.ScheduleDate,
                        SocialNetworkID = obj.SocialNetworkID,
                        TargetDealerLocationID = obj.TargetDealerLocationID,
                        Title = obj.Title,
                        Token = obj.Token,
                        TokenSecret = obj.TokenSecret,
                        UniqueID = obj.UniqueID,
                        UserID = obj.UserID,
                        VideoPath = obj.VideoPath,
                        VideoSource = obj.VideoSource                       
                    };
                                        
                    if (param != null)
                    {
                        PublishToSocialNetwork.PostToSocialNetworks(param);

                        //switch (param.SocialNetworkID)
                        //{
                        //    case 2:
                        //        //Facebook
                        //        PublishToSocialNetwork.PostToFacebook(param);
                        //        break;
                        //    case 3:
                        //        //Twitter
                        //        //PublishToSocialNetwork.PostToTwitter(param.Comment, param.Link, param.Token, param.TokenSecret, param.AppKey, param.AppSecret, param.PostID, param.PostQueueID, param.ReTryNumber ?? 0, param.PostExpirationDate ?? DateTime.Now.AddDays(+1), param.isScheduledPost ?? false);

                                
                        //        break;
                        //    case 29:
                        //        //Google+
                        //        PublishToSocialNetwork.PostToGooglePlus(param);
                        //        break;
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                ex = ex;
            }
        }
    }
}
