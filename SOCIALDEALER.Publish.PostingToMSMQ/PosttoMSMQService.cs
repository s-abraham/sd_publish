﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Timers;
using System.Configuration;
using DataLayer;
using BusinessLogic;

namespace SOCIALDEALER.Publish.PostingToMSMQ
{
    partial class PosttoMSMQService : ServiceBase
    {
        //Make the service to call the for every 5 min  
        private int IntervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);
        System.Timers.Timer TimerObj;
  


        public PosttoMSMQService()
        {
            InitializeComponent();
            if (IntervalMinutes == 0)
                IntervalMinutes = 2;

            TimerObj = new System.Timers.Timer(ConvertMinutesToMilliseconds(IntervalMinutes));  
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.

            try
            {
                TimerObj.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                TimerObj.Enabled = true;  
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PosttoMSMQService\\OnStart";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;

                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }                       
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            try
            {                
                TimerObj.Enabled = false;  
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PosttoMSMQService\\OnStop";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;

                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }

        /// <summary>  
        /// At the end of every 5 min the event will be triggered.  
        /// </summary>  
        /// <param name="source">  

        /// <param name="e">  
        protected void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            //Write the current datetime to the file  
            //WriteToFile();

            try
            {
                PublishToSocialNetwork.PostToMSMQ();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

                PublishExceptionLog ObjPublishExceptionLog = new PublishExceptionLog();
                PublishManager objPublishManager = new PublishManager();

                ObjPublishExceptionLog.DealerLocationSocialNetworkPostID = 0;
                ObjPublishExceptionLog.DealerLocationSocialNetworkPostQueueID = 0;
                ObjPublishExceptionLog.ExceptionMessage = ex.Message;
                ObjPublishExceptionLog.InnerException = ex.InnerException == null ? "" : ex.InnerException.Message;
                ObjPublishExceptionLog.PostResponse = string.Empty;
                ObjPublishExceptionLog.StackTrace = ex.StackTrace;
                ObjPublishExceptionLog.MethodName = "PosttoMSMQService\\OnElapsedTime";
                ObjPublishExceptionLog.ExtraInfo = string.Empty;

                objPublishManager.AddPublishExceptionLog(ObjPublishExceptionLog);

                #endregion
            }
        }  

        void ScheduleElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                //EventLog.WriteEntry("Schedule Tick In", EventLogEntryType.Information);
                //List<SocialNetworkPost> posts = PublishManager.GetDealerLocationSocialNetworkPostTimeZone();
                //foreach (SocialNetworkPost post in posts)
                //{
                //    Switcher(post);
                //}
            }
            catch (Exception ex)
            {
                
            }            
        }

        /// <returns></returns>  
        public static double ConvertMinutesToMilliseconds(double minutes)
        {
            return TimeSpan.FromMinutes(minutes).TotalMilliseconds;
        }

        public void WriteToFile()
        {
            string strDateTime = DateTime.Now.ToString();
            StreamWriter sw = new StreamWriter("c:\\TimeZoneInfo.txt", true);
            sw.WriteLine(strDateTime);

            sw.Close();
        }
        
    }
}
