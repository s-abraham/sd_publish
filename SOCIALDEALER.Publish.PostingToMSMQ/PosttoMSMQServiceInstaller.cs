﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace SOCIALDEALER.Publish.PostingToMSMQ
{
    [RunInstaller(true)]
    public partial class PosttoMSMQServiceInstaller : System.Configuration.Install.Installer
    {
        public PosttoMSMQServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
